-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Sat Dec  4 15:05:56 2021
-- Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_uart_PL_recieved_azi_0_0_sim_netlist.vhdl
-- Design      : design_1_uart_PL_recieved_azi_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_rx is
  port (
    o_Rx_DV : out STD_LOGIC;
    o_Rx_Byte : out STD_LOGIC_VECTOR ( 7 downto 0 );
    i_Rx_Serial : in STD_LOGIC;
    i_Clock : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_rx;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_rx is
  signal \^o_rx_byte\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \^o_rx_dv\ : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \r_Bit_Index[0]_i_1_n_0\ : STD_LOGIC;
  signal \r_Bit_Index[1]_i_1_n_0\ : STD_LOGIC;
  signal \r_Bit_Index[2]_i_1_n_0\ : STD_LOGIC;
  signal \r_Bit_Index_reg_n_0_[0]\ : STD_LOGIC;
  signal \r_Bit_Index_reg_n_0_[1]\ : STD_LOGIC;
  signal \r_Bit_Index_reg_n_0_[2]\ : STD_LOGIC;
  signal \r_Clock_Count[2]_i_2_n_0\ : STD_LOGIC;
  signal \r_Clock_Count[3]_i_2_n_0\ : STD_LOGIC;
  signal \r_Clock_Count[4]_i_2_n_0\ : STD_LOGIC;
  signal \r_Clock_Count[5]_i_2_n_0\ : STD_LOGIC;
  signal \r_Clock_Count[6]_i_2__0_n_0\ : STD_LOGIC;
  signal \r_Clock_Count[7]_i_1_n_0\ : STD_LOGIC;
  signal \r_Clock_Count[7]_i_3_n_0\ : STD_LOGIC;
  signal \r_Clock_Count[7]_i_4_n_0\ : STD_LOGIC;
  signal \r_Clock_Count[7]_i_5_n_0\ : STD_LOGIC;
  signal \r_Clock_Count[7]_i_6_n_0\ : STD_LOGIC;
  signal \r_Clock_Count_reg_n_0_[0]\ : STD_LOGIC;
  signal \r_Clock_Count_reg_n_0_[1]\ : STD_LOGIC;
  signal \r_Clock_Count_reg_n_0_[2]\ : STD_LOGIC;
  signal \r_Clock_Count_reg_n_0_[3]\ : STD_LOGIC;
  signal \r_Clock_Count_reg_n_0_[4]\ : STD_LOGIC;
  signal \r_Clock_Count_reg_n_0_[5]\ : STD_LOGIC;
  signal \r_Clock_Count_reg_n_0_[6]\ : STD_LOGIC;
  signal \r_Clock_Count_reg_n_0_[7]\ : STD_LOGIC;
  signal \r_Rx_Byte[0]_i_1_n_0\ : STD_LOGIC;
  signal \r_Rx_Byte[1]_i_1_n_0\ : STD_LOGIC;
  signal \r_Rx_Byte[2]_i_1_n_0\ : STD_LOGIC;
  signal \r_Rx_Byte[3]_i_1_n_0\ : STD_LOGIC;
  signal \r_Rx_Byte[4]_i_1_n_0\ : STD_LOGIC;
  signal \r_Rx_Byte[5]_i_1_n_0\ : STD_LOGIC;
  signal \r_Rx_Byte[6]_i_1_n_0\ : STD_LOGIC;
  signal \r_Rx_Byte[7]_i_1_n_0\ : STD_LOGIC;
  signal \r_Rx_Byte[7]_i_2_n_0\ : STD_LOGIC;
  signal r_Rx_DV_i_1_n_0 : STD_LOGIC;
  signal r_Rx_DV_i_2_n_0 : STD_LOGIC;
  signal r_Rx_DV_i_3_n_0 : STD_LOGIC;
  signal r_Rx_Data : STD_LOGIC;
  signal r_Rx_Data_R : STD_LOGIC;
  signal \r_SM_Main[0]_i_1_n_0\ : STD_LOGIC;
  signal \r_SM_Main[0]_i_2_n_0\ : STD_LOGIC;
  signal \r_SM_Main[0]_i_3_n_0\ : STD_LOGIC;
  signal \r_SM_Main[0]_i_4_n_0\ : STD_LOGIC;
  signal \r_SM_Main[0]_i_5_n_0\ : STD_LOGIC;
  signal \r_SM_Main[1]_i_1_n_0\ : STD_LOGIC;
  signal \r_SM_Main[1]_i_2_n_0\ : STD_LOGIC;
  signal \r_SM_Main[1]_i_3_n_0\ : STD_LOGIC;
  signal \r_SM_Main[2]_i_1_n_0\ : STD_LOGIC;
  signal \r_SM_Main_reg_n_0_[0]\ : STD_LOGIC;
  signal \r_SM_Main_reg_n_0_[1]\ : STD_LOGIC;
  signal \r_SM_Main_reg_n_0_[2]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \r_Clock_Count[3]_i_2\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \r_Clock_Count[4]_i_1__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \r_Clock_Count[4]_i_2\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \r_Clock_Count[7]_i_3\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \r_Clock_Count[7]_i_4\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \r_Clock_Count[7]_i_5\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \r_Clock_Count[7]_i_6\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of r_Rx_DV_i_1 : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of r_Rx_DV_i_2 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \r_SM_Main[2]_i_1\ : label is "soft_lutpair6";
begin
  o_Rx_Byte(7 downto 0) <= \^o_rx_byte\(7 downto 0);
  o_Rx_DV <= \^o_rx_dv\;
\r_Bit_Index[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2626262626262600"
    )
        port map (
      I0 => \r_Bit_Index_reg_n_0_[0]\,
      I1 => \r_Rx_Byte[7]_i_2_n_0\,
      I2 => \r_SM_Main[0]_i_4_n_0\,
      I3 => \r_SM_Main_reg_n_0_[2]\,
      I4 => \r_SM_Main_reg_n_0_[0]\,
      I5 => \r_SM_Main_reg_n_0_[1]\,
      O => \r_Bit_Index[0]_i_1_n_0\
    );
\r_Bit_Index[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6A006A6A"
    )
        port map (
      I0 => \r_Bit_Index_reg_n_0_[1]\,
      I1 => \r_Rx_Byte[7]_i_2_n_0\,
      I2 => \r_Bit_Index_reg_n_0_[0]\,
      I3 => \r_SM_Main_reg_n_0_[2]\,
      I4 => \r_SM_Main[0]_i_3_n_0\,
      O => \r_Bit_Index[1]_i_1_n_0\
    );
\r_Bit_Index[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAA00006AAA6AAA"
    )
        port map (
      I0 => \r_Bit_Index_reg_n_0_[2]\,
      I1 => \r_Rx_Byte[7]_i_2_n_0\,
      I2 => \r_Bit_Index_reg_n_0_[0]\,
      I3 => \r_Bit_Index_reg_n_0_[1]\,
      I4 => \r_SM_Main_reg_n_0_[2]\,
      I5 => \r_SM_Main[0]_i_3_n_0\,
      O => \r_Bit_Index[2]_i_1_n_0\
    );
\r_Bit_Index_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_Bit_Index[0]_i_1_n_0\,
      Q => \r_Bit_Index_reg_n_0_[0]\,
      R => '0'
    );
\r_Bit_Index_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_Bit_Index[1]_i_1_n_0\,
      Q => \r_Bit_Index_reg_n_0_[1]\,
      R => '0'
    );
\r_Bit_Index_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_Bit_Index[2]_i_1_n_0\,
      Q => \r_Bit_Index_reg_n_0_[2]\,
      R => '0'
    );
\r_Clock_Count[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"33F22222"
    )
        port map (
      I0 => \r_Clock_Count[7]_i_4_n_0\,
      I1 => \r_Clock_Count_reg_n_0_[0]\,
      I2 => r_Rx_Data,
      I3 => \r_SM_Main[1]_i_3_n_0\,
      I4 => \r_Clock_Count[7]_i_6_n_0\,
      O => p_1_in(0)
    );
\r_Clock_Count[1]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3C3CFF2828282828"
    )
        port map (
      I0 => \r_Clock_Count[7]_i_4_n_0\,
      I1 => \r_Clock_Count_reg_n_0_[0]\,
      I2 => \r_Clock_Count_reg_n_0_[1]\,
      I3 => r_Rx_Data,
      I4 => \r_SM_Main[1]_i_3_n_0\,
      I5 => \r_Clock_Count[7]_i_6_n_0\,
      O => p_1_in(1)
    );
\r_Clock_Count[2]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C3C3FF8282828282"
    )
        port map (
      I0 => \r_Clock_Count[7]_i_4_n_0\,
      I1 => \r_Clock_Count[2]_i_2_n_0\,
      I2 => \r_Clock_Count_reg_n_0_[2]\,
      I3 => r_Rx_Data,
      I4 => \r_SM_Main[1]_i_3_n_0\,
      I5 => \r_Clock_Count[7]_i_6_n_0\,
      O => p_1_in(2)
    );
\r_Clock_Count[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \r_Clock_Count_reg_n_0_[0]\,
      I1 => \r_Clock_Count_reg_n_0_[1]\,
      O => \r_Clock_Count[2]_i_2_n_0\
    );
\r_Clock_Count[3]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCF88888"
    )
        port map (
      I0 => \r_Clock_Count[7]_i_4_n_0\,
      I1 => \r_Clock_Count[3]_i_2_n_0\,
      I2 => r_Rx_Data,
      I3 => \r_SM_Main[1]_i_3_n_0\,
      I4 => \r_Clock_Count[7]_i_6_n_0\,
      O => p_1_in(3)
    );
\r_Clock_Count[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \r_Clock_Count_reg_n_0_[0]\,
      I1 => \r_Clock_Count_reg_n_0_[1]\,
      I2 => \r_Clock_Count_reg_n_0_[2]\,
      I3 => \r_Clock_Count_reg_n_0_[3]\,
      O => \r_Clock_Count[3]_i_2_n_0\
    );
\r_Clock_Count[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCF88888"
    )
        port map (
      I0 => \r_Clock_Count[7]_i_4_n_0\,
      I1 => \r_Clock_Count[4]_i_2_n_0\,
      I2 => r_Rx_Data,
      I3 => \r_SM_Main[1]_i_3_n_0\,
      I4 => \r_Clock_Count[7]_i_6_n_0\,
      O => p_1_in(4)
    );
\r_Clock_Count[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \r_Clock_Count_reg_n_0_[3]\,
      I1 => \r_Clock_Count_reg_n_0_[2]\,
      I2 => \r_Clock_Count_reg_n_0_[1]\,
      I3 => \r_Clock_Count_reg_n_0_[0]\,
      I4 => \r_Clock_Count_reg_n_0_[4]\,
      O => \r_Clock_Count[4]_i_2_n_0\
    );
\r_Clock_Count[5]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCF88888"
    )
        port map (
      I0 => \r_Clock_Count[7]_i_4_n_0\,
      I1 => \r_Clock_Count[5]_i_2_n_0\,
      I2 => r_Rx_Data,
      I3 => \r_SM_Main[1]_i_3_n_0\,
      I4 => \r_Clock_Count[7]_i_6_n_0\,
      O => p_1_in(5)
    );
\r_Clock_Count[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \r_Clock_Count_reg_n_0_[0]\,
      I1 => \r_Clock_Count_reg_n_0_[1]\,
      I2 => \r_Clock_Count_reg_n_0_[2]\,
      I3 => \r_Clock_Count_reg_n_0_[3]\,
      I4 => \r_Clock_Count_reg_n_0_[4]\,
      I5 => \r_Clock_Count_reg_n_0_[5]\,
      O => \r_Clock_Count[5]_i_2_n_0\
    );
\r_Clock_Count[6]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C3C3FF8282828282"
    )
        port map (
      I0 => \r_Clock_Count[7]_i_4_n_0\,
      I1 => \r_Clock_Count[6]_i_2__0_n_0\,
      I2 => \r_Clock_Count_reg_n_0_[6]\,
      I3 => r_Rx_Data,
      I4 => \r_SM_Main[1]_i_3_n_0\,
      I5 => \r_Clock_Count[7]_i_6_n_0\,
      O => p_1_in(6)
    );
\r_Clock_Count[6]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \r_Clock_Count_reg_n_0_[0]\,
      I1 => \r_Clock_Count_reg_n_0_[1]\,
      I2 => \r_Clock_Count_reg_n_0_[2]\,
      I3 => \r_Clock_Count_reg_n_0_[3]\,
      I4 => \r_Clock_Count_reg_n_0_[4]\,
      I5 => \r_Clock_Count_reg_n_0_[5]\,
      O => \r_Clock_Count[6]_i_2__0_n_0\
    );
\r_Clock_Count[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFE0000FFFF"
    )
        port map (
      I0 => \r_Clock_Count[7]_i_3_n_0\,
      I1 => \r_Clock_Count_reg_n_0_[6]\,
      I2 => \r_Clock_Count_reg_n_0_[7]\,
      I3 => \r_SM_Main_reg_n_0_[1]\,
      I4 => \r_SM_Main_reg_n_0_[2]\,
      I5 => \r_SM_Main_reg_n_0_[0]\,
      O => \r_Clock_Count[7]_i_1_n_0\
    );
\r_Clock_Count[7]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCF88888"
    )
        port map (
      I0 => \r_Clock_Count[7]_i_4_n_0\,
      I1 => \r_Clock_Count[7]_i_5_n_0\,
      I2 => r_Rx_Data,
      I3 => \r_SM_Main[1]_i_3_n_0\,
      I4 => \r_Clock_Count[7]_i_6_n_0\,
      O => p_1_in(7)
    );
\r_Clock_Count[7]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \r_SM_Main[1]_i_3_n_0\,
      I1 => r_Rx_Data,
      O => \r_Clock_Count[7]_i_3_n_0\
    );
\r_Clock_Count[7]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F1EEF100"
    )
        port map (
      I0 => \r_Clock_Count_reg_n_0_[6]\,
      I1 => \r_Clock_Count_reg_n_0_[7]\,
      I2 => r_Rx_DV_i_3_n_0,
      I3 => \r_SM_Main_reg_n_0_[1]\,
      I4 => \r_SM_Main_reg_n_0_[0]\,
      O => \r_Clock_Count[7]_i_4_n_0\
    );
\r_Clock_Count[7]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => \r_Clock_Count_reg_n_0_[6]\,
      I1 => \r_Clock_Count[6]_i_2__0_n_0\,
      I2 => \r_Clock_Count_reg_n_0_[7]\,
      O => \r_Clock_Count[7]_i_5_n_0\
    );
\r_Clock_Count[7]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0100"
    )
        port map (
      I0 => \r_Clock_Count_reg_n_0_[7]\,
      I1 => \r_Clock_Count_reg_n_0_[6]\,
      I2 => \r_SM_Main_reg_n_0_[1]\,
      I3 => \r_SM_Main_reg_n_0_[0]\,
      O => \r_Clock_Count[7]_i_6_n_0\
    );
\r_Clock_Count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => \r_Clock_Count[7]_i_1_n_0\,
      D => p_1_in(0),
      Q => \r_Clock_Count_reg_n_0_[0]\,
      R => '0'
    );
\r_Clock_Count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => \r_Clock_Count[7]_i_1_n_0\,
      D => p_1_in(1),
      Q => \r_Clock_Count_reg_n_0_[1]\,
      R => '0'
    );
\r_Clock_Count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => \r_Clock_Count[7]_i_1_n_0\,
      D => p_1_in(2),
      Q => \r_Clock_Count_reg_n_0_[2]\,
      R => '0'
    );
\r_Clock_Count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => \r_Clock_Count[7]_i_1_n_0\,
      D => p_1_in(3),
      Q => \r_Clock_Count_reg_n_0_[3]\,
      R => '0'
    );
\r_Clock_Count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => \r_Clock_Count[7]_i_1_n_0\,
      D => p_1_in(4),
      Q => \r_Clock_Count_reg_n_0_[4]\,
      R => '0'
    );
\r_Clock_Count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => \r_Clock_Count[7]_i_1_n_0\,
      D => p_1_in(5),
      Q => \r_Clock_Count_reg_n_0_[5]\,
      R => '0'
    );
\r_Clock_Count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => \r_Clock_Count[7]_i_1_n_0\,
      D => p_1_in(6),
      Q => \r_Clock_Count_reg_n_0_[6]\,
      R => '0'
    );
\r_Clock_Count_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => \r_Clock_Count[7]_i_1_n_0\,
      D => p_1_in(7),
      Q => \r_Clock_Count_reg_n_0_[7]\,
      R => '0'
    );
\r_Rx_Byte[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFEFFFF00020000"
    )
        port map (
      I0 => r_Rx_Data,
      I1 => \r_Bit_Index_reg_n_0_[2]\,
      I2 => \r_Bit_Index_reg_n_0_[1]\,
      I3 => \r_Bit_Index_reg_n_0_[0]\,
      I4 => \r_Rx_Byte[7]_i_2_n_0\,
      I5 => \^o_rx_byte\(0),
      O => \r_Rx_Byte[0]_i_1_n_0\
    );
\r_Rx_Byte[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFBFFFF00080000"
    )
        port map (
      I0 => r_Rx_Data,
      I1 => \r_Bit_Index_reg_n_0_[0]\,
      I2 => \r_Bit_Index_reg_n_0_[1]\,
      I3 => \r_Bit_Index_reg_n_0_[2]\,
      I4 => \r_Rx_Byte[7]_i_2_n_0\,
      I5 => \^o_rx_byte\(1),
      O => \r_Rx_Byte[1]_i_1_n_0\
    );
\r_Rx_Byte[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEFFFFF00200000"
    )
        port map (
      I0 => r_Rx_Data,
      I1 => \r_Bit_Index_reg_n_0_[2]\,
      I2 => \r_Bit_Index_reg_n_0_[1]\,
      I3 => \r_Bit_Index_reg_n_0_[0]\,
      I4 => \r_Rx_Byte[7]_i_2_n_0\,
      I5 => \^o_rx_byte\(2),
      O => \r_Rx_Byte[2]_i_1_n_0\
    );
\r_Rx_Byte[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFF20000000"
    )
        port map (
      I0 => r_Rx_Data,
      I1 => \r_Bit_Index_reg_n_0_[2]\,
      I2 => \r_Bit_Index_reg_n_0_[0]\,
      I3 => \r_Bit_Index_reg_n_0_[1]\,
      I4 => \r_Rx_Byte[7]_i_2_n_0\,
      I5 => \^o_rx_byte\(3),
      O => \r_Rx_Byte[3]_i_1_n_0\
    );
\r_Rx_Byte[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEFFFFF00200000"
    )
        port map (
      I0 => r_Rx_Data,
      I1 => \r_Bit_Index_reg_n_0_[1]\,
      I2 => \r_Bit_Index_reg_n_0_[2]\,
      I3 => \r_Bit_Index_reg_n_0_[0]\,
      I4 => \r_Rx_Byte[7]_i_2_n_0\,
      I5 => \^o_rx_byte\(4),
      O => \r_Rx_Byte[4]_i_1_n_0\
    );
\r_Rx_Byte[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFBFFFFF00800000"
    )
        port map (
      I0 => r_Rx_Data,
      I1 => \r_Bit_Index_reg_n_0_[0]\,
      I2 => \r_Bit_Index_reg_n_0_[2]\,
      I3 => \r_Bit_Index_reg_n_0_[1]\,
      I4 => \r_Rx_Byte[7]_i_2_n_0\,
      I5 => \^o_rx_byte\(5),
      O => \r_Rx_Byte[5]_i_1_n_0\
    );
\r_Rx_Byte[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFBFFFFF00800000"
    )
        port map (
      I0 => r_Rx_Data,
      I1 => \r_Bit_Index_reg_n_0_[2]\,
      I2 => \r_Bit_Index_reg_n_0_[1]\,
      I3 => \r_Bit_Index_reg_n_0_[0]\,
      I4 => \r_Rx_Byte[7]_i_2_n_0\,
      I5 => \^o_rx_byte\(6),
      O => \r_Rx_Byte[6]_i_1_n_0\
    );
\r_Rx_Byte[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF80000000"
    )
        port map (
      I0 => r_Rx_Data,
      I1 => \r_Bit_Index_reg_n_0_[0]\,
      I2 => \r_Bit_Index_reg_n_0_[1]\,
      I3 => \r_Bit_Index_reg_n_0_[2]\,
      I4 => \r_Rx_Byte[7]_i_2_n_0\,
      I5 => \^o_rx_byte\(7),
      O => \r_Rx_Byte[7]_i_1_n_0\
    );
\r_Rx_Byte[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000005400"
    )
        port map (
      I0 => r_Rx_DV_i_3_n_0,
      I1 => \r_Clock_Count_reg_n_0_[6]\,
      I2 => \r_Clock_Count_reg_n_0_[7]\,
      I3 => \r_SM_Main_reg_n_0_[1]\,
      I4 => \r_SM_Main_reg_n_0_[2]\,
      I5 => \r_SM_Main_reg_n_0_[0]\,
      O => \r_Rx_Byte[7]_i_2_n_0\
    );
\r_Rx_Byte_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_Rx_Byte[0]_i_1_n_0\,
      Q => \^o_rx_byte\(0),
      R => '0'
    );
\r_Rx_Byte_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_Rx_Byte[1]_i_1_n_0\,
      Q => \^o_rx_byte\(1),
      R => '0'
    );
\r_Rx_Byte_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_Rx_Byte[2]_i_1_n_0\,
      Q => \^o_rx_byte\(2),
      R => '0'
    );
\r_Rx_Byte_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_Rx_Byte[3]_i_1_n_0\,
      Q => \^o_rx_byte\(3),
      R => '0'
    );
\r_Rx_Byte_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_Rx_Byte[4]_i_1_n_0\,
      Q => \^o_rx_byte\(4),
      R => '0'
    );
\r_Rx_Byte_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_Rx_Byte[5]_i_1_n_0\,
      Q => \^o_rx_byte\(5),
      R => '0'
    );
\r_Rx_Byte_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_Rx_Byte[6]_i_1_n_0\,
      Q => \^o_rx_byte\(6),
      R => '0'
    );
\r_Rx_Byte_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_Rx_Byte[7]_i_1_n_0\,
      Q => \^o_rx_byte\(7),
      R => '0'
    );
r_Rx_DV_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF01000"
    )
        port map (
      I0 => \r_SM_Main_reg_n_0_[2]\,
      I1 => r_Rx_DV_i_2_n_0,
      I2 => \r_SM_Main_reg_n_0_[1]\,
      I3 => \r_SM_Main_reg_n_0_[0]\,
      I4 => \^o_rx_dv\,
      O => r_Rx_DV_i_1_n_0
    );
r_Rx_DV_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AB"
    )
        port map (
      I0 => r_Rx_DV_i_3_n_0,
      I1 => \r_Clock_Count_reg_n_0_[6]\,
      I2 => \r_Clock_Count_reg_n_0_[7]\,
      O => r_Rx_DV_i_2_n_0
    );
r_Rx_DV_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0101010101111111"
    )
        port map (
      I0 => \r_Clock_Count_reg_n_0_[7]\,
      I1 => \r_Clock_Count_reg_n_0_[5]\,
      I2 => \r_Clock_Count_reg_n_0_[4]\,
      I3 => \r_Clock_Count_reg_n_0_[1]\,
      I4 => \r_Clock_Count_reg_n_0_[2]\,
      I5 => \r_Clock_Count_reg_n_0_[3]\,
      O => r_Rx_DV_i_3_n_0
    );
r_Rx_DV_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => r_Rx_DV_i_1_n_0,
      Q => \^o_rx_dv\,
      R => '0'
    );
r_Rx_Data_R_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => i_Rx_Serial,
      Q => r_Rx_Data_R,
      R => '0'
    );
r_Rx_Data_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => r_Rx_Data_R,
      Q => r_Rx_Data,
      R => '0'
    );
\r_SM_Main[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFABAAABAAABAA"
    )
        port map (
      I0 => \r_SM_Main[0]_i_2_n_0\,
      I1 => r_Rx_Data,
      I2 => \r_SM_Main_reg_n_0_[2]\,
      I3 => \r_SM_Main[0]_i_3_n_0\,
      I4 => \r_Rx_Byte[7]_i_2_n_0\,
      I5 => \r_SM_Main[0]_i_4_n_0\,
      O => \r_SM_Main[0]_i_1_n_0\
    );
\r_SM_Main[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000AFFC00000000"
    )
        port map (
      I0 => r_Rx_DV_i_3_n_0,
      I1 => \r_SM_Main[1]_i_3_n_0\,
      I2 => \r_SM_Main[0]_i_5_n_0\,
      I3 => \r_SM_Main_reg_n_0_[1]\,
      I4 => \r_SM_Main_reg_n_0_[2]\,
      I5 => \r_SM_Main_reg_n_0_[0]\,
      O => \r_SM_Main[0]_i_2_n_0\
    );
\r_SM_Main[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \r_SM_Main_reg_n_0_[0]\,
      I1 => \r_SM_Main_reg_n_0_[1]\,
      O => \r_SM_Main[0]_i_3_n_0\
    );
\r_SM_Main[0]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \r_Bit_Index_reg_n_0_[2]\,
      I1 => \r_Bit_Index_reg_n_0_[1]\,
      I2 => \r_Bit_Index_reg_n_0_[0]\,
      O => \r_SM_Main[0]_i_4_n_0\
    );
\r_SM_Main[0]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \r_Clock_Count_reg_n_0_[7]\,
      I1 => \r_Clock_Count_reg_n_0_[6]\,
      O => \r_SM_Main[0]_i_5_n_0\
    );
\r_SM_Main[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"22222222FF2F2222"
    )
        port map (
      I0 => \r_SM_Main[1]_i_2_n_0\,
      I1 => \r_SM_Main[1]_i_3_n_0\,
      I2 => \r_SM_Main_reg_n_0_[0]\,
      I3 => r_Rx_DV_i_2_n_0,
      I4 => \r_SM_Main_reg_n_0_[1]\,
      I5 => \r_SM_Main_reg_n_0_[2]\,
      O => \r_SM_Main[1]_i_1_n_0\
    );
\r_SM_Main[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => \r_SM_Main_reg_n_0_[0]\,
      I1 => \r_SM_Main_reg_n_0_[2]\,
      I2 => r_Rx_Data,
      I3 => \r_SM_Main_reg_n_0_[1]\,
      I4 => \r_Clock_Count_reg_n_0_[7]\,
      I5 => \r_Clock_Count_reg_n_0_[6]\,
      O => \r_SM_Main[1]_i_2_n_0\
    );
\r_SM_Main[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFBFFFFFFFFFFFFF"
    )
        port map (
      I0 => \r_Clock_Count_reg_n_0_[4]\,
      I1 => \r_Clock_Count_reg_n_0_[3]\,
      I2 => \r_Clock_Count_reg_n_0_[5]\,
      I3 => \r_Clock_Count_reg_n_0_[2]\,
      I4 => \r_Clock_Count_reg_n_0_[1]\,
      I5 => \r_Clock_Count_reg_n_0_[0]\,
      O => \r_SM_Main[1]_i_3_n_0\
    );
\r_SM_Main[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => r_Rx_DV_i_2_n_0,
      I1 => \r_SM_Main_reg_n_0_[1]\,
      I2 => \r_SM_Main_reg_n_0_[2]\,
      I3 => \r_SM_Main_reg_n_0_[0]\,
      O => \r_SM_Main[2]_i_1_n_0\
    );
\r_SM_Main_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_SM_Main[0]_i_1_n_0\,
      Q => \r_SM_Main_reg_n_0_[0]\,
      R => '0'
    );
\r_SM_Main_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_SM_Main[1]_i_1_n_0\,
      Q => \r_SM_Main_reg_n_0_[1]\,
      R => '0'
    );
\r_SM_Main_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_SM_Main[2]_i_1_n_0\,
      Q => \r_SM_Main_reg_n_0_[2]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_tx is
  port (
    o_Tx_Active : out STD_LOGIC;
    o_Tx_Serial : out STD_LOGIC;
    o_Tx_Done : out STD_LOGIC;
    i_Clock : in STD_LOGIC;
    i_Tx_DV : in STD_LOGIC;
    i_Tx_Byte : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_tx;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_tx is
  signal \FSM_sequential_r_SM_Main[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_r_SM_Main[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_r_SM_Main[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_r_SM_Main[2]_i_1_n_0\ : STD_LOGIC;
  signal \^o_tx_active\ : STD_LOGIC;
  signal \^o_tx_done\ : STD_LOGIC;
  signal \^o_tx_serial\ : STD_LOGIC;
  signal o_Tx_Serial_i_1_n_0 : STD_LOGIC;
  signal o_Tx_Serial_i_3_n_0 : STD_LOGIC;
  signal o_Tx_Serial_i_4_n_0 : STD_LOGIC;
  signal o_Tx_Serial_reg_i_2_n_0 : STD_LOGIC;
  signal \r_Bit_Index[0]_i_1_n_0\ : STD_LOGIC;
  signal \r_Bit_Index[1]_i_1_n_0\ : STD_LOGIC;
  signal \r_Bit_Index[2]_i_1_n_0\ : STD_LOGIC;
  signal \r_Bit_Index[2]_i_2_n_0\ : STD_LOGIC;
  signal \r_Bit_Index_reg_n_0_[0]\ : STD_LOGIC;
  signal \r_Bit_Index_reg_n_0_[1]\ : STD_LOGIC;
  signal \r_Bit_Index_reg_n_0_[2]\ : STD_LOGIC;
  signal r_Clock_Count : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal r_Clock_Count0 : STD_LOGIC;
  signal \r_Clock_Count[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \r_Clock_Count[6]_i_4_n_0\ : STD_LOGIC;
  signal r_Clock_Count_0 : STD_LOGIC;
  signal r_Clock_Count_reg : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \r_Clock_Count_reg_n_0_[0]\ : STD_LOGIC;
  signal r_SM_Main : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal r_Tx_Active_i_1_n_0 : STD_LOGIC;
  signal r_Tx_Active_i_2_n_0 : STD_LOGIC;
  signal r_Tx_Data : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal r_Tx_Data_1 : STD_LOGIC;
  signal r_Tx_Done_i_1_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_r_SM_Main[2]_i_1\ : label is "soft_lutpair1";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_r_SM_Main_reg[0]\ : label is "s_IDLE:000,s_TX_START_BIT:001,s_TX_DATA_BITS:010,s_CLEANUP:100,s_TX_STOP_BIT:011";
  attribute FSM_ENCODED_STATES of \FSM_sequential_r_SM_Main_reg[1]\ : label is "s_IDLE:000,s_TX_START_BIT:001,s_TX_DATA_BITS:010,s_CLEANUP:100,s_TX_STOP_BIT:011";
  attribute FSM_ENCODED_STATES of \FSM_sequential_r_SM_Main_reg[2]\ : label is "s_IDLE:000,s_TX_START_BIT:001,s_TX_DATA_BITS:010,s_CLEANUP:100,s_TX_STOP_BIT:011";
  attribute SOFT_HLUTNM of o_Tx_Serial_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \r_Bit_Index[2]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \r_Clock_Count[0]_i_1__0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \r_Clock_Count[1]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \r_Clock_Count[2]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \r_Clock_Count[3]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \r_Clock_Count[5]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \r_Clock_Count[6]_i_3\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of r_Tx_Done_i_1 : label is "soft_lutpair1";
begin
  o_Tx_Active <= \^o_tx_active\;
  o_Tx_Done <= \^o_tx_done\;
  o_Tx_Serial <= \^o_tx_serial\;
\FSM_sequential_r_SM_Main[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000C0E2C3E2"
    )
        port map (
      I0 => i_Tx_DV,
      I1 => r_SM_Main(0),
      I2 => r_Tx_Active_i_2_n_0,
      I3 => r_SM_Main(1),
      I4 => \FSM_sequential_r_SM_Main[0]_i_2_n_0\,
      I5 => r_SM_Main(2),
      O => \FSM_sequential_r_SM_Main[0]_i_1_n_0\
    );
\FSM_sequential_r_SM_Main[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => \r_Bit_Index_reg_n_0_[2]\,
      I1 => \r_Bit_Index_reg_n_0_[1]\,
      I2 => \r_Bit_Index_reg_n_0_[0]\,
      O => \FSM_sequential_r_SM_Main[0]_i_2_n_0\
    );
\FSM_sequential_r_SM_Main[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00D2"
    )
        port map (
      I0 => r_SM_Main(0),
      I1 => r_Tx_Active_i_2_n_0,
      I2 => r_SM_Main(1),
      I3 => r_SM_Main(2),
      O => \FSM_sequential_r_SM_Main[1]_i_1_n_0\
    );
\FSM_sequential_r_SM_Main[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => r_SM_Main(0),
      I1 => r_Tx_Active_i_2_n_0,
      I2 => r_SM_Main(1),
      I3 => r_SM_Main(2),
      O => \FSM_sequential_r_SM_Main[2]_i_1_n_0\
    );
\FSM_sequential_r_SM_Main_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \FSM_sequential_r_SM_Main[0]_i_1_n_0\,
      Q => r_SM_Main(0),
      R => '0'
    );
\FSM_sequential_r_SM_Main_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \FSM_sequential_r_SM_Main[1]_i_1_n_0\,
      Q => r_SM_Main(1),
      R => '0'
    );
\FSM_sequential_r_SM_Main_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \FSM_sequential_r_SM_Main[2]_i_1_n_0\,
      Q => r_SM_Main(2),
      R => '0'
    );
o_Tx_Serial_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BB8BB88B"
    )
        port map (
      I0 => \^o_tx_serial\,
      I1 => r_SM_Main(2),
      I2 => r_SM_Main(0),
      I3 => r_SM_Main(1),
      I4 => o_Tx_Serial_reg_i_2_n_0,
      O => o_Tx_Serial_i_1_n_0
    );
o_Tx_Serial_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r_Tx_Data(3),
      I1 => r_Tx_Data(2),
      I2 => \r_Bit_Index_reg_n_0_[1]\,
      I3 => r_Tx_Data(1),
      I4 => \r_Bit_Index_reg_n_0_[0]\,
      I5 => r_Tx_Data(0),
      O => o_Tx_Serial_i_3_n_0
    );
o_Tx_Serial_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => r_Tx_Data(7),
      I1 => r_Tx_Data(6),
      I2 => \r_Bit_Index_reg_n_0_[1]\,
      I3 => r_Tx_Data(5),
      I4 => \r_Bit_Index_reg_n_0_[0]\,
      I5 => r_Tx_Data(4),
      O => o_Tx_Serial_i_4_n_0
    );
o_Tx_Serial_reg: unisim.vcomponents.FDRE
     port map (
      C => i_Clock,
      CE => '1',
      D => o_Tx_Serial_i_1_n_0,
      Q => \^o_tx_serial\,
      R => '0'
    );
o_Tx_Serial_reg_i_2: unisim.vcomponents.MUXF7
     port map (
      I0 => o_Tx_Serial_i_3_n_0,
      I1 => o_Tx_Serial_i_4_n_0,
      O => o_Tx_Serial_reg_i_2_n_0,
      S => \r_Bit_Index_reg_n_0_[2]\
    );
\r_Bit_Index[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAA98AAAAAA00"
    )
        port map (
      I0 => \r_Bit_Index_reg_n_0_[0]\,
      I1 => r_Tx_Active_i_2_n_0,
      I2 => \FSM_sequential_r_SM_Main[0]_i_2_n_0\,
      I3 => r_SM_Main(2),
      I4 => r_SM_Main(0),
      I5 => r_SM_Main(1),
      O => \r_Bit_Index[0]_i_1_n_0\
    );
\r_Bit_Index[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA9AAA00"
    )
        port map (
      I0 => \r_Bit_Index_reg_n_0_[1]\,
      I1 => r_Tx_Active_i_2_n_0,
      I2 => \r_Bit_Index_reg_n_0_[0]\,
      I3 => \r_Bit_Index[2]_i_2_n_0\,
      I4 => r_SM_Main(1),
      O => \r_Bit_Index[1]_i_1_n_0\
    );
\r_Bit_Index[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAA9AAAAAAA0000"
    )
        port map (
      I0 => \r_Bit_Index_reg_n_0_[2]\,
      I1 => r_Tx_Active_i_2_n_0,
      I2 => \r_Bit_Index_reg_n_0_[0]\,
      I3 => \r_Bit_Index_reg_n_0_[1]\,
      I4 => \r_Bit_Index[2]_i_2_n_0\,
      I5 => r_SM_Main(1),
      O => \r_Bit_Index[2]_i_1_n_0\
    );
\r_Bit_Index[2]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => r_SM_Main(2),
      I1 => r_SM_Main(0),
      O => \r_Bit_Index[2]_i_2_n_0\
    );
\r_Bit_Index_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_Bit_Index[0]_i_1_n_0\,
      Q => \r_Bit_Index_reg_n_0_[0]\,
      R => '0'
    );
\r_Bit_Index_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_Bit_Index[1]_i_1_n_0\,
      Q => \r_Bit_Index_reg_n_0_[1]\,
      R => '0'
    );
\r_Bit_Index_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => \r_Bit_Index[2]_i_1_n_0\,
      Q => \r_Bit_Index_reg_n_0_[2]\,
      R => '0'
    );
\r_Clock_Count[0]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => r_Tx_Active_i_2_n_0,
      I1 => \r_Clock_Count_reg_n_0_[0]\,
      O => \r_Clock_Count[0]_i_1__0_n_0\
    );
\r_Clock_Count[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => r_Clock_Count_reg(1),
      I1 => \r_Clock_Count_reg_n_0_[0]\,
      I2 => r_Tx_Active_i_2_n_0,
      O => r_Clock_Count(1)
    );
\r_Clock_Count[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6A00"
    )
        port map (
      I0 => r_Clock_Count_reg(2),
      I1 => r_Clock_Count_reg(1),
      I2 => \r_Clock_Count_reg_n_0_[0]\,
      I3 => r_Tx_Active_i_2_n_0,
      O => r_Clock_Count(2)
    );
\r_Clock_Count[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAA0000"
    )
        port map (
      I0 => r_Clock_Count_reg(3),
      I1 => r_Clock_Count_reg(2),
      I2 => \r_Clock_Count_reg_n_0_[0]\,
      I3 => r_Clock_Count_reg(1),
      I4 => r_Tx_Active_i_2_n_0,
      O => r_Clock_Count(3)
    );
\r_Clock_Count[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAA00000000"
    )
        port map (
      I0 => r_Clock_Count_reg(4),
      I1 => r_Clock_Count_reg(3),
      I2 => r_Clock_Count_reg(1),
      I3 => \r_Clock_Count_reg_n_0_[0]\,
      I4 => r_Clock_Count_reg(2),
      I5 => r_Tx_Active_i_2_n_0,
      O => r_Clock_Count(4)
    );
\r_Clock_Count[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => r_Clock_Count_reg(5),
      I1 => \r_Clock_Count[6]_i_4_n_0\,
      I2 => r_Tx_Active_i_2_n_0,
      O => r_Clock_Count(5)
    );
\r_Clock_Count[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => r_SM_Main(1),
      I1 => r_SM_Main(0),
      I2 => r_SM_Main(2),
      O => r_Clock_Count0
    );
\r_Clock_Count[6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"54"
    )
        port map (
      I0 => r_SM_Main(2),
      I1 => r_SM_Main(1),
      I2 => r_SM_Main(0),
      O => r_Clock_Count_0
    );
\r_Clock_Count[6]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6A00"
    )
        port map (
      I0 => r_Clock_Count_reg(6),
      I1 => r_Clock_Count_reg(5),
      I2 => \r_Clock_Count[6]_i_4_n_0\,
      I3 => r_Tx_Active_i_2_n_0,
      O => r_Clock_Count(6)
    );
\r_Clock_Count[6]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80000000"
    )
        port map (
      I0 => r_Clock_Count_reg(4),
      I1 => r_Clock_Count_reg(2),
      I2 => \r_Clock_Count_reg_n_0_[0]\,
      I3 => r_Clock_Count_reg(1),
      I4 => r_Clock_Count_reg(3),
      O => \r_Clock_Count[6]_i_4_n_0\
    );
\r_Clock_Count_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => r_Clock_Count_0,
      D => \r_Clock_Count[0]_i_1__0_n_0\,
      Q => \r_Clock_Count_reg_n_0_[0]\,
      R => r_Clock_Count0
    );
\r_Clock_Count_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => r_Clock_Count_0,
      D => r_Clock_Count(1),
      Q => r_Clock_Count_reg(1),
      R => r_Clock_Count0
    );
\r_Clock_Count_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => r_Clock_Count_0,
      D => r_Clock_Count(2),
      Q => r_Clock_Count_reg(2),
      R => r_Clock_Count0
    );
\r_Clock_Count_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => r_Clock_Count_0,
      D => r_Clock_Count(3),
      Q => r_Clock_Count_reg(3),
      R => r_Clock_Count0
    );
\r_Clock_Count_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => r_Clock_Count_0,
      D => r_Clock_Count(4),
      Q => r_Clock_Count_reg(4),
      R => r_Clock_Count0
    );
\r_Clock_Count_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => r_Clock_Count_0,
      D => r_Clock_Count(5),
      Q => r_Clock_Count_reg(5),
      R => r_Clock_Count0
    );
\r_Clock_Count_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => r_Clock_Count_0,
      D => r_Clock_Count(6),
      Q => r_Clock_Count_reg(6),
      R => r_Clock_Count0
    );
r_Tx_Active_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBBFF00000030"
    )
        port map (
      I0 => r_Tx_Active_i_2_n_0,
      I1 => r_SM_Main(0),
      I2 => i_Tx_DV,
      I3 => r_SM_Main(1),
      I4 => r_SM_Main(2),
      I5 => \^o_tx_active\,
      O => r_Tx_Active_i_1_n_0
    );
r_Tx_Active_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00155555FFFFFFFF"
    )
        port map (
      I0 => r_Clock_Count_reg(5),
      I1 => r_Clock_Count_reg(1),
      I2 => r_Clock_Count_reg(2),
      I3 => r_Clock_Count_reg(3),
      I4 => r_Clock_Count_reg(4),
      I5 => r_Clock_Count_reg(6),
      O => r_Tx_Active_i_2_n_0
    );
r_Tx_Active_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => r_Tx_Active_i_1_n_0,
      Q => \^o_tx_active\,
      R => '0'
    );
\r_Tx_Data[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0010"
    )
        port map (
      I0 => r_SM_Main(0),
      I1 => r_SM_Main(2),
      I2 => i_Tx_DV,
      I3 => r_SM_Main(1),
      O => r_Tx_Data_1
    );
\r_Tx_Data_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => r_Tx_Data_1,
      D => i_Tx_Byte(0),
      Q => r_Tx_Data(0),
      R => '0'
    );
\r_Tx_Data_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => r_Tx_Data_1,
      D => i_Tx_Byte(1),
      Q => r_Tx_Data(1),
      R => '0'
    );
\r_Tx_Data_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => r_Tx_Data_1,
      D => i_Tx_Byte(2),
      Q => r_Tx_Data(2),
      R => '0'
    );
\r_Tx_Data_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => r_Tx_Data_1,
      D => i_Tx_Byte(3),
      Q => r_Tx_Data(3),
      R => '0'
    );
\r_Tx_Data_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => r_Tx_Data_1,
      D => i_Tx_Byte(4),
      Q => r_Tx_Data(4),
      R => '0'
    );
\r_Tx_Data_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => r_Tx_Data_1,
      D => i_Tx_Byte(5),
      Q => r_Tx_Data(5),
      R => '0'
    );
\r_Tx_Data_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => r_Tx_Data_1,
      D => i_Tx_Byte(6),
      Q => r_Tx_Data(6),
      R => '0'
    );
\r_Tx_Data_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => r_Tx_Data_1,
      D => i_Tx_Byte(7),
      Q => r_Tx_Data(7),
      R => '0'
    );
r_Tx_Done_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFA100A"
    )
        port map (
      I0 => r_SM_Main(2),
      I1 => r_Tx_Active_i_2_n_0,
      I2 => r_SM_Main(0),
      I3 => r_SM_Main(1),
      I4 => \^o_tx_done\,
      O => r_Tx_Done_i_1_n_0
    );
r_Tx_Done_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => i_Clock,
      CE => '1',
      D => r_Tx_Done_i_1_n_0,
      Q => \^o_tx_done\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_PL_recieved_azimut_v1_0 is
  port (
    o_Rx_Byte : out STD_LOGIC_VECTOR ( 7 downto 0 );
    o_Tx_Active : out STD_LOGIC;
    o_Tx_Serial : out STD_LOGIC;
    o_Tx_Done : out STD_LOGIC;
    o_Rx_DV : out STD_LOGIC;
    i_Tx_DV : in STD_LOGIC;
    i_Clock : in STD_LOGIC;
    i_Tx_Byte : in STD_LOGIC_VECTOR ( 7 downto 0 );
    i_Rx_Serial : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_PL_recieved_azimut_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_PL_recieved_azimut_v1_0 is
begin
nolabel_line27: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_tx
     port map (
      i_Clock => i_Clock,
      i_Tx_Byte(7 downto 0) => i_Tx_Byte(7 downto 0),
      i_Tx_DV => i_Tx_DV,
      o_Tx_Active => o_Tx_Active,
      o_Tx_Done => o_Tx_Done,
      o_Tx_Serial => o_Tx_Serial
    );
nolabel_line36: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_rx
     port map (
      i_Clock => i_Clock,
      i_Rx_Serial => i_Rx_Serial,
      o_Rx_Byte(7 downto 0) => o_Rx_Byte(7 downto 0),
      o_Rx_DV => o_Rx_DV
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    i_Clock : in STD_LOGIC;
    i_Tx_DV : in STD_LOGIC;
    i_Tx_Byte : in STD_LOGIC_VECTOR ( 7 downto 0 );
    o_Tx_Active : out STD_LOGIC;
    o_Tx_Serial : out STD_LOGIC;
    o_Tx_Done : out STD_LOGIC;
    i_Rx_Serial : in STD_LOGIC;
    o_Rx_DV : out STD_LOGIC;
    o_Rx_Byte : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_uart_PL_recieved_azi_0_0,uart_PL_recieved_azimut_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "uart_PL_recieved_azimut_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of i_Clock : signal is "xilinx.com:signal:clock:1.0 i_Clock CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of i_Clock : signal is "XIL_INTERFACENAME i_Clock, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_PL_recieved_azimut_v1_0
     port map (
      i_Clock => i_Clock,
      i_Rx_Serial => i_Rx_Serial,
      i_Tx_Byte(7 downto 0) => i_Tx_Byte(7 downto 0),
      i_Tx_DV => i_Tx_DV,
      o_Rx_Byte(7 downto 0) => o_Rx_Byte(7 downto 0),
      o_Rx_DV => o_Rx_DV,
      o_Tx_Active => o_Tx_Active,
      o_Tx_Done => o_Tx_Done,
      o_Tx_Serial => o_Tx_Serial
    );
end STRUCTURE;

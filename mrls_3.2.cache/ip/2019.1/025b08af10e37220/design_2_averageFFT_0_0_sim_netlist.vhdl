-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Wed Dec  8 13:30:13 2021
-- Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_averageFFT_0_0_sim_netlist.vhdl
-- Design      : design_2_averageFFT_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0 is
  port (
    reset_cnt_trig_ila : out STD_LOGIC;
    \azimut_r_reg[2]_0\ : out STD_LOGIC;
    \azimut_r_reg[0]_0\ : out STD_LOGIC;
    \azimut_r_reg[1]_0\ : out STD_LOGIC;
    azimut8 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    interrupt_frame : out STD_LOGIC;
    allowed_clk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    clk_10MHz : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    azimut_0 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0 is
  signal \^azimut8\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal azimut8_r : STD_LOGIC;
  signal \azimut8_r[15]_i_1_n_0\ : STD_LOGIC;
  signal \azimut8_r[15]_i_4_n_0\ : STD_LOGIC;
  signal \azimut8_r[15]_i_5_n_0\ : STD_LOGIC;
  signal \azimut8_r[15]_i_6_n_0\ : STD_LOGIC;
  signal \azimut8_r[3]_i_2_n_0\ : STD_LOGIC;
  signal \azimut8_r_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \azimut8_r_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \azimut8_r_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \azimut8_r_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \azimut8_r_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \azimut8_r_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \azimut8_r_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \azimut8_r_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \azimut8_r_reg[15]_i_3_n_1\ : STD_LOGIC;
  signal \azimut8_r_reg[15]_i_3_n_2\ : STD_LOGIC;
  signal \azimut8_r_reg[15]_i_3_n_3\ : STD_LOGIC;
  signal \azimut8_r_reg[15]_i_3_n_4\ : STD_LOGIC;
  signal \azimut8_r_reg[15]_i_3_n_5\ : STD_LOGIC;
  signal \azimut8_r_reg[15]_i_3_n_6\ : STD_LOGIC;
  signal \azimut8_r_reg[15]_i_3_n_7\ : STD_LOGIC;
  signal \azimut8_r_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \azimut8_r_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \azimut8_r_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \azimut8_r_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \azimut8_r_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \azimut8_r_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \azimut8_r_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \azimut8_r_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \azimut8_r_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \azimut8_r_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \azimut8_r_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \azimut8_r_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \azimut8_r_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \azimut8_r_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \azimut8_r_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \azimut8_r_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal azimut_0_prev : STD_LOGIC;
  signal azimut_r : STD_LOGIC;
  signal \azimut_r[0]_i_1_n_0\ : STD_LOGIC;
  signal \azimut_r[1]_i_1_n_0\ : STD_LOGIC;
  signal \azimut_r[2]_i_1_n_0\ : STD_LOGIC;
  signal \^azimut_r_reg[0]_0\ : STD_LOGIC;
  signal \^azimut_r_reg[1]_0\ : STD_LOGIC;
  signal \^azimut_r_reg[2]_0\ : STD_LOGIC;
  signal clear : STD_LOGIC;
  signal \cnt100[0]_i_3_n_0\ : STD_LOGIC;
  signal cnt100_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \cnt100_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \cnt100_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_low_allowed_clk[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_allowed_clk[0]_i_3_n_0\ : STD_LOGIC;
  signal cnt_low_allowed_clk_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \^interrupt_frame\ : STD_LOGIC;
  signal interrupt_frame_i_10_n_0 : STD_LOGIC;
  signal interrupt_frame_i_11_n_0 : STD_LOGIC;
  signal interrupt_frame_i_12_n_0 : STD_LOGIC;
  signal interrupt_frame_i_13_n_0 : STD_LOGIC;
  signal interrupt_frame_i_14_n_0 : STD_LOGIC;
  signal interrupt_frame_i_15_n_0 : STD_LOGIC;
  signal interrupt_frame_i_16_n_0 : STD_LOGIC;
  signal interrupt_frame_i_1_n_0 : STD_LOGIC;
  signal interrupt_frame_i_2_n_0 : STD_LOGIC;
  signal interrupt_frame_i_3_n_0 : STD_LOGIC;
  signal interrupt_frame_i_4_n_0 : STD_LOGIC;
  signal interrupt_frame_i_5_n_0 : STD_LOGIC;
  signal interrupt_frame_i_6_n_0 : STD_LOGIC;
  signal interrupt_frame_i_7_n_0 : STD_LOGIC;
  signal interrupt_frame_i_8_n_0 : STD_LOGIC;
  signal interrupt_frame_i_9_n_0 : STD_LOGIC;
  signal reset_cnt_trig0 : STD_LOGIC;
  signal \^reset_cnt_trig_ila\ : STD_LOGIC;
  signal \NLW_azimut8_r_reg[15]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_cnt100_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_cnt_low_allowed_clk_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \azimut_r[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \azimut_r[1]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of interrupt_frame_i_16 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of interrupt_frame_i_4 : label is "soft_lutpair1";
begin
  azimut8(15 downto 0) <= \^azimut8\(15 downto 0);
  \azimut_r_reg[0]_0\ <= \^azimut_r_reg[0]_0\;
  \azimut_r_reg[1]_0\ <= \^azimut_r_reg[1]_0\;
  \azimut_r_reg[2]_0\ <= \^azimut_r_reg[2]_0\;
  interrupt_frame <= \^interrupt_frame\;
  reset_cnt_trig_ila <= \^reset_cnt_trig_ila\;
\azimut8_r[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^reset_cnt_trig_ila\,
      I1 => m00_axis_aresetn,
      O => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \azimut8_r[15]_i_4_n_0\,
      I1 => \azimut8_r[15]_i_5_n_0\,
      I2 => \^azimut_r_reg[2]_0\,
      I3 => \^azimut_r_reg[0]_0\,
      I4 => \^azimut_r_reg[1]_0\,
      I5 => \azimut8_r[15]_i_6_n_0\,
      O => azimut8_r
    );
\azimut8_r[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => cnt_low_allowed_clk_reg(6),
      I1 => cnt_low_allowed_clk_reg(7),
      I2 => cnt_low_allowed_clk_reg(4),
      I3 => cnt_low_allowed_clk_reg(5),
      I4 => cnt_low_allowed_clk_reg(9),
      I5 => cnt_low_allowed_clk_reg(8),
      O => \azimut8_r[15]_i_4_n_0\
    );
\azimut8_r[15]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => cnt_low_allowed_clk_reg(0),
      I1 => cnt_low_allowed_clk_reg(1),
      I2 => cnt_low_allowed_clk_reg(2),
      I3 => cnt_low_allowed_clk_reg(3),
      O => \azimut8_r[15]_i_5_n_0\
    );
\azimut8_r[15]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => cnt_low_allowed_clk_reg(12),
      I1 => cnt_low_allowed_clk_reg(13),
      I2 => cnt_low_allowed_clk_reg(10),
      I3 => cnt_low_allowed_clk_reg(11),
      I4 => cnt_low_allowed_clk_reg(15),
      I5 => cnt_low_allowed_clk_reg(14),
      O => \azimut8_r[15]_i_6_n_0\
    );
\azimut8_r[3]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^azimut8\(0),
      O => \azimut8_r[3]_i_2_n_0\
    );
\azimut8_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[3]_i_1_n_7\,
      Q => \^azimut8\(0),
      R => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[11]_i_1_n_5\,
      Q => \^azimut8\(10),
      R => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[11]_i_1_n_4\,
      Q => \^azimut8\(11),
      R => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \azimut8_r_reg[7]_i_1_n_0\,
      CO(3) => \azimut8_r_reg[11]_i_1_n_0\,
      CO(2) => \azimut8_r_reg[11]_i_1_n_1\,
      CO(1) => \azimut8_r_reg[11]_i_1_n_2\,
      CO(0) => \azimut8_r_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \azimut8_r_reg[11]_i_1_n_4\,
      O(2) => \azimut8_r_reg[11]_i_1_n_5\,
      O(1) => \azimut8_r_reg[11]_i_1_n_6\,
      O(0) => \azimut8_r_reg[11]_i_1_n_7\,
      S(3 downto 0) => \^azimut8\(11 downto 8)
    );
\azimut8_r_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[15]_i_3_n_7\,
      Q => \^azimut8\(12),
      R => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[15]_i_3_n_6\,
      Q => \^azimut8\(13),
      R => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[15]_i_3_n_5\,
      Q => \^azimut8\(14),
      R => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[15]_i_3_n_4\,
      Q => \^azimut8\(15),
      R => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r_reg[15]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \azimut8_r_reg[11]_i_1_n_0\,
      CO(3) => \NLW_azimut8_r_reg[15]_i_3_CO_UNCONNECTED\(3),
      CO(2) => \azimut8_r_reg[15]_i_3_n_1\,
      CO(1) => \azimut8_r_reg[15]_i_3_n_2\,
      CO(0) => \azimut8_r_reg[15]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \azimut8_r_reg[15]_i_3_n_4\,
      O(2) => \azimut8_r_reg[15]_i_3_n_5\,
      O(1) => \azimut8_r_reg[15]_i_3_n_6\,
      O(0) => \azimut8_r_reg[15]_i_3_n_7\,
      S(3 downto 0) => \^azimut8\(15 downto 12)
    );
\azimut8_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[3]_i_1_n_6\,
      Q => \^azimut8\(1),
      R => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[3]_i_1_n_5\,
      Q => \^azimut8\(2),
      R => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[3]_i_1_n_4\,
      Q => \^azimut8\(3),
      R => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \azimut8_r_reg[3]_i_1_n_0\,
      CO(2) => \azimut8_r_reg[3]_i_1_n_1\,
      CO(1) => \azimut8_r_reg[3]_i_1_n_2\,
      CO(0) => \azimut8_r_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \azimut8_r_reg[3]_i_1_n_4\,
      O(2) => \azimut8_r_reg[3]_i_1_n_5\,
      O(1) => \azimut8_r_reg[3]_i_1_n_6\,
      O(0) => \azimut8_r_reg[3]_i_1_n_7\,
      S(3 downto 1) => \^azimut8\(3 downto 1),
      S(0) => \azimut8_r[3]_i_2_n_0\
    );
\azimut8_r_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[7]_i_1_n_7\,
      Q => \^azimut8\(4),
      R => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[7]_i_1_n_6\,
      Q => \^azimut8\(5),
      R => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[7]_i_1_n_5\,
      Q => \^azimut8\(6),
      R => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[7]_i_1_n_4\,
      Q => \^azimut8\(7),
      R => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \azimut8_r_reg[3]_i_1_n_0\,
      CO(3) => \azimut8_r_reg[7]_i_1_n_0\,
      CO(2) => \azimut8_r_reg[7]_i_1_n_1\,
      CO(1) => \azimut8_r_reg[7]_i_1_n_2\,
      CO(0) => \azimut8_r_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \azimut8_r_reg[7]_i_1_n_4\,
      O(2) => \azimut8_r_reg[7]_i_1_n_5\,
      O(1) => \azimut8_r_reg[7]_i_1_n_6\,
      O(0) => \azimut8_r_reg[7]_i_1_n_7\,
      S(3 downto 0) => \^azimut8\(7 downto 4)
    );
\azimut8_r_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[11]_i_1_n_7\,
      Q => \^azimut8\(8),
      R => \azimut8_r[15]_i_1_n_0\
    );
\azimut8_r_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut8_r,
      D => \azimut8_r_reg[11]_i_1_n_6\,
      Q => \^azimut8\(9),
      R => \azimut8_r[15]_i_1_n_0\
    );
azimut_0_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => azimut_0,
      Q => azimut_0_prev,
      R => '0'
    );
\azimut_r[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0060"
    )
        port map (
      I0 => \^azimut_r_reg[0]_0\,
      I1 => azimut_r,
      I2 => m00_axis_aresetn,
      I3 => \^reset_cnt_trig_ila\,
      O => \azimut_r[0]_i_1_n_0\
    );
\azimut_r[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00006A00"
    )
        port map (
      I0 => \^azimut_r_reg[1]_0\,
      I1 => azimut_r,
      I2 => \^azimut_r_reg[0]_0\,
      I3 => m00_axis_aresetn,
      I4 => \^reset_cnt_trig_ila\,
      O => \azimut_r[1]_i_1_n_0\
    );
\azimut_r[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000006AAA0000"
    )
        port map (
      I0 => \^azimut_r_reg[2]_0\,
      I1 => azimut_r,
      I2 => \^azimut_r_reg[0]_0\,
      I3 => \^azimut_r_reg[1]_0\,
      I4 => m00_axis_aresetn,
      I5 => \^reset_cnt_trig_ila\,
      O => \azimut_r[2]_i_1_n_0\
    );
\azimut_r[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0020000000000000"
    )
        port map (
      I0 => \azimut8_r[15]_i_6_n_0\,
      I1 => cnt_low_allowed_clk_reg(0),
      I2 => cnt_low_allowed_clk_reg(1),
      I3 => cnt_low_allowed_clk_reg(2),
      I4 => cnt_low_allowed_clk_reg(3),
      I5 => \azimut8_r[15]_i_4_n_0\,
      O => azimut_r
    );
\azimut_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \azimut_r[0]_i_1_n_0\,
      Q => \^azimut_r_reg[0]_0\,
      R => '0'
    );
\azimut_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \azimut_r[1]_i_1_n_0\,
      Q => \^azimut_r_reg[1]_0\,
      R => '0'
    );
\azimut_r_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \azimut_r[2]_i_1_n_0\,
      Q => \^azimut_r_reg[2]_0\,
      R => '0'
    );
\cnt100[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => allowed_clk,
      O => clear
    );
\cnt100[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt100_reg(0),
      O => \cnt100[0]_i_3_n_0\
    );
\cnt100_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[0]_i_2_n_7\,
      Q => cnt100_reg(0),
      R => clear
    );
\cnt100_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt100_reg[0]_i_2_n_0\,
      CO(2) => \cnt100_reg[0]_i_2_n_1\,
      CO(1) => \cnt100_reg[0]_i_2_n_2\,
      CO(0) => \cnt100_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt100_reg[0]_i_2_n_4\,
      O(2) => \cnt100_reg[0]_i_2_n_5\,
      O(1) => \cnt100_reg[0]_i_2_n_6\,
      O(0) => \cnt100_reg[0]_i_2_n_7\,
      S(3 downto 1) => cnt100_reg(3 downto 1),
      S(0) => \cnt100[0]_i_3_n_0\
    );
\cnt100_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[8]_i_1_n_5\,
      Q => cnt100_reg(10),
      R => clear
    );
\cnt100_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[8]_i_1_n_4\,
      Q => cnt100_reg(11),
      R => clear
    );
\cnt100_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[12]_i_1_n_7\,
      Q => cnt100_reg(12),
      R => clear
    );
\cnt100_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[8]_i_1_n_0\,
      CO(3) => \cnt100_reg[12]_i_1_n_0\,
      CO(2) => \cnt100_reg[12]_i_1_n_1\,
      CO(1) => \cnt100_reg[12]_i_1_n_2\,
      CO(0) => \cnt100_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[12]_i_1_n_4\,
      O(2) => \cnt100_reg[12]_i_1_n_5\,
      O(1) => \cnt100_reg[12]_i_1_n_6\,
      O(0) => \cnt100_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(15 downto 12)
    );
\cnt100_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[12]_i_1_n_6\,
      Q => cnt100_reg(13),
      R => clear
    );
\cnt100_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[12]_i_1_n_5\,
      Q => cnt100_reg(14),
      R => clear
    );
\cnt100_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[12]_i_1_n_4\,
      Q => cnt100_reg(15),
      R => clear
    );
\cnt100_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[16]_i_1_n_7\,
      Q => cnt100_reg(16),
      R => clear
    );
\cnt100_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[12]_i_1_n_0\,
      CO(3) => \cnt100_reg[16]_i_1_n_0\,
      CO(2) => \cnt100_reg[16]_i_1_n_1\,
      CO(1) => \cnt100_reg[16]_i_1_n_2\,
      CO(0) => \cnt100_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[16]_i_1_n_4\,
      O(2) => \cnt100_reg[16]_i_1_n_5\,
      O(1) => \cnt100_reg[16]_i_1_n_6\,
      O(0) => \cnt100_reg[16]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(19 downto 16)
    );
\cnt100_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[16]_i_1_n_6\,
      Q => cnt100_reg(17),
      R => clear
    );
\cnt100_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[16]_i_1_n_5\,
      Q => cnt100_reg(18),
      R => clear
    );
\cnt100_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[16]_i_1_n_4\,
      Q => cnt100_reg(19),
      R => clear
    );
\cnt100_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[0]_i_2_n_6\,
      Q => cnt100_reg(1),
      R => clear
    );
\cnt100_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[20]_i_1_n_7\,
      Q => cnt100_reg(20),
      R => clear
    );
\cnt100_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[16]_i_1_n_0\,
      CO(3) => \cnt100_reg[20]_i_1_n_0\,
      CO(2) => \cnt100_reg[20]_i_1_n_1\,
      CO(1) => \cnt100_reg[20]_i_1_n_2\,
      CO(0) => \cnt100_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[20]_i_1_n_4\,
      O(2) => \cnt100_reg[20]_i_1_n_5\,
      O(1) => \cnt100_reg[20]_i_1_n_6\,
      O(0) => \cnt100_reg[20]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(23 downto 20)
    );
\cnt100_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[20]_i_1_n_6\,
      Q => cnt100_reg(21),
      R => clear
    );
\cnt100_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[20]_i_1_n_5\,
      Q => cnt100_reg(22),
      R => clear
    );
\cnt100_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[20]_i_1_n_4\,
      Q => cnt100_reg(23),
      R => clear
    );
\cnt100_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[24]_i_1_n_7\,
      Q => cnt100_reg(24),
      R => clear
    );
\cnt100_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[20]_i_1_n_0\,
      CO(3) => \cnt100_reg[24]_i_1_n_0\,
      CO(2) => \cnt100_reg[24]_i_1_n_1\,
      CO(1) => \cnt100_reg[24]_i_1_n_2\,
      CO(0) => \cnt100_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[24]_i_1_n_4\,
      O(2) => \cnt100_reg[24]_i_1_n_5\,
      O(1) => \cnt100_reg[24]_i_1_n_6\,
      O(0) => \cnt100_reg[24]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(27 downto 24)
    );
\cnt100_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[24]_i_1_n_6\,
      Q => cnt100_reg(25),
      R => clear
    );
\cnt100_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[24]_i_1_n_5\,
      Q => cnt100_reg(26),
      R => clear
    );
\cnt100_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[24]_i_1_n_4\,
      Q => cnt100_reg(27),
      R => clear
    );
\cnt100_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[28]_i_1_n_7\,
      Q => cnt100_reg(28),
      R => clear
    );
\cnt100_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[24]_i_1_n_0\,
      CO(3) => \NLW_cnt100_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt100_reg[28]_i_1_n_1\,
      CO(1) => \cnt100_reg[28]_i_1_n_2\,
      CO(0) => \cnt100_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[28]_i_1_n_4\,
      O(2) => \cnt100_reg[28]_i_1_n_5\,
      O(1) => \cnt100_reg[28]_i_1_n_6\,
      O(0) => \cnt100_reg[28]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(31 downto 28)
    );
\cnt100_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[28]_i_1_n_6\,
      Q => cnt100_reg(29),
      R => clear
    );
\cnt100_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[0]_i_2_n_5\,
      Q => cnt100_reg(2),
      R => clear
    );
\cnt100_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[28]_i_1_n_5\,
      Q => cnt100_reg(30),
      R => clear
    );
\cnt100_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[28]_i_1_n_4\,
      Q => cnt100_reg(31),
      R => clear
    );
\cnt100_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[0]_i_2_n_4\,
      Q => cnt100_reg(3),
      R => clear
    );
\cnt100_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[4]_i_1_n_7\,
      Q => cnt100_reg(4),
      R => clear
    );
\cnt100_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[0]_i_2_n_0\,
      CO(3) => \cnt100_reg[4]_i_1_n_0\,
      CO(2) => \cnt100_reg[4]_i_1_n_1\,
      CO(1) => \cnt100_reg[4]_i_1_n_2\,
      CO(0) => \cnt100_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[4]_i_1_n_4\,
      O(2) => \cnt100_reg[4]_i_1_n_5\,
      O(1) => \cnt100_reg[4]_i_1_n_6\,
      O(0) => \cnt100_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(7 downto 4)
    );
\cnt100_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[4]_i_1_n_6\,
      Q => cnt100_reg(5),
      R => clear
    );
\cnt100_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[4]_i_1_n_5\,
      Q => cnt100_reg(6),
      R => clear
    );
\cnt100_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[4]_i_1_n_4\,
      Q => cnt100_reg(7),
      R => clear
    );
\cnt100_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[8]_i_1_n_7\,
      Q => cnt100_reg(8),
      R => clear
    );
\cnt100_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[4]_i_1_n_0\,
      CO(3) => \cnt100_reg[8]_i_1_n_0\,
      CO(2) => \cnt100_reg[8]_i_1_n_1\,
      CO(1) => \cnt100_reg[8]_i_1_n_2\,
      CO(0) => \cnt100_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[8]_i_1_n_4\,
      O(2) => \cnt100_reg[8]_i_1_n_5\,
      O(1) => \cnt100_reg[8]_i_1_n_6\,
      O(0) => \cnt100_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(11 downto 8)
    );
\cnt100_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[8]_i_1_n_6\,
      Q => cnt100_reg(9),
      R => clear
    );
\cnt_low_allowed_clk[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => allowed_clk,
      I1 => m00_axis_aresetn,
      O => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_low_allowed_clk_reg(0),
      O => \cnt_low_allowed_clk[0]_i_3_n_0\
    );
\cnt_low_allowed_clk_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[0]_i_2_n_7\,
      Q => cnt_low_allowed_clk_reg(0),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_low_allowed_clk_reg[0]_i_2_n_0\,
      CO(2) => \cnt_low_allowed_clk_reg[0]_i_2_n_1\,
      CO(1) => \cnt_low_allowed_clk_reg[0]_i_2_n_2\,
      CO(0) => \cnt_low_allowed_clk_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_low_allowed_clk_reg[0]_i_2_n_4\,
      O(2) => \cnt_low_allowed_clk_reg[0]_i_2_n_5\,
      O(1) => \cnt_low_allowed_clk_reg[0]_i_2_n_6\,
      O(0) => \cnt_low_allowed_clk_reg[0]_i_2_n_7\,
      S(3 downto 1) => cnt_low_allowed_clk_reg(3 downto 1),
      S(0) => \cnt_low_allowed_clk[0]_i_3_n_0\
    );
\cnt_low_allowed_clk_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[8]_i_1_n_5\,
      Q => cnt_low_allowed_clk_reg(10),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[8]_i_1_n_4\,
      Q => cnt_low_allowed_clk_reg(11),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[12]_i_1_n_7\,
      Q => cnt_low_allowed_clk_reg(12),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_low_allowed_clk_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt_low_allowed_clk_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_low_allowed_clk_reg[12]_i_1_n_1\,
      CO(1) => \cnt_low_allowed_clk_reg[12]_i_1_n_2\,
      CO(0) => \cnt_low_allowed_clk_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_low_allowed_clk_reg[12]_i_1_n_4\,
      O(2) => \cnt_low_allowed_clk_reg[12]_i_1_n_5\,
      O(1) => \cnt_low_allowed_clk_reg[12]_i_1_n_6\,
      O(0) => \cnt_low_allowed_clk_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt_low_allowed_clk_reg(15 downto 12)
    );
\cnt_low_allowed_clk_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[12]_i_1_n_6\,
      Q => cnt_low_allowed_clk_reg(13),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[12]_i_1_n_5\,
      Q => cnt_low_allowed_clk_reg(14),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[12]_i_1_n_4\,
      Q => cnt_low_allowed_clk_reg(15),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[0]_i_2_n_6\,
      Q => cnt_low_allowed_clk_reg(1),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[0]_i_2_n_5\,
      Q => cnt_low_allowed_clk_reg(2),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[0]_i_2_n_4\,
      Q => cnt_low_allowed_clk_reg(3),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[4]_i_1_n_7\,
      Q => cnt_low_allowed_clk_reg(4),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_low_allowed_clk_reg[0]_i_2_n_0\,
      CO(3) => \cnt_low_allowed_clk_reg[4]_i_1_n_0\,
      CO(2) => \cnt_low_allowed_clk_reg[4]_i_1_n_1\,
      CO(1) => \cnt_low_allowed_clk_reg[4]_i_1_n_2\,
      CO(0) => \cnt_low_allowed_clk_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_low_allowed_clk_reg[4]_i_1_n_4\,
      O(2) => \cnt_low_allowed_clk_reg[4]_i_1_n_5\,
      O(1) => \cnt_low_allowed_clk_reg[4]_i_1_n_6\,
      O(0) => \cnt_low_allowed_clk_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt_low_allowed_clk_reg(7 downto 4)
    );
\cnt_low_allowed_clk_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[4]_i_1_n_6\,
      Q => cnt_low_allowed_clk_reg(5),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[4]_i_1_n_5\,
      Q => cnt_low_allowed_clk_reg(6),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[4]_i_1_n_4\,
      Q => cnt_low_allowed_clk_reg(7),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[8]_i_1_n_7\,
      Q => cnt_low_allowed_clk_reg(8),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_low_allowed_clk_reg[4]_i_1_n_0\,
      CO(3) => \cnt_low_allowed_clk_reg[8]_i_1_n_0\,
      CO(2) => \cnt_low_allowed_clk_reg[8]_i_1_n_1\,
      CO(1) => \cnt_low_allowed_clk_reg[8]_i_1_n_2\,
      CO(0) => \cnt_low_allowed_clk_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_low_allowed_clk_reg[8]_i_1_n_4\,
      O(2) => \cnt_low_allowed_clk_reg[8]_i_1_n_5\,
      O(1) => \cnt_low_allowed_clk_reg[8]_i_1_n_6\,
      O(0) => \cnt_low_allowed_clk_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt_low_allowed_clk_reg(11 downto 8)
    );
\cnt_low_allowed_clk_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[8]_i_1_n_6\,
      Q => cnt_low_allowed_clk_reg(9),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
interrupt_frame_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFF0D0D0D0"
    )
        port map (
      I0 => cnt100_reg(8),
      I1 => interrupt_frame_i_2_n_0,
      I2 => interrupt_frame_i_3_n_0,
      I3 => interrupt_frame_i_4_n_0,
      I4 => cnt100_reg(7),
      I5 => interrupt_frame_i_5_n_0,
      O => interrupt_frame_i_1_n_0
    );
interrupt_frame_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => cnt100_reg(10),
      I1 => cnt100_reg(11),
      I2 => cnt100_reg(12),
      I3 => cnt100_reg(13),
      O => interrupt_frame_i_10_n_0
    );
interrupt_frame_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FDFFFFFFFFFFFFFF"
    )
        port map (
      I0 => cnt100_reg(9),
      I1 => cnt100_reg(30),
      I2 => cnt100_reg(31),
      I3 => \^azimut_r_reg[2]_0\,
      I4 => \^azimut_r_reg[0]_0\,
      I5 => \^azimut_r_reg[1]_0\,
      O => interrupt_frame_i_11_n_0
    );
interrupt_frame_i_12: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00CF00000000AA00"
    )
        port map (
      I0 => interrupt_frame_i_14_n_0,
      I1 => cnt100_reg(5),
      I2 => interrupt_frame_i_15_n_0,
      I3 => cnt100_reg(8),
      I4 => cnt100_reg(7),
      I5 => cnt100_reg(6),
      O => interrupt_frame_i_12_n_0
    );
interrupt_frame_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFF7F7F00FF00FF"
    )
        port map (
      I0 => cnt100_reg(4),
      I1 => cnt100_reg(3),
      I2 => cnt100_reg(2),
      I3 => cnt100_reg(7),
      I4 => interrupt_frame_i_16_n_0,
      I5 => cnt100_reg(5),
      O => interrupt_frame_i_13_n_0
    );
interrupt_frame_i_14: unisim.vcomponents.LUT6
    generic map(
      INIT => X"57FFFFFFFFFFFFFF"
    )
        port map (
      I0 => cnt100_reg(5),
      I1 => cnt100_reg(0),
      I2 => cnt100_reg(1),
      I3 => cnt100_reg(2),
      I4 => cnt100_reg(3),
      I5 => cnt100_reg(4),
      O => interrupt_frame_i_14_n_0
    );
interrupt_frame_i_15: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => cnt100_reg(3),
      I1 => cnt100_reg(4),
      O => interrupt_frame_i_15_n_0
    );
interrupt_frame_i_16: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt100_reg(0),
      I1 => cnt100_reg(1),
      O => interrupt_frame_i_16_n_0
    );
interrupt_frame_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => interrupt_frame_i_6_n_0,
      I1 => interrupt_frame_i_7_n_0,
      I2 => interrupt_frame_i_8_n_0,
      I3 => interrupt_frame_i_9_n_0,
      I4 => interrupt_frame_i_10_n_0,
      I5 => interrupt_frame_i_11_n_0,
      O => interrupt_frame_i_2_n_0
    );
interrupt_frame_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88A8"
    )
        port map (
      I0 => m00_axis_aresetn,
      I1 => \^interrupt_frame\,
      I2 => interrupt_frame_i_12_n_0,
      I3 => interrupt_frame_i_2_n_0,
      O => interrupt_frame_i_3_n_0
    );
interrupt_frame_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFE0"
    )
        port map (
      I0 => cnt100_reg(1),
      I1 => cnt100_reg(0),
      I2 => cnt100_reg(5),
      I3 => cnt100_reg(6),
      O => interrupt_frame_i_4_n_0
    );
interrupt_frame_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4040444000000000"
    )
        port map (
      I0 => cnt100_reg(6),
      I1 => m00_axis_aresetn,
      I2 => \^interrupt_frame\,
      I3 => interrupt_frame_i_12_n_0,
      I4 => interrupt_frame_i_2_n_0,
      I5 => interrupt_frame_i_13_n_0,
      O => interrupt_frame_i_5_n_0
    );
interrupt_frame_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt100_reg(23),
      I1 => cnt100_reg(22),
      I2 => cnt100_reg(25),
      I3 => cnt100_reg(24),
      O => interrupt_frame_i_6_n_0
    );
interrupt_frame_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt100_reg(27),
      I1 => cnt100_reg(26),
      I2 => cnt100_reg(29),
      I3 => cnt100_reg(28),
      O => interrupt_frame_i_7_n_0
    );
interrupt_frame_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF7"
    )
        port map (
      I0 => cnt100_reg(15),
      I1 => cnt100_reg(14),
      I2 => cnt100_reg(17),
      I3 => cnt100_reg(16),
      O => interrupt_frame_i_8_n_0
    );
interrupt_frame_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt100_reg(19),
      I1 => cnt100_reg(18),
      I2 => cnt100_reg(21),
      I3 => cnt100_reg(20),
      O => interrupt_frame_i_9_n_0
    );
interrupt_frame_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => interrupt_frame_i_1_n_0,
      Q => \^interrupt_frame\,
      R => '0'
    );
reset_cnt_trig_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => azimut_0,
      I1 => azimut_0_prev,
      O => reset_cnt_trig0
    );
reset_cnt_trig_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => reset_cnt_trig0,
      Q => \^reset_cnt_trig_ila\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    s00_axis_tready : out STD_LOGIC;
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s00_axis_tstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s00_axis_tlast : in STD_LOGIC;
    s00_axis_tvalid : in STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    allowed_clk : in STD_LOGIC;
    azimut_0 : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    azimut_ila : out STD_LOGIC_VECTOR ( 15 downto 0 );
    low_azimut_ila : out STD_LOGIC_VECTOR ( 15 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    clk_10MHz : in STD_LOGIC;
    interrupt_frame : out STD_LOGIC;
    reset_cnt_trig_ila : out STD_LOGIC;
    azimut : out STD_LOGIC_VECTOR ( 31 downto 0 );
    azimut8 : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_2_averageFFT_0_0,averageFFT_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "averageFFT_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^azimut\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^azimut8\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^m00_axis_tready\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of allowed_clk : signal is "xilinx.com:signal:clock:1.0 allowed_clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of allowed_clk : signal is "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of interrupt_frame : signal is "xilinx.com:signal:interrupt:1.0 interrupt_frame INTERRUPT";
  attribute X_INTERFACE_PARAMETER of interrupt_frame : signal is "XIL_INTERFACENAME interrupt_frame, SENSITIVITY LEVEL_HIGH, PortWidth 1";
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK";
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_RESET m00_axis_aresetn, ASSOCIATED_BUSIF s00_axis:m00_axis, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 m00_axis TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 m00_axis TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME m00_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 m00_axis TVALID";
  attribute X_INTERFACE_INFO of reset_cnt_trig_ila : signal is "xilinx.com:signal:reset:1.0 reset_cnt_trig_ila RST";
  attribute X_INTERFACE_PARAMETER of reset_cnt_trig_ila : signal is "XIL_INTERFACENAME reset_cnt_trig_ila, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 s00_axis TLAST";
  attribute X_INTERFACE_INFO of s00_axis_tready : signal is "xilinx.com:interface:axis:1.0 s00_axis TREADY";
  attribute X_INTERFACE_INFO of s00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 s00_axis TVALID";
  attribute X_INTERFACE_PARAMETER of s00_axis_tvalid : signal is "XIL_INTERFACENAME s00_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 m00_axis TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 m00_axis TSTRB";
  attribute X_INTERFACE_INFO of s00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 s00_axis TDATA";
  attribute X_INTERFACE_INFO of s00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 s00_axis TSTRB";
begin
  \^m00_axis_tready\ <= m00_axis_tready;
  azimut(31) <= \<const0>\;
  azimut(30) <= \<const0>\;
  azimut(29) <= \<const0>\;
  azimut(28) <= \<const0>\;
  azimut(27) <= \<const0>\;
  azimut(26) <= \<const0>\;
  azimut(25) <= \<const0>\;
  azimut(24) <= \<const0>\;
  azimut(23) <= \<const0>\;
  azimut(22) <= \<const0>\;
  azimut(21) <= \<const0>\;
  azimut(20) <= \<const0>\;
  azimut(19) <= \<const0>\;
  azimut(18) <= \<const0>\;
  azimut(17) <= \<const0>\;
  azimut(16) <= \<const0>\;
  azimut(15) <= \<const0>\;
  azimut(14) <= \<const0>\;
  azimut(13) <= \<const0>\;
  azimut(12) <= \<const0>\;
  azimut(11) <= \<const0>\;
  azimut(10) <= \<const0>\;
  azimut(9) <= \<const0>\;
  azimut(8) <= \<const0>\;
  azimut(7) <= \<const0>\;
  azimut(6) <= \<const0>\;
  azimut(5) <= \<const0>\;
  azimut(4) <= \<const0>\;
  azimut(3) <= \<const0>\;
  azimut(2 downto 0) <= \^azimut\(2 downto 0);
  azimut8(31) <= \<const0>\;
  azimut8(30) <= \<const0>\;
  azimut8(29) <= \<const0>\;
  azimut8(28) <= \<const0>\;
  azimut8(27) <= \<const0>\;
  azimut8(26) <= \<const0>\;
  azimut8(25) <= \<const0>\;
  azimut8(24) <= \<const0>\;
  azimut8(23) <= \<const0>\;
  azimut8(22) <= \<const0>\;
  azimut8(21) <= \<const0>\;
  azimut8(20) <= \<const0>\;
  azimut8(19) <= \<const0>\;
  azimut8(18) <= \<const0>\;
  azimut8(17) <= \<const0>\;
  azimut8(16) <= \<const0>\;
  azimut8(15 downto 0) <= \^azimut8\(15 downto 0);
  azimut_ila(15) <= \<const0>\;
  azimut_ila(14) <= \<const0>\;
  azimut_ila(13) <= \<const0>\;
  azimut_ila(12) <= \<const0>\;
  azimut_ila(11) <= \<const0>\;
  azimut_ila(10) <= \<const0>\;
  azimut_ila(9) <= \<const0>\;
  azimut_ila(8) <= \<const0>\;
  azimut_ila(7) <= \<const0>\;
  azimut_ila(6) <= \<const0>\;
  azimut_ila(5) <= \<const0>\;
  azimut_ila(4) <= \<const0>\;
  azimut_ila(3) <= \<const0>\;
  azimut_ila(2) <= \<const0>\;
  azimut_ila(1) <= \<const0>\;
  azimut_ila(0) <= \<const0>\;
  m00_axis_tdata(31) <= \<const0>\;
  m00_axis_tdata(30) <= \<const0>\;
  m00_axis_tdata(29) <= \<const0>\;
  m00_axis_tdata(28) <= \<const0>\;
  m00_axis_tdata(27) <= \<const0>\;
  m00_axis_tdata(26) <= \<const0>\;
  m00_axis_tdata(25) <= \<const0>\;
  m00_axis_tdata(24) <= \<const0>\;
  m00_axis_tdata(23) <= \<const0>\;
  m00_axis_tdata(22) <= \<const0>\;
  m00_axis_tdata(21) <= \<const0>\;
  m00_axis_tdata(20) <= \<const0>\;
  m00_axis_tdata(19) <= \<const0>\;
  m00_axis_tdata(18) <= \<const0>\;
  m00_axis_tdata(17) <= \<const0>\;
  m00_axis_tdata(16) <= \<const0>\;
  m00_axis_tdata(15) <= \<const0>\;
  m00_axis_tdata(14) <= \<const0>\;
  m00_axis_tdata(13) <= \<const0>\;
  m00_axis_tdata(12) <= \<const0>\;
  m00_axis_tdata(11) <= \<const0>\;
  m00_axis_tdata(10) <= \<const0>\;
  m00_axis_tdata(9) <= \<const0>\;
  m00_axis_tdata(8) <= \<const0>\;
  m00_axis_tdata(7) <= \<const0>\;
  m00_axis_tdata(6) <= \<const0>\;
  m00_axis_tdata(5) <= \<const0>\;
  m00_axis_tdata(4) <= \<const0>\;
  m00_axis_tdata(3) <= \<const0>\;
  m00_axis_tdata(2) <= \<const0>\;
  m00_axis_tdata(1) <= \<const0>\;
  m00_axis_tdata(0) <= \<const0>\;
  m00_axis_tlast <= \<const0>\;
  m00_axis_tstrb(3) <= \<const1>\;
  m00_axis_tstrb(2) <= \<const1>\;
  m00_axis_tstrb(1) <= \<const1>\;
  m00_axis_tstrb(0) <= \<const1>\;
  m00_axis_tvalid <= \<const0>\;
  s00_axis_tready <= \^m00_axis_tready\;
  low_azimut_ila(0) <= 'Z';
  low_azimut_ila(1) <= 'Z';
  low_azimut_ila(2) <= 'Z';
  low_azimut_ila(3) <= 'Z';
  low_azimut_ila(4) <= 'Z';
  low_azimut_ila(5) <= 'Z';
  low_azimut_ila(6) <= 'Z';
  low_azimut_ila(7) <= 'Z';
  low_azimut_ila(8) <= 'Z';
  low_azimut_ila(9) <= 'Z';
  low_azimut_ila(10) <= 'Z';
  low_azimut_ila(11) <= 'Z';
  low_azimut_ila(12) <= 'Z';
  low_azimut_ila(13) <= 'Z';
  low_azimut_ila(14) <= 'Z';
  low_azimut_ila(15) <= 'Z';
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0
     port map (
      allowed_clk => allowed_clk,
      azimut8(15 downto 0) => \^azimut8\(15 downto 0),
      azimut_0 => azimut_0,
      \azimut_r_reg[0]_0\ => \^azimut\(0),
      \azimut_r_reg[1]_0\ => \^azimut\(1),
      \azimut_r_reg[2]_0\ => \^azimut\(2),
      clk_10MHz => clk_10MHz,
      interrupt_frame => interrupt_frame,
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      reset_cnt_trig_ila => reset_cnt_trig_ila
    );
end STRUCTURE;

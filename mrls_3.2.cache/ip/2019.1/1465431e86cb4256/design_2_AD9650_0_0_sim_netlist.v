// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Tue Nov 30 12:43:56 2021
// Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_AD9650_0_0_sim_netlist.v
// Design      : design_2_AD9650_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0
   (adc_spi_sdio,
    m00_fft_axis_tlast,
    adc_spi_sck,
    m01_fft_axis_tdata,
    m00_fft_axis_tvalid,
    m01_fft_axis_tvalid,
    adc_spi_cs,
    clk_10MHz,
    s00_axi_aclk,
    dco_or_dcoa,
    allowed_clk,
    s00_axi_aresetn,
    DATA_INA,
    mux_fft,
    DATA_INB);
  output adc_spi_sdio;
  output m00_fft_axis_tlast;
  output adc_spi_sck;
  output [31:0]m01_fft_axis_tdata;
  output m00_fft_axis_tvalid;
  output m01_fft_axis_tvalid;
  output adc_spi_cs;
  input clk_10MHz;
  input s00_axi_aclk;
  input dco_or_dcoa;
  input allowed_clk;
  input s00_axi_aresetn;
  input [15:0]DATA_INA;
  input mux_fft;
  input [15:0]DATA_INB;

  wire [15:0]DATA_INA;
  wire [15:0]DATA_INB;
  wire adc_spi_cs;
  wire adc_spi_sck;
  wire adc_spi_sdio;
  wire allowed_clk;
  wire clear;
  wire clk_10MHz;
  wire cnt_DCO1;
  wire cnt_DCO1_carry__0_i_1_n_0;
  wire cnt_DCO1_carry__0_i_2_n_0;
  wire cnt_DCO1_carry__0_n_3;
  wire cnt_DCO1_carry_i_1_n_0;
  wire cnt_DCO1_carry_i_2_n_0;
  wire cnt_DCO1_carry_i_3_n_0;
  wire cnt_DCO1_carry_i_4_n_0;
  wire cnt_DCO1_carry_n_0;
  wire cnt_DCO1_carry_n_1;
  wire cnt_DCO1_carry_n_2;
  wire cnt_DCO1_carry_n_3;
  wire \cnt_DCO[0]_i_3_n_0 ;
  wire \cnt_DCO[0]_i_4_n_0 ;
  wire \cnt_DCO[0]_i_5_n_0 ;
  wire \cnt_DCO[0]_i_6_n_0 ;
  wire \cnt_DCO[12]_i_2_n_0 ;
  wire \cnt_DCO[12]_i_3_n_0 ;
  wire \cnt_DCO[12]_i_4_n_0 ;
  wire \cnt_DCO[12]_i_5_n_0 ;
  wire \cnt_DCO[4]_i_2_n_0 ;
  wire \cnt_DCO[4]_i_3_n_0 ;
  wire \cnt_DCO[4]_i_4_n_0 ;
  wire \cnt_DCO[4]_i_5_n_0 ;
  wire \cnt_DCO[8]_i_2_n_0 ;
  wire \cnt_DCO[8]_i_3_n_0 ;
  wire \cnt_DCO[8]_i_4_n_0 ;
  wire \cnt_DCO[8]_i_5_n_0 ;
  wire [15:0]cnt_DCO_reg;
  wire \cnt_DCO_reg[0]_i_2_n_0 ;
  wire \cnt_DCO_reg[0]_i_2_n_1 ;
  wire \cnt_DCO_reg[0]_i_2_n_2 ;
  wire \cnt_DCO_reg[0]_i_2_n_3 ;
  wire \cnt_DCO_reg[0]_i_2_n_4 ;
  wire \cnt_DCO_reg[0]_i_2_n_5 ;
  wire \cnt_DCO_reg[0]_i_2_n_6 ;
  wire \cnt_DCO_reg[0]_i_2_n_7 ;
  wire \cnt_DCO_reg[12]_i_1_n_1 ;
  wire \cnt_DCO_reg[12]_i_1_n_2 ;
  wire \cnt_DCO_reg[12]_i_1_n_3 ;
  wire \cnt_DCO_reg[12]_i_1_n_4 ;
  wire \cnt_DCO_reg[12]_i_1_n_5 ;
  wire \cnt_DCO_reg[12]_i_1_n_6 ;
  wire \cnt_DCO_reg[12]_i_1_n_7 ;
  wire \cnt_DCO_reg[4]_i_1_n_0 ;
  wire \cnt_DCO_reg[4]_i_1_n_1 ;
  wire \cnt_DCO_reg[4]_i_1_n_2 ;
  wire \cnt_DCO_reg[4]_i_1_n_3 ;
  wire \cnt_DCO_reg[4]_i_1_n_4 ;
  wire \cnt_DCO_reg[4]_i_1_n_5 ;
  wire \cnt_DCO_reg[4]_i_1_n_6 ;
  wire \cnt_DCO_reg[4]_i_1_n_7 ;
  wire \cnt_DCO_reg[8]_i_1_n_0 ;
  wire \cnt_DCO_reg[8]_i_1_n_1 ;
  wire \cnt_DCO_reg[8]_i_1_n_2 ;
  wire \cnt_DCO_reg[8]_i_1_n_3 ;
  wire \cnt_DCO_reg[8]_i_1_n_4 ;
  wire \cnt_DCO_reg[8]_i_1_n_5 ;
  wire \cnt_DCO_reg[8]_i_1_n_6 ;
  wire \cnt_DCO_reg[8]_i_1_n_7 ;
  wire \cnt_in_DCO[0]_i_1_n_0 ;
  wire \cnt_in_DCO[0]_i_3_n_0 ;
  wire cnt_in_DCO__14;
  wire [15:0]cnt_in_DCO_reg;
  wire \cnt_in_DCO_reg[0]_i_2_n_0 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_1 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_2 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_3 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_4 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_5 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_6 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_7 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_1 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_2 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_3 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_4 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_5 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_6 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_7 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_0 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_1 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_2 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_3 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_4 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_5 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_6 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_7 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_0 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_1 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_2 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_3 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_4 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_5 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_6 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_7 ;
  wire dco_or_dcoa;
  wire m00_fft_axis_tlast;
  wire m00_fft_axis_tlast_r0;
  wire m00_fft_axis_tlast_r1;
  wire m00_fft_axis_tlast_r1_carry__0_i_1_n_0;
  wire m00_fft_axis_tlast_r1_carry__0_i_2_n_0;
  wire m00_fft_axis_tlast_r1_carry__0_n_3;
  wire m00_fft_axis_tlast_r1_carry_i_1_n_0;
  wire m00_fft_axis_tlast_r1_carry_i_2_n_0;
  wire m00_fft_axis_tlast_r1_carry_i_3_n_0;
  wire m00_fft_axis_tlast_r1_carry_i_4_n_0;
  wire m00_fft_axis_tlast_r1_carry_n_0;
  wire m00_fft_axis_tlast_r1_carry_n_1;
  wire m00_fft_axis_tlast_r1_carry_n_2;
  wire m00_fft_axis_tlast_r1_carry_n_3;
  wire m00_fft_axis_tvalid;
  wire m00_fft_axis_tvalid_r1;
  wire m00_fft_axis_tvalid_r1_carry__0_i_1_n_0;
  wire m00_fft_axis_tvalid_r1_carry__0_i_2_n_0;
  wire m00_fft_axis_tvalid_r1_carry__0_i_3_n_0;
  wire m00_fft_axis_tvalid_r1_carry__0_i_4_n_0;
  wire m00_fft_axis_tvalid_r1_carry__0_n_1;
  wire m00_fft_axis_tvalid_r1_carry__0_n_2;
  wire m00_fft_axis_tvalid_r1_carry__0_n_3;
  wire m00_fft_axis_tvalid_r1_carry_i_1_n_0;
  wire m00_fft_axis_tvalid_r1_carry_i_2_n_0;
  wire m00_fft_axis_tvalid_r1_carry_i_3_n_0;
  wire m00_fft_axis_tvalid_r1_carry_i_4_n_0;
  wire m00_fft_axis_tvalid_r1_carry_n_0;
  wire m00_fft_axis_tvalid_r1_carry_n_1;
  wire m00_fft_axis_tvalid_r1_carry_n_2;
  wire m00_fft_axis_tvalid_r1_carry_n_3;
  wire m00_fft_axis_tvalid_r_i_10_n_0;
  wire m00_fft_axis_tvalid_r_i_1_n_0;
  wire m00_fft_axis_tvalid_r_i_3_n_0;
  wire m00_fft_axis_tvalid_r_i_4_n_0;
  wire m00_fft_axis_tvalid_r_i_5_n_0;
  wire m00_fft_axis_tvalid_r_i_6_n_0;
  wire m00_fft_axis_tvalid_r_i_7_n_0;
  wire m00_fft_axis_tvalid_r_i_8_n_0;
  wire m00_fft_axis_tvalid_r_i_9_n_0;
  wire [31:0]m01_fft_axis_tdata;
  wire m01_fft_axis_tvalid;
  wire m01_fft_axis_tvalid_r_i_1_n_0;
  wire mux_fft;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire [3:0]NLW_cnt_DCO1_carry_O_UNCONNECTED;
  wire [3:2]NLW_cnt_DCO1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_cnt_DCO1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_cnt_DCO_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_cnt_in_DCO_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:0]NLW_m00_fft_axis_tlast_r1_carry_O_UNCONNECTED;
  wire [3:2]NLW_m00_fft_axis_tlast_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_m00_fft_axis_tlast_r1_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_m00_fft_axis_tvalid_r1_carry_O_UNCONNECTED;
  wire [3:0]NLW_m00_fft_axis_tvalid_r1_carry__0_O_UNCONNECTED;

  CARRY4 cnt_DCO1_carry
       (.CI(1'b0),
        .CO({cnt_DCO1_carry_n_0,cnt_DCO1_carry_n_1,cnt_DCO1_carry_n_2,cnt_DCO1_carry_n_3}),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b1}),
        .O(NLW_cnt_DCO1_carry_O_UNCONNECTED[3:0]),
        .S({cnt_DCO1_carry_i_1_n_0,cnt_DCO1_carry_i_2_n_0,cnt_DCO1_carry_i_3_n_0,cnt_DCO1_carry_i_4_n_0}));
  CARRY4 cnt_DCO1_carry__0
       (.CI(cnt_DCO1_carry_n_0),
        .CO({NLW_cnt_DCO1_carry__0_CO_UNCONNECTED[3:2],cnt_DCO1,cnt_DCO1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,1'b1}),
        .O(NLW_cnt_DCO1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,cnt_DCO1_carry__0_i_1_n_0,cnt_DCO1_carry__0_i_2_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    cnt_DCO1_carry__0_i_1
       (.I0(cnt_DCO_reg[15]),
        .O(cnt_DCO1_carry__0_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    cnt_DCO1_carry__0_i_2
       (.I0(cnt_DCO_reg[13]),
        .I1(cnt_DCO_reg[14]),
        .I2(cnt_DCO_reg[12]),
        .O(cnt_DCO1_carry__0_i_2_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    cnt_DCO1_carry_i_1
       (.I0(cnt_DCO_reg[11]),
        .I1(cnt_DCO_reg[10]),
        .I2(cnt_DCO_reg[9]),
        .O(cnt_DCO1_carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    cnt_DCO1_carry_i_2
       (.I0(cnt_DCO_reg[7]),
        .I1(cnt_DCO_reg[8]),
        .I2(cnt_DCO_reg[6]),
        .O(cnt_DCO1_carry_i_2_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    cnt_DCO1_carry_i_3
       (.I0(cnt_DCO_reg[5]),
        .I1(cnt_DCO_reg[4]),
        .I2(cnt_DCO_reg[3]),
        .O(cnt_DCO1_carry_i_3_n_0));
  LUT3 #(
    .INIT(8'h02)) 
    cnt_DCO1_carry_i_4
       (.I0(cnt_DCO_reg[0]),
        .I1(cnt_DCO_reg[1]),
        .I2(cnt_DCO_reg[2]),
        .O(cnt_DCO1_carry_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_DCO[0]_i_1 
       (.I0(allowed_clk),
        .O(clear));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_DCO[0]_i_3 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[3]),
        .O(\cnt_DCO[0]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_DCO[0]_i_4 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[2]),
        .O(\cnt_DCO[0]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_DCO[0]_i_5 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[1]),
        .O(\cnt_DCO[0]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \cnt_DCO[0]_i_6 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[0]),
        .O(\cnt_DCO[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_DCO[12]_i_2 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[15]),
        .O(\cnt_DCO[12]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_DCO[12]_i_3 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[14]),
        .O(\cnt_DCO[12]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_DCO[12]_i_4 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[13]),
        .O(\cnt_DCO[12]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_DCO[12]_i_5 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[12]),
        .O(\cnt_DCO[12]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_DCO[4]_i_2 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[7]),
        .O(\cnt_DCO[4]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_DCO[4]_i_3 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[6]),
        .O(\cnt_DCO[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_DCO[4]_i_4 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[5]),
        .O(\cnt_DCO[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_DCO[4]_i_5 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[4]),
        .O(\cnt_DCO[4]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_DCO[8]_i_2 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[11]),
        .O(\cnt_DCO[8]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_DCO[8]_i_3 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[10]),
        .O(\cnt_DCO[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_DCO[8]_i_4 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[9]),
        .O(\cnt_DCO[8]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \cnt_DCO[8]_i_5 
       (.I0(cnt_DCO1),
        .I1(cnt_DCO_reg[8]),
        .O(\cnt_DCO[8]_i_5_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[0] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[0]_i_2_n_7 ),
        .Q(cnt_DCO_reg[0]),
        .R(clear));
  CARRY4 \cnt_DCO_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_DCO_reg[0]_i_2_n_0 ,\cnt_DCO_reg[0]_i_2_n_1 ,\cnt_DCO_reg[0]_i_2_n_2 ,\cnt_DCO_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_DCO_reg[0]_i_2_n_4 ,\cnt_DCO_reg[0]_i_2_n_5 ,\cnt_DCO_reg[0]_i_2_n_6 ,\cnt_DCO_reg[0]_i_2_n_7 }),
        .S({\cnt_DCO[0]_i_3_n_0 ,\cnt_DCO[0]_i_4_n_0 ,\cnt_DCO[0]_i_5_n_0 ,\cnt_DCO[0]_i_6_n_0 }));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[10] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[8]_i_1_n_5 ),
        .Q(cnt_DCO_reg[10]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[11] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[8]_i_1_n_4 ),
        .Q(cnt_DCO_reg[11]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[12] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[12]_i_1_n_7 ),
        .Q(cnt_DCO_reg[12]),
        .R(clear));
  CARRY4 \cnt_DCO_reg[12]_i_1 
       (.CI(\cnt_DCO_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_DCO_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_DCO_reg[12]_i_1_n_1 ,\cnt_DCO_reg[12]_i_1_n_2 ,\cnt_DCO_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_DCO_reg[12]_i_1_n_4 ,\cnt_DCO_reg[12]_i_1_n_5 ,\cnt_DCO_reg[12]_i_1_n_6 ,\cnt_DCO_reg[12]_i_1_n_7 }),
        .S({\cnt_DCO[12]_i_2_n_0 ,\cnt_DCO[12]_i_3_n_0 ,\cnt_DCO[12]_i_4_n_0 ,\cnt_DCO[12]_i_5_n_0 }));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[13] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[12]_i_1_n_6 ),
        .Q(cnt_DCO_reg[13]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[14] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[12]_i_1_n_5 ),
        .Q(cnt_DCO_reg[14]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[15] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[12]_i_1_n_4 ),
        .Q(cnt_DCO_reg[15]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[1] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[0]_i_2_n_6 ),
        .Q(cnt_DCO_reg[1]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[2] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[0]_i_2_n_5 ),
        .Q(cnt_DCO_reg[2]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[3] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[0]_i_2_n_4 ),
        .Q(cnt_DCO_reg[3]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[4] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[4]_i_1_n_7 ),
        .Q(cnt_DCO_reg[4]),
        .R(clear));
  CARRY4 \cnt_DCO_reg[4]_i_1 
       (.CI(\cnt_DCO_reg[0]_i_2_n_0 ),
        .CO({\cnt_DCO_reg[4]_i_1_n_0 ,\cnt_DCO_reg[4]_i_1_n_1 ,\cnt_DCO_reg[4]_i_1_n_2 ,\cnt_DCO_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_DCO_reg[4]_i_1_n_4 ,\cnt_DCO_reg[4]_i_1_n_5 ,\cnt_DCO_reg[4]_i_1_n_6 ,\cnt_DCO_reg[4]_i_1_n_7 }),
        .S({\cnt_DCO[4]_i_2_n_0 ,\cnt_DCO[4]_i_3_n_0 ,\cnt_DCO[4]_i_4_n_0 ,\cnt_DCO[4]_i_5_n_0 }));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[5] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[4]_i_1_n_6 ),
        .Q(cnt_DCO_reg[5]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[6] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[4]_i_1_n_5 ),
        .Q(cnt_DCO_reg[6]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[7] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[4]_i_1_n_4 ),
        .Q(cnt_DCO_reg[7]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[8] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[8]_i_1_n_7 ),
        .Q(cnt_DCO_reg[8]),
        .R(clear));
  CARRY4 \cnt_DCO_reg[8]_i_1 
       (.CI(\cnt_DCO_reg[4]_i_1_n_0 ),
        .CO({\cnt_DCO_reg[8]_i_1_n_0 ,\cnt_DCO_reg[8]_i_1_n_1 ,\cnt_DCO_reg[8]_i_1_n_2 ,\cnt_DCO_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_DCO_reg[8]_i_1_n_4 ,\cnt_DCO_reg[8]_i_1_n_5 ,\cnt_DCO_reg[8]_i_1_n_6 ,\cnt_DCO_reg[8]_i_1_n_7 }),
        .S({\cnt_DCO[8]_i_2_n_0 ,\cnt_DCO[8]_i_3_n_0 ,\cnt_DCO[8]_i_4_n_0 ,\cnt_DCO[8]_i_5_n_0 }));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[9] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO_reg[8]_i_1_n_6 ),
        .Q(cnt_DCO_reg[9]),
        .R(clear));
  LUT3 #(
    .INIT(8'h7F)) 
    \cnt_in_DCO[0]_i_1 
       (.I0(dco_or_dcoa),
        .I1(allowed_clk),
        .I2(s00_axi_aresetn),
        .O(\cnt_in_DCO[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_in_DCO[0]_i_3 
       (.I0(cnt_in_DCO_reg[0]),
        .O(\cnt_in_DCO[0]_i_3_n_0 ));
  FDRE \cnt_in_DCO_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[0]_i_2_n_7 ),
        .Q(cnt_in_DCO_reg[0]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  CARRY4 \cnt_in_DCO_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_in_DCO_reg[0]_i_2_n_0 ,\cnt_in_DCO_reg[0]_i_2_n_1 ,\cnt_in_DCO_reg[0]_i_2_n_2 ,\cnt_in_DCO_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_in_DCO_reg[0]_i_2_n_4 ,\cnt_in_DCO_reg[0]_i_2_n_5 ,\cnt_in_DCO_reg[0]_i_2_n_6 ,\cnt_in_DCO_reg[0]_i_2_n_7 }),
        .S({cnt_in_DCO_reg[3:1],\cnt_in_DCO[0]_i_3_n_0 }));
  FDRE \cnt_in_DCO_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[8]_i_1_n_5 ),
        .Q(cnt_in_DCO_reg[10]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[8]_i_1_n_4 ),
        .Q(cnt_in_DCO_reg[11]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[12]_i_1_n_7 ),
        .Q(cnt_in_DCO_reg[12]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  CARRY4 \cnt_in_DCO_reg[12]_i_1 
       (.CI(\cnt_in_DCO_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_in_DCO_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_in_DCO_reg[12]_i_1_n_1 ,\cnt_in_DCO_reg[12]_i_1_n_2 ,\cnt_in_DCO_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_in_DCO_reg[12]_i_1_n_4 ,\cnt_in_DCO_reg[12]_i_1_n_5 ,\cnt_in_DCO_reg[12]_i_1_n_6 ,\cnt_in_DCO_reg[12]_i_1_n_7 }),
        .S(cnt_in_DCO_reg[15:12]));
  FDRE \cnt_in_DCO_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[12]_i_1_n_6 ),
        .Q(cnt_in_DCO_reg[13]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[12]_i_1_n_5 ),
        .Q(cnt_in_DCO_reg[14]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[12]_i_1_n_4 ),
        .Q(cnt_in_DCO_reg[15]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[0]_i_2_n_6 ),
        .Q(cnt_in_DCO_reg[1]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[0]_i_2_n_5 ),
        .Q(cnt_in_DCO_reg[2]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[0]_i_2_n_4 ),
        .Q(cnt_in_DCO_reg[3]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[4]_i_1_n_7 ),
        .Q(cnt_in_DCO_reg[4]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  CARRY4 \cnt_in_DCO_reg[4]_i_1 
       (.CI(\cnt_in_DCO_reg[0]_i_2_n_0 ),
        .CO({\cnt_in_DCO_reg[4]_i_1_n_0 ,\cnt_in_DCO_reg[4]_i_1_n_1 ,\cnt_in_DCO_reg[4]_i_1_n_2 ,\cnt_in_DCO_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_in_DCO_reg[4]_i_1_n_4 ,\cnt_in_DCO_reg[4]_i_1_n_5 ,\cnt_in_DCO_reg[4]_i_1_n_6 ,\cnt_in_DCO_reg[4]_i_1_n_7 }),
        .S(cnt_in_DCO_reg[7:4]));
  FDRE \cnt_in_DCO_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[4]_i_1_n_6 ),
        .Q(cnt_in_DCO_reg[5]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[4]_i_1_n_5 ),
        .Q(cnt_in_DCO_reg[6]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[4]_i_1_n_4 ),
        .Q(cnt_in_DCO_reg[7]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[8]_i_1_n_7 ),
        .Q(cnt_in_DCO_reg[8]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  CARRY4 \cnt_in_DCO_reg[8]_i_1 
       (.CI(\cnt_in_DCO_reg[4]_i_1_n_0 ),
        .CO({\cnt_in_DCO_reg[8]_i_1_n_0 ,\cnt_in_DCO_reg[8]_i_1_n_1 ,\cnt_in_DCO_reg[8]_i_1_n_2 ,\cnt_in_DCO_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_in_DCO_reg[8]_i_1_n_4 ,\cnt_in_DCO_reg[8]_i_1_n_5 ,\cnt_in_DCO_reg[8]_i_1_n_6 ,\cnt_in_DCO_reg[8]_i_1_n_7 }),
        .S(cnt_in_DCO_reg[11:8]));
  FDRE \cnt_in_DCO_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[8]_i_1_n_6 ),
        .Q(cnt_in_DCO_reg[9]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  CARRY4 m00_fft_axis_tlast_r1_carry
       (.CI(1'b0),
        .CO({m00_fft_axis_tlast_r1_carry_n_0,m00_fft_axis_tlast_r1_carry_n_1,m00_fft_axis_tlast_r1_carry_n_2,m00_fft_axis_tlast_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_m00_fft_axis_tlast_r1_carry_O_UNCONNECTED[3:0]),
        .S({m00_fft_axis_tlast_r1_carry_i_1_n_0,m00_fft_axis_tlast_r1_carry_i_2_n_0,m00_fft_axis_tlast_r1_carry_i_3_n_0,m00_fft_axis_tlast_r1_carry_i_4_n_0}));
  CARRY4 m00_fft_axis_tlast_r1_carry__0
       (.CI(m00_fft_axis_tlast_r1_carry_n_0),
        .CO({NLW_m00_fft_axis_tlast_r1_carry__0_CO_UNCONNECTED[3:2],m00_fft_axis_tlast_r1,m00_fft_axis_tlast_r1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_m00_fft_axis_tlast_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,m00_fft_axis_tlast_r1_carry__0_i_1_n_0,m00_fft_axis_tlast_r1_carry__0_i_2_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    m00_fft_axis_tlast_r1_carry__0_i_1
       (.I0(cnt_DCO_reg[15]),
        .O(m00_fft_axis_tlast_r1_carry__0_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    m00_fft_axis_tlast_r1_carry__0_i_2
       (.I0(cnt_DCO_reg[13]),
        .I1(cnt_DCO_reg[14]),
        .I2(cnt_DCO_reg[12]),
        .O(m00_fft_axis_tlast_r1_carry__0_i_2_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    m00_fft_axis_tlast_r1_carry_i_1
       (.I0(cnt_DCO_reg[11]),
        .I1(cnt_DCO_reg[10]),
        .I2(cnt_DCO_reg[9]),
        .O(m00_fft_axis_tlast_r1_carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    m00_fft_axis_tlast_r1_carry_i_2
       (.I0(cnt_DCO_reg[7]),
        .I1(cnt_DCO_reg[8]),
        .I2(cnt_DCO_reg[6]),
        .O(m00_fft_axis_tlast_r1_carry_i_2_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    m00_fft_axis_tlast_r1_carry_i_3
       (.I0(cnt_DCO_reg[5]),
        .I1(cnt_DCO_reg[4]),
        .I2(cnt_DCO_reg[3]),
        .O(m00_fft_axis_tlast_r1_carry_i_3_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    m00_fft_axis_tlast_r1_carry_i_4
       (.I0(cnt_DCO_reg[1]),
        .I1(cnt_DCO_reg[2]),
        .I2(cnt_DCO_reg[0]),
        .O(m00_fft_axis_tlast_r1_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    m00_fft_axis_tlast_r_i_1
       (.I0(cnt_in_DCO__14),
        .I1(m00_fft_axis_tlast_r1),
        .O(m00_fft_axis_tlast_r0));
  FDRE m00_fft_axis_tlast_r_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(m00_fft_axis_tlast_r0),
        .Q(m00_fft_axis_tlast),
        .R(1'b0));
  CARRY4 m00_fft_axis_tvalid_r1_carry
       (.CI(1'b0),
        .CO({m00_fft_axis_tvalid_r1_carry_n_0,m00_fft_axis_tvalid_r1_carry_n_1,m00_fft_axis_tvalid_r1_carry_n_2,m00_fft_axis_tvalid_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_m00_fft_axis_tvalid_r1_carry_O_UNCONNECTED[3:0]),
        .S({m00_fft_axis_tvalid_r1_carry_i_1_n_0,m00_fft_axis_tvalid_r1_carry_i_2_n_0,m00_fft_axis_tvalid_r1_carry_i_3_n_0,m00_fft_axis_tvalid_r1_carry_i_4_n_0}));
  CARRY4 m00_fft_axis_tvalid_r1_carry__0
       (.CI(m00_fft_axis_tvalid_r1_carry_n_0),
        .CO({m00_fft_axis_tvalid_r1,m00_fft_axis_tvalid_r1_carry__0_n_1,m00_fft_axis_tvalid_r1_carry__0_n_2,m00_fft_axis_tvalid_r1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_m00_fft_axis_tvalid_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({m00_fft_axis_tvalid_r1_carry__0_i_1_n_0,m00_fft_axis_tvalid_r1_carry__0_i_2_n_0,m00_fft_axis_tvalid_r1_carry__0_i_3_n_0,m00_fft_axis_tvalid_r1_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h1)) 
    m00_fft_axis_tvalid_r1_carry__0_i_1
       (.I0(cnt_DCO_reg[14]),
        .I1(cnt_DCO_reg[15]),
        .O(m00_fft_axis_tvalid_r1_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    m00_fft_axis_tvalid_r1_carry__0_i_2
       (.I0(cnt_DCO_reg[12]),
        .I1(cnt_DCO_reg[13]),
        .O(m00_fft_axis_tvalid_r1_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    m00_fft_axis_tvalid_r1_carry__0_i_3
       (.I0(cnt_DCO_reg[10]),
        .I1(cnt_DCO_reg[11]),
        .O(m00_fft_axis_tvalid_r1_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    m00_fft_axis_tvalid_r1_carry__0_i_4
       (.I0(cnt_DCO_reg[8]),
        .I1(cnt_DCO_reg[9]),
        .O(m00_fft_axis_tvalid_r1_carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    m00_fft_axis_tvalid_r1_carry_i_1
       (.I0(cnt_DCO_reg[6]),
        .I1(cnt_DCO_reg[7]),
        .O(m00_fft_axis_tvalid_r1_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    m00_fft_axis_tvalid_r1_carry_i_2
       (.I0(cnt_DCO_reg[4]),
        .I1(cnt_DCO_reg[5]),
        .O(m00_fft_axis_tvalid_r1_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    m00_fft_axis_tvalid_r1_carry_i_3
       (.I0(cnt_DCO_reg[2]),
        .I1(cnt_DCO_reg[3]),
        .O(m00_fft_axis_tvalid_r1_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    m00_fft_axis_tvalid_r1_carry_i_4
       (.I0(cnt_DCO_reg[0]),
        .I1(cnt_DCO_reg[1]),
        .O(m00_fft_axis_tvalid_r1_carry_i_4_n_0));
  LUT6 #(
    .INIT(64'hA800FFFFA8000000)) 
    m00_fft_axis_tvalid_r_i_1
       (.I0(cnt_in_DCO__14),
        .I1(m00_fft_axis_tvalid_r_i_3_n_0),
        .I2(m00_fft_axis_tvalid_r_i_4_n_0),
        .I3(m00_fft_axis_tvalid_r1),
        .I4(mux_fft),
        .I5(m00_fft_axis_tvalid),
        .O(m00_fft_axis_tvalid_r_i_1_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_fft_axis_tvalid_r_i_10
       (.I0(cnt_DCO_reg[14]),
        .I1(cnt_DCO_reg[13]),
        .I2(cnt_DCO_reg[0]),
        .I3(cnt_DCO_reg[15]),
        .O(m00_fft_axis_tvalid_r_i_10_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    m00_fft_axis_tvalid_r_i_2
       (.I0(m00_fft_axis_tvalid_r_i_5_n_0),
        .I1(m00_fft_axis_tvalid_r_i_6_n_0),
        .I2(m00_fft_axis_tvalid_r_i_7_n_0),
        .I3(m00_fft_axis_tvalid_r_i_8_n_0),
        .O(cnt_in_DCO__14));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    m00_fft_axis_tvalid_r_i_3
       (.I0(cnt_DCO_reg[3]),
        .I1(cnt_DCO_reg[4]),
        .I2(cnt_DCO_reg[1]),
        .I3(cnt_DCO_reg[2]),
        .I4(m00_fft_axis_tvalid_r_i_9_n_0),
        .O(m00_fft_axis_tvalid_r_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    m00_fft_axis_tvalid_r_i_4
       (.I0(cnt_DCO_reg[11]),
        .I1(cnt_DCO_reg[12]),
        .I2(cnt_DCO_reg[9]),
        .I3(cnt_DCO_reg[10]),
        .I4(m00_fft_axis_tvalid_r_i_10_n_0),
        .O(m00_fft_axis_tvalid_r_i_4_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_fft_axis_tvalid_r_i_5
       (.I0(cnt_in_DCO_reg[10]),
        .I1(cnt_in_DCO_reg[11]),
        .I2(cnt_in_DCO_reg[8]),
        .I3(cnt_in_DCO_reg[9]),
        .O(m00_fft_axis_tvalid_r_i_5_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_fft_axis_tvalid_r_i_6
       (.I0(cnt_in_DCO_reg[15]),
        .I1(cnt_in_DCO_reg[14]),
        .I2(cnt_in_DCO_reg[12]),
        .I3(cnt_in_DCO_reg[13]),
        .O(m00_fft_axis_tvalid_r_i_6_n_0));
  LUT4 #(
    .INIT(16'hFFEF)) 
    m00_fft_axis_tvalid_r_i_7
       (.I0(cnt_in_DCO_reg[2]),
        .I1(cnt_in_DCO_reg[3]),
        .I2(cnt_in_DCO_reg[0]),
        .I3(cnt_in_DCO_reg[1]),
        .O(m00_fft_axis_tvalid_r_i_7_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_fft_axis_tvalid_r_i_8
       (.I0(cnt_in_DCO_reg[6]),
        .I1(cnt_in_DCO_reg[7]),
        .I2(cnt_in_DCO_reg[4]),
        .I3(cnt_in_DCO_reg[5]),
        .O(m00_fft_axis_tvalid_r_i_8_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_fft_axis_tvalid_r_i_9
       (.I0(cnt_DCO_reg[6]),
        .I1(cnt_DCO_reg[5]),
        .I2(cnt_DCO_reg[8]),
        .I3(cnt_DCO_reg[7]),
        .O(m00_fft_axis_tvalid_r_i_9_n_0));
  FDRE m00_fft_axis_tvalid_r_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(m00_fft_axis_tvalid_r_i_1_n_0),
        .Q(m00_fft_axis_tvalid),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[0]_INST_0 
       (.I0(DATA_INA[0]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[0]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[10]_INST_0 
       (.I0(DATA_INA[10]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[10]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[11]_INST_0 
       (.I0(DATA_INA[11]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[11]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[12]_INST_0 
       (.I0(DATA_INA[12]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[12]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[13]_INST_0 
       (.I0(DATA_INA[13]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[13]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[14]_INST_0 
       (.I0(DATA_INA[14]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[14]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[15]_INST_0 
       (.I0(DATA_INA[15]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[15]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[16]_INST_0 
       (.I0(DATA_INB[0]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[16]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[17]_INST_0 
       (.I0(DATA_INB[1]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[17]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[18]_INST_0 
       (.I0(DATA_INB[2]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[18]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[19]_INST_0 
       (.I0(DATA_INB[3]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[19]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[1]_INST_0 
       (.I0(DATA_INA[1]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[1]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[20]_INST_0 
       (.I0(DATA_INB[4]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[20]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[21]_INST_0 
       (.I0(DATA_INB[5]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[21]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[22]_INST_0 
       (.I0(DATA_INB[6]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[22]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[23]_INST_0 
       (.I0(DATA_INB[7]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[23]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[24]_INST_0 
       (.I0(DATA_INB[8]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[24]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[25]_INST_0 
       (.I0(DATA_INB[9]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[25]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[26]_INST_0 
       (.I0(DATA_INB[10]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[26]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[27]_INST_0 
       (.I0(DATA_INB[11]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[27]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[28]_INST_0 
       (.I0(DATA_INB[12]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[28]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[29]_INST_0 
       (.I0(DATA_INB[13]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[29]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[2]_INST_0 
       (.I0(DATA_INA[2]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[2]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[30]_INST_0 
       (.I0(DATA_INB[14]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[30]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[31]_INST_0 
       (.I0(DATA_INB[15]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[31]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[3]_INST_0 
       (.I0(DATA_INA[3]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[3]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[4]_INST_0 
       (.I0(DATA_INA[4]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[4]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[5]_INST_0 
       (.I0(DATA_INA[5]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[5]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[6]_INST_0 
       (.I0(DATA_INA[6]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[6]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[7]_INST_0 
       (.I0(DATA_INA[7]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[7]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[8]_INST_0 
       (.I0(DATA_INA[8]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[8]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[9]_INST_0 
       (.I0(DATA_INA[9]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[9]));
  LUT6 #(
    .INIT(64'hB8B8B88888888888)) 
    m01_fft_axis_tvalid_r_i_1
       (.I0(m01_fft_axis_tvalid),
        .I1(mux_fft),
        .I2(cnt_in_DCO__14),
        .I3(m00_fft_axis_tvalid_r_i_3_n_0),
        .I4(m00_fft_axis_tvalid_r_i_4_n_0),
        .I5(m00_fft_axis_tvalid_r1),
        .O(m01_fft_axis_tvalid_r_i_1_n_0));
  FDRE m01_fft_axis_tvalid_r_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(m01_fft_axis_tvalid_r_i_1_n_0),
        .Q(m01_fft_axis_tvalid),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_AD9650 spi_AD9650_inst
       (.adc_spi_cs(adc_spi_cs),
        .adc_spi_sck(adc_spi_sck),
        .adc_spi_sdio(adc_spi_sdio),
        .clk_10MHz(clk_10MHz));
endmodule

(* CHECK_LICENSE_TYPE = "design_2_AD9650_0_0,AD9650_v2_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "AD9650_v2_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk_10MHz,
    m00_fft_axis_tvalid,
    m00_fft_axis_tdata,
    m00_fft_axis_tstrb,
    m00_fft_axis_tlast,
    m00_fft_axis_tready,
    m01_fft_axis_tvalid,
    m01_fft_axis_tdata,
    m01_fft_axis_tstrb,
    m01_fft_axis_tlast,
    m01_fft_axis_tready,
    dco_or_dcoa,
    dco_or_dcob,
    dco_or_ora,
    dco_or_orb,
    adc_spi_sck,
    adc_spi_cs,
    adc_spi_sdio,
    ADC_PDwN,
    SYNC,
    DATA_INA,
    DATA_INB,
    allowed_clk,
    mux_fft,
    s00_axi_aclk,
    s00_axi_aresetn,
    DCO);
  input clk_10MHz;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TVALID" *) output m00_fft_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TDATA" *) output [31:0]m00_fft_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TSTRB" *) output [3:0]m00_fft_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TLAST" *) output m00_fft_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_fft_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_fft_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TVALID" *) output m01_fft_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TDATA" *) output [31:0]m01_fft_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TSTRB" *) output [3:0]m01_fft_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TLAST" *) output m01_fft_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m01_fft_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA undef, INSERT_VIP 0" *) input m01_fft_axis_tready;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR dcoa" *) input dco_or_dcoa;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR dcob" *) input dco_or_dcob;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR ora" *) input dco_or_ora;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR orb" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME DCO_OR, SV_INTERFACE true" *) input dco_or_orb;
  (* X_INTERFACE_INFO = "bt.local:interface:adc_spi:1.0 ADC_SPI sck" *) output adc_spi_sck;
  (* X_INTERFACE_INFO = "bt.local:interface:adc_spi:1.0 ADC_SPI cs" *) output adc_spi_cs;
  (* X_INTERFACE_INFO = "bt.local:interface:adc_spi:1.0 ADC_SPI sdio" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME ADC_SPI, SV_INTERFACE true" *) inout adc_spi_sdio;
  output ADC_PDwN;
  output SYNC;
  input [15:0]DATA_INA;
  input [15:0]DATA_INB;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 allowed_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_allowed_clk_0, INSERT_VIP 0" *) input allowed_clk;
  input mux_fft;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI:m01_fft_axis:m00_fft_axis, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, INSERT_VIP 0" *) input s00_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;
  output DCO;

  wire \<const0> ;
  wire \<const1> ;
  wire [15:0]DATA_INA;
  wire [15:0]DATA_INB;
  wire adc_spi_cs;
  wire adc_spi_sck;
  (* DRIVE = "12" *) (* SLEW = "SLOW" *) wire adc_spi_sdio;
  wire allowed_clk;
  wire clk_10MHz;
  wire dco_or_dcoa;
  wire [31:0]m00_fft_axis_tdata;
  wire m00_fft_axis_tlast;
  wire m00_fft_axis_tvalid;
  wire [31:0]m01_fft_axis_tdata;
  wire m01_fft_axis_tvalid;
  wire mux_fft;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;

  assign DCO = dco_or_dcoa;
  assign SYNC = \<const0> ;
  assign m00_fft_axis_tstrb[3] = \<const1> ;
  assign m00_fft_axis_tstrb[2] = \<const1> ;
  assign m00_fft_axis_tstrb[1] = \<const1> ;
  assign m00_fft_axis_tstrb[0] = \<const1> ;
  assign m01_fft_axis_tstrb[3] = \<const1> ;
  assign m01_fft_axis_tstrb[2] = \<const1> ;
  assign m01_fft_axis_tstrb[1] = \<const1> ;
  assign m01_fft_axis_tstrb[0] = \<const1> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0 inst
       (.DATA_INA(DATA_INA),
        .DATA_INB(DATA_INB),
        .adc_spi_cs(adc_spi_cs),
        .adc_spi_sck(adc_spi_sck),
        .adc_spi_sdio(adc_spi_sdio),
        .allowed_clk(allowed_clk),
        .clk_10MHz(clk_10MHz),
        .dco_or_dcoa(dco_or_dcoa),
        .m00_fft_axis_tlast(m00_fft_axis_tlast),
        .m00_fft_axis_tvalid(m00_fft_axis_tvalid),
        .m01_fft_axis_tdata(m01_fft_axis_tdata),
        .m01_fft_axis_tvalid(m01_fft_axis_tvalid),
        .mux_fft(mux_fft),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[0]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[0]),
        .O(m00_fft_axis_tdata[0]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[10]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[10]),
        .O(m00_fft_axis_tdata[10]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[11]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[11]),
        .O(m00_fft_axis_tdata[11]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[12]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[12]),
        .O(m00_fft_axis_tdata[12]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[13]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[13]),
        .O(m00_fft_axis_tdata[13]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[14]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[14]),
        .O(m00_fft_axis_tdata[14]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[15]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[15]),
        .O(m00_fft_axis_tdata[15]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[16]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[0]),
        .O(m00_fft_axis_tdata[16]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[17]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[1]),
        .O(m00_fft_axis_tdata[17]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[18]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[2]),
        .O(m00_fft_axis_tdata[18]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[19]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[3]),
        .O(m00_fft_axis_tdata[19]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[1]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[1]),
        .O(m00_fft_axis_tdata[1]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[20]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[4]),
        .O(m00_fft_axis_tdata[20]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[21]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[5]),
        .O(m00_fft_axis_tdata[21]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[22]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[6]),
        .O(m00_fft_axis_tdata[22]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[23]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[7]),
        .O(m00_fft_axis_tdata[23]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[24]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[8]),
        .O(m00_fft_axis_tdata[24]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[25]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[9]),
        .O(m00_fft_axis_tdata[25]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[26]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[10]),
        .O(m00_fft_axis_tdata[26]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[27]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[11]),
        .O(m00_fft_axis_tdata[27]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[28]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[12]),
        .O(m00_fft_axis_tdata[28]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[29]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[13]),
        .O(m00_fft_axis_tdata[29]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[2]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[2]),
        .O(m00_fft_axis_tdata[2]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[30]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[14]),
        .O(m00_fft_axis_tdata[30]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[31]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[15]),
        .O(m00_fft_axis_tdata[31]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[3]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[3]),
        .O(m00_fft_axis_tdata[3]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[4]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[4]),
        .O(m00_fft_axis_tdata[4]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[5]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[5]),
        .O(m00_fft_axis_tdata[5]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[6]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[6]),
        .O(m00_fft_axis_tdata[6]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[7]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[7]),
        .O(m00_fft_axis_tdata[7]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[8]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[8]),
        .O(m00_fft_axis_tdata[8]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[9]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[9]),
        .O(m00_fft_axis_tdata[9]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_AD9650
   (adc_spi_sdio,
    adc_spi_cs,
    adc_spi_sck,
    clk_10MHz);
  output adc_spi_sdio;
  output adc_spi_cs;
  output adc_spi_sck;
  input clk_10MHz;

  wire adc_spi_cs;
  wire adc_spi_sck;
  wire adc_spi_sdio;
  wire clk_10MHz;
  wire \cnt[7]_i_1_n_0 ;
  wire \cnt[7]_i_3_n_0 ;
  wire \cnt[7]_i_4_n_0 ;
  wire [7:0]cnt_reg;
  wire cs_r_i_1_n_0;
  wire cs_r_i_2_n_0;
  wire enable_cnt_i_1_n_0;
  wire enable_sck;
  wire enable_sck_i_1_n_0;
  wire enable_sck_i_2_n_0;
  wire [7:0]p_0_in;
  wire start_prev;

  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  OBUFT #(
    .IOSTANDARD("DEFAULT")) 
    OBUFT_inst
       (.I(1'b0),
        .O(adc_spi_sdio),
        .T(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    adc_spi_sck_INST_0
       (.I0(clk_10MHz),
        .I1(enable_sck),
        .O(adc_spi_sck));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt[0]_i_1 
       (.I0(cnt_reg[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \cnt[1]_i_1 
       (.I0(cnt_reg[0]),
        .I1(cnt_reg[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \cnt[2]_i_1 
       (.I0(cnt_reg[0]),
        .I1(cnt_reg[1]),
        .I2(cnt_reg[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \cnt[3]_i_1 
       (.I0(cnt_reg[1]),
        .I1(cnt_reg[0]),
        .I2(cnt_reg[2]),
        .I3(cnt_reg[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \cnt[4]_i_1 
       (.I0(cnt_reg[2]),
        .I1(cnt_reg[0]),
        .I2(cnt_reg[1]),
        .I3(cnt_reg[3]),
        .I4(cnt_reg[4]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \cnt[5]_i_1 
       (.I0(cnt_reg[3]),
        .I1(cnt_reg[1]),
        .I2(cnt_reg[0]),
        .I3(cnt_reg[2]),
        .I4(cnt_reg[4]),
        .I5(cnt_reg[5]),
        .O(p_0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \cnt[6]_i_1 
       (.I0(\cnt[7]_i_4_n_0 ),
        .I1(cnt_reg[6]),
        .O(p_0_in[6]));
  LUT6 #(
    .INIT(64'h00000040FFFFFFFF)) 
    \cnt[7]_i_1 
       (.I0(\cnt[7]_i_3_n_0 ),
        .I1(cnt_reg[6]),
        .I2(cnt_reg[4]),
        .I3(cnt_reg[5]),
        .I4(cnt_reg[7]),
        .I5(start_prev),
        .O(\cnt[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \cnt[7]_i_2 
       (.I0(cnt_reg[6]),
        .I1(\cnt[7]_i_4_n_0 ),
        .I2(cnt_reg[7]),
        .O(p_0_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \cnt[7]_i_3 
       (.I0(cnt_reg[2]),
        .I1(cnt_reg[3]),
        .I2(cnt_reg[0]),
        .I3(cnt_reg[1]),
        .O(\cnt[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \cnt[7]_i_4 
       (.I0(cnt_reg[5]),
        .I1(cnt_reg[3]),
        .I2(cnt_reg[1]),
        .I3(cnt_reg[0]),
        .I4(cnt_reg[2]),
        .I5(cnt_reg[4]),
        .O(\cnt[7]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[0] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(cnt_reg[0]),
        .R(\cnt[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[1] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(cnt_reg[1]),
        .R(\cnt[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[2] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(cnt_reg[2]),
        .R(\cnt[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[3] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(cnt_reg[3]),
        .R(\cnt[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[4] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(cnt_reg[4]),
        .R(\cnt[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[5] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(cnt_reg[5]),
        .R(\cnt[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[6] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(cnt_reg[6]),
        .R(\cnt[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[7] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(cnt_reg[7]),
        .R(\cnt[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000002)) 
    cs_r_i_1
       (.I0(cs_r_i_2_n_0),
        .I1(cnt_reg[7]),
        .I2(cnt_reg[6]),
        .I3(cnt_reg[2]),
        .I4(cnt_reg[3]),
        .I5(adc_spi_cs),
        .O(cs_r_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    cs_r_i_2
       (.I0(cnt_reg[0]),
        .I1(cnt_reg[1]),
        .I2(cnt_reg[4]),
        .I3(cnt_reg[5]),
        .O(cs_r_i_2_n_0));
  FDRE #(
    .INIT(1'b1),
    .IS_C_INVERTED(1'b1)) 
    cs_r_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(cs_r_i_1_n_0),
        .Q(adc_spi_cs),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFBF00000000)) 
    enable_cnt_i_1
       (.I0(\cnt[7]_i_3_n_0 ),
        .I1(cnt_reg[6]),
        .I2(cnt_reg[4]),
        .I3(cnt_reg[5]),
        .I4(cnt_reg[7]),
        .I5(start_prev),
        .O(enable_cnt_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    enable_cnt_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(enable_cnt_i_1_n_0),
        .Q(start_prev),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000666DFFFE)) 
    enable_sck_i_1
       (.I0(cnt_reg[2]),
        .I1(cnt_reg[3]),
        .I2(cnt_reg[0]),
        .I3(cnt_reg[1]),
        .I4(cnt_reg[4]),
        .I5(enable_sck_i_2_n_0),
        .O(enable_sck_i_1_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    enable_sck_i_2
       (.I0(cnt_reg[5]),
        .I1(cnt_reg[7]),
        .I2(cnt_reg[6]),
        .O(enable_sck_i_2_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    enable_sck_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(enable_sck_i_1_n_0),
        .Q(enable_sck),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

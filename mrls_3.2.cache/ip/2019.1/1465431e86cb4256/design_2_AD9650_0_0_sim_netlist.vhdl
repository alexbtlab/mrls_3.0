-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Tue Nov 30 12:43:56 2021
-- Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_AD9650_0_0_sim_netlist.vhdl
-- Design      : design_2_AD9650_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_AD9650 is
  port (
    adc_spi_sdio : out STD_LOGIC;
    adc_spi_cs : out STD_LOGIC;
    adc_spi_sck : out STD_LOGIC;
    clk_10MHz : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_AD9650;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_AD9650 is
  signal \^adc_spi_cs\ : STD_LOGIC;
  signal \cnt[7]_i_1_n_0\ : STD_LOGIC;
  signal \cnt[7]_i_3_n_0\ : STD_LOGIC;
  signal \cnt[7]_i_4_n_0\ : STD_LOGIC;
  signal cnt_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal cs_r_i_1_n_0 : STD_LOGIC;
  signal cs_r_i_2_n_0 : STD_LOGIC;
  signal enable_cnt_i_1_n_0 : STD_LOGIC;
  signal enable_sck : STD_LOGIC;
  signal enable_sck_i_1_n_0 : STD_LOGIC;
  signal enable_sck_i_2_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal start_prev : STD_LOGIC;
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of OBUFT_inst : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of OBUFT_inst : label is "DONT_CARE";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \cnt[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cnt[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \cnt[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \cnt[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \cnt[6]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \cnt[7]_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \cnt[7]_i_3\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of cs_r_i_2 : label is "soft_lutpair1";
begin
  adc_spi_cs <= \^adc_spi_cs\;
OBUFT_inst: unisim.vcomponents.OBUFT
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => '0',
      O => adc_spi_sdio,
      T => '0'
    );
adc_spi_sck_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_10MHz,
      I1 => enable_sck,
      O => adc_spi_sck
    );
\cnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(0),
      O => p_0_in(0)
    );
\cnt[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cnt_reg(0),
      I1 => cnt_reg(1),
      O => p_0_in(1)
    );
\cnt[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => cnt_reg(0),
      I1 => cnt_reg(1),
      I2 => cnt_reg(2),
      O => p_0_in(2)
    );
\cnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => cnt_reg(1),
      I1 => cnt_reg(0),
      I2 => cnt_reg(2),
      I3 => cnt_reg(3),
      O => p_0_in(3)
    );
\cnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => cnt_reg(2),
      I1 => cnt_reg(0),
      I2 => cnt_reg(1),
      I3 => cnt_reg(3),
      I4 => cnt_reg(4),
      O => p_0_in(4)
    );
\cnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => cnt_reg(3),
      I1 => cnt_reg(1),
      I2 => cnt_reg(0),
      I3 => cnt_reg(2),
      I4 => cnt_reg(4),
      I5 => cnt_reg(5),
      O => p_0_in(5)
    );
\cnt[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \cnt[7]_i_4_n_0\,
      I1 => cnt_reg(6),
      O => p_0_in(6)
    );
\cnt[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000040FFFFFFFF"
    )
        port map (
      I0 => \cnt[7]_i_3_n_0\,
      I1 => cnt_reg(6),
      I2 => cnt_reg(4),
      I3 => cnt_reg(5),
      I4 => cnt_reg(7),
      I5 => start_prev,
      O => \cnt[7]_i_1_n_0\
    );
\cnt[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => cnt_reg(6),
      I1 => \cnt[7]_i_4_n_0\,
      I2 => cnt_reg(7),
      O => p_0_in(7)
    );
\cnt[7]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_reg(2),
      I1 => cnt_reg(3),
      I2 => cnt_reg(0),
      I3 => cnt_reg(1),
      O => \cnt[7]_i_3_n_0\
    );
\cnt[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => cnt_reg(5),
      I1 => cnt_reg(3),
      I2 => cnt_reg(1),
      I3 => cnt_reg(0),
      I4 => cnt_reg(2),
      I5 => cnt_reg(4),
      O => \cnt[7]_i_4_n_0\
    );
\cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => p_0_in(0),
      Q => cnt_reg(0),
      R => \cnt[7]_i_1_n_0\
    );
\cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => p_0_in(1),
      Q => cnt_reg(1),
      R => \cnt[7]_i_1_n_0\
    );
\cnt_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => p_0_in(2),
      Q => cnt_reg(2),
      R => \cnt[7]_i_1_n_0\
    );
\cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => p_0_in(3),
      Q => cnt_reg(3),
      R => \cnt[7]_i_1_n_0\
    );
\cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => p_0_in(4),
      Q => cnt_reg(4),
      R => \cnt[7]_i_1_n_0\
    );
\cnt_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => p_0_in(5),
      Q => cnt_reg(5),
      R => \cnt[7]_i_1_n_0\
    );
\cnt_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => p_0_in(6),
      Q => cnt_reg(6),
      R => \cnt[7]_i_1_n_0\
    );
\cnt_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => p_0_in(7),
      Q => cnt_reg(7),
      R => \cnt[7]_i_1_n_0\
    );
cs_r_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000002"
    )
        port map (
      I0 => cs_r_i_2_n_0,
      I1 => cnt_reg(7),
      I2 => cnt_reg(6),
      I3 => cnt_reg(2),
      I4 => cnt_reg(3),
      I5 => \^adc_spi_cs\,
      O => cs_r_i_1_n_0
    );
cs_r_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => cnt_reg(0),
      I1 => cnt_reg(1),
      I2 => cnt_reg(4),
      I3 => cnt_reg(5),
      O => cs_r_i_2_n_0
    );
cs_r_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => cs_r_i_1_n_0,
      Q => \^adc_spi_cs\,
      R => '0'
    );
enable_cnt_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFBF00000000"
    )
        port map (
      I0 => \cnt[7]_i_3_n_0\,
      I1 => cnt_reg(6),
      I2 => cnt_reg(4),
      I3 => cnt_reg(5),
      I4 => cnt_reg(7),
      I5 => start_prev,
      O => enable_cnt_i_1_n_0
    );
enable_cnt_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => enable_cnt_i_1_n_0,
      Q => start_prev,
      R => '0'
    );
enable_sck_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000666DFFFE"
    )
        port map (
      I0 => cnt_reg(2),
      I1 => cnt_reg(3),
      I2 => cnt_reg(0),
      I3 => cnt_reg(1),
      I4 => cnt_reg(4),
      I5 => enable_sck_i_2_n_0,
      O => enable_sck_i_1_n_0
    );
enable_sck_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => cnt_reg(5),
      I1 => cnt_reg(7),
      I2 => cnt_reg(6),
      O => enable_sck_i_2_n_0
    );
enable_sck_reg: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => enable_sck_i_1_n_0,
      Q => enable_sck,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0 is
  port (
    adc_spi_sdio : out STD_LOGIC;
    m00_fft_axis_tlast : out STD_LOGIC;
    adc_spi_sck : out STD_LOGIC;
    m01_fft_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_fft_axis_tvalid : out STD_LOGIC;
    m01_fft_axis_tvalid : out STD_LOGIC;
    adc_spi_cs : out STD_LOGIC;
    clk_10MHz : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    dco_or_dcoa : in STD_LOGIC;
    allowed_clk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    DATA_INA : in STD_LOGIC_VECTOR ( 15 downto 0 );
    mux_fft : in STD_LOGIC;
    DATA_INB : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0 is
  signal clear : STD_LOGIC;
  signal cnt_DCO1 : STD_LOGIC;
  signal \cnt_DCO1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \cnt_DCO1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \cnt_DCO1_carry__0_n_3\ : STD_LOGIC;
  signal cnt_DCO1_carry_i_1_n_0 : STD_LOGIC;
  signal cnt_DCO1_carry_i_2_n_0 : STD_LOGIC;
  signal cnt_DCO1_carry_i_3_n_0 : STD_LOGIC;
  signal cnt_DCO1_carry_i_4_n_0 : STD_LOGIC;
  signal cnt_DCO1_carry_n_0 : STD_LOGIC;
  signal cnt_DCO1_carry_n_1 : STD_LOGIC;
  signal cnt_DCO1_carry_n_2 : STD_LOGIC;
  signal cnt_DCO1_carry_n_3 : STD_LOGIC;
  signal \cnt_DCO[0]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_DCO[0]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_DCO[0]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_DCO[0]_i_6_n_0\ : STD_LOGIC;
  signal \cnt_DCO[12]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_DCO[12]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_DCO[12]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_DCO[12]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_DCO[4]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_DCO[4]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_DCO[4]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_DCO[4]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_DCO[8]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_DCO[8]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_DCO[8]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_DCO[8]_i_5_n_0\ : STD_LOGIC;
  signal cnt_DCO_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_DCO_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_DCO_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_DCO_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_DCO_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_DCO_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \cnt_DCO_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \cnt_DCO_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \cnt_DCO_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \cnt_DCO_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_DCO_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_DCO_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_DCO_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_DCO_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_DCO_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_DCO_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_DCO_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_DCO_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_DCO_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_DCO_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_DCO_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_DCO_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_DCO_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_DCO_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_DCO_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_DCO_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_DCO_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_DCO_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_DCO_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_DCO_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_DCO_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_DCO_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_in_DCO[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_in_DCO[0]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_in_DCO__14\ : STD_LOGIC;
  signal cnt_in_DCO_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_in_DCO_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal m00_fft_axis_tlast_r0 : STD_LOGIC;
  signal m00_fft_axis_tlast_r1 : STD_LOGIC;
  signal \m00_fft_axis_tlast_r1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \m00_fft_axis_tlast_r1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \m00_fft_axis_tlast_r1_carry__0_n_3\ : STD_LOGIC;
  signal m00_fft_axis_tlast_r1_carry_i_1_n_0 : STD_LOGIC;
  signal m00_fft_axis_tlast_r1_carry_i_2_n_0 : STD_LOGIC;
  signal m00_fft_axis_tlast_r1_carry_i_3_n_0 : STD_LOGIC;
  signal m00_fft_axis_tlast_r1_carry_i_4_n_0 : STD_LOGIC;
  signal m00_fft_axis_tlast_r1_carry_n_0 : STD_LOGIC;
  signal m00_fft_axis_tlast_r1_carry_n_1 : STD_LOGIC;
  signal m00_fft_axis_tlast_r1_carry_n_2 : STD_LOGIC;
  signal m00_fft_axis_tlast_r1_carry_n_3 : STD_LOGIC;
  signal \^m00_fft_axis_tvalid\ : STD_LOGIC;
  signal m00_fft_axis_tvalid_r1 : STD_LOGIC;
  signal \m00_fft_axis_tvalid_r1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \m00_fft_axis_tvalid_r1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \m00_fft_axis_tvalid_r1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \m00_fft_axis_tvalid_r1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \m00_fft_axis_tvalid_r1_carry__0_n_1\ : STD_LOGIC;
  signal \m00_fft_axis_tvalid_r1_carry__0_n_2\ : STD_LOGIC;
  signal \m00_fft_axis_tvalid_r1_carry__0_n_3\ : STD_LOGIC;
  signal m00_fft_axis_tvalid_r1_carry_i_1_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r1_carry_i_2_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r1_carry_i_3_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r1_carry_i_4_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r1_carry_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r1_carry_n_1 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r1_carry_n_2 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r1_carry_n_3 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r_i_10_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r_i_1_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r_i_3_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r_i_4_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r_i_5_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r_i_6_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r_i_7_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r_i_8_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r_i_9_n_0 : STD_LOGIC;
  signal \^m01_fft_axis_tvalid\ : STD_LOGIC;
  signal m01_fft_axis_tvalid_r_i_1_n_0 : STD_LOGIC;
  signal NLW_cnt_DCO1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cnt_DCO1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_cnt_DCO1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cnt_DCO_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_cnt_in_DCO_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_m00_fft_axis_tlast_r1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_m00_fft_axis_tlast_r1_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_m00_fft_axis_tlast_r1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_m00_fft_axis_tvalid_r1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_m00_fft_axis_tvalid_r1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[0]_INST_0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[10]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[11]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[12]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[13]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[14]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[15]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[16]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[17]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[18]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[19]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[1]_INST_0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[20]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[21]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[22]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[23]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[24]_INST_0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[25]_INST_0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[26]_INST_0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[27]_INST_0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[28]_INST_0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[29]_INST_0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[2]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[30]_INST_0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[31]_INST_0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[3]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[4]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[5]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[6]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[7]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[8]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[9]_INST_0\ : label is "soft_lutpair8";
begin
  m00_fft_axis_tvalid <= \^m00_fft_axis_tvalid\;
  m01_fft_axis_tvalid <= \^m01_fft_axis_tvalid\;
cnt_DCO1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cnt_DCO1_carry_n_0,
      CO(2) => cnt_DCO1_carry_n_1,
      CO(1) => cnt_DCO1_carry_n_2,
      CO(0) => cnt_DCO1_carry_n_3,
      CYINIT => '0',
      DI(3 downto 0) => B"1111",
      O(3 downto 0) => NLW_cnt_DCO1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => cnt_DCO1_carry_i_1_n_0,
      S(2) => cnt_DCO1_carry_i_2_n_0,
      S(1) => cnt_DCO1_carry_i_3_n_0,
      S(0) => cnt_DCO1_carry_i_4_n_0
    );
\cnt_DCO1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => cnt_DCO1_carry_n_0,
      CO(3 downto 2) => \NLW_cnt_DCO1_carry__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => cnt_DCO1,
      CO(0) => \cnt_DCO1_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0011",
      O(3 downto 0) => \NLW_cnt_DCO1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \cnt_DCO1_carry__0_i_1_n_0\,
      S(0) => \cnt_DCO1_carry__0_i_2_n_0\
    );
\cnt_DCO1_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_DCO_reg(15),
      O => \cnt_DCO1_carry__0_i_1_n_0\
    );
\cnt_DCO1_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => cnt_DCO_reg(13),
      I1 => cnt_DCO_reg(14),
      I2 => cnt_DCO_reg(12),
      O => \cnt_DCO1_carry__0_i_2_n_0\
    );
cnt_DCO1_carry_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => cnt_DCO_reg(11),
      I1 => cnt_DCO_reg(10),
      I2 => cnt_DCO_reg(9),
      O => cnt_DCO1_carry_i_1_n_0
    );
cnt_DCO1_carry_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => cnt_DCO_reg(7),
      I1 => cnt_DCO_reg(8),
      I2 => cnt_DCO_reg(6),
      O => cnt_DCO1_carry_i_2_n_0
    );
cnt_DCO1_carry_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => cnt_DCO_reg(5),
      I1 => cnt_DCO_reg(4),
      I2 => cnt_DCO_reg(3),
      O => cnt_DCO1_carry_i_3_n_0
    );
cnt_DCO1_carry_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => cnt_DCO_reg(0),
      I1 => cnt_DCO_reg(1),
      I2 => cnt_DCO_reg(2),
      O => cnt_DCO1_carry_i_4_n_0
    );
\cnt_DCO[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => allowed_clk,
      O => clear
    );
\cnt_DCO[0]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(3),
      O => \cnt_DCO[0]_i_3_n_0\
    );
\cnt_DCO[0]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(2),
      O => \cnt_DCO[0]_i_4_n_0\
    );
\cnt_DCO[0]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(1),
      O => \cnt_DCO[0]_i_5_n_0\
    );
\cnt_DCO[0]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(0),
      O => \cnt_DCO[0]_i_6_n_0\
    );
\cnt_DCO[12]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(15),
      O => \cnt_DCO[12]_i_2_n_0\
    );
\cnt_DCO[12]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(14),
      O => \cnt_DCO[12]_i_3_n_0\
    );
\cnt_DCO[12]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(13),
      O => \cnt_DCO[12]_i_4_n_0\
    );
\cnt_DCO[12]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(12),
      O => \cnt_DCO[12]_i_5_n_0\
    );
\cnt_DCO[4]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(7),
      O => \cnt_DCO[4]_i_2_n_0\
    );
\cnt_DCO[4]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(6),
      O => \cnt_DCO[4]_i_3_n_0\
    );
\cnt_DCO[4]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(5),
      O => \cnt_DCO[4]_i_4_n_0\
    );
\cnt_DCO[4]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(4),
      O => \cnt_DCO[4]_i_5_n_0\
    );
\cnt_DCO[8]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(11),
      O => \cnt_DCO[8]_i_2_n_0\
    );
\cnt_DCO[8]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(10),
      O => \cnt_DCO[8]_i_3_n_0\
    );
\cnt_DCO[8]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(9),
      O => \cnt_DCO[8]_i_4_n_0\
    );
\cnt_DCO[8]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt_DCO1,
      I1 => cnt_DCO_reg(8),
      O => \cnt_DCO[8]_i_5_n_0\
    );
\cnt_DCO_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[0]_i_2_n_7\,
      Q => cnt_DCO_reg(0),
      R => clear
    );
\cnt_DCO_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_DCO_reg[0]_i_2_n_0\,
      CO(2) => \cnt_DCO_reg[0]_i_2_n_1\,
      CO(1) => \cnt_DCO_reg[0]_i_2_n_2\,
      CO(0) => \cnt_DCO_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_DCO_reg[0]_i_2_n_4\,
      O(2) => \cnt_DCO_reg[0]_i_2_n_5\,
      O(1) => \cnt_DCO_reg[0]_i_2_n_6\,
      O(0) => \cnt_DCO_reg[0]_i_2_n_7\,
      S(3) => \cnt_DCO[0]_i_3_n_0\,
      S(2) => \cnt_DCO[0]_i_4_n_0\,
      S(1) => \cnt_DCO[0]_i_5_n_0\,
      S(0) => \cnt_DCO[0]_i_6_n_0\
    );
\cnt_DCO_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[8]_i_1_n_5\,
      Q => cnt_DCO_reg(10),
      R => clear
    );
\cnt_DCO_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[8]_i_1_n_4\,
      Q => cnt_DCO_reg(11),
      R => clear
    );
\cnt_DCO_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[12]_i_1_n_7\,
      Q => cnt_DCO_reg(12),
      R => clear
    );
\cnt_DCO_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_DCO_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt_DCO_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_DCO_reg[12]_i_1_n_1\,
      CO(1) => \cnt_DCO_reg[12]_i_1_n_2\,
      CO(0) => \cnt_DCO_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_DCO_reg[12]_i_1_n_4\,
      O(2) => \cnt_DCO_reg[12]_i_1_n_5\,
      O(1) => \cnt_DCO_reg[12]_i_1_n_6\,
      O(0) => \cnt_DCO_reg[12]_i_1_n_7\,
      S(3) => \cnt_DCO[12]_i_2_n_0\,
      S(2) => \cnt_DCO[12]_i_3_n_0\,
      S(1) => \cnt_DCO[12]_i_4_n_0\,
      S(0) => \cnt_DCO[12]_i_5_n_0\
    );
\cnt_DCO_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[12]_i_1_n_6\,
      Q => cnt_DCO_reg(13),
      R => clear
    );
\cnt_DCO_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[12]_i_1_n_5\,
      Q => cnt_DCO_reg(14),
      R => clear
    );
\cnt_DCO_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[12]_i_1_n_4\,
      Q => cnt_DCO_reg(15),
      R => clear
    );
\cnt_DCO_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[0]_i_2_n_6\,
      Q => cnt_DCO_reg(1),
      R => clear
    );
\cnt_DCO_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[0]_i_2_n_5\,
      Q => cnt_DCO_reg(2),
      R => clear
    );
\cnt_DCO_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[0]_i_2_n_4\,
      Q => cnt_DCO_reg(3),
      R => clear
    );
\cnt_DCO_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[4]_i_1_n_7\,
      Q => cnt_DCO_reg(4),
      R => clear
    );
\cnt_DCO_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_DCO_reg[0]_i_2_n_0\,
      CO(3) => \cnt_DCO_reg[4]_i_1_n_0\,
      CO(2) => \cnt_DCO_reg[4]_i_1_n_1\,
      CO(1) => \cnt_DCO_reg[4]_i_1_n_2\,
      CO(0) => \cnt_DCO_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_DCO_reg[4]_i_1_n_4\,
      O(2) => \cnt_DCO_reg[4]_i_1_n_5\,
      O(1) => \cnt_DCO_reg[4]_i_1_n_6\,
      O(0) => \cnt_DCO_reg[4]_i_1_n_7\,
      S(3) => \cnt_DCO[4]_i_2_n_0\,
      S(2) => \cnt_DCO[4]_i_3_n_0\,
      S(1) => \cnt_DCO[4]_i_4_n_0\,
      S(0) => \cnt_DCO[4]_i_5_n_0\
    );
\cnt_DCO_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[4]_i_1_n_6\,
      Q => cnt_DCO_reg(5),
      R => clear
    );
\cnt_DCO_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[4]_i_1_n_5\,
      Q => cnt_DCO_reg(6),
      R => clear
    );
\cnt_DCO_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[4]_i_1_n_4\,
      Q => cnt_DCO_reg(7),
      R => clear
    );
\cnt_DCO_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[8]_i_1_n_7\,
      Q => cnt_DCO_reg(8),
      R => clear
    );
\cnt_DCO_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_DCO_reg[4]_i_1_n_0\,
      CO(3) => \cnt_DCO_reg[8]_i_1_n_0\,
      CO(2) => \cnt_DCO_reg[8]_i_1_n_1\,
      CO(1) => \cnt_DCO_reg[8]_i_1_n_2\,
      CO(0) => \cnt_DCO_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_DCO_reg[8]_i_1_n_4\,
      O(2) => \cnt_DCO_reg[8]_i_1_n_5\,
      O(1) => \cnt_DCO_reg[8]_i_1_n_6\,
      O(0) => \cnt_DCO_reg[8]_i_1_n_7\,
      S(3) => \cnt_DCO[8]_i_2_n_0\,
      S(2) => \cnt_DCO[8]_i_3_n_0\,
      S(1) => \cnt_DCO[8]_i_4_n_0\,
      S(0) => \cnt_DCO[8]_i_5_n_0\
    );
\cnt_DCO_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => dco_or_dcoa,
      CE => '1',
      D => \cnt_DCO_reg[8]_i_1_n_6\,
      Q => cnt_DCO_reg(9),
      R => clear
    );
\cnt_in_DCO[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => dco_or_dcoa,
      I1 => allowed_clk,
      I2 => s00_axi_aresetn,
      O => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_in_DCO_reg(0),
      O => \cnt_in_DCO[0]_i_3_n_0\
    );
\cnt_in_DCO_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[0]_i_2_n_7\,
      Q => cnt_in_DCO_reg(0),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_in_DCO_reg[0]_i_2_n_0\,
      CO(2) => \cnt_in_DCO_reg[0]_i_2_n_1\,
      CO(1) => \cnt_in_DCO_reg[0]_i_2_n_2\,
      CO(0) => \cnt_in_DCO_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_in_DCO_reg[0]_i_2_n_4\,
      O(2) => \cnt_in_DCO_reg[0]_i_2_n_5\,
      O(1) => \cnt_in_DCO_reg[0]_i_2_n_6\,
      O(0) => \cnt_in_DCO_reg[0]_i_2_n_7\,
      S(3 downto 1) => cnt_in_DCO_reg(3 downto 1),
      S(0) => \cnt_in_DCO[0]_i_3_n_0\
    );
\cnt_in_DCO_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[8]_i_1_n_5\,
      Q => cnt_in_DCO_reg(10),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[8]_i_1_n_4\,
      Q => cnt_in_DCO_reg(11),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[12]_i_1_n_7\,
      Q => cnt_in_DCO_reg(12),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_in_DCO_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt_in_DCO_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_in_DCO_reg[12]_i_1_n_1\,
      CO(1) => \cnt_in_DCO_reg[12]_i_1_n_2\,
      CO(0) => \cnt_in_DCO_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_in_DCO_reg[12]_i_1_n_4\,
      O(2) => \cnt_in_DCO_reg[12]_i_1_n_5\,
      O(1) => \cnt_in_DCO_reg[12]_i_1_n_6\,
      O(0) => \cnt_in_DCO_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt_in_DCO_reg(15 downto 12)
    );
\cnt_in_DCO_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[12]_i_1_n_6\,
      Q => cnt_in_DCO_reg(13),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[12]_i_1_n_5\,
      Q => cnt_in_DCO_reg(14),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[12]_i_1_n_4\,
      Q => cnt_in_DCO_reg(15),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[0]_i_2_n_6\,
      Q => cnt_in_DCO_reg(1),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[0]_i_2_n_5\,
      Q => cnt_in_DCO_reg(2),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[0]_i_2_n_4\,
      Q => cnt_in_DCO_reg(3),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[4]_i_1_n_7\,
      Q => cnt_in_DCO_reg(4),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_in_DCO_reg[0]_i_2_n_0\,
      CO(3) => \cnt_in_DCO_reg[4]_i_1_n_0\,
      CO(2) => \cnt_in_DCO_reg[4]_i_1_n_1\,
      CO(1) => \cnt_in_DCO_reg[4]_i_1_n_2\,
      CO(0) => \cnt_in_DCO_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_in_DCO_reg[4]_i_1_n_4\,
      O(2) => \cnt_in_DCO_reg[4]_i_1_n_5\,
      O(1) => \cnt_in_DCO_reg[4]_i_1_n_6\,
      O(0) => \cnt_in_DCO_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt_in_DCO_reg(7 downto 4)
    );
\cnt_in_DCO_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[4]_i_1_n_6\,
      Q => cnt_in_DCO_reg(5),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[4]_i_1_n_5\,
      Q => cnt_in_DCO_reg(6),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[4]_i_1_n_4\,
      Q => cnt_in_DCO_reg(7),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[8]_i_1_n_7\,
      Q => cnt_in_DCO_reg(8),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_in_DCO_reg[4]_i_1_n_0\,
      CO(3) => \cnt_in_DCO_reg[8]_i_1_n_0\,
      CO(2) => \cnt_in_DCO_reg[8]_i_1_n_1\,
      CO(1) => \cnt_in_DCO_reg[8]_i_1_n_2\,
      CO(0) => \cnt_in_DCO_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_in_DCO_reg[8]_i_1_n_4\,
      O(2) => \cnt_in_DCO_reg[8]_i_1_n_5\,
      O(1) => \cnt_in_DCO_reg[8]_i_1_n_6\,
      O(0) => \cnt_in_DCO_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt_in_DCO_reg(11 downto 8)
    );
\cnt_in_DCO_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \cnt_in_DCO_reg[8]_i_1_n_6\,
      Q => cnt_in_DCO_reg(9),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
m00_fft_axis_tlast_r1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => m00_fft_axis_tlast_r1_carry_n_0,
      CO(2) => m00_fft_axis_tlast_r1_carry_n_1,
      CO(1) => m00_fft_axis_tlast_r1_carry_n_2,
      CO(0) => m00_fft_axis_tlast_r1_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_m00_fft_axis_tlast_r1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => m00_fft_axis_tlast_r1_carry_i_1_n_0,
      S(2) => m00_fft_axis_tlast_r1_carry_i_2_n_0,
      S(1) => m00_fft_axis_tlast_r1_carry_i_3_n_0,
      S(0) => m00_fft_axis_tlast_r1_carry_i_4_n_0
    );
\m00_fft_axis_tlast_r1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => m00_fft_axis_tlast_r1_carry_n_0,
      CO(3 downto 2) => \NLW_m00_fft_axis_tlast_r1_carry__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => m00_fft_axis_tlast_r1,
      CO(0) => \m00_fft_axis_tlast_r1_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_m00_fft_axis_tlast_r1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \m00_fft_axis_tlast_r1_carry__0_i_1_n_0\,
      S(0) => \m00_fft_axis_tlast_r1_carry__0_i_2_n_0\
    );
\m00_fft_axis_tlast_r1_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_DCO_reg(15),
      O => \m00_fft_axis_tlast_r1_carry__0_i_1_n_0\
    );
\m00_fft_axis_tlast_r1_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => cnt_DCO_reg(13),
      I1 => cnt_DCO_reg(14),
      I2 => cnt_DCO_reg(12),
      O => \m00_fft_axis_tlast_r1_carry__0_i_2_n_0\
    );
m00_fft_axis_tlast_r1_carry_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => cnt_DCO_reg(11),
      I1 => cnt_DCO_reg(10),
      I2 => cnt_DCO_reg(9),
      O => m00_fft_axis_tlast_r1_carry_i_1_n_0
    );
m00_fft_axis_tlast_r1_carry_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => cnt_DCO_reg(7),
      I1 => cnt_DCO_reg(8),
      I2 => cnt_DCO_reg(6),
      O => m00_fft_axis_tlast_r1_carry_i_2_n_0
    );
m00_fft_axis_tlast_r1_carry_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => cnt_DCO_reg(5),
      I1 => cnt_DCO_reg(4),
      I2 => cnt_DCO_reg(3),
      O => m00_fft_axis_tlast_r1_carry_i_3_n_0
    );
m00_fft_axis_tlast_r1_carry_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => cnt_DCO_reg(1),
      I1 => cnt_DCO_reg(2),
      I2 => cnt_DCO_reg(0),
      O => m00_fft_axis_tlast_r1_carry_i_4_n_0
    );
m00_fft_axis_tlast_r_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \cnt_in_DCO__14\,
      I1 => m00_fft_axis_tlast_r1,
      O => m00_fft_axis_tlast_r0
    );
m00_fft_axis_tlast_r_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => m00_fft_axis_tlast_r0,
      Q => m00_fft_axis_tlast,
      R => '0'
    );
m00_fft_axis_tvalid_r1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => m00_fft_axis_tvalid_r1_carry_n_0,
      CO(2) => m00_fft_axis_tvalid_r1_carry_n_1,
      CO(1) => m00_fft_axis_tvalid_r1_carry_n_2,
      CO(0) => m00_fft_axis_tvalid_r1_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_m00_fft_axis_tvalid_r1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => m00_fft_axis_tvalid_r1_carry_i_1_n_0,
      S(2) => m00_fft_axis_tvalid_r1_carry_i_2_n_0,
      S(1) => m00_fft_axis_tvalid_r1_carry_i_3_n_0,
      S(0) => m00_fft_axis_tvalid_r1_carry_i_4_n_0
    );
\m00_fft_axis_tvalid_r1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => m00_fft_axis_tvalid_r1_carry_n_0,
      CO(3) => m00_fft_axis_tvalid_r1,
      CO(2) => \m00_fft_axis_tvalid_r1_carry__0_n_1\,
      CO(1) => \m00_fft_axis_tvalid_r1_carry__0_n_2\,
      CO(0) => \m00_fft_axis_tvalid_r1_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_m00_fft_axis_tvalid_r1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \m00_fft_axis_tvalid_r1_carry__0_i_1_n_0\,
      S(2) => \m00_fft_axis_tvalid_r1_carry__0_i_2_n_0\,
      S(1) => \m00_fft_axis_tvalid_r1_carry__0_i_3_n_0\,
      S(0) => \m00_fft_axis_tvalid_r1_carry__0_i_4_n_0\
    );
\m00_fft_axis_tvalid_r1_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_DCO_reg(14),
      I1 => cnt_DCO_reg(15),
      O => \m00_fft_axis_tvalid_r1_carry__0_i_1_n_0\
    );
\m00_fft_axis_tvalid_r1_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_DCO_reg(12),
      I1 => cnt_DCO_reg(13),
      O => \m00_fft_axis_tvalid_r1_carry__0_i_2_n_0\
    );
\m00_fft_axis_tvalid_r1_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_DCO_reg(10),
      I1 => cnt_DCO_reg(11),
      O => \m00_fft_axis_tvalid_r1_carry__0_i_3_n_0\
    );
\m00_fft_axis_tvalid_r1_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_DCO_reg(8),
      I1 => cnt_DCO_reg(9),
      O => \m00_fft_axis_tvalid_r1_carry__0_i_4_n_0\
    );
m00_fft_axis_tvalid_r1_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_DCO_reg(6),
      I1 => cnt_DCO_reg(7),
      O => m00_fft_axis_tvalid_r1_carry_i_1_n_0
    );
m00_fft_axis_tvalid_r1_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_DCO_reg(4),
      I1 => cnt_DCO_reg(5),
      O => m00_fft_axis_tvalid_r1_carry_i_2_n_0
    );
m00_fft_axis_tvalid_r1_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_DCO_reg(2),
      I1 => cnt_DCO_reg(3),
      O => m00_fft_axis_tvalid_r1_carry_i_3_n_0
    );
m00_fft_axis_tvalid_r1_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_DCO_reg(0),
      I1 => cnt_DCO_reg(1),
      O => m00_fft_axis_tvalid_r1_carry_i_4_n_0
    );
m00_fft_axis_tvalid_r_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A800FFFFA8000000"
    )
        port map (
      I0 => \cnt_in_DCO__14\,
      I1 => m00_fft_axis_tvalid_r_i_3_n_0,
      I2 => m00_fft_axis_tvalid_r_i_4_n_0,
      I3 => m00_fft_axis_tvalid_r1,
      I4 => mux_fft,
      I5 => \^m00_fft_axis_tvalid\,
      O => m00_fft_axis_tvalid_r_i_1_n_0
    );
m00_fft_axis_tvalid_r_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_DCO_reg(14),
      I1 => cnt_DCO_reg(13),
      I2 => cnt_DCO_reg(0),
      I3 => cnt_DCO_reg(15),
      O => m00_fft_axis_tvalid_r_i_10_n_0
    );
m00_fft_axis_tvalid_r_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => m00_fft_axis_tvalid_r_i_5_n_0,
      I1 => m00_fft_axis_tvalid_r_i_6_n_0,
      I2 => m00_fft_axis_tvalid_r_i_7_n_0,
      I3 => m00_fft_axis_tvalid_r_i_8_n_0,
      O => \cnt_in_DCO__14\
    );
m00_fft_axis_tvalid_r_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => cnt_DCO_reg(3),
      I1 => cnt_DCO_reg(4),
      I2 => cnt_DCO_reg(1),
      I3 => cnt_DCO_reg(2),
      I4 => m00_fft_axis_tvalid_r_i_9_n_0,
      O => m00_fft_axis_tvalid_r_i_3_n_0
    );
m00_fft_axis_tvalid_r_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => cnt_DCO_reg(11),
      I1 => cnt_DCO_reg(12),
      I2 => cnt_DCO_reg(9),
      I3 => cnt_DCO_reg(10),
      I4 => m00_fft_axis_tvalid_r_i_10_n_0,
      O => m00_fft_axis_tvalid_r_i_4_n_0
    );
m00_fft_axis_tvalid_r_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_in_DCO_reg(10),
      I1 => cnt_in_DCO_reg(11),
      I2 => cnt_in_DCO_reg(8),
      I3 => cnt_in_DCO_reg(9),
      O => m00_fft_axis_tvalid_r_i_5_n_0
    );
m00_fft_axis_tvalid_r_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_in_DCO_reg(15),
      I1 => cnt_in_DCO_reg(14),
      I2 => cnt_in_DCO_reg(12),
      I3 => cnt_in_DCO_reg(13),
      O => m00_fft_axis_tvalid_r_i_6_n_0
    );
m00_fft_axis_tvalid_r_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => cnt_in_DCO_reg(2),
      I1 => cnt_in_DCO_reg(3),
      I2 => cnt_in_DCO_reg(0),
      I3 => cnt_in_DCO_reg(1),
      O => m00_fft_axis_tvalid_r_i_7_n_0
    );
m00_fft_axis_tvalid_r_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_in_DCO_reg(6),
      I1 => cnt_in_DCO_reg(7),
      I2 => cnt_in_DCO_reg(4),
      I3 => cnt_in_DCO_reg(5),
      O => m00_fft_axis_tvalid_r_i_8_n_0
    );
m00_fft_axis_tvalid_r_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_DCO_reg(6),
      I1 => cnt_DCO_reg(5),
      I2 => cnt_DCO_reg(8),
      I3 => cnt_DCO_reg(7),
      O => m00_fft_axis_tvalid_r_i_9_n_0
    );
m00_fft_axis_tvalid_r_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => m00_fft_axis_tvalid_r_i_1_n_0,
      Q => \^m00_fft_axis_tvalid\,
      R => '0'
    );
\m01_fft_axis_tdata[0]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(0),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(0)
    );
\m01_fft_axis_tdata[10]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(10),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(10)
    );
\m01_fft_axis_tdata[11]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(11),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(11)
    );
\m01_fft_axis_tdata[12]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(12),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(12)
    );
\m01_fft_axis_tdata[13]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(13),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(13)
    );
\m01_fft_axis_tdata[14]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(14),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(14)
    );
\m01_fft_axis_tdata[15]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(15),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(15)
    );
\m01_fft_axis_tdata[16]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(0),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(16)
    );
\m01_fft_axis_tdata[17]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(1),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(17)
    );
\m01_fft_axis_tdata[18]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(2),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(18)
    );
\m01_fft_axis_tdata[19]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(3),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(19)
    );
\m01_fft_axis_tdata[1]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(1),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(1)
    );
\m01_fft_axis_tdata[20]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(4),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(20)
    );
\m01_fft_axis_tdata[21]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(5),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(21)
    );
\m01_fft_axis_tdata[22]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(6),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(22)
    );
\m01_fft_axis_tdata[23]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(7),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(23)
    );
\m01_fft_axis_tdata[24]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(8),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(24)
    );
\m01_fft_axis_tdata[25]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(9),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(25)
    );
\m01_fft_axis_tdata[26]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(10),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(26)
    );
\m01_fft_axis_tdata[27]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(11),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(27)
    );
\m01_fft_axis_tdata[28]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(12),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(28)
    );
\m01_fft_axis_tdata[29]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(13),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(29)
    );
\m01_fft_axis_tdata[2]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(2),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(2)
    );
\m01_fft_axis_tdata[30]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(14),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(30)
    );
\m01_fft_axis_tdata[31]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(15),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(31)
    );
\m01_fft_axis_tdata[3]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(3),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(3)
    );
\m01_fft_axis_tdata[4]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(4),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(4)
    );
\m01_fft_axis_tdata[5]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(5),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(5)
    );
\m01_fft_axis_tdata[6]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(6),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(6)
    );
\m01_fft_axis_tdata[7]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(7),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(7)
    );
\m01_fft_axis_tdata[8]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(8),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(8)
    );
\m01_fft_axis_tdata[9]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(9),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(9)
    );
m01_fft_axis_tvalid_r_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"B8B8B88888888888"
    )
        port map (
      I0 => \^m01_fft_axis_tvalid\,
      I1 => mux_fft,
      I2 => \cnt_in_DCO__14\,
      I3 => m00_fft_axis_tvalid_r_i_3_n_0,
      I4 => m00_fft_axis_tvalid_r_i_4_n_0,
      I5 => m00_fft_axis_tvalid_r1,
      O => m01_fft_axis_tvalid_r_i_1_n_0
    );
m01_fft_axis_tvalid_r_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => m01_fft_axis_tvalid_r_i_1_n_0,
      Q => \^m01_fft_axis_tvalid\,
      R => '0'
    );
spi_AD9650_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_AD9650
     port map (
      adc_spi_cs => adc_spi_cs,
      adc_spi_sck => adc_spi_sck,
      adc_spi_sdio => adc_spi_sdio,
      clk_10MHz => clk_10MHz
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk_10MHz : in STD_LOGIC;
    m00_fft_axis_tvalid : out STD_LOGIC;
    m00_fft_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_fft_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_fft_axis_tlast : out STD_LOGIC;
    m00_fft_axis_tready : in STD_LOGIC;
    m01_fft_axis_tvalid : out STD_LOGIC;
    m01_fft_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m01_fft_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m01_fft_axis_tlast : out STD_LOGIC;
    m01_fft_axis_tready : in STD_LOGIC;
    dco_or_dcoa : in STD_LOGIC;
    dco_or_dcob : in STD_LOGIC;
    dco_or_ora : in STD_LOGIC;
    dco_or_orb : in STD_LOGIC;
    adc_spi_sck : out STD_LOGIC;
    adc_spi_cs : out STD_LOGIC;
    adc_spi_sdio : inout STD_LOGIC;
    ADC_PDwN : out STD_LOGIC;
    SYNC : out STD_LOGIC;
    DATA_INA : in STD_LOGIC_VECTOR ( 15 downto 0 );
    DATA_INB : in STD_LOGIC_VECTOR ( 15 downto 0 );
    allowed_clk : in STD_LOGIC;
    mux_fft : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    DCO : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_2_AD9650_0_0,AD9650_v2_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "AD9650_v2_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^dco_or_dcoa\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of adc_spi_cs : signal is "bt.local:interface:adc_spi:1.0 ADC_SPI cs";
  attribute X_INTERFACE_INFO of adc_spi_sck : signal is "bt.local:interface:adc_spi:1.0 ADC_SPI sck";
  attribute X_INTERFACE_INFO of adc_spi_sdio : signal is "bt.local:interface:adc_spi:1.0 ADC_SPI sdio";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of adc_spi_sdio : signal is "XIL_INTERFACENAME ADC_SPI, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of allowed_clk : signal is "xilinx.com:signal:clock:1.0 allowed_clk CLK";
  attribute X_INTERFACE_PARAMETER of allowed_clk : signal is "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_allowed_clk_0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of dco_or_dcoa : signal is "user.org:interface:dco_or:1.0 DCO_OR dcoa";
  attribute X_INTERFACE_INFO of dco_or_dcob : signal is "user.org:interface:dco_or:1.0 DCO_OR dcob";
  attribute X_INTERFACE_INFO of dco_or_ora : signal is "user.org:interface:dco_or:1.0 DCO_OR ora";
  attribute X_INTERFACE_INFO of dco_or_orb : signal is "user.org:interface:dco_or:1.0 DCO_OR orb";
  attribute X_INTERFACE_PARAMETER of dco_or_orb : signal is "XIL_INTERFACENAME DCO_OR, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of m00_fft_axis_tlast : signal is "xilinx.com:interface:axis:1.0 m00_fft_axis TLAST";
  attribute X_INTERFACE_INFO of m00_fft_axis_tready : signal is "xilinx.com:interface:axis:1.0 m00_fft_axis TREADY";
  attribute X_INTERFACE_PARAMETER of m00_fft_axis_tready : signal is "XIL_INTERFACENAME m00_fft_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_fft_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 m00_fft_axis TVALID";
  attribute X_INTERFACE_INFO of m01_fft_axis_tlast : signal is "xilinx.com:interface:axis:1.0 m01_fft_axis TLAST";
  attribute X_INTERFACE_INFO of m01_fft_axis_tready : signal is "xilinx.com:interface:axis:1.0 m01_fft_axis TREADY";
  attribute X_INTERFACE_PARAMETER of m01_fft_axis_tready : signal is "XIL_INTERFACENAME m01_fft_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m01_fft_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 m01_fft_axis TVALID";
  attribute X_INTERFACE_INFO of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute X_INTERFACE_PARAMETER of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI:m01_fft_axis:m00_fft_axis, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_fft_axis_tdata : signal is "xilinx.com:interface:axis:1.0 m00_fft_axis TDATA";
  attribute X_INTERFACE_INFO of m00_fft_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 m00_fft_axis TSTRB";
  attribute X_INTERFACE_INFO of m01_fft_axis_tdata : signal is "xilinx.com:interface:axis:1.0 m01_fft_axis TDATA";
  attribute X_INTERFACE_INFO of m01_fft_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 m01_fft_axis TSTRB";
begin
  DCO <= \^dco_or_dcoa\;
  SYNC <= \<const0>\;
  \^dco_or_dcoa\ <= dco_or_dcoa;
  m00_fft_axis_tstrb(3) <= \<const1>\;
  m00_fft_axis_tstrb(2) <= \<const1>\;
  m00_fft_axis_tstrb(1) <= \<const1>\;
  m00_fft_axis_tstrb(0) <= \<const1>\;
  m01_fft_axis_tstrb(3) <= \<const1>\;
  m01_fft_axis_tstrb(2) <= \<const1>\;
  m01_fft_axis_tstrb(1) <= \<const1>\;
  m01_fft_axis_tstrb(0) <= \<const1>\;
  ADC_PDwN <= 'Z';
  m01_fft_axis_tlast <= 'Z';
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0
     port map (
      DATA_INA(15 downto 0) => DATA_INA(15 downto 0),
      DATA_INB(15 downto 0) => DATA_INB(15 downto 0),
      adc_spi_cs => adc_spi_cs,
      adc_spi_sck => adc_spi_sck,
      adc_spi_sdio => adc_spi_sdio,
      allowed_clk => allowed_clk,
      clk_10MHz => clk_10MHz,
      dco_or_dcoa => \^dco_or_dcoa\,
      m00_fft_axis_tlast => m00_fft_axis_tlast,
      m00_fft_axis_tvalid => m00_fft_axis_tvalid,
      m01_fft_axis_tdata(31 downto 0) => m01_fft_axis_tdata(31 downto 0),
      m01_fft_axis_tvalid => m01_fft_axis_tvalid,
      mux_fft => mux_fft,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn
    );
\m00_fft_axis_tdata[0]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(0),
      O => m00_fft_axis_tdata(0)
    );
\m00_fft_axis_tdata[10]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(10),
      O => m00_fft_axis_tdata(10)
    );
\m00_fft_axis_tdata[11]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(11),
      O => m00_fft_axis_tdata(11)
    );
\m00_fft_axis_tdata[12]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(12),
      O => m00_fft_axis_tdata(12)
    );
\m00_fft_axis_tdata[13]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(13),
      O => m00_fft_axis_tdata(13)
    );
\m00_fft_axis_tdata[14]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(14),
      O => m00_fft_axis_tdata(14)
    );
\m00_fft_axis_tdata[15]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(15),
      O => m00_fft_axis_tdata(15)
    );
\m00_fft_axis_tdata[16]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(0),
      O => m00_fft_axis_tdata(16)
    );
\m00_fft_axis_tdata[17]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(1),
      O => m00_fft_axis_tdata(17)
    );
\m00_fft_axis_tdata[18]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(2),
      O => m00_fft_axis_tdata(18)
    );
\m00_fft_axis_tdata[19]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(3),
      O => m00_fft_axis_tdata(19)
    );
\m00_fft_axis_tdata[1]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(1),
      O => m00_fft_axis_tdata(1)
    );
\m00_fft_axis_tdata[20]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(4),
      O => m00_fft_axis_tdata(20)
    );
\m00_fft_axis_tdata[21]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(5),
      O => m00_fft_axis_tdata(21)
    );
\m00_fft_axis_tdata[22]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(6),
      O => m00_fft_axis_tdata(22)
    );
\m00_fft_axis_tdata[23]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(7),
      O => m00_fft_axis_tdata(23)
    );
\m00_fft_axis_tdata[24]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(8),
      O => m00_fft_axis_tdata(24)
    );
\m00_fft_axis_tdata[25]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(9),
      O => m00_fft_axis_tdata(25)
    );
\m00_fft_axis_tdata[26]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(10),
      O => m00_fft_axis_tdata(26)
    );
\m00_fft_axis_tdata[27]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(11),
      O => m00_fft_axis_tdata(27)
    );
\m00_fft_axis_tdata[28]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(12),
      O => m00_fft_axis_tdata(28)
    );
\m00_fft_axis_tdata[29]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(13),
      O => m00_fft_axis_tdata(29)
    );
\m00_fft_axis_tdata[2]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(2),
      O => m00_fft_axis_tdata(2)
    );
\m00_fft_axis_tdata[30]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(14),
      O => m00_fft_axis_tdata(30)
    );
\m00_fft_axis_tdata[31]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(15),
      O => m00_fft_axis_tdata(31)
    );
\m00_fft_axis_tdata[3]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(3),
      O => m00_fft_axis_tdata(3)
    );
\m00_fft_axis_tdata[4]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(4),
      O => m00_fft_axis_tdata(4)
    );
\m00_fft_axis_tdata[5]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(5),
      O => m00_fft_axis_tdata(5)
    );
\m00_fft_axis_tdata[6]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(6),
      O => m00_fft_axis_tdata(6)
    );
\m00_fft_axis_tdata[7]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(7),
      O => m00_fft_axis_tdata(7)
    );
\m00_fft_axis_tdata[8]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(8),
      O => m00_fft_axis_tdata(8)
    );
\m00_fft_axis_tdata[9]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(9),
      O => m00_fft_axis_tdata(9)
    );
end STRUCTURE;

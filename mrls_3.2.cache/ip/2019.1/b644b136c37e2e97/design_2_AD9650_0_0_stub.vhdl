-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Tue Nov 30 12:55:24 2021
-- Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_AD9650_0_0_stub.vhdl
-- Design      : design_2_AD9650_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    clk_10MHz : in STD_LOGIC;
    m00_fft_axis_tvalid : out STD_LOGIC;
    m00_fft_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_fft_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_fft_axis_tlast : out STD_LOGIC;
    m00_fft_axis_tready : in STD_LOGIC;
    m01_fft_axis_tvalid : out STD_LOGIC;
    m01_fft_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m01_fft_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m01_fft_axis_tlast : out STD_LOGIC;
    m01_fft_axis_tready : in STD_LOGIC;
    dco_or_dcoa : in STD_LOGIC;
    dco_or_dcob : in STD_LOGIC;
    dco_or_ora : in STD_LOGIC;
    dco_or_orb : in STD_LOGIC;
    adc_spi_sck : out STD_LOGIC;
    adc_spi_cs : out STD_LOGIC;
    adc_spi_sdio : inout STD_LOGIC;
    ADC_PDwN : out STD_LOGIC;
    SYNC : out STD_LOGIC;
    DATA_INA : in STD_LOGIC_VECTOR ( 15 downto 0 );
    DATA_INB : in STD_LOGIC_VECTOR ( 15 downto 0 );
    allowed_clk : in STD_LOGIC;
    mux_fft : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    DCO : out STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_10MHz,m00_fft_axis_tvalid,m00_fft_axis_tdata[31:0],m00_fft_axis_tstrb[3:0],m00_fft_axis_tlast,m00_fft_axis_tready,m01_fft_axis_tvalid,m01_fft_axis_tdata[31:0],m01_fft_axis_tstrb[3:0],m01_fft_axis_tlast,m01_fft_axis_tready,dco_or_dcoa,dco_or_dcob,dco_or_ora,dco_or_orb,adc_spi_sck,adc_spi_cs,adc_spi_sdio,ADC_PDwN,SYNC,DATA_INA[15:0],DATA_INB[15:0],allowed_clk,mux_fft,s00_axi_aclk,s00_axi_aresetn,DCO";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "AD9650_v2_0,Vivado 2019.1";
begin
end;

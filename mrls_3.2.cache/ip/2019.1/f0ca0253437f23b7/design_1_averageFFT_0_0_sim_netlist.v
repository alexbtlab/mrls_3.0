// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Tue Dec  7 22:06:46 2021
// Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_averageFFT_0_0_sim_netlist.v
// Design      : design_1_averageFFT_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0
   (allowed_clk_prev_reg_0,
    Q,
    reset_cnt_trig_ila,
    interrupt_frame,
    allowed_clk,
    clk_10MHz,
    m00_axis_aclk,
    m00_axis_aresetn,
    azimut_0);
  output allowed_clk_prev_reg_0;
  output [15:0]Q;
  output reset_cnt_trig_ila;
  output interrupt_frame;
  input allowed_clk;
  input clk_10MHz;
  input m00_axis_aclk;
  input m00_axis_aresetn;
  input azimut_0;

  wire [15:0]Q;
  wire allowed_clk;
  wire allowed_clk_prev_i_1_n_0;
  wire allowed_clk_prev_reg_0;
  wire azimut_0;
  wire azimut_0_prev;
  wire clear;
  wire clk_10MHz;
  wire \cnt100[0]_i_3_n_0 ;
  wire [31:0]cnt100_reg;
  wire \cnt100_reg[0]_i_2_n_0 ;
  wire \cnt100_reg[0]_i_2_n_1 ;
  wire \cnt100_reg[0]_i_2_n_2 ;
  wire \cnt100_reg[0]_i_2_n_3 ;
  wire \cnt100_reg[0]_i_2_n_4 ;
  wire \cnt100_reg[0]_i_2_n_5 ;
  wire \cnt100_reg[0]_i_2_n_6 ;
  wire \cnt100_reg[0]_i_2_n_7 ;
  wire \cnt100_reg[12]_i_1_n_0 ;
  wire \cnt100_reg[12]_i_1_n_1 ;
  wire \cnt100_reg[12]_i_1_n_2 ;
  wire \cnt100_reg[12]_i_1_n_3 ;
  wire \cnt100_reg[12]_i_1_n_4 ;
  wire \cnt100_reg[12]_i_1_n_5 ;
  wire \cnt100_reg[12]_i_1_n_6 ;
  wire \cnt100_reg[12]_i_1_n_7 ;
  wire \cnt100_reg[16]_i_1_n_0 ;
  wire \cnt100_reg[16]_i_1_n_1 ;
  wire \cnt100_reg[16]_i_1_n_2 ;
  wire \cnt100_reg[16]_i_1_n_3 ;
  wire \cnt100_reg[16]_i_1_n_4 ;
  wire \cnt100_reg[16]_i_1_n_5 ;
  wire \cnt100_reg[16]_i_1_n_6 ;
  wire \cnt100_reg[16]_i_1_n_7 ;
  wire \cnt100_reg[20]_i_1_n_0 ;
  wire \cnt100_reg[20]_i_1_n_1 ;
  wire \cnt100_reg[20]_i_1_n_2 ;
  wire \cnt100_reg[20]_i_1_n_3 ;
  wire \cnt100_reg[20]_i_1_n_4 ;
  wire \cnt100_reg[20]_i_1_n_5 ;
  wire \cnt100_reg[20]_i_1_n_6 ;
  wire \cnt100_reg[20]_i_1_n_7 ;
  wire \cnt100_reg[24]_i_1_n_0 ;
  wire \cnt100_reg[24]_i_1_n_1 ;
  wire \cnt100_reg[24]_i_1_n_2 ;
  wire \cnt100_reg[24]_i_1_n_3 ;
  wire \cnt100_reg[24]_i_1_n_4 ;
  wire \cnt100_reg[24]_i_1_n_5 ;
  wire \cnt100_reg[24]_i_1_n_6 ;
  wire \cnt100_reg[24]_i_1_n_7 ;
  wire \cnt100_reg[28]_i_1_n_1 ;
  wire \cnt100_reg[28]_i_1_n_2 ;
  wire \cnt100_reg[28]_i_1_n_3 ;
  wire \cnt100_reg[28]_i_1_n_4 ;
  wire \cnt100_reg[28]_i_1_n_5 ;
  wire \cnt100_reg[28]_i_1_n_6 ;
  wire \cnt100_reg[28]_i_1_n_7 ;
  wire \cnt100_reg[4]_i_1_n_0 ;
  wire \cnt100_reg[4]_i_1_n_1 ;
  wire \cnt100_reg[4]_i_1_n_2 ;
  wire \cnt100_reg[4]_i_1_n_3 ;
  wire \cnt100_reg[4]_i_1_n_4 ;
  wire \cnt100_reg[4]_i_1_n_5 ;
  wire \cnt100_reg[4]_i_1_n_6 ;
  wire \cnt100_reg[4]_i_1_n_7 ;
  wire \cnt100_reg[8]_i_1_n_0 ;
  wire \cnt100_reg[8]_i_1_n_1 ;
  wire \cnt100_reg[8]_i_1_n_2 ;
  wire \cnt100_reg[8]_i_1_n_3 ;
  wire \cnt100_reg[8]_i_1_n_4 ;
  wire \cnt100_reg[8]_i_1_n_5 ;
  wire \cnt100_reg[8]_i_1_n_6 ;
  wire \cnt100_reg[8]_i_1_n_7 ;
  wire [15:1]cnt_az0;
  wire cnt_az0_carry__0_i_1_n_0;
  wire cnt_az0_carry__0_i_2_n_0;
  wire cnt_az0_carry__0_i_3_n_0;
  wire cnt_az0_carry__0_i_4_n_0;
  wire cnt_az0_carry__0_n_0;
  wire cnt_az0_carry__0_n_1;
  wire cnt_az0_carry__0_n_2;
  wire cnt_az0_carry__0_n_3;
  wire cnt_az0_carry__1_i_1_n_0;
  wire cnt_az0_carry__1_i_2_n_0;
  wire cnt_az0_carry__1_i_3_n_0;
  wire cnt_az0_carry__1_i_4_n_0;
  wire cnt_az0_carry__1_n_0;
  wire cnt_az0_carry__1_n_1;
  wire cnt_az0_carry__1_n_2;
  wire cnt_az0_carry__1_n_3;
  wire cnt_az0_carry__2_i_1_n_0;
  wire cnt_az0_carry__2_i_2_n_0;
  wire cnt_az0_carry__2_i_3_n_0;
  wire cnt_az0_carry__2_n_2;
  wire cnt_az0_carry__2_n_3;
  wire cnt_az0_carry_i_1_n_0;
  wire cnt_az0_carry_i_2_n_0;
  wire cnt_az0_carry_i_3_n_0;
  wire cnt_az0_carry_i_4_n_0;
  wire cnt_az0_carry_i_5_n_0;
  wire cnt_az0_carry_n_0;
  wire cnt_az0_carry_n_1;
  wire cnt_az0_carry_n_2;
  wire cnt_az0_carry_n_3;
  wire \cnt_az[0]_i_1_n_0 ;
  wire \cnt_az[12]_i_2_n_0 ;
  wire \cnt_az[12]_i_3_n_0 ;
  wire \cnt_az[12]_i_4_n_0 ;
  wire \cnt_az[15]_i_1_n_0 ;
  wire \cnt_az[15]_i_3_n_0 ;
  wire \cnt_az[15]_i_4_n_0 ;
  wire \cnt_az[15]_i_5_n_0 ;
  wire \cnt_az[15]_i_6_n_0 ;
  wire interrupt_frame;
  wire interrupt_frame_i_10_n_0;
  wire interrupt_frame_i_11_n_0;
  wire interrupt_frame_i_12_n_0;
  wire interrupt_frame_i_13_n_0;
  wire interrupt_frame_i_14_n_0;
  wire interrupt_frame_i_15_n_0;
  wire interrupt_frame_i_16_n_0;
  wire interrupt_frame_i_17_n_0;
  wire interrupt_frame_i_18_n_0;
  wire interrupt_frame_i_19_n_0;
  wire interrupt_frame_i_1_n_0;
  wire interrupt_frame_i_20_n_0;
  wire interrupt_frame_i_21_n_0;
  wire interrupt_frame_i_22_n_0;
  wire interrupt_frame_i_23_n_0;
  wire interrupt_frame_i_2_n_0;
  wire interrupt_frame_i_3_n_0;
  wire interrupt_frame_i_4_n_0;
  wire interrupt_frame_i_5_n_0;
  wire interrupt_frame_i_6_n_0;
  wire interrupt_frame_i_7_n_0;
  wire interrupt_frame_i_8_n_0;
  wire interrupt_frame_i_9_n_0;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [15:1]p_1_in;
  wire reset_cnt_trig0;
  wire reset_cnt_trig_ila;
  wire [3:3]\NLW_cnt100_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:2]NLW_cnt_az0_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_cnt_az0_carry__2_O_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT1 #(
    .INIT(2'h1)) 
    allowed_clk_prev_i_1
       (.I0(allowed_clk),
        .O(allowed_clk_prev_i_1_n_0));
  FDRE allowed_clk_prev_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(allowed_clk_prev_i_1_n_0),
        .Q(allowed_clk_prev_reg_0),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    azimut_0_prev_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(azimut_0),
        .Q(azimut_0_prev),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt100[0]_i_1 
       (.I0(allowed_clk),
        .O(clear));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt100[0]_i_3 
       (.I0(cnt100_reg[0]),
        .O(\cnt100[0]_i_3_n_0 ));
  FDRE \cnt100_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[0]_i_2_n_7 ),
        .Q(cnt100_reg[0]),
        .R(clear));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \cnt100_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt100_reg[0]_i_2_n_0 ,\cnt100_reg[0]_i_2_n_1 ,\cnt100_reg[0]_i_2_n_2 ,\cnt100_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt100_reg[0]_i_2_n_4 ,\cnt100_reg[0]_i_2_n_5 ,\cnt100_reg[0]_i_2_n_6 ,\cnt100_reg[0]_i_2_n_7 }),
        .S({cnt100_reg[3:1],\cnt100[0]_i_3_n_0 }));
  FDRE \cnt100_reg[10] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[8]_i_1_n_5 ),
        .Q(cnt100_reg[10]),
        .R(clear));
  FDRE \cnt100_reg[11] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[8]_i_1_n_4 ),
        .Q(cnt100_reg[11]),
        .R(clear));
  FDRE \cnt100_reg[12] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[12]_i_1_n_7 ),
        .Q(cnt100_reg[12]),
        .R(clear));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \cnt100_reg[12]_i_1 
       (.CI(\cnt100_reg[8]_i_1_n_0 ),
        .CO({\cnt100_reg[12]_i_1_n_0 ,\cnt100_reg[12]_i_1_n_1 ,\cnt100_reg[12]_i_1_n_2 ,\cnt100_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[12]_i_1_n_4 ,\cnt100_reg[12]_i_1_n_5 ,\cnt100_reg[12]_i_1_n_6 ,\cnt100_reg[12]_i_1_n_7 }),
        .S(cnt100_reg[15:12]));
  FDRE \cnt100_reg[13] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[12]_i_1_n_6 ),
        .Q(cnt100_reg[13]),
        .R(clear));
  FDRE \cnt100_reg[14] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[12]_i_1_n_5 ),
        .Q(cnt100_reg[14]),
        .R(clear));
  FDRE \cnt100_reg[15] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[12]_i_1_n_4 ),
        .Q(cnt100_reg[15]),
        .R(clear));
  FDRE \cnt100_reg[16] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[16]_i_1_n_7 ),
        .Q(cnt100_reg[16]),
        .R(clear));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \cnt100_reg[16]_i_1 
       (.CI(\cnt100_reg[12]_i_1_n_0 ),
        .CO({\cnt100_reg[16]_i_1_n_0 ,\cnt100_reg[16]_i_1_n_1 ,\cnt100_reg[16]_i_1_n_2 ,\cnt100_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[16]_i_1_n_4 ,\cnt100_reg[16]_i_1_n_5 ,\cnt100_reg[16]_i_1_n_6 ,\cnt100_reg[16]_i_1_n_7 }),
        .S(cnt100_reg[19:16]));
  FDRE \cnt100_reg[17] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[16]_i_1_n_6 ),
        .Q(cnt100_reg[17]),
        .R(clear));
  FDRE \cnt100_reg[18] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[16]_i_1_n_5 ),
        .Q(cnt100_reg[18]),
        .R(clear));
  FDRE \cnt100_reg[19] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[16]_i_1_n_4 ),
        .Q(cnt100_reg[19]),
        .R(clear));
  FDRE \cnt100_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[0]_i_2_n_6 ),
        .Q(cnt100_reg[1]),
        .R(clear));
  FDRE \cnt100_reg[20] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[20]_i_1_n_7 ),
        .Q(cnt100_reg[20]),
        .R(clear));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \cnt100_reg[20]_i_1 
       (.CI(\cnt100_reg[16]_i_1_n_0 ),
        .CO({\cnt100_reg[20]_i_1_n_0 ,\cnt100_reg[20]_i_1_n_1 ,\cnt100_reg[20]_i_1_n_2 ,\cnt100_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[20]_i_1_n_4 ,\cnt100_reg[20]_i_1_n_5 ,\cnt100_reg[20]_i_1_n_6 ,\cnt100_reg[20]_i_1_n_7 }),
        .S(cnt100_reg[23:20]));
  FDRE \cnt100_reg[21] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[20]_i_1_n_6 ),
        .Q(cnt100_reg[21]),
        .R(clear));
  FDRE \cnt100_reg[22] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[20]_i_1_n_5 ),
        .Q(cnt100_reg[22]),
        .R(clear));
  FDRE \cnt100_reg[23] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[20]_i_1_n_4 ),
        .Q(cnt100_reg[23]),
        .R(clear));
  FDRE \cnt100_reg[24] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[24]_i_1_n_7 ),
        .Q(cnt100_reg[24]),
        .R(clear));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \cnt100_reg[24]_i_1 
       (.CI(\cnt100_reg[20]_i_1_n_0 ),
        .CO({\cnt100_reg[24]_i_1_n_0 ,\cnt100_reg[24]_i_1_n_1 ,\cnt100_reg[24]_i_1_n_2 ,\cnt100_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[24]_i_1_n_4 ,\cnt100_reg[24]_i_1_n_5 ,\cnt100_reg[24]_i_1_n_6 ,\cnt100_reg[24]_i_1_n_7 }),
        .S(cnt100_reg[27:24]));
  FDRE \cnt100_reg[25] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[24]_i_1_n_6 ),
        .Q(cnt100_reg[25]),
        .R(clear));
  FDRE \cnt100_reg[26] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[24]_i_1_n_5 ),
        .Q(cnt100_reg[26]),
        .R(clear));
  FDRE \cnt100_reg[27] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[24]_i_1_n_4 ),
        .Q(cnt100_reg[27]),
        .R(clear));
  FDRE \cnt100_reg[28] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[28]_i_1_n_7 ),
        .Q(cnt100_reg[28]),
        .R(clear));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \cnt100_reg[28]_i_1 
       (.CI(\cnt100_reg[24]_i_1_n_0 ),
        .CO({\NLW_cnt100_reg[28]_i_1_CO_UNCONNECTED [3],\cnt100_reg[28]_i_1_n_1 ,\cnt100_reg[28]_i_1_n_2 ,\cnt100_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[28]_i_1_n_4 ,\cnt100_reg[28]_i_1_n_5 ,\cnt100_reg[28]_i_1_n_6 ,\cnt100_reg[28]_i_1_n_7 }),
        .S(cnt100_reg[31:28]));
  FDRE \cnt100_reg[29] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[28]_i_1_n_6 ),
        .Q(cnt100_reg[29]),
        .R(clear));
  FDRE \cnt100_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[0]_i_2_n_5 ),
        .Q(cnt100_reg[2]),
        .R(clear));
  FDRE \cnt100_reg[30] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[28]_i_1_n_5 ),
        .Q(cnt100_reg[30]),
        .R(clear));
  FDRE \cnt100_reg[31] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[28]_i_1_n_4 ),
        .Q(cnt100_reg[31]),
        .R(clear));
  FDRE \cnt100_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[0]_i_2_n_4 ),
        .Q(cnt100_reg[3]),
        .R(clear));
  FDRE \cnt100_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[4]_i_1_n_7 ),
        .Q(cnt100_reg[4]),
        .R(clear));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \cnt100_reg[4]_i_1 
       (.CI(\cnt100_reg[0]_i_2_n_0 ),
        .CO({\cnt100_reg[4]_i_1_n_0 ,\cnt100_reg[4]_i_1_n_1 ,\cnt100_reg[4]_i_1_n_2 ,\cnt100_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[4]_i_1_n_4 ,\cnt100_reg[4]_i_1_n_5 ,\cnt100_reg[4]_i_1_n_6 ,\cnt100_reg[4]_i_1_n_7 }),
        .S(cnt100_reg[7:4]));
  FDRE \cnt100_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[4]_i_1_n_6 ),
        .Q(cnt100_reg[5]),
        .R(clear));
  FDRE \cnt100_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[4]_i_1_n_5 ),
        .Q(cnt100_reg[6]),
        .R(clear));
  FDRE \cnt100_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[4]_i_1_n_4 ),
        .Q(cnt100_reg[7]),
        .R(clear));
  FDRE \cnt100_reg[8] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[8]_i_1_n_7 ),
        .Q(cnt100_reg[8]),
        .R(clear));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \cnt100_reg[8]_i_1 
       (.CI(\cnt100_reg[4]_i_1_n_0 ),
        .CO({\cnt100_reg[8]_i_1_n_0 ,\cnt100_reg[8]_i_1_n_1 ,\cnt100_reg[8]_i_1_n_2 ,\cnt100_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[8]_i_1_n_4 ,\cnt100_reg[8]_i_1_n_5 ,\cnt100_reg[8]_i_1_n_6 ,\cnt100_reg[8]_i_1_n_7 }),
        .S(cnt100_reg[11:8]));
  FDRE \cnt100_reg[9] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[8]_i_1_n_6 ),
        .Q(cnt100_reg[9]),
        .R(clear));
  CARRY4 cnt_az0_carry
       (.CI(1'b0),
        .CO({cnt_az0_carry_n_0,cnt_az0_carry_n_1,cnt_az0_carry_n_2,cnt_az0_carry_n_3}),
        .CYINIT(cnt_az0_carry_i_1_n_0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(cnt_az0[4:1]),
        .S({cnt_az0_carry_i_2_n_0,cnt_az0_carry_i_3_n_0,cnt_az0_carry_i_4_n_0,cnt_az0_carry_i_5_n_0}));
  CARRY4 cnt_az0_carry__0
       (.CI(cnt_az0_carry_n_0),
        .CO({cnt_az0_carry__0_n_0,cnt_az0_carry__0_n_1,cnt_az0_carry__0_n_2,cnt_az0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(cnt_az0[8:5]),
        .S({cnt_az0_carry__0_i_1_n_0,cnt_az0_carry__0_i_2_n_0,cnt_az0_carry__0_i_3_n_0,cnt_az0_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry__0_i_1
       (.I0(Q[8]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry__0_i_2
       (.I0(Q[7]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry__0_i_3
       (.I0(Q[6]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry__0_i_4
       (.I0(Q[5]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry__0_i_4_n_0));
  CARRY4 cnt_az0_carry__1
       (.CI(cnt_az0_carry__0_n_0),
        .CO({cnt_az0_carry__1_n_0,cnt_az0_carry__1_n_1,cnt_az0_carry__1_n_2,cnt_az0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(cnt_az0[12:9]),
        .S({cnt_az0_carry__1_i_1_n_0,cnt_az0_carry__1_i_2_n_0,cnt_az0_carry__1_i_3_n_0,cnt_az0_carry__1_i_4_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry__1_i_1
       (.I0(Q[12]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry__1_i_2
       (.I0(Q[11]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry__1_i_3
       (.I0(Q[10]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry__1_i_4
       (.I0(Q[9]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry__1_i_4_n_0));
  CARRY4 cnt_az0_carry__2
       (.CI(cnt_az0_carry__1_n_0),
        .CO({NLW_cnt_az0_carry__2_CO_UNCONNECTED[3:2],cnt_az0_carry__2_n_2,cnt_az0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_cnt_az0_carry__2_O_UNCONNECTED[3],cnt_az0[15:13]}),
        .S({1'b0,cnt_az0_carry__2_i_1_n_0,cnt_az0_carry__2_i_2_n_0,cnt_az0_carry__2_i_3_n_0}));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry__2_i_1
       (.I0(Q[15]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry__2_i_2
       (.I0(Q[14]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry__2_i_3
       (.I0(Q[13]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry_i_1
       (.I0(Q[0]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry_i_2
       (.I0(Q[4]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry_i_3
       (.I0(Q[3]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry_i_4
       (.I0(Q[2]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    cnt_az0_carry_i_5
       (.I0(Q[1]),
        .I1(reset_cnt_trig_ila),
        .O(cnt_az0_carry_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h11E1)) 
    \cnt_az[0]_i_1 
       (.I0(allowed_clk_prev_reg_0),
        .I1(allowed_clk),
        .I2(Q[0]),
        .I3(reset_cnt_trig_ila),
        .O(\cnt_az[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \cnt_az[10]_i_1 
       (.I0(cnt_az0[10]),
        .I1(\cnt_az[15]_i_3_n_0 ),
        .I2(Q[10]),
        .I3(reset_cnt_trig_ila),
        .O(p_1_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h88B8)) 
    \cnt_az[11]_i_1 
       (.I0(cnt_az0[11]),
        .I1(\cnt_az[15]_i_3_n_0 ),
        .I2(Q[11]),
        .I3(reset_cnt_trig_ila),
        .O(p_1_in[11]));
  LUT6 #(
    .INIT(64'h00020002FFF20002)) 
    \cnt_az[12]_i_1 
       (.I0(cnt_az0[12]),
        .I1(\cnt_az[12]_i_2_n_0 ),
        .I2(allowed_clk),
        .I3(allowed_clk_prev_reg_0),
        .I4(Q[12]),
        .I5(reset_cnt_trig_ila),
        .O(p_1_in[12]));
  LUT6 #(
    .INIT(64'h0000000055005504)) 
    \cnt_az[12]_i_2 
       (.I0(\cnt_az[12]_i_3_n_0 ),
        .I1(\cnt_az[12]_i_4_n_0 ),
        .I2(Q[8]),
        .I3(reset_cnt_trig_ila),
        .I4(Q[9]),
        .I5(\cnt_az[15]_i_4_n_0 ),
        .O(\cnt_az[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00FF00FE)) 
    \cnt_az[12]_i_3 
       (.I0(Q[12]),
        .I1(Q[13]),
        .I2(Q[14]),
        .I3(reset_cnt_trig_ila),
        .I4(Q[15]),
        .O(\cnt_az[12]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \cnt_az[12]_i_4 
       (.I0(Q[11]),
        .I1(Q[10]),
        .O(\cnt_az[12]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \cnt_az[13]_i_1 
       (.I0(cnt_az0[13]),
        .I1(\cnt_az[15]_i_3_n_0 ),
        .I2(Q[13]),
        .I3(reset_cnt_trig_ila),
        .O(p_1_in[13]));
  LUT4 #(
    .INIT(16'h88B8)) 
    \cnt_az[14]_i_1 
       (.I0(cnt_az0[14]),
        .I1(\cnt_az[15]_i_3_n_0 ),
        .I2(Q[14]),
        .I3(reset_cnt_trig_ila),
        .O(p_1_in[14]));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_az[15]_i_1 
       (.I0(m00_axis_aresetn),
        .O(\cnt_az[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h88B8)) 
    \cnt_az[15]_i_2 
       (.I0(cnt_az0[15]),
        .I1(\cnt_az[15]_i_3_n_0 ),
        .I2(Q[15]),
        .I3(reset_cnt_trig_ila),
        .O(p_1_in[15]));
  LUT4 #(
    .INIT(16'h1011)) 
    \cnt_az[15]_i_3 
       (.I0(allowed_clk_prev_reg_0),
        .I1(allowed_clk),
        .I2(\cnt_az[15]_i_4_n_0 ),
        .I3(\cnt_az[15]_i_5_n_0 ),
        .O(\cnt_az[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFDFFFFFFFFFFFFF)) 
    \cnt_az[15]_i_4 
       (.I0(\cnt_az[15]_i_6_n_0 ),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(reset_cnt_trig_ila),
        .I4(Q[0]),
        .I5(Q[1]),
        .O(\cnt_az[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h00000000CCCCCCCD)) 
    \cnt_az[15]_i_5 
       (.I0(Q[9]),
        .I1(reset_cnt_trig_ila),
        .I2(Q[8]),
        .I3(Q[10]),
        .I4(Q[11]),
        .I5(\cnt_az[12]_i_3_n_0 ),
        .O(\cnt_az[15]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \cnt_az[15]_i_6 
       (.I0(Q[6]),
        .I1(Q[7]),
        .I2(Q[4]),
        .I3(Q[5]),
        .O(\cnt_az[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h00020002FFF20002)) 
    \cnt_az[1]_i_1 
       (.I0(cnt_az0[1]),
        .I1(\cnt_az[12]_i_2_n_0 ),
        .I2(allowed_clk),
        .I3(allowed_clk_prev_reg_0),
        .I4(Q[1]),
        .I5(reset_cnt_trig_ila),
        .O(p_1_in[1]));
  LUT6 #(
    .INIT(64'h00020002FFF20002)) 
    \cnt_az[2]_i_1 
       (.I0(cnt_az0[2]),
        .I1(\cnt_az[12]_i_2_n_0 ),
        .I2(allowed_clk),
        .I3(allowed_clk_prev_reg_0),
        .I4(Q[2]),
        .I5(reset_cnt_trig_ila),
        .O(p_1_in[2]));
  LUT4 #(
    .INIT(16'h88B8)) 
    \cnt_az[3]_i_1 
       (.I0(cnt_az0[3]),
        .I1(\cnt_az[15]_i_3_n_0 ),
        .I2(Q[3]),
        .I3(reset_cnt_trig_ila),
        .O(p_1_in[3]));
  LUT4 #(
    .INIT(16'h88B8)) 
    \cnt_az[4]_i_1 
       (.I0(cnt_az0[4]),
        .I1(\cnt_az[15]_i_3_n_0 ),
        .I2(Q[4]),
        .I3(reset_cnt_trig_ila),
        .O(p_1_in[4]));
  LUT4 #(
    .INIT(16'h88B8)) 
    \cnt_az[5]_i_1 
       (.I0(cnt_az0[5]),
        .I1(\cnt_az[15]_i_3_n_0 ),
        .I2(Q[5]),
        .I3(reset_cnt_trig_ila),
        .O(p_1_in[5]));
  LUT4 #(
    .INIT(16'h88B8)) 
    \cnt_az[6]_i_1 
       (.I0(cnt_az0[6]),
        .I1(\cnt_az[15]_i_3_n_0 ),
        .I2(Q[6]),
        .I3(reset_cnt_trig_ila),
        .O(p_1_in[6]));
  LUT4 #(
    .INIT(16'h88B8)) 
    \cnt_az[7]_i_1 
       (.I0(cnt_az0[7]),
        .I1(\cnt_az[15]_i_3_n_0 ),
        .I2(Q[7]),
        .I3(reset_cnt_trig_ila),
        .O(p_1_in[7]));
  LUT4 #(
    .INIT(16'h88B8)) 
    \cnt_az[8]_i_1 
       (.I0(cnt_az0[8]),
        .I1(\cnt_az[15]_i_3_n_0 ),
        .I2(Q[8]),
        .I3(reset_cnt_trig_ila),
        .O(p_1_in[8]));
  LUT6 #(
    .INIT(64'h00020002FFF20002)) 
    \cnt_az[9]_i_1 
       (.I0(cnt_az0[9]),
        .I1(\cnt_az[12]_i_2_n_0 ),
        .I2(allowed_clk),
        .I3(allowed_clk_prev_reg_0),
        .I4(Q[9]),
        .I5(reset_cnt_trig_ila),
        .O(p_1_in[9]));
  FDRE \cnt_az_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt_az[0]_i_1_n_0 ),
        .Q(Q[0]),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE \cnt_az_reg[10] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_1_in[10]),
        .Q(Q[10]),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE \cnt_az_reg[11] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_1_in[11]),
        .Q(Q[11]),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE \cnt_az_reg[12] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_1_in[12]),
        .Q(Q[12]),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE \cnt_az_reg[13] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_1_in[13]),
        .Q(Q[13]),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE \cnt_az_reg[14] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_1_in[14]),
        .Q(Q[14]),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE \cnt_az_reg[15] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_1_in[15]),
        .Q(Q[15]),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE \cnt_az_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_1_in[1]),
        .Q(Q[1]),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE \cnt_az_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_1_in[2]),
        .Q(Q[2]),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE \cnt_az_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_1_in[3]),
        .Q(Q[3]),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE \cnt_az_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_1_in[4]),
        .Q(Q[4]),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE \cnt_az_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_1_in[5]),
        .Q(Q[5]),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE \cnt_az_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_1_in[6]),
        .Q(Q[6]),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE \cnt_az_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_1_in[7]),
        .Q(Q[7]),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE \cnt_az_reg[8] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_1_in[8]),
        .Q(Q[8]),
        .R(\cnt_az[15]_i_1_n_0 ));
  FDRE \cnt_az_reg[9] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_1_in[9]),
        .Q(Q[9]),
        .R(\cnt_az[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hDDFF0D0000000000)) 
    interrupt_frame_i_1
       (.I0(interrupt_frame_i_2_n_0),
        .I1(interrupt_frame_i_3_n_0),
        .I2(interrupt_frame_i_4_n_0),
        .I3(interrupt_frame_i_5_n_0),
        .I4(interrupt_frame),
        .I5(m00_axis_aresetn),
        .O(interrupt_frame_i_1_n_0));
  LUT6 #(
    .INIT(64'h1111111011111111)) 
    interrupt_frame_i_10
       (.I0(interrupt_frame_i_20_n_0),
        .I1(interrupt_frame_i_11_n_0),
        .I2(cnt100_reg[6]),
        .I3(cnt100_reg[11]),
        .I4(cnt100_reg[7]),
        .I5(interrupt_frame_i_21_n_0),
        .O(interrupt_frame_i_10_n_0));
  LUT3 #(
    .INIT(8'h7F)) 
    interrupt_frame_i_11
       (.I0(cnt100_reg[14]),
        .I1(cnt100_reg[15]),
        .I2(cnt100_reg[12]),
        .O(interrupt_frame_i_11_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFF07FFFFFF)) 
    interrupt_frame_i_12
       (.I0(cnt100_reg[4]),
        .I1(cnt100_reg[3]),
        .I2(cnt100_reg[5]),
        .I3(cnt100_reg[7]),
        .I4(cnt100_reg[6]),
        .I5(interrupt_frame_i_22_n_0),
        .O(interrupt_frame_i_12_n_0));
  LUT6 #(
    .INIT(64'h0000000000000080)) 
    interrupt_frame_i_13
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(interrupt_frame_i_19_n_0),
        .I5(interrupt_frame_i_23_n_0),
        .O(interrupt_frame_i_13_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    interrupt_frame_i_14
       (.I0(cnt100_reg[19]),
        .I1(cnt100_reg[20]),
        .I2(cnt100_reg[24]),
        .I3(cnt100_reg[31]),
        .O(interrupt_frame_i_14_n_0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    interrupt_frame_i_15
       (.I0(\cnt_az[15]_i_6_n_0 ),
        .I1(Q[14]),
        .I2(Q[13]),
        .I3(Q[15]),
        .O(interrupt_frame_i_15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    interrupt_frame_i_16
       (.I0(cnt100_reg[22]),
        .I1(cnt100_reg[17]),
        .I2(cnt100_reg[21]),
        .I3(cnt100_reg[18]),
        .I4(interrupt_frame_i_17_n_0),
        .O(interrupt_frame_i_16_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    interrupt_frame_i_17
       (.I0(cnt100_reg[27]),
        .I1(cnt100_reg[28]),
        .I2(cnt100_reg[25]),
        .I3(cnt100_reg[26]),
        .O(interrupt_frame_i_17_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    interrupt_frame_i_18
       (.I0(cnt100_reg[18]),
        .I1(cnt100_reg[21]),
        .I2(cnt100_reg[17]),
        .I3(cnt100_reg[22]),
        .O(interrupt_frame_i_18_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    interrupt_frame_i_19
       (.I0(cnt100_reg[16]),
        .I1(cnt100_reg[23]),
        .I2(cnt100_reg[29]),
        .I3(cnt100_reg[30]),
        .O(interrupt_frame_i_19_n_0));
  LUT6 #(
    .INIT(64'h557755775577557F)) 
    interrupt_frame_i_2
       (.I0(interrupt_frame_i_6_n_0),
        .I1(cnt100_reg[7]),
        .I2(interrupt_frame_i_7_n_0),
        .I3(cnt100_reg[11]),
        .I4(cnt100_reg[6]),
        .I5(interrupt_frame_i_8_n_0),
        .O(interrupt_frame_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h1555)) 
    interrupt_frame_i_20
       (.I0(cnt100_reg[11]),
        .I1(cnt100_reg[10]),
        .I2(cnt100_reg[9]),
        .I3(cnt100_reg[8]),
        .O(interrupt_frame_i_20_n_0));
  LUT6 #(
    .INIT(64'h1FFFFFFFFFFFFFFF)) 
    interrupt_frame_i_21
       (.I0(cnt100_reg[1]),
        .I1(cnt100_reg[0]),
        .I2(cnt100_reg[5]),
        .I3(cnt100_reg[2]),
        .I4(cnt100_reg[4]),
        .I5(cnt100_reg[3]),
        .O(interrupt_frame_i_21_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    interrupt_frame_i_22
       (.I0(cnt100_reg[10]),
        .I1(cnt100_reg[9]),
        .O(interrupt_frame_i_22_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF80)) 
    interrupt_frame_i_23
       (.I0(cnt100_reg[15]),
        .I1(cnt100_reg[14]),
        .I2(cnt100_reg[13]),
        .I3(Q[12]),
        .I4(Q[9]),
        .I5(Q[8]),
        .O(interrupt_frame_i_23_n_0));
  LUT5 #(
    .INIT(32'h0000007F)) 
    interrupt_frame_i_3
       (.I0(cnt100_reg[13]),
        .I1(cnt100_reg[14]),
        .I2(cnt100_reg[15]),
        .I3(interrupt_frame_i_9_n_0),
        .I4(interrupt_frame_i_10_n_0),
        .O(interrupt_frame_i_3_n_0));
  LUT5 #(
    .INIT(32'hABABABAA)) 
    interrupt_frame_i_4
       (.I0(interrupt_frame_i_10_n_0),
        .I1(interrupt_frame_i_6_n_0),
        .I2(interrupt_frame_i_9_n_0),
        .I3(interrupt_frame_i_11_n_0),
        .I4(interrupt_frame_i_12_n_0),
        .O(interrupt_frame_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    interrupt_frame_i_5
       (.I0(interrupt_frame_i_13_n_0),
        .I1(Q[11]),
        .I2(Q[10]),
        .I3(interrupt_frame_i_14_n_0),
        .I4(interrupt_frame_i_15_n_0),
        .I5(interrupt_frame_i_16_n_0),
        .O(interrupt_frame_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h55554000)) 
    interrupt_frame_i_6
       (.I0(interrupt_frame_i_11_n_0),
        .I1(cnt100_reg[8]),
        .I2(cnt100_reg[9]),
        .I3(cnt100_reg[10]),
        .I4(cnt100_reg[11]),
        .O(interrupt_frame_i_6_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    interrupt_frame_i_7
       (.I0(cnt100_reg[5]),
        .I1(cnt100_reg[2]),
        .O(interrupt_frame_i_7_n_0));
  LUT5 #(
    .INIT(32'hAAAAAAA8)) 
    interrupt_frame_i_8
       (.I0(cnt100_reg[5]),
        .I1(cnt100_reg[4]),
        .I2(cnt100_reg[3]),
        .I3(cnt100_reg[0]),
        .I4(cnt100_reg[1]),
        .O(interrupt_frame_i_8_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    interrupt_frame_i_9
       (.I0(interrupt_frame_i_17_n_0),
        .I1(interrupt_frame_i_18_n_0),
        .I2(interrupt_frame_i_14_n_0),
        .I3(interrupt_frame_i_19_n_0),
        .O(interrupt_frame_i_9_n_0));
  FDRE interrupt_frame_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(interrupt_frame_i_1_n_0),
        .Q(interrupt_frame),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    reset_cnt_trig_i_1
       (.I0(azimut_0),
        .I1(azimut_0_prev),
        .O(reset_cnt_trig0));
  FDRE reset_cnt_trig_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(reset_cnt_trig0),
        .Q(reset_cnt_trig_ila),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_averageFFT_0_0,averageFFT_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "averageFFT_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s00_axis_tready,
    s00_axis_tdata,
    s00_axis_tstrb,
    s00_axis_tlast,
    s00_axis_tvalid,
    m00_axis_tvalid,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tready,
    allowed_clk,
    azimut_0,
    m00_axis_aresetn,
    azimut_ila,
    low_azimut_ila,
    m00_axis_aclk,
    clk_10MHz,
    interrupt_frame,
    reset_cnt_trig_ila,
    allowed_clk_prev_ila);
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TREADY" *) output s00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TDATA" *) input [63:0]s00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TSTRB" *) input [7:0]s00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TLAST" *) input s00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input s00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 allowed_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input allowed_clk;
  input azimut_0;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0, PortWidth 1" *) input m00_axis_aresetn;
  output [15:0]azimut_ila;
  output [15:0]low_azimut_ila;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_RESET m00_axis_aresetn, ASSOCIATED_BUSIF s00_axis:m00_axis, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input m00_axis_aclk;
  input clk_10MHz;
  (* X_INTERFACE_INFO = "xilinx.com:signal:interrupt:1.0 interrupt_frame INTERRUPT" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME interrupt_frame, SENSITIVITY LEVEL_HIGH, PortWidth 1" *) output interrupt_frame;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 reset_cnt_trig_ila RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME reset_cnt_trig_ila, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) output reset_cnt_trig_ila;
  output allowed_clk_prev_ila;

  wire \<const0> ;
  wire \<const1> ;
  wire allowed_clk;
  wire allowed_clk_prev_ila;
  wire azimut_0;
  wire clk_10MHz;
  wire interrupt_frame;
  wire [15:0]low_azimut_ila;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire m00_axis_tready;
  wire reset_cnt_trig_ila;

  assign azimut_ila[15] = \<const0> ;
  assign azimut_ila[14] = \<const0> ;
  assign azimut_ila[13] = \<const0> ;
  assign azimut_ila[12] = \<const0> ;
  assign azimut_ila[11] = \<const0> ;
  assign azimut_ila[10] = \<const0> ;
  assign azimut_ila[9] = \<const0> ;
  assign azimut_ila[8] = \<const0> ;
  assign azimut_ila[7] = \<const0> ;
  assign azimut_ila[6] = \<const0> ;
  assign azimut_ila[5] = \<const0> ;
  assign azimut_ila[4] = \<const0> ;
  assign azimut_ila[3] = \<const0> ;
  assign azimut_ila[2] = \<const0> ;
  assign azimut_ila[1] = \<const0> ;
  assign azimut_ila[0] = \<const0> ;
  assign m00_axis_tdata[31] = \<const0> ;
  assign m00_axis_tdata[30] = \<const0> ;
  assign m00_axis_tdata[29] = \<const0> ;
  assign m00_axis_tdata[28] = \<const0> ;
  assign m00_axis_tdata[27] = \<const0> ;
  assign m00_axis_tdata[26] = \<const0> ;
  assign m00_axis_tdata[25] = \<const0> ;
  assign m00_axis_tdata[24] = \<const0> ;
  assign m00_axis_tdata[23] = \<const0> ;
  assign m00_axis_tdata[22] = \<const0> ;
  assign m00_axis_tdata[21] = \<const0> ;
  assign m00_axis_tdata[20] = \<const0> ;
  assign m00_axis_tdata[19] = \<const0> ;
  assign m00_axis_tdata[18] = \<const0> ;
  assign m00_axis_tdata[17] = \<const0> ;
  assign m00_axis_tdata[16] = \<const0> ;
  assign m00_axis_tdata[15] = \<const0> ;
  assign m00_axis_tdata[14] = \<const0> ;
  assign m00_axis_tdata[13] = \<const0> ;
  assign m00_axis_tdata[12] = \<const0> ;
  assign m00_axis_tdata[11] = \<const0> ;
  assign m00_axis_tdata[10] = \<const0> ;
  assign m00_axis_tdata[9] = \<const0> ;
  assign m00_axis_tdata[8] = \<const0> ;
  assign m00_axis_tdata[7] = \<const0> ;
  assign m00_axis_tdata[6] = \<const0> ;
  assign m00_axis_tdata[5] = \<const0> ;
  assign m00_axis_tdata[4] = \<const0> ;
  assign m00_axis_tdata[3] = \<const0> ;
  assign m00_axis_tdata[2] = \<const0> ;
  assign m00_axis_tdata[1] = \<const0> ;
  assign m00_axis_tdata[0] = \<const0> ;
  assign m00_axis_tlast = \<const0> ;
  assign m00_axis_tstrb[3] = \<const1> ;
  assign m00_axis_tstrb[2] = \<const1> ;
  assign m00_axis_tstrb[1] = \<const1> ;
  assign m00_axis_tstrb[0] = \<const1> ;
  assign m00_axis_tvalid = \<const0> ;
  assign s00_axis_tready = m00_axis_tready;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0 inst
       (.Q(low_azimut_ila),
        .allowed_clk(allowed_clk),
        .allowed_clk_prev_reg_0(allowed_clk_prev_ila),
        .azimut_0(azimut_0),
        .clk_10MHz(clk_10MHz),
        .interrupt_frame(interrupt_frame),
        .m00_axis_aclk(m00_axis_aclk),
        .m00_axis_aresetn(m00_axis_aresetn),
        .reset_cnt_trig_ila(reset_cnt_trig_ila));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Tue Dec  7 22:06:46 2021
-- Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_averageFFT_0_0_sim_netlist.vhdl
-- Design      : design_1_averageFFT_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0 is
  port (
    allowed_clk_prev_reg_0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset_cnt_trig_ila : out STD_LOGIC;
    interrupt_frame : out STD_LOGIC;
    allowed_clk : in STD_LOGIC;
    clk_10MHz : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    azimut_0 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0 is
  signal \^q\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal allowed_clk_prev_i_1_n_0 : STD_LOGIC;
  signal \^allowed_clk_prev_reg_0\ : STD_LOGIC;
  signal azimut_0_prev : STD_LOGIC;
  signal clear : STD_LOGIC;
  signal \cnt100[0]_i_3_n_0\ : STD_LOGIC;
  signal cnt100_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \cnt100_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \cnt100_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \cnt100_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt100_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal cnt_az0 : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal \cnt_az0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \cnt_az0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \cnt_az0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \cnt_az0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \cnt_az0_carry__0_n_0\ : STD_LOGIC;
  signal \cnt_az0_carry__0_n_1\ : STD_LOGIC;
  signal \cnt_az0_carry__0_n_2\ : STD_LOGIC;
  signal \cnt_az0_carry__0_n_3\ : STD_LOGIC;
  signal \cnt_az0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \cnt_az0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \cnt_az0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \cnt_az0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \cnt_az0_carry__1_n_0\ : STD_LOGIC;
  signal \cnt_az0_carry__1_n_1\ : STD_LOGIC;
  signal \cnt_az0_carry__1_n_2\ : STD_LOGIC;
  signal \cnt_az0_carry__1_n_3\ : STD_LOGIC;
  signal \cnt_az0_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \cnt_az0_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \cnt_az0_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \cnt_az0_carry__2_n_2\ : STD_LOGIC;
  signal \cnt_az0_carry__2_n_3\ : STD_LOGIC;
  signal cnt_az0_carry_i_1_n_0 : STD_LOGIC;
  signal cnt_az0_carry_i_2_n_0 : STD_LOGIC;
  signal cnt_az0_carry_i_3_n_0 : STD_LOGIC;
  signal cnt_az0_carry_i_4_n_0 : STD_LOGIC;
  signal cnt_az0_carry_i_5_n_0 : STD_LOGIC;
  signal cnt_az0_carry_n_0 : STD_LOGIC;
  signal cnt_az0_carry_n_1 : STD_LOGIC;
  signal cnt_az0_carry_n_2 : STD_LOGIC;
  signal cnt_az0_carry_n_3 : STD_LOGIC;
  signal \cnt_az[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_az[12]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_az[12]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_az[12]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_az[15]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_az[15]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_az[15]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_az[15]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_az[15]_i_6_n_0\ : STD_LOGIC;
  signal \^interrupt_frame\ : STD_LOGIC;
  signal interrupt_frame_i_10_n_0 : STD_LOGIC;
  signal interrupt_frame_i_11_n_0 : STD_LOGIC;
  signal interrupt_frame_i_12_n_0 : STD_LOGIC;
  signal interrupt_frame_i_13_n_0 : STD_LOGIC;
  signal interrupt_frame_i_14_n_0 : STD_LOGIC;
  signal interrupt_frame_i_15_n_0 : STD_LOGIC;
  signal interrupt_frame_i_16_n_0 : STD_LOGIC;
  signal interrupt_frame_i_17_n_0 : STD_LOGIC;
  signal interrupt_frame_i_18_n_0 : STD_LOGIC;
  signal interrupt_frame_i_19_n_0 : STD_LOGIC;
  signal interrupt_frame_i_1_n_0 : STD_LOGIC;
  signal interrupt_frame_i_20_n_0 : STD_LOGIC;
  signal interrupt_frame_i_21_n_0 : STD_LOGIC;
  signal interrupt_frame_i_22_n_0 : STD_LOGIC;
  signal interrupt_frame_i_23_n_0 : STD_LOGIC;
  signal interrupt_frame_i_2_n_0 : STD_LOGIC;
  signal interrupt_frame_i_3_n_0 : STD_LOGIC;
  signal interrupt_frame_i_4_n_0 : STD_LOGIC;
  signal interrupt_frame_i_5_n_0 : STD_LOGIC;
  signal interrupt_frame_i_6_n_0 : STD_LOGIC;
  signal interrupt_frame_i_7_n_0 : STD_LOGIC;
  signal interrupt_frame_i_8_n_0 : STD_LOGIC;
  signal interrupt_frame_i_9_n_0 : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal reset_cnt_trig0 : STD_LOGIC;
  signal \^reset_cnt_trig_ila\ : STD_LOGIC;
  signal \NLW_cnt100_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_cnt_az0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_cnt_az0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of allowed_clk_prev_i_1 : label is "soft_lutpair2";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of \cnt100_reg[0]_i_2\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \cnt100_reg[12]_i_1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \cnt100_reg[16]_i_1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \cnt100_reg[20]_i_1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \cnt100_reg[24]_i_1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \cnt100_reg[28]_i_1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \cnt100_reg[4]_i_1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \cnt100_reg[8]_i_1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute SOFT_HLUTNM of \cnt_az[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cnt_az[11]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \cnt_az[12]_i_4\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of interrupt_frame_i_16 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of interrupt_frame_i_18 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of interrupt_frame_i_20 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of interrupt_frame_i_6 : label is "soft_lutpair0";
begin
  Q(15 downto 0) <= \^q\(15 downto 0);
  allowed_clk_prev_reg_0 <= \^allowed_clk_prev_reg_0\;
  interrupt_frame <= \^interrupt_frame\;
  reset_cnt_trig_ila <= \^reset_cnt_trig_ila\;
allowed_clk_prev_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => allowed_clk,
      O => allowed_clk_prev_i_1_n_0
    );
allowed_clk_prev_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => allowed_clk_prev_i_1_n_0,
      Q => \^allowed_clk_prev_reg_0\,
      R => \cnt_az[15]_i_1_n_0\
    );
azimut_0_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => azimut_0,
      Q => azimut_0_prev,
      R => '0'
    );
\cnt100[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => allowed_clk,
      O => clear
    );
\cnt100[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt100_reg(0),
      O => \cnt100[0]_i_3_n_0\
    );
\cnt100_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[0]_i_2_n_7\,
      Q => cnt100_reg(0),
      R => clear
    );
\cnt100_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt100_reg[0]_i_2_n_0\,
      CO(2) => \cnt100_reg[0]_i_2_n_1\,
      CO(1) => \cnt100_reg[0]_i_2_n_2\,
      CO(0) => \cnt100_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt100_reg[0]_i_2_n_4\,
      O(2) => \cnt100_reg[0]_i_2_n_5\,
      O(1) => \cnt100_reg[0]_i_2_n_6\,
      O(0) => \cnt100_reg[0]_i_2_n_7\,
      S(3 downto 1) => cnt100_reg(3 downto 1),
      S(0) => \cnt100[0]_i_3_n_0\
    );
\cnt100_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[8]_i_1_n_5\,
      Q => cnt100_reg(10),
      R => clear
    );
\cnt100_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[8]_i_1_n_4\,
      Q => cnt100_reg(11),
      R => clear
    );
\cnt100_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[12]_i_1_n_7\,
      Q => cnt100_reg(12),
      R => clear
    );
\cnt100_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[8]_i_1_n_0\,
      CO(3) => \cnt100_reg[12]_i_1_n_0\,
      CO(2) => \cnt100_reg[12]_i_1_n_1\,
      CO(1) => \cnt100_reg[12]_i_1_n_2\,
      CO(0) => \cnt100_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[12]_i_1_n_4\,
      O(2) => \cnt100_reg[12]_i_1_n_5\,
      O(1) => \cnt100_reg[12]_i_1_n_6\,
      O(0) => \cnt100_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(15 downto 12)
    );
\cnt100_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[12]_i_1_n_6\,
      Q => cnt100_reg(13),
      R => clear
    );
\cnt100_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[12]_i_1_n_5\,
      Q => cnt100_reg(14),
      R => clear
    );
\cnt100_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[12]_i_1_n_4\,
      Q => cnt100_reg(15),
      R => clear
    );
\cnt100_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[16]_i_1_n_7\,
      Q => cnt100_reg(16),
      R => clear
    );
\cnt100_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[12]_i_1_n_0\,
      CO(3) => \cnt100_reg[16]_i_1_n_0\,
      CO(2) => \cnt100_reg[16]_i_1_n_1\,
      CO(1) => \cnt100_reg[16]_i_1_n_2\,
      CO(0) => \cnt100_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[16]_i_1_n_4\,
      O(2) => \cnt100_reg[16]_i_1_n_5\,
      O(1) => \cnt100_reg[16]_i_1_n_6\,
      O(0) => \cnt100_reg[16]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(19 downto 16)
    );
\cnt100_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[16]_i_1_n_6\,
      Q => cnt100_reg(17),
      R => clear
    );
\cnt100_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[16]_i_1_n_5\,
      Q => cnt100_reg(18),
      R => clear
    );
\cnt100_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[16]_i_1_n_4\,
      Q => cnt100_reg(19),
      R => clear
    );
\cnt100_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[0]_i_2_n_6\,
      Q => cnt100_reg(1),
      R => clear
    );
\cnt100_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[20]_i_1_n_7\,
      Q => cnt100_reg(20),
      R => clear
    );
\cnt100_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[16]_i_1_n_0\,
      CO(3) => \cnt100_reg[20]_i_1_n_0\,
      CO(2) => \cnt100_reg[20]_i_1_n_1\,
      CO(1) => \cnt100_reg[20]_i_1_n_2\,
      CO(0) => \cnt100_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[20]_i_1_n_4\,
      O(2) => \cnt100_reg[20]_i_1_n_5\,
      O(1) => \cnt100_reg[20]_i_1_n_6\,
      O(0) => \cnt100_reg[20]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(23 downto 20)
    );
\cnt100_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[20]_i_1_n_6\,
      Q => cnt100_reg(21),
      R => clear
    );
\cnt100_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[20]_i_1_n_5\,
      Q => cnt100_reg(22),
      R => clear
    );
\cnt100_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[20]_i_1_n_4\,
      Q => cnt100_reg(23),
      R => clear
    );
\cnt100_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[24]_i_1_n_7\,
      Q => cnt100_reg(24),
      R => clear
    );
\cnt100_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[20]_i_1_n_0\,
      CO(3) => \cnt100_reg[24]_i_1_n_0\,
      CO(2) => \cnt100_reg[24]_i_1_n_1\,
      CO(1) => \cnt100_reg[24]_i_1_n_2\,
      CO(0) => \cnt100_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[24]_i_1_n_4\,
      O(2) => \cnt100_reg[24]_i_1_n_5\,
      O(1) => \cnt100_reg[24]_i_1_n_6\,
      O(0) => \cnt100_reg[24]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(27 downto 24)
    );
\cnt100_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[24]_i_1_n_6\,
      Q => cnt100_reg(25),
      R => clear
    );
\cnt100_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[24]_i_1_n_5\,
      Q => cnt100_reg(26),
      R => clear
    );
\cnt100_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[24]_i_1_n_4\,
      Q => cnt100_reg(27),
      R => clear
    );
\cnt100_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[28]_i_1_n_7\,
      Q => cnt100_reg(28),
      R => clear
    );
\cnt100_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[24]_i_1_n_0\,
      CO(3) => \NLW_cnt100_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt100_reg[28]_i_1_n_1\,
      CO(1) => \cnt100_reg[28]_i_1_n_2\,
      CO(0) => \cnt100_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[28]_i_1_n_4\,
      O(2) => \cnt100_reg[28]_i_1_n_5\,
      O(1) => \cnt100_reg[28]_i_1_n_6\,
      O(0) => \cnt100_reg[28]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(31 downto 28)
    );
\cnt100_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[28]_i_1_n_6\,
      Q => cnt100_reg(29),
      R => clear
    );
\cnt100_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[0]_i_2_n_5\,
      Q => cnt100_reg(2),
      R => clear
    );
\cnt100_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[28]_i_1_n_5\,
      Q => cnt100_reg(30),
      R => clear
    );
\cnt100_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[28]_i_1_n_4\,
      Q => cnt100_reg(31),
      R => clear
    );
\cnt100_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[0]_i_2_n_4\,
      Q => cnt100_reg(3),
      R => clear
    );
\cnt100_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[4]_i_1_n_7\,
      Q => cnt100_reg(4),
      R => clear
    );
\cnt100_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[0]_i_2_n_0\,
      CO(3) => \cnt100_reg[4]_i_1_n_0\,
      CO(2) => \cnt100_reg[4]_i_1_n_1\,
      CO(1) => \cnt100_reg[4]_i_1_n_2\,
      CO(0) => \cnt100_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[4]_i_1_n_4\,
      O(2) => \cnt100_reg[4]_i_1_n_5\,
      O(1) => \cnt100_reg[4]_i_1_n_6\,
      O(0) => \cnt100_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(7 downto 4)
    );
\cnt100_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[4]_i_1_n_6\,
      Q => cnt100_reg(5),
      R => clear
    );
\cnt100_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[4]_i_1_n_5\,
      Q => cnt100_reg(6),
      R => clear
    );
\cnt100_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[4]_i_1_n_4\,
      Q => cnt100_reg(7),
      R => clear
    );
\cnt100_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[8]_i_1_n_7\,
      Q => cnt100_reg(8),
      R => clear
    );
\cnt100_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt100_reg[4]_i_1_n_0\,
      CO(3) => \cnt100_reg[8]_i_1_n_0\,
      CO(2) => \cnt100_reg[8]_i_1_n_1\,
      CO(1) => \cnt100_reg[8]_i_1_n_2\,
      CO(0) => \cnt100_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt100_reg[8]_i_1_n_4\,
      O(2) => \cnt100_reg[8]_i_1_n_5\,
      O(1) => \cnt100_reg[8]_i_1_n_6\,
      O(0) => \cnt100_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt100_reg(11 downto 8)
    );
\cnt100_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt100_reg[8]_i_1_n_6\,
      Q => cnt100_reg(9),
      R => clear
    );
cnt_az0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cnt_az0_carry_n_0,
      CO(2) => cnt_az0_carry_n_1,
      CO(1) => cnt_az0_carry_n_2,
      CO(0) => cnt_az0_carry_n_3,
      CYINIT => cnt_az0_carry_i_1_n_0,
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => cnt_az0(4 downto 1),
      S(3) => cnt_az0_carry_i_2_n_0,
      S(2) => cnt_az0_carry_i_3_n_0,
      S(1) => cnt_az0_carry_i_4_n_0,
      S(0) => cnt_az0_carry_i_5_n_0
    );
\cnt_az0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => cnt_az0_carry_n_0,
      CO(3) => \cnt_az0_carry__0_n_0\,
      CO(2) => \cnt_az0_carry__0_n_1\,
      CO(1) => \cnt_az0_carry__0_n_2\,
      CO(0) => \cnt_az0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => cnt_az0(8 downto 5),
      S(3) => \cnt_az0_carry__0_i_1_n_0\,
      S(2) => \cnt_az0_carry__0_i_2_n_0\,
      S(1) => \cnt_az0_carry__0_i_3_n_0\,
      S(0) => \cnt_az0_carry__0_i_4_n_0\
    );
\cnt_az0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(8),
      I1 => \^reset_cnt_trig_ila\,
      O => \cnt_az0_carry__0_i_1_n_0\
    );
\cnt_az0_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(7),
      I1 => \^reset_cnt_trig_ila\,
      O => \cnt_az0_carry__0_i_2_n_0\
    );
\cnt_az0_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(6),
      I1 => \^reset_cnt_trig_ila\,
      O => \cnt_az0_carry__0_i_3_n_0\
    );
\cnt_az0_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^reset_cnt_trig_ila\,
      O => \cnt_az0_carry__0_i_4_n_0\
    );
\cnt_az0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_az0_carry__0_n_0\,
      CO(3) => \cnt_az0_carry__1_n_0\,
      CO(2) => \cnt_az0_carry__1_n_1\,
      CO(1) => \cnt_az0_carry__1_n_2\,
      CO(0) => \cnt_az0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => cnt_az0(12 downto 9),
      S(3) => \cnt_az0_carry__1_i_1_n_0\,
      S(2) => \cnt_az0_carry__1_i_2_n_0\,
      S(1) => \cnt_az0_carry__1_i_3_n_0\,
      S(0) => \cnt_az0_carry__1_i_4_n_0\
    );
\cnt_az0_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(12),
      I1 => \^reset_cnt_trig_ila\,
      O => \cnt_az0_carry__1_i_1_n_0\
    );
\cnt_az0_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(11),
      I1 => \^reset_cnt_trig_ila\,
      O => \cnt_az0_carry__1_i_2_n_0\
    );
\cnt_az0_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(10),
      I1 => \^reset_cnt_trig_ila\,
      O => \cnt_az0_carry__1_i_3_n_0\
    );
\cnt_az0_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(9),
      I1 => \^reset_cnt_trig_ila\,
      O => \cnt_az0_carry__1_i_4_n_0\
    );
\cnt_az0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_az0_carry__1_n_0\,
      CO(3 downto 2) => \NLW_cnt_az0_carry__2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \cnt_az0_carry__2_n_2\,
      CO(0) => \cnt_az0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_cnt_az0_carry__2_O_UNCONNECTED\(3),
      O(2 downto 0) => cnt_az0(15 downto 13),
      S(3) => '0',
      S(2) => \cnt_az0_carry__2_i_1_n_0\,
      S(1) => \cnt_az0_carry__2_i_2_n_0\,
      S(0) => \cnt_az0_carry__2_i_3_n_0\
    );
\cnt_az0_carry__2_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(15),
      I1 => \^reset_cnt_trig_ila\,
      O => \cnt_az0_carry__2_i_1_n_0\
    );
\cnt_az0_carry__2_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(14),
      I1 => \^reset_cnt_trig_ila\,
      O => \cnt_az0_carry__2_i_2_n_0\
    );
\cnt_az0_carry__2_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(13),
      I1 => \^reset_cnt_trig_ila\,
      O => \cnt_az0_carry__2_i_3_n_0\
    );
cnt_az0_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^reset_cnt_trig_ila\,
      O => cnt_az0_carry_i_1_n_0
    );
cnt_az0_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^reset_cnt_trig_ila\,
      O => cnt_az0_carry_i_2_n_0
    );
cnt_az0_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^reset_cnt_trig_ila\,
      O => cnt_az0_carry_i_3_n_0
    );
cnt_az0_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^reset_cnt_trig_ila\,
      O => cnt_az0_carry_i_4_n_0
    );
cnt_az0_carry_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^reset_cnt_trig_ila\,
      O => cnt_az0_carry_i_5_n_0
    );
\cnt_az[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"11E1"
    )
        port map (
      I0 => \^allowed_clk_prev_reg_0\,
      I1 => allowed_clk,
      I2 => \^q\(0),
      I3 => \^reset_cnt_trig_ila\,
      O => \cnt_az[0]_i_1_n_0\
    );
\cnt_az[10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => cnt_az0(10),
      I1 => \cnt_az[15]_i_3_n_0\,
      I2 => \^q\(10),
      I3 => \^reset_cnt_trig_ila\,
      O => p_1_in(10)
    );
\cnt_az[11]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => cnt_az0(11),
      I1 => \cnt_az[15]_i_3_n_0\,
      I2 => \^q\(11),
      I3 => \^reset_cnt_trig_ila\,
      O => p_1_in(11)
    );
\cnt_az[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00020002FFF20002"
    )
        port map (
      I0 => cnt_az0(12),
      I1 => \cnt_az[12]_i_2_n_0\,
      I2 => allowed_clk,
      I3 => \^allowed_clk_prev_reg_0\,
      I4 => \^q\(12),
      I5 => \^reset_cnt_trig_ila\,
      O => p_1_in(12)
    );
\cnt_az[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000055005504"
    )
        port map (
      I0 => \cnt_az[12]_i_3_n_0\,
      I1 => \cnt_az[12]_i_4_n_0\,
      I2 => \^q\(8),
      I3 => \^reset_cnt_trig_ila\,
      I4 => \^q\(9),
      I5 => \cnt_az[15]_i_4_n_0\,
      O => \cnt_az[12]_i_2_n_0\
    );
\cnt_az[12]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00FF00FE"
    )
        port map (
      I0 => \^q\(12),
      I1 => \^q\(13),
      I2 => \^q\(14),
      I3 => \^reset_cnt_trig_ila\,
      I4 => \^q\(15),
      O => \cnt_az[12]_i_3_n_0\
    );
\cnt_az[12]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(11),
      I1 => \^q\(10),
      O => \cnt_az[12]_i_4_n_0\
    );
\cnt_az[13]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => cnt_az0(13),
      I1 => \cnt_az[15]_i_3_n_0\,
      I2 => \^q\(13),
      I3 => \^reset_cnt_trig_ila\,
      O => p_1_in(13)
    );
\cnt_az[14]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => cnt_az0(14),
      I1 => \cnt_az[15]_i_3_n_0\,
      I2 => \^q\(14),
      I3 => \^reset_cnt_trig_ila\,
      O => p_1_in(14)
    );
\cnt_az[15]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => m00_axis_aresetn,
      O => \cnt_az[15]_i_1_n_0\
    );
\cnt_az[15]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => cnt_az0(15),
      I1 => \cnt_az[15]_i_3_n_0\,
      I2 => \^q\(15),
      I3 => \^reset_cnt_trig_ila\,
      O => p_1_in(15)
    );
\cnt_az[15]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1011"
    )
        port map (
      I0 => \^allowed_clk_prev_reg_0\,
      I1 => allowed_clk,
      I2 => \cnt_az[15]_i_4_n_0\,
      I3 => \cnt_az[15]_i_5_n_0\,
      O => \cnt_az[15]_i_3_n_0\
    );
\cnt_az[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFDFFFFFFFFFFFFF"
    )
        port map (
      I0 => \cnt_az[15]_i_6_n_0\,
      I1 => \^q\(3),
      I2 => \^q\(2),
      I3 => \^reset_cnt_trig_ila\,
      I4 => \^q\(0),
      I5 => \^q\(1),
      O => \cnt_az[15]_i_4_n_0\
    );
\cnt_az[15]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000CCCCCCCD"
    )
        port map (
      I0 => \^q\(9),
      I1 => \^reset_cnt_trig_ila\,
      I2 => \^q\(8),
      I3 => \^q\(10),
      I4 => \^q\(11),
      I5 => \cnt_az[12]_i_3_n_0\,
      O => \cnt_az[15]_i_5_n_0\
    );
\cnt_az[15]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \^q\(6),
      I1 => \^q\(7),
      I2 => \^q\(4),
      I3 => \^q\(5),
      O => \cnt_az[15]_i_6_n_0\
    );
\cnt_az[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00020002FFF20002"
    )
        port map (
      I0 => cnt_az0(1),
      I1 => \cnt_az[12]_i_2_n_0\,
      I2 => allowed_clk,
      I3 => \^allowed_clk_prev_reg_0\,
      I4 => \^q\(1),
      I5 => \^reset_cnt_trig_ila\,
      O => p_1_in(1)
    );
\cnt_az[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00020002FFF20002"
    )
        port map (
      I0 => cnt_az0(2),
      I1 => \cnt_az[12]_i_2_n_0\,
      I2 => allowed_clk,
      I3 => \^allowed_clk_prev_reg_0\,
      I4 => \^q\(2),
      I5 => \^reset_cnt_trig_ila\,
      O => p_1_in(2)
    );
\cnt_az[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => cnt_az0(3),
      I1 => \cnt_az[15]_i_3_n_0\,
      I2 => \^q\(3),
      I3 => \^reset_cnt_trig_ila\,
      O => p_1_in(3)
    );
\cnt_az[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => cnt_az0(4),
      I1 => \cnt_az[15]_i_3_n_0\,
      I2 => \^q\(4),
      I3 => \^reset_cnt_trig_ila\,
      O => p_1_in(4)
    );
\cnt_az[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => cnt_az0(5),
      I1 => \cnt_az[15]_i_3_n_0\,
      I2 => \^q\(5),
      I3 => \^reset_cnt_trig_ila\,
      O => p_1_in(5)
    );
\cnt_az[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => cnt_az0(6),
      I1 => \cnt_az[15]_i_3_n_0\,
      I2 => \^q\(6),
      I3 => \^reset_cnt_trig_ila\,
      O => p_1_in(6)
    );
\cnt_az[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => cnt_az0(7),
      I1 => \cnt_az[15]_i_3_n_0\,
      I2 => \^q\(7),
      I3 => \^reset_cnt_trig_ila\,
      O => p_1_in(7)
    );
\cnt_az[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"88B8"
    )
        port map (
      I0 => cnt_az0(8),
      I1 => \cnt_az[15]_i_3_n_0\,
      I2 => \^q\(8),
      I3 => \^reset_cnt_trig_ila\,
      O => p_1_in(8)
    );
\cnt_az[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00020002FFF20002"
    )
        port map (
      I0 => cnt_az0(9),
      I1 => \cnt_az[12]_i_2_n_0\,
      I2 => allowed_clk,
      I3 => \^allowed_clk_prev_reg_0\,
      I4 => \^q\(9),
      I5 => \^reset_cnt_trig_ila\,
      O => p_1_in(9)
    );
\cnt_az_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => \cnt_az[0]_i_1_n_0\,
      Q => \^q\(0),
      R => \cnt_az[15]_i_1_n_0\
    );
\cnt_az_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_1_in(10),
      Q => \^q\(10),
      R => \cnt_az[15]_i_1_n_0\
    );
\cnt_az_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_1_in(11),
      Q => \^q\(11),
      R => \cnt_az[15]_i_1_n_0\
    );
\cnt_az_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_1_in(12),
      Q => \^q\(12),
      R => \cnt_az[15]_i_1_n_0\
    );
\cnt_az_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_1_in(13),
      Q => \^q\(13),
      R => \cnt_az[15]_i_1_n_0\
    );
\cnt_az_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_1_in(14),
      Q => \^q\(14),
      R => \cnt_az[15]_i_1_n_0\
    );
\cnt_az_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_1_in(15),
      Q => \^q\(15),
      R => \cnt_az[15]_i_1_n_0\
    );
\cnt_az_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_1_in(1),
      Q => \^q\(1),
      R => \cnt_az[15]_i_1_n_0\
    );
\cnt_az_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_1_in(2),
      Q => \^q\(2),
      R => \cnt_az[15]_i_1_n_0\
    );
\cnt_az_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_1_in(3),
      Q => \^q\(3),
      R => \cnt_az[15]_i_1_n_0\
    );
\cnt_az_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_1_in(4),
      Q => \^q\(4),
      R => \cnt_az[15]_i_1_n_0\
    );
\cnt_az_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_1_in(5),
      Q => \^q\(5),
      R => \cnt_az[15]_i_1_n_0\
    );
\cnt_az_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_1_in(6),
      Q => \^q\(6),
      R => \cnt_az[15]_i_1_n_0\
    );
\cnt_az_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_1_in(7),
      Q => \^q\(7),
      R => \cnt_az[15]_i_1_n_0\
    );
\cnt_az_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_1_in(8),
      Q => \^q\(8),
      R => \cnt_az[15]_i_1_n_0\
    );
\cnt_az_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_1_in(9),
      Q => \^q\(9),
      R => \cnt_az[15]_i_1_n_0\
    );
interrupt_frame_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"DDFF0D0000000000"
    )
        port map (
      I0 => interrupt_frame_i_2_n_0,
      I1 => interrupt_frame_i_3_n_0,
      I2 => interrupt_frame_i_4_n_0,
      I3 => interrupt_frame_i_5_n_0,
      I4 => \^interrupt_frame\,
      I5 => m00_axis_aresetn,
      O => interrupt_frame_i_1_n_0
    );
interrupt_frame_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1111111011111111"
    )
        port map (
      I0 => interrupt_frame_i_20_n_0,
      I1 => interrupt_frame_i_11_n_0,
      I2 => cnt100_reg(6),
      I3 => cnt100_reg(11),
      I4 => cnt100_reg(7),
      I5 => interrupt_frame_i_21_n_0,
      O => interrupt_frame_i_10_n_0
    );
interrupt_frame_i_11: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => cnt100_reg(14),
      I1 => cnt100_reg(15),
      I2 => cnt100_reg(12),
      O => interrupt_frame_i_11_n_0
    );
interrupt_frame_i_12: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF07FFFFFF"
    )
        port map (
      I0 => cnt100_reg(4),
      I1 => cnt100_reg(3),
      I2 => cnt100_reg(5),
      I3 => cnt100_reg(7),
      I4 => cnt100_reg(6),
      I5 => interrupt_frame_i_22_n_0,
      O => interrupt_frame_i_12_n_0
    );
interrupt_frame_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000080"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^q\(2),
      I3 => \^q\(3),
      I4 => interrupt_frame_i_19_n_0,
      I5 => interrupt_frame_i_23_n_0,
      O => interrupt_frame_i_13_n_0
    );
interrupt_frame_i_14: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt100_reg(19),
      I1 => cnt100_reg(20),
      I2 => cnt100_reg(24),
      I3 => cnt100_reg(31),
      O => interrupt_frame_i_14_n_0
    );
interrupt_frame_i_15: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => \cnt_az[15]_i_6_n_0\,
      I1 => \^q\(14),
      I2 => \^q\(13),
      I3 => \^q\(15),
      O => interrupt_frame_i_15_n_0
    );
interrupt_frame_i_16: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => cnt100_reg(22),
      I1 => cnt100_reg(17),
      I2 => cnt100_reg(21),
      I3 => cnt100_reg(18),
      I4 => interrupt_frame_i_17_n_0,
      O => interrupt_frame_i_16_n_0
    );
interrupt_frame_i_17: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt100_reg(27),
      I1 => cnt100_reg(28),
      I2 => cnt100_reg(25),
      I3 => cnt100_reg(26),
      O => interrupt_frame_i_17_n_0
    );
interrupt_frame_i_18: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt100_reg(18),
      I1 => cnt100_reg(21),
      I2 => cnt100_reg(17),
      I3 => cnt100_reg(22),
      O => interrupt_frame_i_18_n_0
    );
interrupt_frame_i_19: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt100_reg(16),
      I1 => cnt100_reg(23),
      I2 => cnt100_reg(29),
      I3 => cnt100_reg(30),
      O => interrupt_frame_i_19_n_0
    );
interrupt_frame_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"557755775577557F"
    )
        port map (
      I0 => interrupt_frame_i_6_n_0,
      I1 => cnt100_reg(7),
      I2 => interrupt_frame_i_7_n_0,
      I3 => cnt100_reg(11),
      I4 => cnt100_reg(6),
      I5 => interrupt_frame_i_8_n_0,
      O => interrupt_frame_i_2_n_0
    );
interrupt_frame_i_20: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1555"
    )
        port map (
      I0 => cnt100_reg(11),
      I1 => cnt100_reg(10),
      I2 => cnt100_reg(9),
      I3 => cnt100_reg(8),
      O => interrupt_frame_i_20_n_0
    );
interrupt_frame_i_21: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => cnt100_reg(1),
      I1 => cnt100_reg(0),
      I2 => cnt100_reg(5),
      I3 => cnt100_reg(2),
      I4 => cnt100_reg(4),
      I5 => cnt100_reg(3),
      O => interrupt_frame_i_21_n_0
    );
interrupt_frame_i_22: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => cnt100_reg(10),
      I1 => cnt100_reg(9),
      O => interrupt_frame_i_22_n_0
    );
interrupt_frame_i_23: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFF80"
    )
        port map (
      I0 => cnt100_reg(15),
      I1 => cnt100_reg(14),
      I2 => cnt100_reg(13),
      I3 => \^q\(12),
      I4 => \^q\(9),
      I5 => \^q\(8),
      O => interrupt_frame_i_23_n_0
    );
interrupt_frame_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000007F"
    )
        port map (
      I0 => cnt100_reg(13),
      I1 => cnt100_reg(14),
      I2 => cnt100_reg(15),
      I3 => interrupt_frame_i_9_n_0,
      I4 => interrupt_frame_i_10_n_0,
      O => interrupt_frame_i_3_n_0
    );
interrupt_frame_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"ABABABAA"
    )
        port map (
      I0 => interrupt_frame_i_10_n_0,
      I1 => interrupt_frame_i_6_n_0,
      I2 => interrupt_frame_i_9_n_0,
      I3 => interrupt_frame_i_11_n_0,
      I4 => interrupt_frame_i_12_n_0,
      O => interrupt_frame_i_4_n_0
    );
interrupt_frame_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => interrupt_frame_i_13_n_0,
      I1 => \^q\(11),
      I2 => \^q\(10),
      I3 => interrupt_frame_i_14_n_0,
      I4 => interrupt_frame_i_15_n_0,
      I5 => interrupt_frame_i_16_n_0,
      O => interrupt_frame_i_5_n_0
    );
interrupt_frame_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55554000"
    )
        port map (
      I0 => interrupt_frame_i_11_n_0,
      I1 => cnt100_reg(8),
      I2 => cnt100_reg(9),
      I3 => cnt100_reg(10),
      I4 => cnt100_reg(11),
      O => interrupt_frame_i_6_n_0
    );
interrupt_frame_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => cnt100_reg(5),
      I1 => cnt100_reg(2),
      O => interrupt_frame_i_7_n_0
    );
interrupt_frame_i_8: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAAAAA8"
    )
        port map (
      I0 => cnt100_reg(5),
      I1 => cnt100_reg(4),
      I2 => cnt100_reg(3),
      I3 => cnt100_reg(0),
      I4 => cnt100_reg(1),
      O => interrupt_frame_i_8_n_0
    );
interrupt_frame_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => interrupt_frame_i_17_n_0,
      I1 => interrupt_frame_i_18_n_0,
      I2 => interrupt_frame_i_14_n_0,
      I3 => interrupt_frame_i_19_n_0,
      O => interrupt_frame_i_9_n_0
    );
interrupt_frame_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => interrupt_frame_i_1_n_0,
      Q => \^interrupt_frame\,
      R => '0'
    );
reset_cnt_trig_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => azimut_0,
      I1 => azimut_0_prev,
      O => reset_cnt_trig0
    );
reset_cnt_trig_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => reset_cnt_trig0,
      Q => \^reset_cnt_trig_ila\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    s00_axis_tready : out STD_LOGIC;
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s00_axis_tstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s00_axis_tlast : in STD_LOGIC;
    s00_axis_tvalid : in STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    allowed_clk : in STD_LOGIC;
    azimut_0 : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    azimut_ila : out STD_LOGIC_VECTOR ( 15 downto 0 );
    low_azimut_ila : out STD_LOGIC_VECTOR ( 15 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    clk_10MHz : in STD_LOGIC;
    interrupt_frame : out STD_LOGIC;
    reset_cnt_trig_ila : out STD_LOGIC;
    allowed_clk_prev_ila : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_averageFFT_0_0,averageFFT_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "averageFFT_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^m00_axis_tready\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of allowed_clk : signal is "xilinx.com:signal:clock:1.0 allowed_clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of allowed_clk : signal is "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of interrupt_frame : signal is "xilinx.com:signal:interrupt:1.0 interrupt_frame INTERRUPT";
  attribute X_INTERFACE_PARAMETER of interrupt_frame : signal is "XIL_INTERFACENAME interrupt_frame, SENSITIVITY LEVEL_HIGH, PortWidth 1";
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK";
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_RESET m00_axis_aresetn, ASSOCIATED_BUSIF s00_axis:m00_axis, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0, PortWidth 1";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 m00_axis TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 m00_axis TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME m00_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 m00_axis TVALID";
  attribute X_INTERFACE_INFO of reset_cnt_trig_ila : signal is "xilinx.com:signal:reset:1.0 reset_cnt_trig_ila RST";
  attribute X_INTERFACE_PARAMETER of reset_cnt_trig_ila : signal is "XIL_INTERFACENAME reset_cnt_trig_ila, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 s00_axis TLAST";
  attribute X_INTERFACE_INFO of s00_axis_tready : signal is "xilinx.com:interface:axis:1.0 s00_axis TREADY";
  attribute X_INTERFACE_INFO of s00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 s00_axis TVALID";
  attribute X_INTERFACE_PARAMETER of s00_axis_tvalid : signal is "XIL_INTERFACENAME s00_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 m00_axis TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 m00_axis TSTRB";
  attribute X_INTERFACE_INFO of s00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 s00_axis TDATA";
  attribute X_INTERFACE_INFO of s00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 s00_axis TSTRB";
begin
  \^m00_axis_tready\ <= m00_axis_tready;
  azimut_ila(15) <= \<const0>\;
  azimut_ila(14) <= \<const0>\;
  azimut_ila(13) <= \<const0>\;
  azimut_ila(12) <= \<const0>\;
  azimut_ila(11) <= \<const0>\;
  azimut_ila(10) <= \<const0>\;
  azimut_ila(9) <= \<const0>\;
  azimut_ila(8) <= \<const0>\;
  azimut_ila(7) <= \<const0>\;
  azimut_ila(6) <= \<const0>\;
  azimut_ila(5) <= \<const0>\;
  azimut_ila(4) <= \<const0>\;
  azimut_ila(3) <= \<const0>\;
  azimut_ila(2) <= \<const0>\;
  azimut_ila(1) <= \<const0>\;
  azimut_ila(0) <= \<const0>\;
  m00_axis_tdata(31) <= \<const0>\;
  m00_axis_tdata(30) <= \<const0>\;
  m00_axis_tdata(29) <= \<const0>\;
  m00_axis_tdata(28) <= \<const0>\;
  m00_axis_tdata(27) <= \<const0>\;
  m00_axis_tdata(26) <= \<const0>\;
  m00_axis_tdata(25) <= \<const0>\;
  m00_axis_tdata(24) <= \<const0>\;
  m00_axis_tdata(23) <= \<const0>\;
  m00_axis_tdata(22) <= \<const0>\;
  m00_axis_tdata(21) <= \<const0>\;
  m00_axis_tdata(20) <= \<const0>\;
  m00_axis_tdata(19) <= \<const0>\;
  m00_axis_tdata(18) <= \<const0>\;
  m00_axis_tdata(17) <= \<const0>\;
  m00_axis_tdata(16) <= \<const0>\;
  m00_axis_tdata(15) <= \<const0>\;
  m00_axis_tdata(14) <= \<const0>\;
  m00_axis_tdata(13) <= \<const0>\;
  m00_axis_tdata(12) <= \<const0>\;
  m00_axis_tdata(11) <= \<const0>\;
  m00_axis_tdata(10) <= \<const0>\;
  m00_axis_tdata(9) <= \<const0>\;
  m00_axis_tdata(8) <= \<const0>\;
  m00_axis_tdata(7) <= \<const0>\;
  m00_axis_tdata(6) <= \<const0>\;
  m00_axis_tdata(5) <= \<const0>\;
  m00_axis_tdata(4) <= \<const0>\;
  m00_axis_tdata(3) <= \<const0>\;
  m00_axis_tdata(2) <= \<const0>\;
  m00_axis_tdata(1) <= \<const0>\;
  m00_axis_tdata(0) <= \<const0>\;
  m00_axis_tlast <= \<const0>\;
  m00_axis_tstrb(3) <= \<const1>\;
  m00_axis_tstrb(2) <= \<const1>\;
  m00_axis_tstrb(1) <= \<const1>\;
  m00_axis_tstrb(0) <= \<const1>\;
  m00_axis_tvalid <= \<const0>\;
  s00_axis_tready <= \^m00_axis_tready\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0
     port map (
      Q(15 downto 0) => low_azimut_ila(15 downto 0),
      allowed_clk => allowed_clk,
      allowed_clk_prev_reg_0 => allowed_clk_prev_ila,
      azimut_0 => azimut_0,
      clk_10MHz => clk_10MHz,
      interrupt_frame => interrupt_frame,
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      reset_cnt_trig_ila => reset_cnt_trig_ila
    );
end STRUCTURE;

// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Wed Dec  8 15:09:24 2021
// Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_AD9650_0_0_sim_netlist.v
// Design      : design_2_AD9650_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v1_0_S00_AXI
   (s00_axi_wready,
    clear,
    s00_axi_awready,
    s00_axi_arready,
    s00_axi_bvalid,
    s00_axi_rvalid,
    \slv_reg0_reg[20]_0 ,
    \slv_reg0_reg[16]_0 ,
    \slv_reg0_reg[11]_0 ,
    \cnt_reg[2] ,
    \slv_reg1_reg[0]_0 ,
    \slv_reg3_reg[16]_0 ,
    \slv_reg0_reg[12]_0 ,
    \slv_reg2_reg[1]_0 ,
    s00_axi_rdata,
    s00_axi_aclk,
    s00_axi_aresetn,
    Q,
    DATA_TX_serial_r_i_3,
    DATA_RX_r,
    data4,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_bready,
    s00_axi_arvalid,
    s00_axi_rready,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wstrb);
  output s00_axi_wready;
  output clear;
  output s00_axi_awready;
  output s00_axi_arready;
  output s00_axi_bvalid;
  output s00_axi_rvalid;
  output \slv_reg0_reg[20]_0 ;
  output \slv_reg0_reg[16]_0 ;
  output \slv_reg0_reg[11]_0 ;
  output \cnt_reg[2] ;
  output [0:0]\slv_reg1_reg[0]_0 ;
  output [0:0]\slv_reg3_reg[16]_0 ;
  output [0:0]\slv_reg0_reg[12]_0 ;
  output [1:0]\slv_reg2_reg[1]_0 ;
  output [31:0]s00_axi_rdata;
  input s00_axi_aclk;
  input s00_axi_aresetn;
  input [1:0]Q;
  input DATA_TX_serial_r_i_3;
  input [7:0]DATA_RX_r;
  input [0:0]data4;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input s00_axi_bready;
  input s00_axi_arvalid;
  input s00_axi_rready;
  input [2:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;

  wire [7:0]DATA_RX_r;
  wire [7:0]DATA_TX;
  wire DATA_TX_serial_r_i_17_n_0;
  wire DATA_TX_serial_r_i_18_n_0;
  wire DATA_TX_serial_r_i_3;
  wire [1:0]Q;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire [4:2]axi_araddr;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire \axi_araddr[4]_i_1_n_0 ;
  wire axi_arready0;
  wire [4:2]axi_awaddr;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire \axi_awaddr[4]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[31]_i_2_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire clear;
  wire \cnt_reg[2] ;
  wire [0:0]data4;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire \slv_reg0_reg[11]_0 ;
  wire [0:0]\slv_reg0_reg[12]_0 ;
  wire \slv_reg0_reg[16]_0 ;
  wire \slv_reg0_reg[20]_0 ;
  wire \slv_reg0_reg_n_0_[0] ;
  wire \slv_reg0_reg_n_0_[10] ;
  wire \slv_reg0_reg_n_0_[11] ;
  wire \slv_reg0_reg_n_0_[1] ;
  wire \slv_reg0_reg_n_0_[21] ;
  wire \slv_reg0_reg_n_0_[22] ;
  wire \slv_reg0_reg_n_0_[23] ;
  wire \slv_reg0_reg_n_0_[24] ;
  wire \slv_reg0_reg_n_0_[25] ;
  wire \slv_reg0_reg_n_0_[26] ;
  wire \slv_reg0_reg_n_0_[27] ;
  wire \slv_reg0_reg_n_0_[28] ;
  wire \slv_reg0_reg_n_0_[29] ;
  wire \slv_reg0_reg_n_0_[2] ;
  wire \slv_reg0_reg_n_0_[30] ;
  wire \slv_reg0_reg_n_0_[31] ;
  wire \slv_reg0_reg_n_0_[3] ;
  wire \slv_reg0_reg_n_0_[4] ;
  wire \slv_reg0_reg_n_0_[5] ;
  wire \slv_reg0_reg_n_0_[6] ;
  wire \slv_reg0_reg_n_0_[7] ;
  wire \slv_reg0_reg_n_0_[8] ;
  wire \slv_reg0_reg_n_0_[9] ;
  wire [31:1]slv_reg1;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [0:0]\slv_reg1_reg[0]_0 ;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[1]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire [1:0]\slv_reg2_reg[1]_0 ;
  wire \slv_reg2_reg_n_0_[10] ;
  wire \slv_reg2_reg_n_0_[11] ;
  wire \slv_reg2_reg_n_0_[12] ;
  wire \slv_reg2_reg_n_0_[13] ;
  wire \slv_reg2_reg_n_0_[14] ;
  wire \slv_reg2_reg_n_0_[15] ;
  wire \slv_reg2_reg_n_0_[16] ;
  wire \slv_reg2_reg_n_0_[17] ;
  wire \slv_reg2_reg_n_0_[18] ;
  wire \slv_reg2_reg_n_0_[19] ;
  wire \slv_reg2_reg_n_0_[20] ;
  wire \slv_reg2_reg_n_0_[21] ;
  wire \slv_reg2_reg_n_0_[22] ;
  wire \slv_reg2_reg_n_0_[23] ;
  wire \slv_reg2_reg_n_0_[24] ;
  wire \slv_reg2_reg_n_0_[25] ;
  wire \slv_reg2_reg_n_0_[26] ;
  wire \slv_reg2_reg_n_0_[27] ;
  wire \slv_reg2_reg_n_0_[28] ;
  wire \slv_reg2_reg_n_0_[29] ;
  wire \slv_reg2_reg_n_0_[2] ;
  wire \slv_reg2_reg_n_0_[30] ;
  wire \slv_reg2_reg_n_0_[31] ;
  wire \slv_reg2_reg_n_0_[3] ;
  wire \slv_reg2_reg_n_0_[4] ;
  wire \slv_reg2_reg_n_0_[5] ;
  wire \slv_reg2_reg_n_0_[6] ;
  wire \slv_reg2_reg_n_0_[7] ;
  wire \slv_reg2_reg_n_0_[8] ;
  wire \slv_reg2_reg_n_0_[9] ;
  wire \slv_reg3[15]_i_1_n_0 ;
  wire \slv_reg3[23]_i_1_n_0 ;
  wire \slv_reg3[31]_i_1_n_0 ;
  wire \slv_reg3[7]_i_1_n_0 ;
  wire [0:0]\slv_reg3_reg[16]_0 ;
  wire \slv_reg3_reg_n_0_[0] ;
  wire \slv_reg3_reg_n_0_[10] ;
  wire \slv_reg3_reg_n_0_[11] ;
  wire \slv_reg3_reg_n_0_[12] ;
  wire \slv_reg3_reg_n_0_[13] ;
  wire \slv_reg3_reg_n_0_[14] ;
  wire \slv_reg3_reg_n_0_[15] ;
  wire \slv_reg3_reg_n_0_[17] ;
  wire \slv_reg3_reg_n_0_[18] ;
  wire \slv_reg3_reg_n_0_[19] ;
  wire \slv_reg3_reg_n_0_[1] ;
  wire \slv_reg3_reg_n_0_[20] ;
  wire \slv_reg3_reg_n_0_[21] ;
  wire \slv_reg3_reg_n_0_[22] ;
  wire \slv_reg3_reg_n_0_[23] ;
  wire \slv_reg3_reg_n_0_[24] ;
  wire \slv_reg3_reg_n_0_[25] ;
  wire \slv_reg3_reg_n_0_[26] ;
  wire \slv_reg3_reg_n_0_[27] ;
  wire \slv_reg3_reg_n_0_[28] ;
  wire \slv_reg3_reg_n_0_[29] ;
  wire \slv_reg3_reg_n_0_[2] ;
  wire \slv_reg3_reg_n_0_[30] ;
  wire \slv_reg3_reg_n_0_[31] ;
  wire \slv_reg3_reg_n_0_[3] ;
  wire \slv_reg3_reg_n_0_[4] ;
  wire \slv_reg3_reg_n_0_[5] ;
  wire \slv_reg3_reg_n_0_[6] ;
  wire \slv_reg3_reg_n_0_[7] ;
  wire \slv_reg3_reg_n_0_[8] ;
  wire \slv_reg3_reg_n_0_[9] ;
  wire slv_reg_rden__0;
  wire slv_reg_wren__0;

  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    DATA_TX_serial_r_i_13
       (.I0(DATA_TX[3]),
        .I1(DATA_TX[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(DATA_TX[1]),
        .I5(DATA_TX[0]),
        .O(\slv_reg0_reg[16]_0 ));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    DATA_TX_serial_r_i_14
       (.I0(DATA_TX[7]),
        .I1(DATA_TX[6]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(DATA_TX[5]),
        .I5(DATA_TX[4]),
        .O(\slv_reg0_reg[20]_0 ));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    DATA_TX_serial_r_i_15
       (.I0(\slv_reg0_reg_n_0_[11] ),
        .I1(\slv_reg0_reg_n_0_[10] ),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(\slv_reg0_reg_n_0_[9] ),
        .I5(\slv_reg0_reg_n_0_[8] ),
        .O(\slv_reg0_reg[11]_0 ));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    DATA_TX_serial_r_i_17
       (.I0(\slv_reg0_reg_n_0_[3] ),
        .I1(\slv_reg0_reg_n_0_[2] ),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(\slv_reg0_reg_n_0_[1] ),
        .I5(\slv_reg0_reg_n_0_[0] ),
        .O(DATA_TX_serial_r_i_17_n_0));
  LUT6 #(
    .INIT(64'hFACF0ACFFAC00AC0)) 
    DATA_TX_serial_r_i_18
       (.I0(\slv_reg0_reg_n_0_[7] ),
        .I1(\slv_reg0_reg_n_0_[6] ),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(\slv_reg0_reg_n_0_[5] ),
        .I5(\slv_reg0_reg_n_0_[4] ),
        .O(DATA_TX_serial_r_i_18_n_0));
  MUXF7 DATA_TX_serial_r_reg_i_11
       (.I0(DATA_TX_serial_r_i_17_n_0),
        .I1(DATA_TX_serial_r_i_18_n_0),
        .O(\cnt_reg[2] ),
        .S(DATA_TX_serial_r_i_3));
  LUT6 #(
    .INIT(64'hF7FFC4CCC4CCC4CC)) 
    aw_en_i_1
       (.I0(s00_axi_awvalid),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_awready),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(clear));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_arready),
        .I3(axi_araddr[2]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_arready),
        .I3(axi_araddr[3]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[4]_i_1 
       (.I0(s00_axi_araddr[2]),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_arready),
        .I3(axi_araddr[4]),
        .O(\axi_araddr[4]_i_1_n_0 ));
  FDRE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(axi_araddr[2]),
        .R(clear));
  FDRE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(axi_araddr[3]),
        .R(clear));
  FDRE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[4]_i_1_n_0 ),
        .Q(axi_araddr[4]),
        .R(clear));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_arready),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(s00_axi_arready),
        .R(clear));
  LUT6 #(
    .INIT(64'hFBFFFFFF08000000)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awready),
        .I3(aw_en_reg_n_0),
        .I4(s00_axi_awvalid),
        .I5(axi_awaddr[2]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBFFFFFF08000000)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awready),
        .I3(aw_en_reg_n_0),
        .I4(s00_axi_awvalid),
        .I5(axi_awaddr[3]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBFFFFFF08000000)) 
    \axi_awaddr[4]_i_1 
       (.I0(s00_axi_awaddr[2]),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awready),
        .I3(aw_en_reg_n_0),
        .I4(s00_axi_awvalid),
        .I5(axi_awaddr[4]),
        .O(\axi_awaddr[4]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(axi_awaddr[2]),
        .R(clear));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(axi_awaddr[3]),
        .R(clear));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[4]_i_1_n_0 ),
        .Q(axi_awaddr[4]),
        .R(clear));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(clear));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h2000)) 
    axi_awready_i_2
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awready),
        .I2(aw_en_reg_n_0),
        .I3(s00_axi_awvalid),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(s00_axi_awready),
        .R(clear));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awready),
        .I3(s00_axi_wready),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(clear));
  LUT6 #(
    .INIT(64'h00E2FFFF00E20000)) 
    \axi_rdata[0]_i_1 
       (.I0(data4),
        .I1(axi_araddr[2]),
        .I2(DATA_RX_r[0]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[4]),
        .I5(\axi_rdata[0]_i_2_n_0 ),
        .O(reg_data_out[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(\slv_reg3_reg_n_0_[0] ),
        .I1(\slv_reg2_reg[1]_0 [0]),
        .I2(axi_araddr[3]),
        .I3(\slv_reg1_reg[0]_0 ),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[0] ),
        .O(\axi_rdata[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[10]_i_1 
       (.I0(\axi_rdata[10]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[10]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_2 
       (.I0(\slv_reg3_reg_n_0_[10] ),
        .I1(\slv_reg2_reg_n_0_[10] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[10]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[10] ),
        .O(\axi_rdata[10]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[11]_i_1 
       (.I0(\axi_rdata[11]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[11]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_2 
       (.I0(\slv_reg3_reg_n_0_[11] ),
        .I1(\slv_reg2_reg_n_0_[11] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[11]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[11] ),
        .O(\axi_rdata[11]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[12]_i_1 
       (.I0(\axi_rdata[12]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_2 
       (.I0(\slv_reg3_reg_n_0_[12] ),
        .I1(\slv_reg2_reg_n_0_[12] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[12]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg[12]_0 ),
        .O(\axi_rdata[12]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[13]_i_1 
       (.I0(\axi_rdata[13]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_2 
       (.I0(\slv_reg3_reg_n_0_[13] ),
        .I1(\slv_reg2_reg_n_0_[13] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[13]),
        .I4(axi_araddr[2]),
        .I5(DATA_TX[0]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[14]_i_1 
       (.I0(\axi_rdata[14]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_2 
       (.I0(\slv_reg3_reg_n_0_[14] ),
        .I1(\slv_reg2_reg_n_0_[14] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[14]),
        .I4(axi_araddr[2]),
        .I5(DATA_TX[1]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[15]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[15]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_2 
       (.I0(\slv_reg3_reg_n_0_[15] ),
        .I1(\slv_reg2_reg_n_0_[15] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[15]),
        .I4(axi_araddr[2]),
        .I5(DATA_TX[2]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[16]_i_1 
       (.I0(\axi_rdata[16]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_2 
       (.I0(\slv_reg3_reg[16]_0 ),
        .I1(\slv_reg2_reg_n_0_[16] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[16]),
        .I4(axi_araddr[2]),
        .I5(DATA_TX[3]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[17]_i_1 
       (.I0(\axi_rdata[17]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_2 
       (.I0(\slv_reg3_reg_n_0_[17] ),
        .I1(\slv_reg2_reg_n_0_[17] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[17]),
        .I4(axi_araddr[2]),
        .I5(DATA_TX[4]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[18]_i_1 
       (.I0(\axi_rdata[18]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[18]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_2 
       (.I0(\slv_reg3_reg_n_0_[18] ),
        .I1(\slv_reg2_reg_n_0_[18] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[18]),
        .I4(axi_araddr[2]),
        .I5(DATA_TX[5]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[19]_i_1 
       (.I0(\axi_rdata[19]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[19]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_2 
       (.I0(\slv_reg3_reg_n_0_[19] ),
        .I1(\slv_reg2_reg_n_0_[19] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[19]),
        .I4(axi_araddr[2]),
        .I5(DATA_TX[6]),
        .O(\axi_rdata[19]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h40FF4000)) 
    \axi_rdata[1]_i_1 
       (.I0(axi_araddr[3]),
        .I1(DATA_RX_r[1]),
        .I2(axi_araddr[2]),
        .I3(axi_araddr[4]),
        .I4(\axi_rdata[1]_i_2_n_0 ),
        .O(reg_data_out[1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_2 
       (.I0(\slv_reg3_reg_n_0_[1] ),
        .I1(\slv_reg2_reg[1]_0 [1]),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[1]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[1] ),
        .O(\axi_rdata[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[20]_i_1 
       (.I0(\axi_rdata[20]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[20]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_2 
       (.I0(\slv_reg3_reg_n_0_[20] ),
        .I1(\slv_reg2_reg_n_0_[20] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[20]),
        .I4(axi_araddr[2]),
        .I5(DATA_TX[7]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[21]_i_1 
       (.I0(\axi_rdata[21]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_2 
       (.I0(\slv_reg3_reg_n_0_[21] ),
        .I1(\slv_reg2_reg_n_0_[21] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[21]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[21] ),
        .O(\axi_rdata[21]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[22]_i_1 
       (.I0(\axi_rdata[22]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[22]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_2 
       (.I0(\slv_reg3_reg_n_0_[22] ),
        .I1(\slv_reg2_reg_n_0_[22] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[22]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[22] ),
        .O(\axi_rdata[22]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[23]_i_1 
       (.I0(\axi_rdata[23]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[23]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_2 
       (.I0(\slv_reg3_reg_n_0_[23] ),
        .I1(\slv_reg2_reg_n_0_[23] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[23]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[23] ),
        .O(\axi_rdata[23]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[24]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_2 
       (.I0(\slv_reg3_reg_n_0_[24] ),
        .I1(\slv_reg2_reg_n_0_[24] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[24]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[24] ),
        .O(\axi_rdata[24]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[25]_i_1 
       (.I0(\axi_rdata[25]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[25]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_2 
       (.I0(\slv_reg3_reg_n_0_[25] ),
        .I1(\slv_reg2_reg_n_0_[25] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[25]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[25] ),
        .O(\axi_rdata[25]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[26]_i_1 
       (.I0(\axi_rdata[26]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_2 
       (.I0(\slv_reg3_reg_n_0_[26] ),
        .I1(\slv_reg2_reg_n_0_[26] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[26]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[26] ),
        .O(\axi_rdata[26]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[27]_i_1 
       (.I0(\axi_rdata[27]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_2 
       (.I0(\slv_reg3_reg_n_0_[27] ),
        .I1(\slv_reg2_reg_n_0_[27] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[27]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[27] ),
        .O(\axi_rdata[27]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[28]_i_1 
       (.I0(\axi_rdata[28]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[28]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_2 
       (.I0(\slv_reg3_reg_n_0_[28] ),
        .I1(\slv_reg2_reg_n_0_[28] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[28]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[28] ),
        .O(\axi_rdata[28]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[29]_i_1 
       (.I0(\axi_rdata[29]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_2 
       (.I0(\slv_reg3_reg_n_0_[29] ),
        .I1(\slv_reg2_reg_n_0_[29] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[29]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[29] ),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h40FF4000)) 
    \axi_rdata[2]_i_1 
       (.I0(axi_araddr[3]),
        .I1(DATA_RX_r[2]),
        .I2(axi_araddr[2]),
        .I3(axi_araddr[4]),
        .I4(\axi_rdata[2]_i_2_n_0 ),
        .O(reg_data_out[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_2 
       (.I0(\slv_reg3_reg_n_0_[2] ),
        .I1(\slv_reg2_reg_n_0_[2] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[2]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[2] ),
        .O(\axi_rdata[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[30]_i_1 
       (.I0(\axi_rdata[30]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[30]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_2 
       (.I0(\slv_reg3_reg_n_0_[30] ),
        .I1(\slv_reg2_reg_n_0_[30] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[30]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[30] ),
        .O(\axi_rdata[30]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[31]_i_1 
       (.I0(\axi_rdata[31]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[31]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_2 
       (.I0(\slv_reg3_reg_n_0_[31] ),
        .I1(\slv_reg2_reg_n_0_[31] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[31]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[31] ),
        .O(\axi_rdata[31]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h40FF4000)) 
    \axi_rdata[3]_i_1 
       (.I0(axi_araddr[3]),
        .I1(DATA_RX_r[3]),
        .I2(axi_araddr[2]),
        .I3(axi_araddr[4]),
        .I4(\axi_rdata[3]_i_2_n_0 ),
        .O(reg_data_out[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_2 
       (.I0(\slv_reg3_reg_n_0_[3] ),
        .I1(\slv_reg2_reg_n_0_[3] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[3]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[3] ),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h40FF4000)) 
    \axi_rdata[4]_i_1 
       (.I0(axi_araddr[3]),
        .I1(DATA_RX_r[4]),
        .I2(axi_araddr[2]),
        .I3(axi_araddr[4]),
        .I4(\axi_rdata[4]_i_2_n_0 ),
        .O(reg_data_out[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_2 
       (.I0(\slv_reg3_reg_n_0_[4] ),
        .I1(\slv_reg2_reg_n_0_[4] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[4]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[4] ),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h40FF4000)) 
    \axi_rdata[5]_i_1 
       (.I0(axi_araddr[3]),
        .I1(DATA_RX_r[5]),
        .I2(axi_araddr[2]),
        .I3(axi_araddr[4]),
        .I4(\axi_rdata[5]_i_2_n_0 ),
        .O(reg_data_out[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_2 
       (.I0(\slv_reg3_reg_n_0_[5] ),
        .I1(\slv_reg2_reg_n_0_[5] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[5]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[5] ),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h40FF4000)) 
    \axi_rdata[6]_i_1 
       (.I0(axi_araddr[3]),
        .I1(DATA_RX_r[6]),
        .I2(axi_araddr[2]),
        .I3(axi_araddr[4]),
        .I4(\axi_rdata[6]_i_2_n_0 ),
        .O(reg_data_out[6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_2 
       (.I0(\slv_reg3_reg_n_0_[6] ),
        .I1(\slv_reg2_reg_n_0_[6] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[6]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[6] ),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h40FF4000)) 
    \axi_rdata[7]_i_1 
       (.I0(axi_araddr[3]),
        .I1(DATA_RX_r[7]),
        .I2(axi_araddr[2]),
        .I3(axi_araddr[4]),
        .I4(\axi_rdata[7]_i_2_n_0 ),
        .O(reg_data_out[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_2 
       (.I0(\slv_reg3_reg_n_0_[7] ),
        .I1(\slv_reg2_reg_n_0_[7] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[7]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[7] ),
        .O(\axi_rdata[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[8]_i_1 
       (.I0(\axi_rdata[8]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_2 
       (.I0(\slv_reg3_reg_n_0_[8] ),
        .I1(\slv_reg2_reg_n_0_[8] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[8]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[8] ),
        .O(\axi_rdata[8]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[9]_i_1 
       (.I0(\axi_rdata[9]_i_2_n_0 ),
        .I1(axi_araddr[4]),
        .O(reg_data_out[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_2 
       (.I0(\slv_reg3_reg_n_0_[9] ),
        .I1(\slv_reg2_reg_n_0_[9] ),
        .I2(axi_araddr[3]),
        .I3(slv_reg1[9]),
        .I4(axi_araddr[2]),
        .I5(\slv_reg0_reg_n_0_[9] ),
        .O(\axi_rdata[9]_i_2_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(clear));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(clear));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(clear));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(clear));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(clear));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(clear));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(clear));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(clear));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(clear));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(clear));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(clear));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(clear));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(clear));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(clear));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(clear));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(clear));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(clear));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(clear));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(clear));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(clear));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(clear));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(clear));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(clear));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(clear));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(clear));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(clear));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(clear));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(clear));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(clear));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(clear));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(clear));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(clear));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arready),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(clear));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    axi_wready_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_wready),
        .I3(aw_en_reg_n_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(s00_axi_wready),
        .R(clear));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(axi_awaddr[4]),
        .I4(s00_axi_wstrb[1]),
        .O(p_1_in[15]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(axi_awaddr[4]),
        .I4(s00_axi_wstrb[2]),
        .O(p_1_in[23]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(axi_awaddr[4]),
        .I4(s00_axi_wstrb[3]),
        .O(p_1_in[31]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(axi_awaddr[4]),
        .I4(s00_axi_wstrb[0]),
        .O(p_1_in[7]));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg0_reg_n_0_[0] ),
        .R(clear));
  FDRE \slv_reg0_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(\slv_reg0_reg_n_0_[10] ),
        .R(clear));
  FDRE \slv_reg0_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(\slv_reg0_reg_n_0_[11] ),
        .R(clear));
  FDRE \slv_reg0_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(\slv_reg0_reg[12]_0 ),
        .R(clear));
  FDRE \slv_reg0_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(DATA_TX[0]),
        .R(clear));
  FDRE \slv_reg0_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(DATA_TX[1]),
        .R(clear));
  FDRE \slv_reg0_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(DATA_TX[2]),
        .R(clear));
  FDRE \slv_reg0_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(DATA_TX[3]),
        .R(clear));
  FDRE \slv_reg0_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(DATA_TX[4]),
        .R(clear));
  FDRE \slv_reg0_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(DATA_TX[5]),
        .R(clear));
  FDRE \slv_reg0_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(DATA_TX[6]),
        .R(clear));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg0_reg_n_0_[1] ),
        .R(clear));
  FDRE \slv_reg0_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(DATA_TX[7]),
        .R(clear));
  FDRE \slv_reg0_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(\slv_reg0_reg_n_0_[21] ),
        .R(clear));
  FDRE \slv_reg0_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(\slv_reg0_reg_n_0_[22] ),
        .R(clear));
  FDRE \slv_reg0_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(\slv_reg0_reg_n_0_[23] ),
        .R(clear));
  FDRE \slv_reg0_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(\slv_reg0_reg_n_0_[24] ),
        .R(clear));
  FDRE \slv_reg0_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(\slv_reg0_reg_n_0_[25] ),
        .R(clear));
  FDRE \slv_reg0_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(\slv_reg0_reg_n_0_[26] ),
        .R(clear));
  FDRE \slv_reg0_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(\slv_reg0_reg_n_0_[27] ),
        .R(clear));
  FDRE \slv_reg0_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(\slv_reg0_reg_n_0_[28] ),
        .R(clear));
  FDRE \slv_reg0_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(\slv_reg0_reg_n_0_[29] ),
        .R(clear));
  FDRE \slv_reg0_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg0_reg_n_0_[2] ),
        .R(clear));
  FDRE \slv_reg0_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(\slv_reg0_reg_n_0_[30] ),
        .R(clear));
  FDRE \slv_reg0_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(\slv_reg0_reg_n_0_[31] ),
        .R(clear));
  FDRE \slv_reg0_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg0_reg_n_0_[3] ),
        .R(clear));
  FDRE \slv_reg0_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg0_reg_n_0_[4] ),
        .R(clear));
  FDRE \slv_reg0_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(\slv_reg0_reg_n_0_[5] ),
        .R(clear));
  FDRE \slv_reg0_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(\slv_reg0_reg_n_0_[6] ),
        .R(clear));
  FDRE \slv_reg0_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(\slv_reg0_reg_n_0_[7] ),
        .R(clear));
  FDRE \slv_reg0_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(\slv_reg0_reg_n_0_[8] ),
        .R(clear));
  FDRE \slv_reg0_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(\slv_reg0_reg_n_0_[9] ),
        .R(clear));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(s00_axi_wstrb[1]),
        .I4(axi_awaddr[2]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(s00_axi_wstrb[2]),
        .I4(axi_awaddr[2]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(s00_axi_wstrb[3]),
        .I4(axi_awaddr[2]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(s00_axi_wstrb[0]),
        .I4(axi_awaddr[2]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg1_reg[0]_0 ),
        .R(clear));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(clear));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(clear));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(clear));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(clear));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(clear));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(clear));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(clear));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(clear));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(clear));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(clear));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(clear));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(clear));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(clear));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(clear));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(clear));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(clear));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(clear));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(clear));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(clear));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(clear));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(clear));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(clear));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(clear));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(clear));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(clear));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(clear));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(clear));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(clear));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(clear));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(clear));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(clear));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[1]),
        .I4(axi_awaddr[3]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[1]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[0]),
        .I4(axi_awaddr[3]),
        .O(\slv_reg2[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg2[1]_i_2 
       (.I0(s00_axi_wready),
        .I1(s00_axi_awready),
        .I2(s00_axi_awvalid),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__0));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[2]),
        .I4(axi_awaddr[3]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[3]),
        .I4(axi_awaddr[3]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg2_reg[1]_0 [0]),
        .R(clear));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(\slv_reg2_reg_n_0_[10] ),
        .R(clear));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(\slv_reg2_reg_n_0_[11] ),
        .R(clear));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(\slv_reg2_reg_n_0_[12] ),
        .R(clear));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(\slv_reg2_reg_n_0_[13] ),
        .R(clear));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(\slv_reg2_reg_n_0_[14] ),
        .R(clear));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(\slv_reg2_reg_n_0_[15] ),
        .R(clear));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(\slv_reg2_reg_n_0_[16] ),
        .R(clear));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(\slv_reg2_reg_n_0_[17] ),
        .R(clear));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(\slv_reg2_reg_n_0_[18] ),
        .R(clear));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(\slv_reg2_reg_n_0_[19] ),
        .R(clear));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg2_reg[1]_0 [1]),
        .R(clear));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(\slv_reg2_reg_n_0_[20] ),
        .R(clear));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(\slv_reg2_reg_n_0_[21] ),
        .R(clear));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(\slv_reg2_reg_n_0_[22] ),
        .R(clear));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(\slv_reg2_reg_n_0_[23] ),
        .R(clear));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(\slv_reg2_reg_n_0_[24] ),
        .R(clear));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(\slv_reg2_reg_n_0_[25] ),
        .R(clear));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(\slv_reg2_reg_n_0_[26] ),
        .R(clear));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(\slv_reg2_reg_n_0_[27] ),
        .R(clear));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(\slv_reg2_reg_n_0_[28] ),
        .R(clear));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(\slv_reg2_reg_n_0_[29] ),
        .R(clear));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg2_reg_n_0_[2] ),
        .R(clear));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(\slv_reg2_reg_n_0_[30] ),
        .R(clear));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(\slv_reg2_reg_n_0_[31] ),
        .R(clear));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg2_reg_n_0_[3] ),
        .R(clear));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg2_reg_n_0_[4] ),
        .R(clear));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(\slv_reg2_reg_n_0_[5] ),
        .R(clear));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(\slv_reg2_reg_n_0_[6] ),
        .R(clear));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(\slv_reg2_reg_n_0_[7] ),
        .R(clear));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(\slv_reg2_reg_n_0_[8] ),
        .R(clear));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(\slv_reg2_reg_n_0_[9] ),
        .R(clear));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\slv_reg3[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\slv_reg3[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\slv_reg3[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[0]),
        .O(\slv_reg3[7]_i_1_n_0 ));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg3_reg_n_0_[0] ),
        .R(clear));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(\slv_reg3_reg_n_0_[10] ),
        .R(clear));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(\slv_reg3_reg_n_0_[11] ),
        .R(clear));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(\slv_reg3_reg_n_0_[12] ),
        .R(clear));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(\slv_reg3_reg_n_0_[13] ),
        .R(clear));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(\slv_reg3_reg_n_0_[14] ),
        .R(clear));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(\slv_reg3_reg_n_0_[15] ),
        .R(clear));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(\slv_reg3_reg[16]_0 ),
        .R(clear));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(\slv_reg3_reg_n_0_[17] ),
        .R(clear));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(\slv_reg3_reg_n_0_[18] ),
        .R(clear));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(\slv_reg3_reg_n_0_[19] ),
        .R(clear));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg3_reg_n_0_[1] ),
        .R(clear));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(\slv_reg3_reg_n_0_[20] ),
        .R(clear));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(\slv_reg3_reg_n_0_[21] ),
        .R(clear));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(\slv_reg3_reg_n_0_[22] ),
        .R(clear));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(\slv_reg3_reg_n_0_[23] ),
        .R(clear));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(\slv_reg3_reg_n_0_[24] ),
        .R(clear));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(\slv_reg3_reg_n_0_[25] ),
        .R(clear));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(\slv_reg3_reg_n_0_[26] ),
        .R(clear));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(\slv_reg3_reg_n_0_[27] ),
        .R(clear));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(\slv_reg3_reg_n_0_[28] ),
        .R(clear));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(\slv_reg3_reg_n_0_[29] ),
        .R(clear));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg3_reg_n_0_[2] ),
        .R(clear));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(\slv_reg3_reg_n_0_[30] ),
        .R(clear));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(\slv_reg3_reg_n_0_[31] ),
        .R(clear));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg3_reg_n_0_[3] ),
        .R(clear));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg3_reg_n_0_[4] ),
        .R(clear));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(\slv_reg3_reg_n_0_[5] ),
        .R(clear));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(\slv_reg3_reg_n_0_[6] ),
        .R(clear));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(\slv_reg3_reg_n_0_[7] ),
        .R(clear));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(\slv_reg3_reg_n_0_[8] ),
        .R(clear));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(\slv_reg3_reg_n_0_[9] ),
        .R(clear));
  LUT3 #(
    .INIT(8'h20)) 
    slv_reg_rden
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_rvalid),
        .I2(s00_axi_arready),
        .O(slv_reg_rden__0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0
   (s00_axi_wready,
    s00_axi_awready,
    s00_axi_arready,
    ADC_PDwN,
    s00_axi_rdata,
    SYNC,
    s00_axi_rvalid,
    m00_dma_axis_tdata,
    m00_dma_axis_tvalid,
    m00_dma_axis_tlast,
    m01_fft_axis_tdata,
    s00_axi_bvalid,
    m00_fft_axis_tvalid,
    m00_fft_axis_tlast,
    m01_fft_axis_tvalid,
    m01_fft_axis_tlast,
    mux_fft_in,
    allowed_clk,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    clk_10MHz,
    s00_axi_araddr,
    s00_axi_arvalid,
    dco_or_dcoa,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s01_fft_axis_tdata,
    s00_fft_axis_tdata,
    s01_fft_axis_tvalid,
    s00_fft_axis_tvalid,
    s01_fft_axis_tlast,
    s00_fft_axis_tlast,
    DATA_INA,
    DATA_INB,
    s00_axi_bready,
    s00_axi_rready);
  output s00_axi_wready;
  output s00_axi_awready;
  output s00_axi_arready;
  output ADC_PDwN;
  output [31:0]s00_axi_rdata;
  output SYNC;
  output s00_axi_rvalid;
  output [63:0]m00_dma_axis_tdata;
  output m00_dma_axis_tvalid;
  output m00_dma_axis_tlast;
  output [31:0]m01_fft_axis_tdata;
  output s00_axi_bvalid;
  output m00_fft_axis_tvalid;
  output m00_fft_axis_tlast;
  output m01_fft_axis_tvalid;
  output m01_fft_axis_tlast;
  input mux_fft_in;
  input allowed_clk;
  input s00_axi_aclk;
  input [2:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [31:0]s00_axi_wdata;
  input clk_10MHz;
  input [2:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input dco_or_dcoa;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input [63:0]s01_fft_axis_tdata;
  input [63:0]s00_fft_axis_tdata;
  input s01_fft_axis_tvalid;
  input s00_fft_axis_tvalid;
  input s01_fft_axis_tlast;
  input s00_fft_axis_tlast;
  input [15:0]DATA_INA;
  input [15:0]DATA_INB;
  input s00_axi_bready;
  input s00_axi_rready;

  wire AD9650_v1_0_S00_AXI_inst_n_12;
  wire AD9650_v1_0_S00_AXI_inst_n_14;
  wire AD9650_v1_0_S00_AXI_inst_n_6;
  wire AD9650_v1_0_S00_AXI_inst_n_7;
  wire AD9650_v1_0_S00_AXI_inst_n_8;
  wire AD9650_v1_0_S00_AXI_inst_n_9;
  wire ADC_PDwN;
  wire [15:0]DATA_INA;
  wire [15:0]DATA_INB;
  wire [7:0]DATA_RX_r;
  wire SYNC;
  wire SYNC_signal;
  wire allowed_clk;
  wire clear;
  wire clk_10MHz;
  wire [15:0]cnt_DCO;
  wire \cnt_DCO[0]_i_1_n_0 ;
  wire \cnt_DCO[13]_i_1_n_0 ;
  wire \cnt_DCO[15]_i_1_n_0 ;
  wire \cnt_DCO[15]_i_3_n_0 ;
  wire \cnt_DCO[15]_i_4_n_0 ;
  wire \cnt_DCO_reg[12]_i_1_n_0 ;
  wire \cnt_DCO_reg[12]_i_1_n_1 ;
  wire \cnt_DCO_reg[12]_i_1_n_2 ;
  wire \cnt_DCO_reg[12]_i_1_n_3 ;
  wire \cnt_DCO_reg[15]_i_2_n_2 ;
  wire \cnt_DCO_reg[15]_i_2_n_3 ;
  wire \cnt_DCO_reg[4]_i_1_n_0 ;
  wire \cnt_DCO_reg[4]_i_1_n_1 ;
  wire \cnt_DCO_reg[4]_i_1_n_2 ;
  wire \cnt_DCO_reg[4]_i_1_n_3 ;
  wire \cnt_DCO_reg[8]_i_1_n_0 ;
  wire \cnt_DCO_reg[8]_i_1_n_1 ;
  wire \cnt_DCO_reg[8]_i_1_n_2 ;
  wire \cnt_DCO_reg[8]_i_1_n_3 ;
  wire \cnt_allowed_clk[0]_i_2_n_0 ;
  wire [15:0]cnt_allowed_clk_reg;
  wire \cnt_allowed_clk_reg[0]_i_1_n_0 ;
  wire \cnt_allowed_clk_reg[0]_i_1_n_1 ;
  wire \cnt_allowed_clk_reg[0]_i_1_n_2 ;
  wire \cnt_allowed_clk_reg[0]_i_1_n_3 ;
  wire \cnt_allowed_clk_reg[0]_i_1_n_4 ;
  wire \cnt_allowed_clk_reg[0]_i_1_n_5 ;
  wire \cnt_allowed_clk_reg[0]_i_1_n_6 ;
  wire \cnt_allowed_clk_reg[0]_i_1_n_7 ;
  wire \cnt_allowed_clk_reg[12]_i_1_n_1 ;
  wire \cnt_allowed_clk_reg[12]_i_1_n_2 ;
  wire \cnt_allowed_clk_reg[12]_i_1_n_3 ;
  wire \cnt_allowed_clk_reg[12]_i_1_n_4 ;
  wire \cnt_allowed_clk_reg[12]_i_1_n_5 ;
  wire \cnt_allowed_clk_reg[12]_i_1_n_6 ;
  wire \cnt_allowed_clk_reg[12]_i_1_n_7 ;
  wire \cnt_allowed_clk_reg[4]_i_1_n_0 ;
  wire \cnt_allowed_clk_reg[4]_i_1_n_1 ;
  wire \cnt_allowed_clk_reg[4]_i_1_n_2 ;
  wire \cnt_allowed_clk_reg[4]_i_1_n_3 ;
  wire \cnt_allowed_clk_reg[4]_i_1_n_4 ;
  wire \cnt_allowed_clk_reg[4]_i_1_n_5 ;
  wire \cnt_allowed_clk_reg[4]_i_1_n_6 ;
  wire \cnt_allowed_clk_reg[4]_i_1_n_7 ;
  wire \cnt_allowed_clk_reg[8]_i_1_n_0 ;
  wire \cnt_allowed_clk_reg[8]_i_1_n_1 ;
  wire \cnt_allowed_clk_reg[8]_i_1_n_2 ;
  wire \cnt_allowed_clk_reg[8]_i_1_n_3 ;
  wire \cnt_allowed_clk_reg[8]_i_1_n_4 ;
  wire \cnt_allowed_clk_reg[8]_i_1_n_5 ;
  wire \cnt_allowed_clk_reg[8]_i_1_n_6 ;
  wire \cnt_allowed_clk_reg[8]_i_1_n_7 ;
  wire \cnt_in_DCO[0]_i_1_n_0 ;
  wire \cnt_in_DCO[0]_i_3_n_0 ;
  wire [15:0]cnt_in_DCO_reg;
  wire \cnt_in_DCO_reg[0]_i_2_n_0 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_1 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_2 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_3 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_4 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_5 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_6 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_7 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_1 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_2 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_3 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_4 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_5 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_6 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_7 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_0 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_1 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_2 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_3 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_4 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_5 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_6 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_7 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_0 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_1 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_2 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_3 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_4 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_5 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_6 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_7 ;
  wire [1:0]cnt_reg;
  wire [0:0]data4;
  wire dco_or_dcoa;
  wire [63:0]m00_dma_axis_tdata;
  wire m00_dma_axis_tlast;
  wire m00_dma_axis_tvalid;
  wire m00_fft_axis_tlast;
  wire m00_fft_axis_tlast_r_i_1_n_0;
  wire m00_fft_axis_tvalid;
  wire m00_fft_axis_tvalid_r_i_1_n_0;
  wire m00_fft_axis_tvalid_r_i_2_n_0;
  wire m00_fft_axis_tvalid_r_i_3_n_0;
  wire m00_fft_axis_tvalid_r_i_4_n_0;
  wire m00_fft_axis_tvalid_r_i_5_n_0;
  wire m00_fft_axis_tvalid_r_i_6_n_0;
  wire m00_fft_axis_tvalid_r_i_7_n_0;
  wire m00_fft_axis_tvalid_r_i_8_n_0;
  wire [31:0]m01_fft_axis_tdata;
  wire m01_fft_axis_tlast;
  wire m01_fft_axis_tlast_r_i_1_n_0;
  wire m01_fft_axis_tvalid;
  wire m01_fft_axis_tvalid_r_i_1_n_0;
  wire mux_fft_in;
  wire mux_fft_out_i_1_n_0;
  wire mux_fft_out_i_2_n_0;
  wire mux_fft_out_i_3_n_0;
  wire mux_fft_out_i_4_n_0;
  wire mux_fft_out_i_5_n_0;
  wire mux_fft_out_reg_n_0;
  wire [15:1]p_2_in;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [63:0]s00_fft_axis_tdata;
  wire s00_fft_axis_tlast;
  wire s00_fft_axis_tvalid;
  wire [63:0]s01_fft_axis_tdata;
  wire s01_fft_axis_tlast;
  wire s01_fft_axis_tvalid;
  wire [0:0]slv_reg1;
  wire spi_AD9650_inst_n_3;
  wire [3:2]\NLW_cnt_DCO_reg[15]_i_2_CO_UNCONNECTED ;
  wire [3:3]\NLW_cnt_DCO_reg[15]_i_2_O_UNCONNECTED ;
  wire [3:3]\NLW_cnt_allowed_clk_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_cnt_in_DCO_reg[12]_i_1_CO_UNCONNECTED ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v1_0_S00_AXI AD9650_v1_0_S00_AXI_inst
       (.DATA_RX_r(DATA_RX_r),
        .DATA_TX_serial_r_i_3(spi_AD9650_inst_n_3),
        .Q(cnt_reg),
        .clear(clear),
        .\cnt_reg[2] (AD9650_v1_0_S00_AXI_inst_n_9),
        .data4(data4),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arready(s00_axi_arready),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awready(s00_axi_awready),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wready(s00_axi_wready),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .\slv_reg0_reg[11]_0 (AD9650_v1_0_S00_AXI_inst_n_8),
        .\slv_reg0_reg[12]_0 (AD9650_v1_0_S00_AXI_inst_n_12),
        .\slv_reg0_reg[16]_0 (AD9650_v1_0_S00_AXI_inst_n_7),
        .\slv_reg0_reg[20]_0 (AD9650_v1_0_S00_AXI_inst_n_6),
        .\slv_reg1_reg[0]_0 (slv_reg1),
        .\slv_reg2_reg[1]_0 ({ADC_PDwN,AD9650_v1_0_S00_AXI_inst_n_14}),
        .\slv_reg3_reg[16]_0 (SYNC_signal));
  FDRE SYNC_reg_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(SYNC_signal),
        .Q(SYNC),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h2A)) 
    \cnt_DCO[0]_i_1 
       (.I0(allowed_clk),
        .I1(\cnt_DCO[15]_i_3_n_0 ),
        .I2(cnt_DCO[0]),
        .O(\cnt_DCO[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \cnt_DCO[13]_i_1 
       (.I0(allowed_clk),
        .I1(\cnt_DCO[15]_i_3_n_0 ),
        .I2(p_2_in[13]),
        .O(\cnt_DCO[13]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \cnt_DCO[15]_i_1 
       (.I0(allowed_clk),
        .I1(\cnt_DCO[15]_i_3_n_0 ),
        .O(\cnt_DCO[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFEFFFFFFFFFFFF)) 
    \cnt_DCO[15]_i_3 
       (.I0(m00_fft_axis_tvalid_r_i_6_n_0),
        .I1(\cnt_DCO[15]_i_4_n_0 ),
        .I2(cnt_DCO[1]),
        .I3(cnt_DCO[12]),
        .I4(cnt_DCO[13]),
        .I5(cnt_DCO[0]),
        .O(\cnt_DCO[15]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \cnt_DCO[15]_i_4 
       (.I0(cnt_DCO[15]),
        .I1(cnt_DCO[14]),
        .O(\cnt_DCO[15]_i_4_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[0] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO[0]_i_1_n_0 ),
        .Q(cnt_DCO[0]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[10] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(p_2_in[10]),
        .Q(cnt_DCO[10]),
        .R(\cnt_DCO[15]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[11] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(p_2_in[11]),
        .Q(cnt_DCO[11]),
        .R(\cnt_DCO[15]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[12] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(p_2_in[12]),
        .Q(cnt_DCO[12]),
        .R(\cnt_DCO[15]_i_1_n_0 ));
  CARRY4 \cnt_DCO_reg[12]_i_1 
       (.CI(\cnt_DCO_reg[8]_i_1_n_0 ),
        .CO({\cnt_DCO_reg[12]_i_1_n_0 ,\cnt_DCO_reg[12]_i_1_n_1 ,\cnt_DCO_reg[12]_i_1_n_2 ,\cnt_DCO_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[12:9]),
        .S(cnt_DCO[12:9]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[13] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(\cnt_DCO[13]_i_1_n_0 ),
        .Q(cnt_DCO[13]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[14] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(p_2_in[14]),
        .Q(cnt_DCO[14]),
        .R(\cnt_DCO[15]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[15] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(p_2_in[15]),
        .Q(cnt_DCO[15]),
        .R(\cnt_DCO[15]_i_1_n_0 ));
  CARRY4 \cnt_DCO_reg[15]_i_2 
       (.CI(\cnt_DCO_reg[12]_i_1_n_0 ),
        .CO({\NLW_cnt_DCO_reg[15]_i_2_CO_UNCONNECTED [3:2],\cnt_DCO_reg[15]_i_2_n_2 ,\cnt_DCO_reg[15]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_cnt_DCO_reg[15]_i_2_O_UNCONNECTED [3],p_2_in[15:13]}),
        .S({1'b0,cnt_DCO[15:13]}));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[1] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(p_2_in[1]),
        .Q(cnt_DCO[1]),
        .R(\cnt_DCO[15]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[2] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(p_2_in[2]),
        .Q(cnt_DCO[2]),
        .R(\cnt_DCO[15]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[3] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(p_2_in[3]),
        .Q(cnt_DCO[3]),
        .R(\cnt_DCO[15]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[4] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(p_2_in[4]),
        .Q(cnt_DCO[4]),
        .R(\cnt_DCO[15]_i_1_n_0 ));
  CARRY4 \cnt_DCO_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\cnt_DCO_reg[4]_i_1_n_0 ,\cnt_DCO_reg[4]_i_1_n_1 ,\cnt_DCO_reg[4]_i_1_n_2 ,\cnt_DCO_reg[4]_i_1_n_3 }),
        .CYINIT(cnt_DCO[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[4:1]),
        .S(cnt_DCO[4:1]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[5] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(p_2_in[5]),
        .Q(cnt_DCO[5]),
        .R(\cnt_DCO[15]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[6] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(p_2_in[6]),
        .Q(cnt_DCO[6]),
        .R(\cnt_DCO[15]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[7] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(p_2_in[7]),
        .Q(cnt_DCO[7]),
        .R(\cnt_DCO[15]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[8] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(p_2_in[8]),
        .Q(cnt_DCO[8]),
        .R(\cnt_DCO[15]_i_1_n_0 ));
  CARRY4 \cnt_DCO_reg[8]_i_1 
       (.CI(\cnt_DCO_reg[4]_i_1_n_0 ),
        .CO({\cnt_DCO_reg[8]_i_1_n_0 ,\cnt_DCO_reg[8]_i_1_n_1 ,\cnt_DCO_reg[8]_i_1_n_2 ,\cnt_DCO_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[8:5]),
        .S(cnt_DCO[8:5]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[9] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(p_2_in[9]),
        .Q(cnt_DCO[9]),
        .R(\cnt_DCO[15]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_allowed_clk[0]_i_2 
       (.I0(cnt_allowed_clk_reg[0]),
        .O(\cnt_allowed_clk[0]_i_2_n_0 ));
  FDRE \cnt_allowed_clk_reg[0] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[0]_i_1_n_7 ),
        .Q(cnt_allowed_clk_reg[0]),
        .R(clear));
  CARRY4 \cnt_allowed_clk_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\cnt_allowed_clk_reg[0]_i_1_n_0 ,\cnt_allowed_clk_reg[0]_i_1_n_1 ,\cnt_allowed_clk_reg[0]_i_1_n_2 ,\cnt_allowed_clk_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_allowed_clk_reg[0]_i_1_n_4 ,\cnt_allowed_clk_reg[0]_i_1_n_5 ,\cnt_allowed_clk_reg[0]_i_1_n_6 ,\cnt_allowed_clk_reg[0]_i_1_n_7 }),
        .S({cnt_allowed_clk_reg[3:1],\cnt_allowed_clk[0]_i_2_n_0 }));
  FDRE \cnt_allowed_clk_reg[10] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[8]_i_1_n_5 ),
        .Q(cnt_allowed_clk_reg[10]),
        .R(clear));
  FDRE \cnt_allowed_clk_reg[11] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[8]_i_1_n_4 ),
        .Q(cnt_allowed_clk_reg[11]),
        .R(clear));
  FDRE \cnt_allowed_clk_reg[12] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[12]_i_1_n_7 ),
        .Q(cnt_allowed_clk_reg[12]),
        .R(clear));
  CARRY4 \cnt_allowed_clk_reg[12]_i_1 
       (.CI(\cnt_allowed_clk_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_allowed_clk_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_allowed_clk_reg[12]_i_1_n_1 ,\cnt_allowed_clk_reg[12]_i_1_n_2 ,\cnt_allowed_clk_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_allowed_clk_reg[12]_i_1_n_4 ,\cnt_allowed_clk_reg[12]_i_1_n_5 ,\cnt_allowed_clk_reg[12]_i_1_n_6 ,\cnt_allowed_clk_reg[12]_i_1_n_7 }),
        .S(cnt_allowed_clk_reg[15:12]));
  FDRE \cnt_allowed_clk_reg[13] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[12]_i_1_n_6 ),
        .Q(cnt_allowed_clk_reg[13]),
        .R(clear));
  FDRE \cnt_allowed_clk_reg[14] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[12]_i_1_n_5 ),
        .Q(cnt_allowed_clk_reg[14]),
        .R(clear));
  FDRE \cnt_allowed_clk_reg[15] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[12]_i_1_n_4 ),
        .Q(cnt_allowed_clk_reg[15]),
        .R(clear));
  FDRE \cnt_allowed_clk_reg[1] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[0]_i_1_n_6 ),
        .Q(cnt_allowed_clk_reg[1]),
        .R(clear));
  FDRE \cnt_allowed_clk_reg[2] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[0]_i_1_n_5 ),
        .Q(cnt_allowed_clk_reg[2]),
        .R(clear));
  FDRE \cnt_allowed_clk_reg[3] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[0]_i_1_n_4 ),
        .Q(cnt_allowed_clk_reg[3]),
        .R(clear));
  FDRE \cnt_allowed_clk_reg[4] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[4]_i_1_n_7 ),
        .Q(cnt_allowed_clk_reg[4]),
        .R(clear));
  CARRY4 \cnt_allowed_clk_reg[4]_i_1 
       (.CI(\cnt_allowed_clk_reg[0]_i_1_n_0 ),
        .CO({\cnt_allowed_clk_reg[4]_i_1_n_0 ,\cnt_allowed_clk_reg[4]_i_1_n_1 ,\cnt_allowed_clk_reg[4]_i_1_n_2 ,\cnt_allowed_clk_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_allowed_clk_reg[4]_i_1_n_4 ,\cnt_allowed_clk_reg[4]_i_1_n_5 ,\cnt_allowed_clk_reg[4]_i_1_n_6 ,\cnt_allowed_clk_reg[4]_i_1_n_7 }),
        .S(cnt_allowed_clk_reg[7:4]));
  FDRE \cnt_allowed_clk_reg[5] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[4]_i_1_n_6 ),
        .Q(cnt_allowed_clk_reg[5]),
        .R(clear));
  FDRE \cnt_allowed_clk_reg[6] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[4]_i_1_n_5 ),
        .Q(cnt_allowed_clk_reg[6]),
        .R(clear));
  FDRE \cnt_allowed_clk_reg[7] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[4]_i_1_n_4 ),
        .Q(cnt_allowed_clk_reg[7]),
        .R(clear));
  FDRE \cnt_allowed_clk_reg[8] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[8]_i_1_n_7 ),
        .Q(cnt_allowed_clk_reg[8]),
        .R(clear));
  CARRY4 \cnt_allowed_clk_reg[8]_i_1 
       (.CI(\cnt_allowed_clk_reg[4]_i_1_n_0 ),
        .CO({\cnt_allowed_clk_reg[8]_i_1_n_0 ,\cnt_allowed_clk_reg[8]_i_1_n_1 ,\cnt_allowed_clk_reg[8]_i_1_n_2 ,\cnt_allowed_clk_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_allowed_clk_reg[8]_i_1_n_4 ,\cnt_allowed_clk_reg[8]_i_1_n_5 ,\cnt_allowed_clk_reg[8]_i_1_n_6 ,\cnt_allowed_clk_reg[8]_i_1_n_7 }),
        .S(cnt_allowed_clk_reg[11:8]));
  FDRE \cnt_allowed_clk_reg[9] 
       (.C(clk_10MHz),
        .CE(allowed_clk),
        .D(\cnt_allowed_clk_reg[8]_i_1_n_6 ),
        .Q(cnt_allowed_clk_reg[9]),
        .R(clear));
  LUT3 #(
    .INIT(8'h7F)) 
    \cnt_in_DCO[0]_i_1 
       (.I0(dco_or_dcoa),
        .I1(allowed_clk),
        .I2(s00_axi_aresetn),
        .O(\cnt_in_DCO[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_in_DCO[0]_i_3 
       (.I0(cnt_in_DCO_reg[0]),
        .O(\cnt_in_DCO[0]_i_3_n_0 ));
  FDRE \cnt_in_DCO_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[0]_i_2_n_7 ),
        .Q(cnt_in_DCO_reg[0]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  CARRY4 \cnt_in_DCO_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_in_DCO_reg[0]_i_2_n_0 ,\cnt_in_DCO_reg[0]_i_2_n_1 ,\cnt_in_DCO_reg[0]_i_2_n_2 ,\cnt_in_DCO_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_in_DCO_reg[0]_i_2_n_4 ,\cnt_in_DCO_reg[0]_i_2_n_5 ,\cnt_in_DCO_reg[0]_i_2_n_6 ,\cnt_in_DCO_reg[0]_i_2_n_7 }),
        .S({cnt_in_DCO_reg[3:1],\cnt_in_DCO[0]_i_3_n_0 }));
  FDRE \cnt_in_DCO_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[8]_i_1_n_5 ),
        .Q(cnt_in_DCO_reg[10]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[8]_i_1_n_4 ),
        .Q(cnt_in_DCO_reg[11]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[12]_i_1_n_7 ),
        .Q(cnt_in_DCO_reg[12]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  CARRY4 \cnt_in_DCO_reg[12]_i_1 
       (.CI(\cnt_in_DCO_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_in_DCO_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_in_DCO_reg[12]_i_1_n_1 ,\cnt_in_DCO_reg[12]_i_1_n_2 ,\cnt_in_DCO_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_in_DCO_reg[12]_i_1_n_4 ,\cnt_in_DCO_reg[12]_i_1_n_5 ,\cnt_in_DCO_reg[12]_i_1_n_6 ,\cnt_in_DCO_reg[12]_i_1_n_7 }),
        .S(cnt_in_DCO_reg[15:12]));
  FDRE \cnt_in_DCO_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[12]_i_1_n_6 ),
        .Q(cnt_in_DCO_reg[13]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[12]_i_1_n_5 ),
        .Q(cnt_in_DCO_reg[14]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[12]_i_1_n_4 ),
        .Q(cnt_in_DCO_reg[15]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[0]_i_2_n_6 ),
        .Q(cnt_in_DCO_reg[1]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[0]_i_2_n_5 ),
        .Q(cnt_in_DCO_reg[2]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[0]_i_2_n_4 ),
        .Q(cnt_in_DCO_reg[3]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[4]_i_1_n_7 ),
        .Q(cnt_in_DCO_reg[4]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  CARRY4 \cnt_in_DCO_reg[4]_i_1 
       (.CI(\cnt_in_DCO_reg[0]_i_2_n_0 ),
        .CO({\cnt_in_DCO_reg[4]_i_1_n_0 ,\cnt_in_DCO_reg[4]_i_1_n_1 ,\cnt_in_DCO_reg[4]_i_1_n_2 ,\cnt_in_DCO_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_in_DCO_reg[4]_i_1_n_4 ,\cnt_in_DCO_reg[4]_i_1_n_5 ,\cnt_in_DCO_reg[4]_i_1_n_6 ,\cnt_in_DCO_reg[4]_i_1_n_7 }),
        .S(cnt_in_DCO_reg[7:4]));
  FDRE \cnt_in_DCO_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[4]_i_1_n_6 ),
        .Q(cnt_in_DCO_reg[5]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[4]_i_1_n_5 ),
        .Q(cnt_in_DCO_reg[6]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[4]_i_1_n_4 ),
        .Q(cnt_in_DCO_reg[7]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[8]_i_1_n_7 ),
        .Q(cnt_in_DCO_reg[8]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  CARRY4 \cnt_in_DCO_reg[8]_i_1 
       (.CI(\cnt_in_DCO_reg[4]_i_1_n_0 ),
        .CO({\cnt_in_DCO_reg[8]_i_1_n_0 ,\cnt_in_DCO_reg[8]_i_1_n_1 ,\cnt_in_DCO_reg[8]_i_1_n_2 ,\cnt_in_DCO_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_in_DCO_reg[8]_i_1_n_4 ,\cnt_in_DCO_reg[8]_i_1_n_5 ,\cnt_in_DCO_reg[8]_i_1_n_6 ,\cnt_in_DCO_reg[8]_i_1_n_7 }),
        .S(cnt_in_DCO_reg[11:8]));
  FDRE \cnt_in_DCO_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[8]_i_1_n_6 ),
        .Q(cnt_in_DCO_reg[9]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[0]_INST_0 
       (.I0(s01_fft_axis_tdata[0]),
        .I1(s00_fft_axis_tdata[0]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[0]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[10]_INST_0 
       (.I0(s01_fft_axis_tdata[10]),
        .I1(s00_fft_axis_tdata[10]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[10]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[11]_INST_0 
       (.I0(s01_fft_axis_tdata[11]),
        .I1(s00_fft_axis_tdata[11]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[11]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[12]_INST_0 
       (.I0(s01_fft_axis_tdata[12]),
        .I1(s00_fft_axis_tdata[12]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[12]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[13]_INST_0 
       (.I0(s01_fft_axis_tdata[13]),
        .I1(s00_fft_axis_tdata[13]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[13]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[14]_INST_0 
       (.I0(s01_fft_axis_tdata[14]),
        .I1(s00_fft_axis_tdata[14]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[14]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[15]_INST_0 
       (.I0(s01_fft_axis_tdata[15]),
        .I1(s00_fft_axis_tdata[15]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[15]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[16]_INST_0 
       (.I0(s01_fft_axis_tdata[16]),
        .I1(s00_fft_axis_tdata[16]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[16]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[17]_INST_0 
       (.I0(s01_fft_axis_tdata[17]),
        .I1(s00_fft_axis_tdata[17]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[17]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[18]_INST_0 
       (.I0(s01_fft_axis_tdata[18]),
        .I1(s00_fft_axis_tdata[18]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[18]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[19]_INST_0 
       (.I0(s01_fft_axis_tdata[19]),
        .I1(s00_fft_axis_tdata[19]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[19]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[1]_INST_0 
       (.I0(s01_fft_axis_tdata[1]),
        .I1(s00_fft_axis_tdata[1]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[1]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[20]_INST_0 
       (.I0(s01_fft_axis_tdata[20]),
        .I1(s00_fft_axis_tdata[20]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[20]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[21]_INST_0 
       (.I0(s01_fft_axis_tdata[21]),
        .I1(s00_fft_axis_tdata[21]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[21]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[22]_INST_0 
       (.I0(s01_fft_axis_tdata[22]),
        .I1(s00_fft_axis_tdata[22]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[22]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[23]_INST_0 
       (.I0(s01_fft_axis_tdata[23]),
        .I1(s00_fft_axis_tdata[23]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[23]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[24]_INST_0 
       (.I0(s01_fft_axis_tdata[24]),
        .I1(s00_fft_axis_tdata[24]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[24]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[25]_INST_0 
       (.I0(s01_fft_axis_tdata[25]),
        .I1(s00_fft_axis_tdata[25]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[25]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[26]_INST_0 
       (.I0(s01_fft_axis_tdata[26]),
        .I1(s00_fft_axis_tdata[26]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[26]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[27]_INST_0 
       (.I0(s01_fft_axis_tdata[27]),
        .I1(s00_fft_axis_tdata[27]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[27]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[28]_INST_0 
       (.I0(s01_fft_axis_tdata[28]),
        .I1(s00_fft_axis_tdata[28]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[28]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[29]_INST_0 
       (.I0(s01_fft_axis_tdata[29]),
        .I1(s00_fft_axis_tdata[29]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[29]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[2]_INST_0 
       (.I0(s01_fft_axis_tdata[2]),
        .I1(s00_fft_axis_tdata[2]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[2]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[30]_INST_0 
       (.I0(s01_fft_axis_tdata[30]),
        .I1(s00_fft_axis_tdata[30]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[30]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[31]_INST_0 
       (.I0(s01_fft_axis_tdata[31]),
        .I1(s00_fft_axis_tdata[31]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[31]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[32]_INST_0 
       (.I0(s01_fft_axis_tdata[32]),
        .I1(s00_fft_axis_tdata[32]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[32]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[33]_INST_0 
       (.I0(s01_fft_axis_tdata[33]),
        .I1(s00_fft_axis_tdata[33]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[33]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[34]_INST_0 
       (.I0(s01_fft_axis_tdata[34]),
        .I1(s00_fft_axis_tdata[34]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[34]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[35]_INST_0 
       (.I0(s01_fft_axis_tdata[35]),
        .I1(s00_fft_axis_tdata[35]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[35]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[36]_INST_0 
       (.I0(s01_fft_axis_tdata[36]),
        .I1(s00_fft_axis_tdata[36]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[36]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[37]_INST_0 
       (.I0(s01_fft_axis_tdata[37]),
        .I1(s00_fft_axis_tdata[37]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[37]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[38]_INST_0 
       (.I0(s01_fft_axis_tdata[38]),
        .I1(s00_fft_axis_tdata[38]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[38]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[39]_INST_0 
       (.I0(s01_fft_axis_tdata[39]),
        .I1(s00_fft_axis_tdata[39]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[39]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[3]_INST_0 
       (.I0(s01_fft_axis_tdata[3]),
        .I1(s00_fft_axis_tdata[3]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[3]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[40]_INST_0 
       (.I0(s01_fft_axis_tdata[40]),
        .I1(s00_fft_axis_tdata[40]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[40]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[41]_INST_0 
       (.I0(s01_fft_axis_tdata[41]),
        .I1(s00_fft_axis_tdata[41]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[41]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[42]_INST_0 
       (.I0(s01_fft_axis_tdata[42]),
        .I1(s00_fft_axis_tdata[42]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[42]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[43]_INST_0 
       (.I0(s01_fft_axis_tdata[43]),
        .I1(s00_fft_axis_tdata[43]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[43]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[44]_INST_0 
       (.I0(s01_fft_axis_tdata[44]),
        .I1(s00_fft_axis_tdata[44]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[44]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[45]_INST_0 
       (.I0(s01_fft_axis_tdata[45]),
        .I1(s00_fft_axis_tdata[45]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[45]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[46]_INST_0 
       (.I0(s01_fft_axis_tdata[46]),
        .I1(s00_fft_axis_tdata[46]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[46]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[47]_INST_0 
       (.I0(s01_fft_axis_tdata[47]),
        .I1(s00_fft_axis_tdata[47]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[47]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[48]_INST_0 
       (.I0(s01_fft_axis_tdata[48]),
        .I1(s00_fft_axis_tdata[48]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[48]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[49]_INST_0 
       (.I0(s01_fft_axis_tdata[49]),
        .I1(s00_fft_axis_tdata[49]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[49]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[4]_INST_0 
       (.I0(s01_fft_axis_tdata[4]),
        .I1(s00_fft_axis_tdata[4]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[4]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[50]_INST_0 
       (.I0(s01_fft_axis_tdata[50]),
        .I1(s00_fft_axis_tdata[50]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[50]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[51]_INST_0 
       (.I0(s01_fft_axis_tdata[51]),
        .I1(s00_fft_axis_tdata[51]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[51]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[52]_INST_0 
       (.I0(s01_fft_axis_tdata[52]),
        .I1(s00_fft_axis_tdata[52]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[52]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[53]_INST_0 
       (.I0(s01_fft_axis_tdata[53]),
        .I1(s00_fft_axis_tdata[53]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[53]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[54]_INST_0 
       (.I0(s01_fft_axis_tdata[54]),
        .I1(s00_fft_axis_tdata[54]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[54]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[55]_INST_0 
       (.I0(s01_fft_axis_tdata[55]),
        .I1(s00_fft_axis_tdata[55]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[55]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[56]_INST_0 
       (.I0(s01_fft_axis_tdata[56]),
        .I1(s00_fft_axis_tdata[56]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[56]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[57]_INST_0 
       (.I0(s01_fft_axis_tdata[57]),
        .I1(s00_fft_axis_tdata[57]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[57]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[58]_INST_0 
       (.I0(s01_fft_axis_tdata[58]),
        .I1(s00_fft_axis_tdata[58]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[58]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[59]_INST_0 
       (.I0(s01_fft_axis_tdata[59]),
        .I1(s00_fft_axis_tdata[59]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[59]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[5]_INST_0 
       (.I0(s01_fft_axis_tdata[5]),
        .I1(s00_fft_axis_tdata[5]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[5]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[60]_INST_0 
       (.I0(s01_fft_axis_tdata[60]),
        .I1(s00_fft_axis_tdata[60]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[60]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[61]_INST_0 
       (.I0(s01_fft_axis_tdata[61]),
        .I1(s00_fft_axis_tdata[61]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[61]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[62]_INST_0 
       (.I0(s01_fft_axis_tdata[62]),
        .I1(s00_fft_axis_tdata[62]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[62]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[63]_INST_0 
       (.I0(s01_fft_axis_tdata[63]),
        .I1(s00_fft_axis_tdata[63]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[63]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[6]_INST_0 
       (.I0(s01_fft_axis_tdata[6]),
        .I1(s00_fft_axis_tdata[6]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[6]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[7]_INST_0 
       (.I0(s01_fft_axis_tdata[7]),
        .I1(s00_fft_axis_tdata[7]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[7]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[8]_INST_0 
       (.I0(s01_fft_axis_tdata[8]),
        .I1(s00_fft_axis_tdata[8]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[8]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \m00_dma_axis_tdata[9]_INST_0 
       (.I0(s01_fft_axis_tdata[9]),
        .I1(s00_fft_axis_tdata[9]),
        .I2(mux_fft_out_reg_n_0),
        .O(m00_dma_axis_tdata[9]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    m00_dma_axis_tlast_INST_0
       (.I0(s01_fft_axis_tlast),
        .I1(mux_fft_out_reg_n_0),
        .I2(s00_fft_axis_tlast),
        .O(m00_dma_axis_tlast));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    m00_dma_axis_tvalid_INST_0
       (.I0(s01_fft_axis_tvalid),
        .I1(mux_fft_out_reg_n_0),
        .I2(s00_fft_axis_tvalid),
        .O(m00_dma_axis_tvalid));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    m00_fft_axis_tlast_r_i_1
       (.I0(m00_fft_axis_tvalid_r_i_2_n_0),
        .I1(cnt_DCO[13]),
        .I2(m00_fft_axis_tvalid_r_i_3_n_0),
        .I3(mux_fft_in),
        .O(m00_fft_axis_tlast_r_i_1_n_0));
  FDRE m00_fft_axis_tlast_r_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(m00_fft_axis_tlast_r_i_1_n_0),
        .Q(m00_fft_axis_tlast),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'h2800)) 
    m00_fft_axis_tvalid_r_i_1
       (.I0(m00_fft_axis_tvalid_r_i_2_n_0),
        .I1(m00_fft_axis_tvalid_r_i_3_n_0),
        .I2(cnt_DCO[13]),
        .I3(mux_fft_in),
        .O(m00_fft_axis_tvalid_r_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    m00_fft_axis_tvalid_r_i_2
       (.I0(m00_fft_axis_tvalid_r_i_4_n_0),
        .I1(m00_fft_axis_tvalid_r_i_5_n_0),
        .I2(cnt_in_DCO_reg[0]),
        .I3(cnt_in_DCO_reg[15]),
        .I4(cnt_in_DCO_reg[11]),
        .I5(cnt_in_DCO_reg[5]),
        .O(m00_fft_axis_tvalid_r_i_2_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_fft_axis_tvalid_r_i_3
       (.I0(m00_fft_axis_tvalid_r_i_6_n_0),
        .I1(cnt_DCO[12]),
        .I2(cnt_DCO[1]),
        .I3(cnt_DCO[0]),
        .O(m00_fft_axis_tvalid_r_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    m00_fft_axis_tvalid_r_i_4
       (.I0(cnt_DCO[14]),
        .I1(cnt_DCO[15]),
        .I2(cnt_in_DCO_reg[1]),
        .I3(cnt_in_DCO_reg[7]),
        .I4(cnt_in_DCO_reg[3]),
        .I5(cnt_in_DCO_reg[13]),
        .O(m00_fft_axis_tvalid_r_i_4_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    m00_fft_axis_tvalid_r_i_5
       (.I0(cnt_in_DCO_reg[6]),
        .I1(cnt_in_DCO_reg[8]),
        .I2(cnt_in_DCO_reg[10]),
        .I3(cnt_in_DCO_reg[12]),
        .I4(m00_fft_axis_tvalid_r_i_7_n_0),
        .O(m00_fft_axis_tvalid_r_i_5_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    m00_fft_axis_tvalid_r_i_6
       (.I0(m00_fft_axis_tvalid_r_i_8_n_0),
        .I1(cnt_DCO[4]),
        .I2(cnt_DCO[8]),
        .I3(cnt_DCO[2]),
        .I4(cnt_DCO[9]),
        .O(m00_fft_axis_tvalid_r_i_6_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_fft_axis_tvalid_r_i_7
       (.I0(cnt_in_DCO_reg[14]),
        .I1(cnt_in_DCO_reg[2]),
        .I2(cnt_in_DCO_reg[9]),
        .I3(cnt_in_DCO_reg[4]),
        .O(m00_fft_axis_tvalid_r_i_7_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    m00_fft_axis_tvalid_r_i_8
       (.I0(cnt_DCO[7]),
        .I1(cnt_DCO[5]),
        .I2(cnt_DCO[3]),
        .I3(cnt_DCO[6]),
        .I4(cnt_DCO[10]),
        .I5(cnt_DCO[11]),
        .O(m00_fft_axis_tvalid_r_i_8_n_0));
  FDRE m00_fft_axis_tvalid_r_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(m00_fft_axis_tvalid_r_i_1_n_0),
        .Q(m00_fft_axis_tvalid),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[0]_INST_0 
       (.I0(DATA_INA[0]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[0]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[10]_INST_0 
       (.I0(DATA_INA[10]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[10]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[11]_INST_0 
       (.I0(DATA_INA[11]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[11]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[12]_INST_0 
       (.I0(DATA_INA[12]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[12]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[13]_INST_0 
       (.I0(DATA_INA[13]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[13]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[14]_INST_0 
       (.I0(DATA_INA[14]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[14]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[15]_INST_0 
       (.I0(DATA_INA[15]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[15]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[16]_INST_0 
       (.I0(DATA_INB[0]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[16]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[17]_INST_0 
       (.I0(DATA_INB[1]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[17]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[18]_INST_0 
       (.I0(DATA_INB[2]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[18]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[19]_INST_0 
       (.I0(DATA_INB[3]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[19]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[1]_INST_0 
       (.I0(DATA_INA[1]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[1]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[20]_INST_0 
       (.I0(DATA_INB[4]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[20]));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[21]_INST_0 
       (.I0(DATA_INB[5]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[21]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[22]_INST_0 
       (.I0(DATA_INB[6]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[22]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[23]_INST_0 
       (.I0(DATA_INB[7]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[23]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[24]_INST_0 
       (.I0(DATA_INB[8]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[24]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[25]_INST_0 
       (.I0(DATA_INB[9]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[25]));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[26]_INST_0 
       (.I0(DATA_INB[10]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[26]));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[27]_INST_0 
       (.I0(DATA_INB[11]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[27]));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[28]_INST_0 
       (.I0(DATA_INB[12]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[28]));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[29]_INST_0 
       (.I0(DATA_INB[13]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[29]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[2]_INST_0 
       (.I0(DATA_INA[2]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[2]));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[30]_INST_0 
       (.I0(DATA_INB[14]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[30]));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[31]_INST_0 
       (.I0(DATA_INB[15]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[31]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[3]_INST_0 
       (.I0(DATA_INA[3]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[3]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[4]_INST_0 
       (.I0(DATA_INA[4]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[4]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[5]_INST_0 
       (.I0(DATA_INA[5]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[5]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[6]_INST_0 
       (.I0(DATA_INA[6]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[6]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[7]_INST_0 
       (.I0(DATA_INA[7]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[7]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[8]_INST_0 
       (.I0(DATA_INA[8]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[8]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[9]_INST_0 
       (.I0(DATA_INA[9]),
        .I1(mux_fft_in),
        .O(m01_fft_axis_tdata[9]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    m01_fft_axis_tlast_r_i_1
       (.I0(m00_fft_axis_tvalid_r_i_2_n_0),
        .I1(cnt_DCO[13]),
        .I2(m00_fft_axis_tvalid_r_i_3_n_0),
        .I3(mux_fft_in),
        .O(m01_fft_axis_tlast_r_i_1_n_0));
  FDRE m01_fft_axis_tlast_r_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(m01_fft_axis_tlast_r_i_1_n_0),
        .Q(m01_fft_axis_tlast),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'h0028)) 
    m01_fft_axis_tvalid_r_i_1
       (.I0(m00_fft_axis_tvalid_r_i_2_n_0),
        .I1(m00_fft_axis_tvalid_r_i_3_n_0),
        .I2(cnt_DCO[13]),
        .I3(mux_fft_in),
        .O(m01_fft_axis_tvalid_r_i_1_n_0));
  FDRE m01_fft_axis_tvalid_r_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(m01_fft_axis_tvalid_r_i_1_n_0),
        .Q(m01_fft_axis_tvalid),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAAA9A00000000)) 
    mux_fft_out_i_1
       (.I0(mux_fft_out_reg_n_0),
        .I1(mux_fft_out_i_2_n_0),
        .I2(mux_fft_out_i_3_n_0),
        .I3(mux_fft_out_i_4_n_0),
        .I4(mux_fft_out_i_5_n_0),
        .I5(s00_axi_aresetn),
        .O(mux_fft_out_i_1_n_0));
  LUT4 #(
    .INIT(16'hFFDF)) 
    mux_fft_out_i_2
       (.I0(cnt_allowed_clk_reg[9]),
        .I1(cnt_allowed_clk_reg[3]),
        .I2(cnt_allowed_clk_reg[10]),
        .I3(cnt_allowed_clk_reg[13]),
        .O(mux_fft_out_i_2_n_0));
  LUT4 #(
    .INIT(16'h0004)) 
    mux_fft_out_i_3
       (.I0(cnt_allowed_clk_reg[0]),
        .I1(cnt_allowed_clk_reg[7]),
        .I2(cnt_allowed_clk_reg[14]),
        .I3(cnt_allowed_clk_reg[2]),
        .O(mux_fft_out_i_3_n_0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    mux_fft_out_i_4
       (.I0(cnt_allowed_clk_reg[8]),
        .I1(cnt_allowed_clk_reg[15]),
        .I2(cnt_allowed_clk_reg[11]),
        .I3(cnt_allowed_clk_reg[5]),
        .O(mux_fft_out_i_4_n_0));
  LUT4 #(
    .INIT(16'hFFF7)) 
    mux_fft_out_i_5
       (.I0(cnt_allowed_clk_reg[6]),
        .I1(cnt_allowed_clk_reg[4]),
        .I2(cnt_allowed_clk_reg[12]),
        .I3(cnt_allowed_clk_reg[1]),
        .O(mux_fft_out_i_5_n_0));
  FDRE mux_fft_out_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(mux_fft_out_i_1_n_0),
        .Q(mux_fft_out_reg_n_0),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_AD9650 spi_AD9650_inst
       (.DATA_RX_r(DATA_RX_r),
        .DATA_TX_serial_r_i_3_0(AD9650_v1_0_S00_AXI_inst_n_12),
        .DATA_TX_serial_r_i_3_1(AD9650_v1_0_S00_AXI_inst_n_8),
        .DATA_TX_serial_r_reg_0(AD9650_v1_0_S00_AXI_inst_n_7),
        .DATA_TX_serial_r_reg_1(AD9650_v1_0_S00_AXI_inst_n_6),
        .DATA_TX_serial_r_reg_2(AD9650_v1_0_S00_AXI_inst_n_9),
        .Q(cnt_reg),
        .clk_10MHz(clk_10MHz),
        .\cnt_re_reg[0]_0 (AD9650_v1_0_S00_AXI_inst_n_14),
        .\cnt_reg[2]_0 (spi_AD9650_inst_n_3),
        .data4(data4),
        .s00_axi_aclk(s00_axi_aclk),
        .start_sync_reg_0(slv_reg1));
endmodule

(* CHECK_LICENSE_TYPE = "design_2_AD9650_0_0,AD9650_v2_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "AD9650_v2_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk_10MHz,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    m00_fft_axis_tvalid,
    m00_fft_axis_tdata,
    m00_fft_axis_tstrb,
    m00_fft_axis_tlast,
    m00_fft_axis_tready,
    m01_fft_axis_tvalid,
    m01_fft_axis_tdata,
    m01_fft_axis_tstrb,
    m01_fft_axis_tlast,
    m01_fft_axis_tready,
    s00_fft_axis_tvalid,
    s00_fft_axis_tdata,
    s00_fft_axis_tstrb,
    s00_fft_axis_tlast,
    s00_fft_axis_tready,
    s01_fft_axis_tvalid,
    s01_fft_axis_tdata,
    s01_fft_axis_tstrb,
    s01_fft_axis_tlast,
    s01_fft_axis_tready,
    m00_dma_axis_tvalid,
    m00_dma_axis_tdata,
    m00_dma_axis_tstrb,
    m00_dma_axis_tlast,
    m00_dma_axis_tready,
    dco_or_dcoa,
    dco_or_dcob,
    dco_or_ora,
    dco_or_orb,
    ADC_PDwN,
    SYNC,
    DATA_INA,
    DATA_INB,
    allowed_clk,
    mux_fft_in,
    s00_axi_aclk,
    s00_axi_aresetn,
    DCO);
  input clk_10MHz;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) input [5:0]s00_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [5:0]s00_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input s00_axi_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TVALID" *) output m00_fft_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TDATA" *) output [31:0]m00_fft_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TSTRB" *) output [3:0]m00_fft_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TLAST" *) output m00_fft_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_fft_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_fft_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TVALID" *) output m01_fft_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TDATA" *) output [31:0]m01_fft_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TSTRB" *) output [3:0]m01_fft_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TLAST" *) output m01_fft_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m01_fft_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA undef, INSERT_VIP 0" *) input m01_fft_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_fft_axis TVALID" *) input s00_fft_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_fft_axis TDATA" *) input [63:0]s00_fft_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_fft_axis TSTRB" *) input [7:0]s00_fft_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_fft_axis TLAST" *) input s00_fft_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_fft_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_fft_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524286} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value chan} size {attribs {resolve_type generated dependency chan_size format long minimum {} maximum {}} value 1} stride {attribs {resolve_type generated dependency chan_stride format long minimum {} maximum {}} value 64} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524286} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} struct {field_xn_re {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value xn_re} enabled {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524254} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency frame_size format long minimum {} maximum {}} value 8192} stride {attribs {resolve_type generated dependency frame_stride format long minimum {} maximum {}} value 64} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency xn_width format long minimum {} maximum {}} value 30} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type generated dependency xn_fractwidth format long minimum {} maximum {}} value 15} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}} field_xn_im {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value xn_im} enabled {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524254} bitoffset {attribs {resolve_type generated dependency xn_im_offset format long minimum {} maximum {}} value 32} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency frame_size format long minimum {} maximum {}} value 8192} stride {attribs {resolve_type generated dependency frame_stride format long minimum {} maximum {}} value 64} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency xn_width format long minimum {} maximum {}} value 30} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type generated dependency xn_fractwidth format long minimum {} maximum {}} value 15} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}}}}}}} TDATA_WIDTH 64 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 0} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} struct {field_xk_index {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value xk_index} enabled {attribs {resolve_type generated dependency xk_index_enabled format bool minimum {} maximum {}} value false} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency xk_index_width format long minimum {} maximum {}} value 0} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} field_blk_exp {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value blk_exp} enabled {attribs {resolve_type generated dependency blk_exp_enabled format bool minimum {} maximum {}} value false} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type generated dependency blk_exp_offset format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency chan_size format long minimum {} maximum {}} value 1} stride {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 8} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}} field_ovflo {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value ovflo} enabled {attribs {resolve_type generated dependency ovflo_enabled format bool minimum {} maximum {}} value false} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type generated dependency ovflo_offset format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency chan_size format long minimum {} maximum {}} value 1} stride {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}}}} TUSER_WIDTH 0}, INSERT_VIP 0" *) output s00_fft_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s01_fft_axis TVALID" *) input s01_fft_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s01_fft_axis TDATA" *) input [63:0]s01_fft_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s01_fft_axis TSTRB" *) input [7:0]s01_fft_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s01_fft_axis TLAST" *) input s01_fft_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s01_fft_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s01_fft_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524286} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value chan} size {attribs {resolve_type generated dependency chan_size format long minimum {} maximum {}} value 1} stride {attribs {resolve_type generated dependency chan_stride format long minimum {} maximum {}} value 64} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524286} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} struct {field_xn_re {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value xn_re} enabled {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524254} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency frame_size format long minimum {} maximum {}} value 8192} stride {attribs {resolve_type generated dependency frame_stride format long minimum {} maximum {}} value 64} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency xn_width format long minimum {} maximum {}} value 30} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type generated dependency xn_fractwidth format long minimum {} maximum {}} value 15} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}} field_xn_im {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value xn_im} enabled {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524254} bitoffset {attribs {resolve_type generated dependency xn_im_offset format long minimum {} maximum {}} value 32} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency frame_size format long minimum {} maximum {}} value 8192} stride {attribs {resolve_type generated dependency frame_stride format long minimum {} maximum {}} value 64} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency xn_width format long minimum {} maximum {}} value 30} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type generated dependency xn_fractwidth format long minimum {} maximum {}} value 15} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}}}}}}} TDATA_WIDTH 64 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 0} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} struct {field_xk_index {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value xk_index} enabled {attribs {resolve_type generated dependency xk_index_enabled format bool minimum {} maximum {}} value false} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency xk_index_width format long minimum {} maximum {}} value 0} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} field_blk_exp {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value blk_exp} enabled {attribs {resolve_type generated dependency blk_exp_enabled format bool minimum {} maximum {}} value false} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type generated dependency blk_exp_offset format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency chan_size format long minimum {} maximum {}} value 1} stride {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 8} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}} field_ovflo {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value ovflo} enabled {attribs {resolve_type generated dependency ovflo_enabled format bool minimum {} maximum {}} value false} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type generated dependency ovflo_offset format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency chan_size format long minimum {} maximum {}} value 1} stride {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}}}} TUSER_WIDTH 0}, INSERT_VIP 0" *) output s01_fft_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_dma_axis TVALID" *) output m00_dma_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_dma_axis TDATA" *) output [63:0]m00_dma_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_dma_axis TSTRB" *) output [7:0]m00_dma_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_dma_axis TLAST" *) output m00_dma_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_dma_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_dma_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_dma_axis_tready;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR dcoa" *) input dco_or_dcoa;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR dcob" *) input dco_or_dcob;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR ora" *) input dco_or_ora;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR orb" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME DCO_OR, SV_INTERFACE true" *) input dco_or_orb;
  output ADC_PDwN;
  output SYNC;
  input [15:0]DATA_INA;
  input [15:0]DATA_INB;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 allowed_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input allowed_clk;
  input mux_fft_in;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI:m01_fft_axis:m00_fft_axis:m00_dma_axis:s01_fft_axis:s00_fft_axis, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, INSERT_VIP 0" *) input s00_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;
  output DCO;

  wire \<const0> ;
  wire \<const1> ;
  wire ADC_PDwN;
  wire [15:0]DATA_INA;
  wire [15:0]DATA_INB;
  wire SYNC;
  wire allowed_clk;
  wire clk_10MHz;
  wire dco_or_dcoa;
  wire [63:0]m00_dma_axis_tdata;
  wire m00_dma_axis_tlast;
  wire m00_dma_axis_tvalid;
  wire [31:0]m00_fft_axis_tdata;
  wire m00_fft_axis_tlast;
  wire m00_fft_axis_tvalid;
  wire [31:0]m01_fft_axis_tdata;
  wire m01_fft_axis_tlast;
  wire m01_fft_axis_tvalid;
  wire mux_fft_in;
  wire s00_axi_aclk;
  wire [5:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [5:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [63:0]s00_fft_axis_tdata;
  wire s00_fft_axis_tlast;
  wire s00_fft_axis_tvalid;
  wire [63:0]s01_fft_axis_tdata;
  wire s01_fft_axis_tlast;
  wire s01_fft_axis_tvalid;

  assign DCO = dco_or_dcoa;
  assign m00_fft_axis_tstrb[3] = \<const1> ;
  assign m00_fft_axis_tstrb[2] = \<const1> ;
  assign m00_fft_axis_tstrb[1] = \<const1> ;
  assign m00_fft_axis_tstrb[0] = \<const1> ;
  assign m01_fft_axis_tstrb[3] = \<const1> ;
  assign m01_fft_axis_tstrb[2] = \<const1> ;
  assign m01_fft_axis_tstrb[1] = \<const1> ;
  assign m01_fft_axis_tstrb[0] = \<const1> ;
  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  assign s00_fft_axis_tready = \<const1> ;
  assign s01_fft_axis_tready = \<const1> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0 inst
       (.ADC_PDwN(ADC_PDwN),
        .DATA_INA(DATA_INA),
        .DATA_INB(DATA_INB),
        .SYNC(SYNC),
        .allowed_clk(allowed_clk),
        .clk_10MHz(clk_10MHz),
        .dco_or_dcoa(dco_or_dcoa),
        .m00_dma_axis_tdata(m00_dma_axis_tdata),
        .m00_dma_axis_tlast(m00_dma_axis_tlast),
        .m00_dma_axis_tvalid(m00_dma_axis_tvalid),
        .m00_fft_axis_tlast(m00_fft_axis_tlast),
        .m00_fft_axis_tvalid(m00_fft_axis_tvalid),
        .m01_fft_axis_tdata(m01_fft_axis_tdata),
        .m01_fft_axis_tlast(m01_fft_axis_tlast),
        .m01_fft_axis_tvalid(m01_fft_axis_tvalid),
        .mux_fft_in(mux_fft_in),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[4:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arready(s00_axi_arready),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[4:2]),
        .s00_axi_awready(s00_axi_awready),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wready(s00_axi_wready),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .s00_fft_axis_tdata(s00_fft_axis_tdata),
        .s00_fft_axis_tlast(s00_fft_axis_tlast),
        .s00_fft_axis_tvalid(s00_fft_axis_tvalid),
        .s01_fft_axis_tdata(s01_fft_axis_tdata),
        .s01_fft_axis_tlast(s01_fft_axis_tlast),
        .s01_fft_axis_tvalid(s01_fft_axis_tvalid));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[0]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[0]),
        .O(m00_fft_axis_tdata[0]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[10]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[10]),
        .O(m00_fft_axis_tdata[10]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[11]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[11]),
        .O(m00_fft_axis_tdata[11]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[12]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[12]),
        .O(m00_fft_axis_tdata[12]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[13]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[13]),
        .O(m00_fft_axis_tdata[13]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[14]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[14]),
        .O(m00_fft_axis_tdata[14]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[15]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[15]),
        .O(m00_fft_axis_tdata[15]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[16]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[0]),
        .O(m00_fft_axis_tdata[16]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[17]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[1]),
        .O(m00_fft_axis_tdata[17]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[18]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[2]),
        .O(m00_fft_axis_tdata[18]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[19]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[3]),
        .O(m00_fft_axis_tdata[19]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[1]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[1]),
        .O(m00_fft_axis_tdata[1]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[20]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[4]),
        .O(m00_fft_axis_tdata[20]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[21]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[5]),
        .O(m00_fft_axis_tdata[21]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[22]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[6]),
        .O(m00_fft_axis_tdata[22]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[23]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[7]),
        .O(m00_fft_axis_tdata[23]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[24]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[8]),
        .O(m00_fft_axis_tdata[24]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[25]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[9]),
        .O(m00_fft_axis_tdata[25]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[26]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[10]),
        .O(m00_fft_axis_tdata[26]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[27]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[11]),
        .O(m00_fft_axis_tdata[27]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[28]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[12]),
        .O(m00_fft_axis_tdata[28]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[29]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[13]),
        .O(m00_fft_axis_tdata[29]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[2]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[2]),
        .O(m00_fft_axis_tdata[2]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[30]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[14]),
        .O(m00_fft_axis_tdata[30]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[31]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INB[15]),
        .O(m00_fft_axis_tdata[31]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[3]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[3]),
        .O(m00_fft_axis_tdata[3]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[4]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[4]),
        .O(m00_fft_axis_tdata[4]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[5]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[5]),
        .O(m00_fft_axis_tdata[5]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[6]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[6]),
        .O(m00_fft_axis_tdata[6]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[7]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[7]),
        .O(m00_fft_axis_tdata[7]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[8]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[8]),
        .O(m00_fft_axis_tdata[8]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[9]_INST_0 
       (.I0(mux_fft_in),
        .I1(DATA_INA[9]),
        .O(m00_fft_axis_tdata[9]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_AD9650
   (data4,
    Q,
    \cnt_reg[2]_0 ,
    DATA_RX_r,
    s00_axi_aclk,
    start_sync_reg_0,
    clk_10MHz,
    \cnt_re_reg[0]_0 ,
    DATA_TX_serial_r_reg_0,
    DATA_TX_serial_r_reg_1,
    DATA_TX_serial_r_reg_2,
    DATA_TX_serial_r_i_3_0,
    DATA_TX_serial_r_i_3_1);
  output [0:0]data4;
  output [1:0]Q;
  output \cnt_reg[2]_0 ;
  output [7:0]DATA_RX_r;
  input s00_axi_aclk;
  input [0:0]start_sync_reg_0;
  input clk_10MHz;
  input [0:0]\cnt_re_reg[0]_0 ;
  input DATA_TX_serial_r_reg_0;
  input DATA_TX_serial_r_reg_1;
  input DATA_TX_serial_r_reg_2;
  input [0:0]DATA_TX_serial_r_i_3_0;
  input DATA_TX_serial_r_i_3_1;

  wire [7:0]DATA_RX_r;
  wire \DATA_RX_r1_inferred__0/i__carry__0_n_0 ;
  wire \DATA_RX_r1_inferred__0/i__carry__0_n_1 ;
  wire \DATA_RX_r1_inferred__0/i__carry__0_n_2 ;
  wire \DATA_RX_r1_inferred__0/i__carry__0_n_3 ;
  wire \DATA_RX_r1_inferred__0/i__carry__0_n_4 ;
  wire \DATA_RX_r1_inferred__0/i__carry__0_n_5 ;
  wire \DATA_RX_r1_inferred__0/i__carry__0_n_6 ;
  wire \DATA_RX_r1_inferred__0/i__carry__0_n_7 ;
  wire \DATA_RX_r1_inferred__0/i__carry__1_n_0 ;
  wire \DATA_RX_r1_inferred__0/i__carry__1_n_1 ;
  wire \DATA_RX_r1_inferred__0/i__carry__1_n_2 ;
  wire \DATA_RX_r1_inferred__0/i__carry__1_n_3 ;
  wire \DATA_RX_r1_inferred__0/i__carry__1_n_4 ;
  wire \DATA_RX_r1_inferred__0/i__carry__1_n_5 ;
  wire \DATA_RX_r1_inferred__0/i__carry__1_n_6 ;
  wire \DATA_RX_r1_inferred__0/i__carry__1_n_7 ;
  wire \DATA_RX_r1_inferred__0/i__carry__2_n_0 ;
  wire \DATA_RX_r1_inferred__0/i__carry__2_n_1 ;
  wire \DATA_RX_r1_inferred__0/i__carry__2_n_2 ;
  wire \DATA_RX_r1_inferred__0/i__carry__2_n_3 ;
  wire \DATA_RX_r1_inferred__0/i__carry__2_n_4 ;
  wire \DATA_RX_r1_inferred__0/i__carry__2_n_5 ;
  wire \DATA_RX_r1_inferred__0/i__carry__2_n_6 ;
  wire \DATA_RX_r1_inferred__0/i__carry__2_n_7 ;
  wire \DATA_RX_r1_inferred__0/i__carry_n_0 ;
  wire \DATA_RX_r1_inferred__0/i__carry_n_1 ;
  wire \DATA_RX_r1_inferred__0/i__carry_n_2 ;
  wire \DATA_RX_r1_inferred__0/i__carry_n_3 ;
  wire \DATA_RX_r1_inferred__0/i__carry_n_4 ;
  wire \DATA_RX_r1_inferred__0/i__carry_n_5 ;
  wire \DATA_RX_r1_inferred__0/i__carry_n_6 ;
  wire \DATA_RX_r[0]_i_1_n_0 ;
  wire \DATA_RX_r[1]_i_1_n_0 ;
  wire \DATA_RX_r[2]_i_1_n_0 ;
  wire \DATA_RX_r[3]_i_1_n_0 ;
  wire \DATA_RX_r[3]_i_2_n_0 ;
  wire \DATA_RX_r[4]_i_1_n_0 ;
  wire \DATA_RX_r[4]_i_2_n_0 ;
  wire \DATA_RX_r[5]_i_1_n_0 ;
  wire \DATA_RX_r[5]_i_2_n_0 ;
  wire \DATA_RX_r[6]_i_1_n_0 ;
  wire \DATA_RX_r[6]_i_2_n_0 ;
  wire \DATA_RX_r[7]_i_10_n_0 ;
  wire \DATA_RX_r[7]_i_1_n_0 ;
  wire \DATA_RX_r[7]_i_2_n_0 ;
  wire \DATA_RX_r[7]_i_3_n_0 ;
  wire \DATA_RX_r[7]_i_4_n_0 ;
  wire \DATA_RX_r[7]_i_5_n_0 ;
  wire \DATA_RX_r[7]_i_6_n_0 ;
  wire \DATA_RX_r[7]_i_7_n_0 ;
  wire \DATA_RX_r[7]_i_9_n_0 ;
  wire \DATA_RX_r_reg[7]_i_8_n_3 ;
  wire DATA_RX_serial;
  wire DATA_TX_serial;
  wire DATA_TX_serial_r_i_10_n_0;
  wire DATA_TX_serial_r_i_12_n_0;
  wire DATA_TX_serial_r_i_1_n_0;
  wire DATA_TX_serial_r_i_2_n_0;
  wire [0:0]DATA_TX_serial_r_i_3_0;
  wire DATA_TX_serial_r_i_3_1;
  wire DATA_TX_serial_r_i_3_n_0;
  wire DATA_TX_serial_r_i_4_n_0;
  wire DATA_TX_serial_r_i_5_n_0;
  wire DATA_TX_serial_r_i_6_n_0;
  wire DATA_TX_serial_r_i_7_n_0;
  wire DATA_TX_serial_r_i_8_n_0;
  wire DATA_TX_serial_r_i_9_n_0;
  wire DATA_TX_serial_r_reg_0;
  wire DATA_TX_serial_r_reg_1;
  wire DATA_TX_serial_r_reg_2;
  wire OBUFT_inst_n_0;
  wire [1:0]Q;
  wire clk_10MHz;
  wire \cnt[0]_i_1_n_0 ;
  wire \cnt[2]_i_1_n_0 ;
  wire \cnt[3]_i_1_n_0 ;
  wire \cnt[4]_i_1_n_0 ;
  wire \cnt[7]_i_1_n_0 ;
  wire \cnt[7]_i_3_n_0 ;
  wire \cnt[7]_i_4_n_0 ;
  wire cnt_re;
  wire \cnt_re[0]_i_3_n_0 ;
  wire \cnt_re[0]_i_4_n_0 ;
  wire \cnt_re[0]_i_5_n_0 ;
  wire \cnt_re[0]_i_6_n_0 ;
  wire [15:0]cnt_re_reg;
  wire [0:0]\cnt_re_reg[0]_0 ;
  wire \cnt_re_reg[0]_i_2_n_0 ;
  wire \cnt_re_reg[0]_i_2_n_1 ;
  wire \cnt_re_reg[0]_i_2_n_2 ;
  wire \cnt_re_reg[0]_i_2_n_3 ;
  wire \cnt_re_reg[0]_i_2_n_4 ;
  wire \cnt_re_reg[0]_i_2_n_5 ;
  wire \cnt_re_reg[0]_i_2_n_6 ;
  wire \cnt_re_reg[0]_i_2_n_7 ;
  wire \cnt_re_reg[12]_i_1_n_1 ;
  wire \cnt_re_reg[12]_i_1_n_2 ;
  wire \cnt_re_reg[12]_i_1_n_3 ;
  wire \cnt_re_reg[12]_i_1_n_4 ;
  wire \cnt_re_reg[12]_i_1_n_5 ;
  wire \cnt_re_reg[12]_i_1_n_6 ;
  wire \cnt_re_reg[12]_i_1_n_7 ;
  wire \cnt_re_reg[4]_i_1_n_0 ;
  wire \cnt_re_reg[4]_i_1_n_1 ;
  wire \cnt_re_reg[4]_i_1_n_2 ;
  wire \cnt_re_reg[4]_i_1_n_3 ;
  wire \cnt_re_reg[4]_i_1_n_4 ;
  wire \cnt_re_reg[4]_i_1_n_5 ;
  wire \cnt_re_reg[4]_i_1_n_6 ;
  wire \cnt_re_reg[4]_i_1_n_7 ;
  wire \cnt_re_reg[8]_i_1_n_0 ;
  wire \cnt_re_reg[8]_i_1_n_1 ;
  wire \cnt_re_reg[8]_i_1_n_2 ;
  wire \cnt_re_reg[8]_i_1_n_3 ;
  wire \cnt_re_reg[8]_i_1_n_4 ;
  wire \cnt_re_reg[8]_i_1_n_5 ;
  wire \cnt_re_reg[8]_i_1_n_6 ;
  wire \cnt_re_reg[8]_i_1_n_7 ;
  wire [7:2]cnt_reg;
  wire \cnt_reg[2]_0 ;
  wire [0:0]data4;
  wire data_rx_ready_i_1_n_0;
  wire data_rx_ready_i_2_n_0;
  wire data_rx_ready_i_3_n_0;
  wire data_rx_ready_i_4_n_0;
  wire data_rx_ready_i_5_n_0;
  wire enable_cnt_i_1_n_0;
  wire enable_cnt_re;
  wire i__carry__0_i_1_n_0;
  wire i__carry__0_i_2_n_0;
  wire i__carry__0_i_3_n_0;
  wire i__carry__0_i_4_n_0;
  wire i__carry__1_i_1_n_0;
  wire i__carry__1_i_2_n_0;
  wire i__carry__1_i_3_n_0;
  wire i__carry__1_i_4_n_0;
  wire i__carry__2_i_1_n_0;
  wire i__carry__2_i_2_n_0;
  wire i__carry__2_i_3_n_0;
  wire i__carry__2_i_4_n_0;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_4_n_0;
  wire [7:1]p_0_in;
  wire s00_axi_aclk;
  wire start_prev;
  wire start_prev_fe_i_1_n_0;
  wire start_prev_fe_i_2_n_0;
  wire start_prev_fe_i_3_n_0;
  wire start_prev_fe_i_4_n_0;
  wire start_sync;
  wire [0:0]start_sync_reg_0;
  wire tristate;
  wire tristate_i_1_n_0;
  wire tristate_i_2_n_0;
  wire tx_wire;
  wire [0:0]\NLW_DATA_RX_r1_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:1]\NLW_DATA_RX_r_reg[7]_i_8_CO_UNCONNECTED ;
  wire [3:0]\NLW_DATA_RX_r_reg[7]_i_8_O_UNCONNECTED ;
  wire [3:3]\NLW_cnt_re_reg[12]_i_1_CO_UNCONNECTED ;

  CARRY4 \DATA_RX_r1_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\DATA_RX_r1_inferred__0/i__carry_n_0 ,\DATA_RX_r1_inferred__0/i__carry_n_1 ,\DATA_RX_r1_inferred__0/i__carry_n_2 ,\DATA_RX_r1_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry_i_1_n_0,i__carry_i_2_n_0,i__carry_i_3_n_0,1'b0}),
        .O({\DATA_RX_r1_inferred__0/i__carry_n_4 ,\DATA_RX_r1_inferred__0/i__carry_n_5 ,\DATA_RX_r1_inferred__0/i__carry_n_6 ,\NLW_DATA_RX_r1_inferred__0/i__carry_O_UNCONNECTED [0]}),
        .S({cnt_re_reg[3:1],i__carry_i_4_n_0}));
  CARRY4 \DATA_RX_r1_inferred__0/i__carry__0 
       (.CI(\DATA_RX_r1_inferred__0/i__carry_n_0 ),
        .CO({\DATA_RX_r1_inferred__0/i__carry__0_n_0 ,\DATA_RX_r1_inferred__0/i__carry__0_n_1 ,\DATA_RX_r1_inferred__0/i__carry__0_n_2 ,\DATA_RX_r1_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,i__carry__0_i_1_n_0}),
        .O({\DATA_RX_r1_inferred__0/i__carry__0_n_4 ,\DATA_RX_r1_inferred__0/i__carry__0_n_5 ,\DATA_RX_r1_inferred__0/i__carry__0_n_6 ,\DATA_RX_r1_inferred__0/i__carry__0_n_7 }),
        .S({i__carry__0_i_2_n_0,i__carry__0_i_3_n_0,i__carry__0_i_4_n_0,cnt_re_reg[4]}));
  CARRY4 \DATA_RX_r1_inferred__0/i__carry__1 
       (.CI(\DATA_RX_r1_inferred__0/i__carry__0_n_0 ),
        .CO({\DATA_RX_r1_inferred__0/i__carry__1_n_0 ,\DATA_RX_r1_inferred__0/i__carry__1_n_1 ,\DATA_RX_r1_inferred__0/i__carry__1_n_2 ,\DATA_RX_r1_inferred__0/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\DATA_RX_r1_inferred__0/i__carry__1_n_4 ,\DATA_RX_r1_inferred__0/i__carry__1_n_5 ,\DATA_RX_r1_inferred__0/i__carry__1_n_6 ,\DATA_RX_r1_inferred__0/i__carry__1_n_7 }),
        .S({i__carry__1_i_1_n_0,i__carry__1_i_2_n_0,i__carry__1_i_3_n_0,i__carry__1_i_4_n_0}));
  CARRY4 \DATA_RX_r1_inferred__0/i__carry__2 
       (.CI(\DATA_RX_r1_inferred__0/i__carry__1_n_0 ),
        .CO({\DATA_RX_r1_inferred__0/i__carry__2_n_0 ,\DATA_RX_r1_inferred__0/i__carry__2_n_1 ,\DATA_RX_r1_inferred__0/i__carry__2_n_2 ,\DATA_RX_r1_inferred__0/i__carry__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\DATA_RX_r1_inferred__0/i__carry__2_n_4 ,\DATA_RX_r1_inferred__0/i__carry__2_n_5 ,\DATA_RX_r1_inferred__0/i__carry__2_n_6 ,\DATA_RX_r1_inferred__0/i__carry__2_n_7 }),
        .S({i__carry__2_i_1_n_0,i__carry__2_i_2_n_0,i__carry__2_i_3_n_0,i__carry__2_i_4_n_0}));
  LUT4 #(
    .INIT(16'hFB08)) 
    \DATA_RX_r[0]_i_1 
       (.I0(DATA_RX_serial),
        .I1(\DATA_RX_r[4]_i_2_n_0 ),
        .I2(\DATA_RX_r[3]_i_2_n_0 ),
        .I3(DATA_RX_r[0]),
        .O(\DATA_RX_r[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \DATA_RX_r[1]_i_1 
       (.I0(DATA_RX_serial),
        .I1(\DATA_RX_r[5]_i_2_n_0 ),
        .I2(\DATA_RX_r[3]_i_2_n_0 ),
        .I3(DATA_RX_r[1]),
        .O(\DATA_RX_r[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \DATA_RX_r[2]_i_1 
       (.I0(DATA_RX_serial),
        .I1(\DATA_RX_r[6]_i_2_n_0 ),
        .I2(\DATA_RX_r[3]_i_2_n_0 ),
        .I3(DATA_RX_r[2]),
        .O(\DATA_RX_r[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \DATA_RX_r[3]_i_1 
       (.I0(DATA_RX_serial),
        .I1(\DATA_RX_r[7]_i_3_n_0 ),
        .I2(\DATA_RX_r[3]_i_2_n_0 ),
        .I3(DATA_RX_r[3]),
        .O(\DATA_RX_r[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \DATA_RX_r[3]_i_2 
       (.I0(\DATA_RX_r1_inferred__0/i__carry_n_5 ),
        .I1(\DATA_RX_r[7]_i_4_n_0 ),
        .I2(\DATA_RX_r[7]_i_5_n_0 ),
        .I3(\DATA_RX_r[7]_i_6_n_0 ),
        .O(\DATA_RX_r[3]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hBF80)) 
    \DATA_RX_r[4]_i_1 
       (.I0(DATA_RX_serial),
        .I1(\DATA_RX_r[7]_i_2_n_0 ),
        .I2(\DATA_RX_r[4]_i_2_n_0 ),
        .I3(DATA_RX_r[4]),
        .O(\DATA_RX_r[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \DATA_RX_r[4]_i_2 
       (.I0(\cnt_re_reg[0]_0 ),
        .I1(cnt_re_reg[0]),
        .I2(\DATA_RX_r[7]_i_7_n_0 ),
        .I3(\DATA_RX_r1_inferred__0/i__carry_n_6 ),
        .O(\DATA_RX_r[4]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hBF80)) 
    \DATA_RX_r[5]_i_1 
       (.I0(DATA_RX_serial),
        .I1(\DATA_RX_r[7]_i_2_n_0 ),
        .I2(\DATA_RX_r[5]_i_2_n_0 ),
        .I3(DATA_RX_r[5]),
        .O(\DATA_RX_r[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \DATA_RX_r[5]_i_2 
       (.I0(cnt_re_reg[0]),
        .I1(\cnt_re_reg[0]_0 ),
        .I2(\DATA_RX_r[7]_i_7_n_0 ),
        .I3(\DATA_RX_r1_inferred__0/i__carry_n_6 ),
        .O(\DATA_RX_r[5]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hBF80)) 
    \DATA_RX_r[6]_i_1 
       (.I0(DATA_RX_serial),
        .I1(\DATA_RX_r[7]_i_2_n_0 ),
        .I2(\DATA_RX_r[6]_i_2_n_0 ),
        .I3(DATA_RX_r[6]),
        .O(\DATA_RX_r[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h2000)) 
    \DATA_RX_r[6]_i_2 
       (.I0(\DATA_RX_r1_inferred__0/i__carry_n_6 ),
        .I1(\DATA_RX_r[7]_i_7_n_0 ),
        .I2(\cnt_re_reg[0]_0 ),
        .I3(cnt_re_reg[0]),
        .O(\DATA_RX_r[6]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hBF80)) 
    \DATA_RX_r[7]_i_1 
       (.I0(DATA_RX_serial),
        .I1(\DATA_RX_r[7]_i_2_n_0 ),
        .I2(\DATA_RX_r[7]_i_3_n_0 ),
        .I3(DATA_RX_r[7]),
        .O(\DATA_RX_r[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \DATA_RX_r[7]_i_10 
       (.I0(cnt_re_reg[5]),
        .I1(cnt_re_reg[4]),
        .O(\DATA_RX_r[7]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT4 #(
    .INIT(16'h0002)) 
    \DATA_RX_r[7]_i_2 
       (.I0(\DATA_RX_r1_inferred__0/i__carry_n_5 ),
        .I1(\DATA_RX_r[7]_i_4_n_0 ),
        .I2(\DATA_RX_r[7]_i_5_n_0 ),
        .I3(\DATA_RX_r[7]_i_6_n_0 ),
        .O(\DATA_RX_r[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h0040)) 
    \DATA_RX_r[7]_i_3 
       (.I0(cnt_re_reg[0]),
        .I1(\cnt_re_reg[0]_0 ),
        .I2(\DATA_RX_r1_inferred__0/i__carry_n_6 ),
        .I3(\DATA_RX_r[7]_i_7_n_0 ),
        .O(\DATA_RX_r[7]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \DATA_RX_r[7]_i_4 
       (.I0(\DATA_RX_r1_inferred__0/i__carry__1_n_7 ),
        .I1(\DATA_RX_r1_inferred__0/i__carry__1_n_5 ),
        .I2(\DATA_RX_r1_inferred__0/i__carry__0_n_7 ),
        .I3(\DATA_RX_r1_inferred__0/i__carry__2_n_6 ),
        .O(\DATA_RX_r[7]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \DATA_RX_r[7]_i_5 
       (.I0(\DATA_RX_r1_inferred__0/i__carry__0_n_5 ),
        .I1(\DATA_RX_r1_inferred__0/i__carry__0_n_4 ),
        .I2(\DATA_RX_r1_inferred__0/i__carry__1_n_6 ),
        .I3(\DATA_RX_r1_inferred__0/i__carry__2_n_7 ),
        .O(\DATA_RX_r[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \DATA_RX_r[7]_i_6 
       (.I0(\DATA_RX_r1_inferred__0/i__carry_n_4 ),
        .I1(\DATA_RX_r1_inferred__0/i__carry__0_n_6 ),
        .I2(\DATA_RX_r1_inferred__0/i__carry__2_n_4 ),
        .I3(\DATA_RX_r1_inferred__0/i__carry__2_n_5 ),
        .I4(\DATA_RX_r1_inferred__0/i__carry__1_n_4 ),
        .I5(\DATA_RX_r_reg[7]_i_8_n_3 ),
        .O(\DATA_RX_r[7]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFEB)) 
    \DATA_RX_r[7]_i_7 
       (.I0(\cnt_re[0]_i_4_n_0 ),
        .I1(\DATA_RX_r[7]_i_9_n_0 ),
        .I2(cnt_re_reg[3]),
        .I3(cnt_re_reg[15]),
        .I4(cnt_re_reg[6]),
        .I5(\DATA_RX_r[7]_i_10_n_0 ),
        .O(\DATA_RX_r[7]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \DATA_RX_r[7]_i_9 
       (.I0(cnt_re_reg[1]),
        .I1(cnt_re_reg[2]),
        .O(\DATA_RX_r[7]_i_9_n_0 ));
  FDRE \DATA_RX_r_reg[0] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\DATA_RX_r[0]_i_1_n_0 ),
        .Q(DATA_RX_r[0]),
        .R(1'b0));
  FDRE \DATA_RX_r_reg[1] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\DATA_RX_r[1]_i_1_n_0 ),
        .Q(DATA_RX_r[1]),
        .R(1'b0));
  FDRE \DATA_RX_r_reg[2] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\DATA_RX_r[2]_i_1_n_0 ),
        .Q(DATA_RX_r[2]),
        .R(1'b0));
  FDRE \DATA_RX_r_reg[3] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\DATA_RX_r[3]_i_1_n_0 ),
        .Q(DATA_RX_r[3]),
        .R(1'b0));
  FDRE \DATA_RX_r_reg[4] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\DATA_RX_r[4]_i_1_n_0 ),
        .Q(DATA_RX_r[4]),
        .R(1'b0));
  FDRE \DATA_RX_r_reg[5] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\DATA_RX_r[5]_i_1_n_0 ),
        .Q(DATA_RX_r[5]),
        .R(1'b0));
  FDRE \DATA_RX_r_reg[6] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\DATA_RX_r[6]_i_1_n_0 ),
        .Q(DATA_RX_r[6]),
        .R(1'b0));
  FDRE \DATA_RX_r_reg[7] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\DATA_RX_r[7]_i_1_n_0 ),
        .Q(DATA_RX_r[7]),
        .R(1'b0));
  CARRY4 \DATA_RX_r_reg[7]_i_8 
       (.CI(\DATA_RX_r1_inferred__0/i__carry__2_n_0 ),
        .CO({\NLW_DATA_RX_r_reg[7]_i_8_CO_UNCONNECTED [3:1],\DATA_RX_r_reg[7]_i_8_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_DATA_RX_r_reg[7]_i_8_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  (* OPT_MODIFIED = "MLO" *) 
  FDRE DATA_RX_serial_r_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(OBUFT_inst_n_0),
        .Q(DATA_RX_serial),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0F0F0EFF0F0F0E00)) 
    DATA_TX_serial_r_i_1
       (.I0(DATA_TX_serial_r_i_2_n_0),
        .I1(DATA_TX_serial_r_i_3_n_0),
        .I2(DATA_TX_serial_r_i_4_n_0),
        .I3(DATA_TX_serial_r_i_5_n_0),
        .I4(DATA_TX_serial_r_i_6_n_0),
        .I5(DATA_TX_serial),
        .O(DATA_TX_serial_r_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h1)) 
    DATA_TX_serial_r_i_10
       (.I0(Q[1]),
        .I1(Q[0]),
        .O(DATA_TX_serial_r_i_10_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFD555555FF)) 
    DATA_TX_serial_r_i_12
       (.I0(DATA_TX_serial_r_i_8_n_0),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(cnt_reg[2]),
        .I4(cnt_reg[3]),
        .I5(cnt_reg[4]),
        .O(DATA_TX_serial_r_i_12_n_0));
  LUT3 #(
    .INIT(8'h56)) 
    DATA_TX_serial_r_i_16
       (.I0(cnt_reg[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(\cnt_reg[2]_0 ));
  LUT6 #(
    .INIT(64'hCC00CCCCC80CCCCC)) 
    DATA_TX_serial_r_i_2
       (.I0(Q[0]),
        .I1(\cnt_re_reg[0]_0 ),
        .I2(DATA_TX_serial_r_i_7_n_0),
        .I3(cnt_reg[4]),
        .I4(DATA_TX_serial_r_i_8_n_0),
        .I5(Q[1]),
        .O(DATA_TX_serial_r_i_2_n_0));
  LUT6 #(
    .INIT(64'h00000000EBEE2822)) 
    DATA_TX_serial_r_i_3
       (.I0(DATA_TX_serial_r_i_9_n_0),
        .I1(cnt_reg[3]),
        .I2(cnt_reg[2]),
        .I3(DATA_TX_serial_r_i_10_n_0),
        .I4(DATA_TX_serial_r_reg_2),
        .I5(DATA_TX_serial_r_i_12_n_0),
        .O(DATA_TX_serial_r_i_3_n_0));
  LUT6 #(
    .INIT(64'h04040440C4C4C44C)) 
    DATA_TX_serial_r_i_4
       (.I0(DATA_TX_serial_r_reg_0),
        .I1(DATA_TX_serial_r_i_6_n_0),
        .I2(cnt_reg[2]),
        .I3(Q[0]),
        .I4(Q[1]),
        .I5(DATA_TX_serial_r_reg_1),
        .O(DATA_TX_serial_r_i_4_n_0));
  LUT6 #(
    .INIT(64'h0001FFFE00000000)) 
    DATA_TX_serial_r_i_5
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(cnt_reg[3]),
        .I3(cnt_reg[2]),
        .I4(cnt_reg[4]),
        .I5(DATA_TX_serial_r_i_8_n_0),
        .O(DATA_TX_serial_r_i_5_n_0));
  LUT6 #(
    .INIT(64'h4040004000004000)) 
    DATA_TX_serial_r_i_6
       (.I0(\cnt_re_reg[0]_0 ),
        .I1(cnt_reg[4]),
        .I2(DATA_TX_serial_r_i_8_n_0),
        .I3(cnt_reg[2]),
        .I4(DATA_TX_serial_r_i_10_n_0),
        .I5(cnt_reg[3]),
        .O(DATA_TX_serial_r_i_6_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    DATA_TX_serial_r_i_7
       (.I0(cnt_reg[2]),
        .I1(cnt_reg[3]),
        .O(DATA_TX_serial_r_i_7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h01)) 
    DATA_TX_serial_r_i_8
       (.I0(cnt_reg[6]),
        .I1(cnt_reg[7]),
        .I2(cnt_reg[5]),
        .O(DATA_TX_serial_r_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'hEEEB2228)) 
    DATA_TX_serial_r_i_9
       (.I0(DATA_TX_serial_r_i_3_0),
        .I1(cnt_reg[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(DATA_TX_serial_r_i_3_1),
        .O(DATA_TX_serial_r_i_9_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    DATA_TX_serial_r_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_TX_serial_r_i_1_n_0),
        .Q(DATA_TX_serial),
        .R(1'b0));
  (* BOX_TYPE = "PRIMITIVE" *) 
  (* CAPACITANCE = "DONT_CARE" *) 
  OBUFT #(
    .DRIVE(12),
    .IOSTANDARD("DEFAULT"),
    .SLEW("SLOW")) 
    OBUFT_inst
       (.I(tx_wire),
        .O(OBUFT_inst_n_0),
        .T(tristate));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \cnt[0]_i_1 
       (.I0(Q[0]),
        .O(\cnt[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \cnt[1]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \cnt[2]_i_1 
       (.I0(cnt_reg[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(\cnt[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \cnt[3]_i_1 
       (.I0(cnt_reg[3]),
        .I1(cnt_reg[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(\cnt[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \cnt[4]_i_1 
       (.I0(cnt_reg[4]),
        .I1(cnt_reg[3]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(cnt_reg[2]),
        .O(\cnt[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \cnt[5]_i_1 
       (.I0(cnt_reg[5]),
        .I1(cnt_reg[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(cnt_reg[3]),
        .I5(cnt_reg[4]),
        .O(p_0_in[5]));
  LUT2 #(
    .INIT(4'h6)) 
    \cnt[6]_i_1 
       (.I0(cnt_reg[6]),
        .I1(\cnt[7]_i_4_n_0 ),
        .O(p_0_in[6]));
  LUT6 #(
    .INIT(64'h00080000FFFFFFFF)) 
    \cnt[7]_i_1 
       (.I0(cnt_reg[6]),
        .I1(cnt_reg[4]),
        .I2(cnt_reg[7]),
        .I3(cnt_reg[5]),
        .I4(\cnt[7]_i_3_n_0 ),
        .I5(start_prev),
        .O(\cnt[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \cnt[7]_i_2 
       (.I0(cnt_reg[7]),
        .I1(cnt_reg[6]),
        .I2(\cnt[7]_i_4_n_0 ),
        .O(p_0_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \cnt[7]_i_3 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(cnt_reg[3]),
        .I3(cnt_reg[2]),
        .O(\cnt[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \cnt[7]_i_4 
       (.I0(cnt_reg[5]),
        .I1(cnt_reg[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(cnt_reg[3]),
        .I5(cnt_reg[4]),
        .O(\cnt[7]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h08AA)) 
    \cnt_re[0]_i_1 
       (.I0(\cnt_re_reg[0]_0 ),
        .I1(\cnt_re[0]_i_3_n_0 ),
        .I2(\cnt_re[0]_i_4_n_0 ),
        .I3(enable_cnt_re),
        .O(cnt_re));
  LUT5 #(
    .INIT(32'h00000010)) 
    \cnt_re[0]_i_3 
       (.I0(start_prev_fe_i_3_n_0),
        .I1(cnt_re_reg[3]),
        .I2(cnt_re_reg[6]),
        .I3(cnt_re_reg[15]),
        .I4(cnt_re_reg[5]),
        .O(\cnt_re[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \cnt_re[0]_i_4 
       (.I0(\cnt_re[0]_i_6_n_0 ),
        .I1(cnt_re_reg[11]),
        .I2(cnt_re_reg[9]),
        .I3(cnt_re_reg[10]),
        .I4(cnt_re_reg[12]),
        .O(\cnt_re[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_re[0]_i_5 
       (.I0(cnt_re_reg[0]),
        .O(\cnt_re[0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \cnt_re[0]_i_6 
       (.I0(cnt_re_reg[8]),
        .I1(cnt_re_reg[7]),
        .I2(cnt_re_reg[14]),
        .I3(cnt_re_reg[13]),
        .O(\cnt_re[0]_i_6_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[0] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[0]_i_2_n_7 ),
        .Q(cnt_re_reg[0]),
        .R(cnt_re));
  CARRY4 \cnt_re_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_re_reg[0]_i_2_n_0 ,\cnt_re_reg[0]_i_2_n_1 ,\cnt_re_reg[0]_i_2_n_2 ,\cnt_re_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_re_reg[0]_i_2_n_4 ,\cnt_re_reg[0]_i_2_n_5 ,\cnt_re_reg[0]_i_2_n_6 ,\cnt_re_reg[0]_i_2_n_7 }),
        .S({cnt_re_reg[3:1],\cnt_re[0]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[10] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[8]_i_1_n_5 ),
        .Q(cnt_re_reg[10]),
        .R(cnt_re));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[11] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[8]_i_1_n_4 ),
        .Q(cnt_re_reg[11]),
        .R(cnt_re));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[12] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[12]_i_1_n_7 ),
        .Q(cnt_re_reg[12]),
        .R(cnt_re));
  CARRY4 \cnt_re_reg[12]_i_1 
       (.CI(\cnt_re_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_re_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_re_reg[12]_i_1_n_1 ,\cnt_re_reg[12]_i_1_n_2 ,\cnt_re_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_re_reg[12]_i_1_n_4 ,\cnt_re_reg[12]_i_1_n_5 ,\cnt_re_reg[12]_i_1_n_6 ,\cnt_re_reg[12]_i_1_n_7 }),
        .S(cnt_re_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[13] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[12]_i_1_n_6 ),
        .Q(cnt_re_reg[13]),
        .R(cnt_re));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[14] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[12]_i_1_n_5 ),
        .Q(cnt_re_reg[14]),
        .R(cnt_re));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[15] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[12]_i_1_n_4 ),
        .Q(cnt_re_reg[15]),
        .R(cnt_re));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[1] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[0]_i_2_n_6 ),
        .Q(cnt_re_reg[1]),
        .R(cnt_re));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[2] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[0]_i_2_n_5 ),
        .Q(cnt_re_reg[2]),
        .R(cnt_re));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[3] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[0]_i_2_n_4 ),
        .Q(cnt_re_reg[3]),
        .R(cnt_re));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[4] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[4]_i_1_n_7 ),
        .Q(cnt_re_reg[4]),
        .R(cnt_re));
  CARRY4 \cnt_re_reg[4]_i_1 
       (.CI(\cnt_re_reg[0]_i_2_n_0 ),
        .CO({\cnt_re_reg[4]_i_1_n_0 ,\cnt_re_reg[4]_i_1_n_1 ,\cnt_re_reg[4]_i_1_n_2 ,\cnt_re_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_re_reg[4]_i_1_n_4 ,\cnt_re_reg[4]_i_1_n_5 ,\cnt_re_reg[4]_i_1_n_6 ,\cnt_re_reg[4]_i_1_n_7 }),
        .S(cnt_re_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[5] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[4]_i_1_n_6 ),
        .Q(cnt_re_reg[5]),
        .R(cnt_re));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[6] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[4]_i_1_n_5 ),
        .Q(cnt_re_reg[6]),
        .R(cnt_re));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[7] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[4]_i_1_n_4 ),
        .Q(cnt_re_reg[7]),
        .R(cnt_re));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[8] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[8]_i_1_n_7 ),
        .Q(cnt_re_reg[8]),
        .R(cnt_re));
  CARRY4 \cnt_re_reg[8]_i_1 
       (.CI(\cnt_re_reg[4]_i_1_n_0 ),
        .CO({\cnt_re_reg[8]_i_1_n_0 ,\cnt_re_reg[8]_i_1_n_1 ,\cnt_re_reg[8]_i_1_n_2 ,\cnt_re_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_re_reg[8]_i_1_n_4 ,\cnt_re_reg[8]_i_1_n_5 ,\cnt_re_reg[8]_i_1_n_6 ,\cnt_re_reg[8]_i_1_n_7 }),
        .S(cnt_re_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_re_reg[9] 
       (.C(clk_10MHz),
        .CE(\cnt_re_reg[0]_0 ),
        .D(\cnt_re_reg[8]_i_1_n_6 ),
        .Q(cnt_re_reg[9]),
        .R(cnt_re));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[0] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt[0]_i_1_n_0 ),
        .Q(Q[0]),
        .R(\cnt[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[1] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(Q[1]),
        .R(\cnt[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[2] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt[2]_i_1_n_0 ),
        .Q(cnt_reg[2]),
        .R(\cnt[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[3] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt[3]_i_1_n_0 ),
        .Q(cnt_reg[3]),
        .R(\cnt[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[4] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt[4]_i_1_n_0 ),
        .Q(cnt_reg[4]),
        .R(\cnt[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[5] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(cnt_reg[5]),
        .R(\cnt[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[6] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(cnt_reg[6]),
        .R(\cnt[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[7] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(cnt_reg[7]),
        .R(\cnt[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hB000FFFFB000B000)) 
    data_rx_ready_i_1
       (.I0(enable_cnt_re),
        .I1(start_sync),
        .I2(\cnt_re_reg[0]_0 ),
        .I3(data4),
        .I4(data_rx_ready_i_2_n_0),
        .I5(data_rx_ready_i_3_n_0),
        .O(data_rx_ready_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    data_rx_ready_i_2
       (.I0(data_rx_ready_i_4_n_0),
        .I1(cnt_re_reg[11]),
        .I2(cnt_re_reg[9]),
        .I3(cnt_re_reg[10]),
        .I4(\cnt_re[0]_i_6_n_0 ),
        .O(data_rx_ready_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000080000000000)) 
    data_rx_ready_i_3
       (.I0(data_rx_ready_i_5_n_0),
        .I1(cnt_re_reg[2]),
        .I2(cnt_re_reg[0]),
        .I3(cnt_re_reg[1]),
        .I4(cnt_re_reg[5]),
        .I5(cnt_re_reg[4]),
        .O(data_rx_ready_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFFFFF4F)) 
    data_rx_ready_i_4
       (.I0(cnt_re_reg[13]),
        .I1(cnt_re_reg[12]),
        .I2(\cnt_re_reg[0]_0 ),
        .I3(cnt_re_reg[15]),
        .I4(cnt_re_reg[11]),
        .O(data_rx_ready_i_4_n_0));
  LUT6 #(
    .INIT(64'h0D0D0D0D0D000000)) 
    data_rx_ready_i_5
       (.I0(cnt_re_reg[6]),
        .I1(cnt_re_reg[7]),
        .I2(cnt_re_reg[8]),
        .I3(cnt_re_reg[4]),
        .I4(cnt_re_reg[3]),
        .I5(cnt_re_reg[5]),
        .O(data_rx_ready_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    data_rx_ready_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(data_rx_ready_i_1_n_0),
        .Q(data4),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h75)) 
    enable_cnt_i_1
       (.I0(\cnt[7]_i_1_n_0 ),
        .I1(start_prev),
        .I2(start_sync),
        .O(enable_cnt_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    enable_cnt_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(enable_cnt_i_1_n_0),
        .Q(start_prev),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_1
       (.I0(cnt_re_reg[4]),
        .O(i__carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_2
       (.I0(cnt_re_reg[7]),
        .O(i__carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_3
       (.I0(cnt_re_reg[6]),
        .O(i__carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_4
       (.I0(cnt_re_reg[5]),
        .O(i__carry__0_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_1
       (.I0(cnt_re_reg[11]),
        .O(i__carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_2
       (.I0(cnt_re_reg[10]),
        .O(i__carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_3
       (.I0(cnt_re_reg[9]),
        .O(i__carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_4
       (.I0(cnt_re_reg[8]),
        .O(i__carry__1_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__2_i_1
       (.I0(cnt_re_reg[15]),
        .O(i__carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__2_i_2
       (.I0(cnt_re_reg[14]),
        .O(i__carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__2_i_3
       (.I0(cnt_re_reg[13]),
        .O(i__carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__2_i_4
       (.I0(cnt_re_reg[12]),
        .O(i__carry__2_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_1
       (.I0(cnt_re_reg[3]),
        .O(i__carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_2
       (.I0(cnt_re_reg[2]),
        .O(i__carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_3
       (.I0(cnt_re_reg[1]),
        .O(i__carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_4
       (.I0(cnt_re_reg[0]),
        .O(i__carry_i_4_n_0));
  LUT6 #(
    .INIT(64'hA8A8A800A8A8A8A8)) 
    start_prev_fe_i_1
       (.I0(\cnt_re_reg[0]_0 ),
        .I1(start_sync),
        .I2(enable_cnt_re),
        .I3(start_prev_fe_i_2_n_0),
        .I4(start_prev_fe_i_3_n_0),
        .I5(start_prev_fe_i_4_n_0),
        .O(start_prev_fe_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h45)) 
    start_prev_fe_i_2
       (.I0(cnt_re_reg[8]),
        .I1(cnt_re_reg[7]),
        .I2(cnt_re_reg[6]),
        .O(start_prev_fe_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'hFFFD)) 
    start_prev_fe_i_3
       (.I0(cnt_re_reg[4]),
        .I1(cnt_re_reg[2]),
        .I2(cnt_re_reg[0]),
        .I3(cnt_re_reg[1]),
        .O(start_prev_fe_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'h0007)) 
    start_prev_fe_i_4
       (.I0(cnt_re_reg[4]),
        .I1(cnt_re_reg[3]),
        .I2(cnt_re_reg[5]),
        .I3(data_rx_ready_i_2_n_0),
        .O(start_prev_fe_i_4_n_0));
  FDRE #(
    .INIT(1'b0)) 
    start_prev_fe_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(start_prev_fe_i_1_n_0),
        .Q(enable_cnt_re),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    start_sync_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(start_sync_reg_0),
        .Q(start_sync),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hB000FFFFB000B000)) 
    tristate_i_1
       (.I0(enable_cnt_re),
        .I1(start_sync),
        .I2(\cnt_re_reg[0]_0 ),
        .I3(tristate),
        .I4(tristate_i_2_n_0),
        .I5(start_prev_fe_i_4_n_0),
        .O(tristate_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFDFFFFFFFFFFF)) 
    tristate_i_2
       (.I0(start_prev_fe_i_2_n_0),
        .I1(cnt_re_reg[2]),
        .I2(cnt_re_reg[0]),
        .I3(cnt_re_reg[1]),
        .I4(cnt_re_reg[5]),
        .I5(cnt_re_reg[4]),
        .O(tristate_i_2_n_0));
  FDRE tristate_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(tristate_i_1_n_0),
        .Q(tristate),
        .R(1'b0));
  FDRE tx_reg_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(DATA_TX_serial),
        .Q(tx_wire),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

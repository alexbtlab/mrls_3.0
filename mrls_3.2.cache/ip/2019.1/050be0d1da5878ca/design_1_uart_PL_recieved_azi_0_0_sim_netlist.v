// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Sat Dec  4 14:52:47 2021
// Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_uart_PL_recieved_azi_0_0_sim_netlist.v
// Design      : design_1_uart_PL_recieved_azi_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_uart_PL_recieved_azi_0_0,uart_PL_recieved_azimut_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "uart_PL_recieved_azimut_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (i_Clock,
    i_Tx_DV,
    i_Tx_Byte,
    o_Tx_Active,
    o_Tx_Serial,
    o_Tx_Done,
    i_Rx_Serial,
    o_Rx_DV,
    o_Rx_Byte);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 i_Clock CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME i_Clock, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input i_Clock;
  input i_Tx_DV;
  input [7:0]i_Tx_Byte;
  output o_Tx_Active;
  output o_Tx_Serial;
  output o_Tx_Done;
  input i_Rx_Serial;
  output o_Rx_DV;
  output [7:0]o_Rx_Byte;

  wire i_Clock;
  wire i_Rx_Serial;
  wire i_Tx_DV;
  wire [7:0]o_Rx_Byte;
  wire o_Rx_DV;
  wire o_Tx_Active;
  wire o_Tx_Done;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_PL_recieved_azimut_v1_0 inst
       (.i_Clock(i_Clock),
        .i_Rx_Serial(i_Rx_Serial),
        .i_Tx_DV(i_Tx_DV),
        .o_Rx_Byte(o_Rx_Byte),
        .o_Rx_DV(o_Rx_DV),
        .o_Tx_Active(o_Tx_Active),
        .o_Tx_Done(o_Tx_Done));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_PL_recieved_azimut_v1_0
   (o_Rx_Byte,
    o_Tx_Active,
    o_Tx_Done,
    o_Rx_DV,
    i_Tx_DV,
    i_Clock,
    i_Rx_Serial);
  output [7:0]o_Rx_Byte;
  output o_Tx_Active;
  output o_Tx_Done;
  output o_Rx_DV;
  input i_Tx_DV;
  input i_Clock;
  input i_Rx_Serial;

  wire i_Clock;
  wire i_Rx_Serial;
  wire i_Tx_DV;
  wire [7:0]o_Rx_Byte;
  wire o_Rx_DV;
  wire o_Tx_Active;
  wire o_Tx_Done;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_tx nolabel_line27
       (.i_Clock(i_Clock),
        .i_Tx_DV(i_Tx_DV),
        .o_Tx_Active(o_Tx_Active),
        .o_Tx_Done(o_Tx_Done));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_rx nolabel_line36
       (.i_Clock(i_Clock),
        .i_Rx_Serial(i_Rx_Serial),
        .o_Rx_Byte(o_Rx_Byte),
        .o_Rx_DV(o_Rx_DV));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_rx
   (o_Rx_DV,
    o_Rx_Byte,
    i_Rx_Serial,
    i_Clock);
  output o_Rx_DV;
  output [7:0]o_Rx_Byte;
  input i_Rx_Serial;
  input i_Clock;

  wire i_Clock;
  wire i_Rx_Serial;
  wire [7:0]o_Rx_Byte;
  wire o_Rx_DV;
  wire [7:0]p_1_in;
  wire \r_Bit_Index[0]_i_1_n_0 ;
  wire \r_Bit_Index[1]_i_1_n_0 ;
  wire \r_Bit_Index[2]_i_1_n_0 ;
  wire \r_Bit_Index_reg_n_0_[0] ;
  wire \r_Bit_Index_reg_n_0_[1] ;
  wire \r_Bit_Index_reg_n_0_[2] ;
  wire \r_Clock_Count[2]_i_2_n_0 ;
  wire \r_Clock_Count[3]_i_2_n_0 ;
  wire \r_Clock_Count[4]_i_2_n_0 ;
  wire \r_Clock_Count[5]_i_2_n_0 ;
  wire \r_Clock_Count[6]_i_2__0_n_0 ;
  wire \r_Clock_Count[7]_i_1_n_0 ;
  wire \r_Clock_Count[7]_i_3_n_0 ;
  wire \r_Clock_Count[7]_i_4_n_0 ;
  wire \r_Clock_Count[7]_i_5_n_0 ;
  wire \r_Clock_Count[7]_i_6_n_0 ;
  wire \r_Clock_Count_reg_n_0_[0] ;
  wire \r_Clock_Count_reg_n_0_[1] ;
  wire \r_Clock_Count_reg_n_0_[2] ;
  wire \r_Clock_Count_reg_n_0_[3] ;
  wire \r_Clock_Count_reg_n_0_[4] ;
  wire \r_Clock_Count_reg_n_0_[5] ;
  wire \r_Clock_Count_reg_n_0_[6] ;
  wire \r_Clock_Count_reg_n_0_[7] ;
  wire \r_Rx_Byte[0]_i_1_n_0 ;
  wire \r_Rx_Byte[1]_i_1_n_0 ;
  wire \r_Rx_Byte[2]_i_1_n_0 ;
  wire \r_Rx_Byte[3]_i_1_n_0 ;
  wire \r_Rx_Byte[4]_i_1_n_0 ;
  wire \r_Rx_Byte[5]_i_1_n_0 ;
  wire \r_Rx_Byte[6]_i_1_n_0 ;
  wire \r_Rx_Byte[7]_i_1_n_0 ;
  wire \r_Rx_Byte[7]_i_2_n_0 ;
  wire r_Rx_DV_i_1_n_0;
  wire r_Rx_DV_i_2_n_0;
  wire r_Rx_DV_i_3_n_0;
  wire r_Rx_Data;
  wire r_Rx_Data_R;
  wire \r_SM_Main[0]_i_1_n_0 ;
  wire \r_SM_Main[0]_i_2_n_0 ;
  wire \r_SM_Main[0]_i_3_n_0 ;
  wire \r_SM_Main[0]_i_4_n_0 ;
  wire \r_SM_Main[0]_i_5_n_0 ;
  wire \r_SM_Main[1]_i_1_n_0 ;
  wire \r_SM_Main[1]_i_2_n_0 ;
  wire \r_SM_Main[1]_i_3_n_0 ;
  wire \r_SM_Main[2]_i_1_n_0 ;
  wire \r_SM_Main_reg_n_0_[0] ;
  wire \r_SM_Main_reg_n_0_[1] ;
  wire \r_SM_Main_reg_n_0_[2] ;

  LUT6 #(
    .INIT(64'h2626262626262600)) 
    \r_Bit_Index[0]_i_1 
       (.I0(\r_Bit_Index_reg_n_0_[0] ),
        .I1(\r_Rx_Byte[7]_i_2_n_0 ),
        .I2(\r_SM_Main[0]_i_4_n_0 ),
        .I3(\r_SM_Main_reg_n_0_[2] ),
        .I4(\r_SM_Main_reg_n_0_[0] ),
        .I5(\r_SM_Main_reg_n_0_[1] ),
        .O(\r_Bit_Index[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h6A006A6A)) 
    \r_Bit_Index[1]_i_1 
       (.I0(\r_Bit_Index_reg_n_0_[1] ),
        .I1(\r_Rx_Byte[7]_i_2_n_0 ),
        .I2(\r_Bit_Index_reg_n_0_[0] ),
        .I3(\r_SM_Main_reg_n_0_[2] ),
        .I4(\r_SM_Main[0]_i_3_n_0 ),
        .O(\r_Bit_Index[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6AAA00006AAA6AAA)) 
    \r_Bit_Index[2]_i_1 
       (.I0(\r_Bit_Index_reg_n_0_[2] ),
        .I1(\r_Rx_Byte[7]_i_2_n_0 ),
        .I2(\r_Bit_Index_reg_n_0_[0] ),
        .I3(\r_Bit_Index_reg_n_0_[1] ),
        .I4(\r_SM_Main_reg_n_0_[2] ),
        .I5(\r_SM_Main[0]_i_3_n_0 ),
        .O(\r_Bit_Index[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \r_Bit_Index_reg[0] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_Bit_Index[0]_i_1_n_0 ),
        .Q(\r_Bit_Index_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Bit_Index_reg[1] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_Bit_Index[1]_i_1_n_0 ),
        .Q(\r_Bit_Index_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Bit_Index_reg[2] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_Bit_Index[2]_i_1_n_0 ),
        .Q(\r_Bit_Index_reg_n_0_[2] ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h33F22222)) 
    \r_Clock_Count[0]_i_1 
       (.I0(\r_Clock_Count[7]_i_4_n_0 ),
        .I1(\r_Clock_Count_reg_n_0_[0] ),
        .I2(r_Rx_Data),
        .I3(\r_SM_Main[1]_i_3_n_0 ),
        .I4(\r_Clock_Count[7]_i_6_n_0 ),
        .O(p_1_in[0]));
  LUT6 #(
    .INIT(64'h3C3CFF2828282828)) 
    \r_Clock_Count[1]_i_1__0 
       (.I0(\r_Clock_Count[7]_i_4_n_0 ),
        .I1(\r_Clock_Count_reg_n_0_[0] ),
        .I2(\r_Clock_Count_reg_n_0_[1] ),
        .I3(r_Rx_Data),
        .I4(\r_SM_Main[1]_i_3_n_0 ),
        .I5(\r_Clock_Count[7]_i_6_n_0 ),
        .O(p_1_in[1]));
  LUT6 #(
    .INIT(64'hC3C3FF8282828282)) 
    \r_Clock_Count[2]_i_1__0 
       (.I0(\r_Clock_Count[7]_i_4_n_0 ),
        .I1(\r_Clock_Count[2]_i_2_n_0 ),
        .I2(\r_Clock_Count_reg_n_0_[2] ),
        .I3(r_Rx_Data),
        .I4(\r_SM_Main[1]_i_3_n_0 ),
        .I5(\r_Clock_Count[7]_i_6_n_0 ),
        .O(p_1_in[2]));
  LUT2 #(
    .INIT(4'h7)) 
    \r_Clock_Count[2]_i_2 
       (.I0(\r_Clock_Count_reg_n_0_[0] ),
        .I1(\r_Clock_Count_reg_n_0_[1] ),
        .O(\r_Clock_Count[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hCCF88888)) 
    \r_Clock_Count[3]_i_1__0 
       (.I0(\r_Clock_Count[7]_i_4_n_0 ),
        .I1(\r_Clock_Count[3]_i_2_n_0 ),
        .I2(r_Rx_Data),
        .I3(\r_SM_Main[1]_i_3_n_0 ),
        .I4(\r_Clock_Count[7]_i_6_n_0 ),
        .O(p_1_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \r_Clock_Count[3]_i_2 
       (.I0(\r_Clock_Count_reg_n_0_[0] ),
        .I1(\r_Clock_Count_reg_n_0_[1] ),
        .I2(\r_Clock_Count_reg_n_0_[2] ),
        .I3(\r_Clock_Count_reg_n_0_[3] ),
        .O(\r_Clock_Count[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'hCCF88888)) 
    \r_Clock_Count[4]_i_1__0 
       (.I0(\r_Clock_Count[7]_i_4_n_0 ),
        .I1(\r_Clock_Count[4]_i_2_n_0 ),
        .I2(r_Rx_Data),
        .I3(\r_SM_Main[1]_i_3_n_0 ),
        .I4(\r_Clock_Count[7]_i_6_n_0 ),
        .O(p_1_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \r_Clock_Count[4]_i_2 
       (.I0(\r_Clock_Count_reg_n_0_[3] ),
        .I1(\r_Clock_Count_reg_n_0_[2] ),
        .I2(\r_Clock_Count_reg_n_0_[1] ),
        .I3(\r_Clock_Count_reg_n_0_[0] ),
        .I4(\r_Clock_Count_reg_n_0_[4] ),
        .O(\r_Clock_Count[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hCCF88888)) 
    \r_Clock_Count[5]_i_1__0 
       (.I0(\r_Clock_Count[7]_i_4_n_0 ),
        .I1(\r_Clock_Count[5]_i_2_n_0 ),
        .I2(r_Rx_Data),
        .I3(\r_SM_Main[1]_i_3_n_0 ),
        .I4(\r_Clock_Count[7]_i_6_n_0 ),
        .O(p_1_in[5]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \r_Clock_Count[5]_i_2 
       (.I0(\r_Clock_Count_reg_n_0_[0] ),
        .I1(\r_Clock_Count_reg_n_0_[1] ),
        .I2(\r_Clock_Count_reg_n_0_[2] ),
        .I3(\r_Clock_Count_reg_n_0_[3] ),
        .I4(\r_Clock_Count_reg_n_0_[4] ),
        .I5(\r_Clock_Count_reg_n_0_[5] ),
        .O(\r_Clock_Count[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hC3C3FF8282828282)) 
    \r_Clock_Count[6]_i_1__0 
       (.I0(\r_Clock_Count[7]_i_4_n_0 ),
        .I1(\r_Clock_Count[6]_i_2__0_n_0 ),
        .I2(\r_Clock_Count_reg_n_0_[6] ),
        .I3(r_Rx_Data),
        .I4(\r_SM_Main[1]_i_3_n_0 ),
        .I5(\r_Clock_Count[7]_i_6_n_0 ),
        .O(p_1_in[6]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \r_Clock_Count[6]_i_2__0 
       (.I0(\r_Clock_Count_reg_n_0_[0] ),
        .I1(\r_Clock_Count_reg_n_0_[1] ),
        .I2(\r_Clock_Count_reg_n_0_[2] ),
        .I3(\r_Clock_Count_reg_n_0_[3] ),
        .I4(\r_Clock_Count_reg_n_0_[4] ),
        .I5(\r_Clock_Count_reg_n_0_[5] ),
        .O(\r_Clock_Count[6]_i_2__0_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFE0000FFFF)) 
    \r_Clock_Count[7]_i_1 
       (.I0(\r_Clock_Count[7]_i_3_n_0 ),
        .I1(\r_Clock_Count_reg_n_0_[6] ),
        .I2(\r_Clock_Count_reg_n_0_[7] ),
        .I3(\r_SM_Main_reg_n_0_[1] ),
        .I4(\r_SM_Main_reg_n_0_[2] ),
        .I5(\r_SM_Main_reg_n_0_[0] ),
        .O(\r_Clock_Count[7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hCCF88888)) 
    \r_Clock_Count[7]_i_2 
       (.I0(\r_Clock_Count[7]_i_4_n_0 ),
        .I1(\r_Clock_Count[7]_i_5_n_0 ),
        .I2(r_Rx_Data),
        .I3(\r_SM_Main[1]_i_3_n_0 ),
        .I4(\r_Clock_Count[7]_i_6_n_0 ),
        .O(p_1_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \r_Clock_Count[7]_i_3 
       (.I0(\r_SM_Main[1]_i_3_n_0 ),
        .I1(r_Rx_Data),
        .O(\r_Clock_Count[7]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'hF1EEF100)) 
    \r_Clock_Count[7]_i_4 
       (.I0(\r_Clock_Count_reg_n_0_[6] ),
        .I1(\r_Clock_Count_reg_n_0_[7] ),
        .I2(r_Rx_DV_i_3_n_0),
        .I3(\r_SM_Main_reg_n_0_[1] ),
        .I4(\r_SM_Main_reg_n_0_[0] ),
        .O(\r_Clock_Count[7]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \r_Clock_Count[7]_i_5 
       (.I0(\r_Clock_Count_reg_n_0_[6] ),
        .I1(\r_Clock_Count[6]_i_2__0_n_0 ),
        .I2(\r_Clock_Count_reg_n_0_[7] ),
        .O(\r_Clock_Count[7]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h0100)) 
    \r_Clock_Count[7]_i_6 
       (.I0(\r_Clock_Count_reg_n_0_[7] ),
        .I1(\r_Clock_Count_reg_n_0_[6] ),
        .I2(\r_SM_Main_reg_n_0_[1] ),
        .I3(\r_SM_Main_reg_n_0_[0] ),
        .O(\r_Clock_Count[7]_i_6_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \r_Clock_Count_reg[0] 
       (.C(i_Clock),
        .CE(\r_Clock_Count[7]_i_1_n_0 ),
        .D(p_1_in[0]),
        .Q(\r_Clock_Count_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Clock_Count_reg[1] 
       (.C(i_Clock),
        .CE(\r_Clock_Count[7]_i_1_n_0 ),
        .D(p_1_in[1]),
        .Q(\r_Clock_Count_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Clock_Count_reg[2] 
       (.C(i_Clock),
        .CE(\r_Clock_Count[7]_i_1_n_0 ),
        .D(p_1_in[2]),
        .Q(\r_Clock_Count_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Clock_Count_reg[3] 
       (.C(i_Clock),
        .CE(\r_Clock_Count[7]_i_1_n_0 ),
        .D(p_1_in[3]),
        .Q(\r_Clock_Count_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Clock_Count_reg[4] 
       (.C(i_Clock),
        .CE(\r_Clock_Count[7]_i_1_n_0 ),
        .D(p_1_in[4]),
        .Q(\r_Clock_Count_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Clock_Count_reg[5] 
       (.C(i_Clock),
        .CE(\r_Clock_Count[7]_i_1_n_0 ),
        .D(p_1_in[5]),
        .Q(\r_Clock_Count_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Clock_Count_reg[6] 
       (.C(i_Clock),
        .CE(\r_Clock_Count[7]_i_1_n_0 ),
        .D(p_1_in[6]),
        .Q(\r_Clock_Count_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Clock_Count_reg[7] 
       (.C(i_Clock),
        .CE(\r_Clock_Count[7]_i_1_n_0 ),
        .D(p_1_in[7]),
        .Q(\r_Clock_Count_reg_n_0_[7] ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFEFFFF00020000)) 
    \r_Rx_Byte[0]_i_1 
       (.I0(r_Rx_Data),
        .I1(\r_Bit_Index_reg_n_0_[2] ),
        .I2(\r_Bit_Index_reg_n_0_[1] ),
        .I3(\r_Bit_Index_reg_n_0_[0] ),
        .I4(\r_Rx_Byte[7]_i_2_n_0 ),
        .I5(o_Rx_Byte[0]),
        .O(\r_Rx_Byte[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFBFFFF00080000)) 
    \r_Rx_Byte[1]_i_1 
       (.I0(r_Rx_Data),
        .I1(\r_Bit_Index_reg_n_0_[0] ),
        .I2(\r_Bit_Index_reg_n_0_[1] ),
        .I3(\r_Bit_Index_reg_n_0_[2] ),
        .I4(\r_Rx_Byte[7]_i_2_n_0 ),
        .I5(o_Rx_Byte[1]),
        .O(\r_Rx_Byte[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFEFFFFF00200000)) 
    \r_Rx_Byte[2]_i_1 
       (.I0(r_Rx_Data),
        .I1(\r_Bit_Index_reg_n_0_[2] ),
        .I2(\r_Bit_Index_reg_n_0_[1] ),
        .I3(\r_Bit_Index_reg_n_0_[0] ),
        .I4(\r_Rx_Byte[7]_i_2_n_0 ),
        .I5(o_Rx_Byte[2]),
        .O(\r_Rx_Byte[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEFFFFFFF20000000)) 
    \r_Rx_Byte[3]_i_1 
       (.I0(r_Rx_Data),
        .I1(\r_Bit_Index_reg_n_0_[2] ),
        .I2(\r_Bit_Index_reg_n_0_[0] ),
        .I3(\r_Bit_Index_reg_n_0_[1] ),
        .I4(\r_Rx_Byte[7]_i_2_n_0 ),
        .I5(o_Rx_Byte[3]),
        .O(\r_Rx_Byte[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFEFFFFF00200000)) 
    \r_Rx_Byte[4]_i_1 
       (.I0(r_Rx_Data),
        .I1(\r_Bit_Index_reg_n_0_[1] ),
        .I2(\r_Bit_Index_reg_n_0_[2] ),
        .I3(\r_Bit_Index_reg_n_0_[0] ),
        .I4(\r_Rx_Byte[7]_i_2_n_0 ),
        .I5(o_Rx_Byte[4]),
        .O(\r_Rx_Byte[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFBFFFFF00800000)) 
    \r_Rx_Byte[5]_i_1 
       (.I0(r_Rx_Data),
        .I1(\r_Bit_Index_reg_n_0_[0] ),
        .I2(\r_Bit_Index_reg_n_0_[2] ),
        .I3(\r_Bit_Index_reg_n_0_[1] ),
        .I4(\r_Rx_Byte[7]_i_2_n_0 ),
        .I5(o_Rx_Byte[5]),
        .O(\r_Rx_Byte[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFBFFFFF00800000)) 
    \r_Rx_Byte[6]_i_1 
       (.I0(r_Rx_Data),
        .I1(\r_Bit_Index_reg_n_0_[2] ),
        .I2(\r_Bit_Index_reg_n_0_[1] ),
        .I3(\r_Bit_Index_reg_n_0_[0] ),
        .I4(\r_Rx_Byte[7]_i_2_n_0 ),
        .I5(o_Rx_Byte[6]),
        .O(\r_Rx_Byte[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \r_Rx_Byte[7]_i_1 
       (.I0(r_Rx_Data),
        .I1(\r_Bit_Index_reg_n_0_[0] ),
        .I2(\r_Bit_Index_reg_n_0_[1] ),
        .I3(\r_Bit_Index_reg_n_0_[2] ),
        .I4(\r_Rx_Byte[7]_i_2_n_0 ),
        .I5(o_Rx_Byte[7]),
        .O(\r_Rx_Byte[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000005400)) 
    \r_Rx_Byte[7]_i_2 
       (.I0(r_Rx_DV_i_3_n_0),
        .I1(\r_Clock_Count_reg_n_0_[6] ),
        .I2(\r_Clock_Count_reg_n_0_[7] ),
        .I3(\r_SM_Main_reg_n_0_[1] ),
        .I4(\r_SM_Main_reg_n_0_[2] ),
        .I5(\r_SM_Main_reg_n_0_[0] ),
        .O(\r_Rx_Byte[7]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \r_Rx_Byte_reg[0] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_Rx_Byte[0]_i_1_n_0 ),
        .Q(o_Rx_Byte[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Rx_Byte_reg[1] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_Rx_Byte[1]_i_1_n_0 ),
        .Q(o_Rx_Byte[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Rx_Byte_reg[2] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_Rx_Byte[2]_i_1_n_0 ),
        .Q(o_Rx_Byte[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Rx_Byte_reg[3] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_Rx_Byte[3]_i_1_n_0 ),
        .Q(o_Rx_Byte[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Rx_Byte_reg[4] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_Rx_Byte[4]_i_1_n_0 ),
        .Q(o_Rx_Byte[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Rx_Byte_reg[5] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_Rx_Byte[5]_i_1_n_0 ),
        .Q(o_Rx_Byte[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Rx_Byte_reg[6] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_Rx_Byte[6]_i_1_n_0 ),
        .Q(o_Rx_Byte[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Rx_Byte_reg[7] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_Rx_Byte[7]_i_1_n_0 ),
        .Q(o_Rx_Byte[7]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'hFFF01000)) 
    r_Rx_DV_i_1
       (.I0(\r_SM_Main_reg_n_0_[2] ),
        .I1(r_Rx_DV_i_2_n_0),
        .I2(\r_SM_Main_reg_n_0_[1] ),
        .I3(\r_SM_Main_reg_n_0_[0] ),
        .I4(o_Rx_DV),
        .O(r_Rx_DV_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hAB)) 
    r_Rx_DV_i_2
       (.I0(r_Rx_DV_i_3_n_0),
        .I1(\r_Clock_Count_reg_n_0_[6] ),
        .I2(\r_Clock_Count_reg_n_0_[7] ),
        .O(r_Rx_DV_i_2_n_0));
  LUT6 #(
    .INIT(64'h0101010101111111)) 
    r_Rx_DV_i_3
       (.I0(\r_Clock_Count_reg_n_0_[7] ),
        .I1(\r_Clock_Count_reg_n_0_[5] ),
        .I2(\r_Clock_Count_reg_n_0_[4] ),
        .I3(\r_Clock_Count_reg_n_0_[1] ),
        .I4(\r_Clock_Count_reg_n_0_[2] ),
        .I5(\r_Clock_Count_reg_n_0_[3] ),
        .O(r_Rx_DV_i_3_n_0));
  FDRE #(
    .INIT(1'b0)) 
    r_Rx_DV_reg
       (.C(i_Clock),
        .CE(1'b1),
        .D(r_Rx_DV_i_1_n_0),
        .Q(o_Rx_DV),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    r_Rx_Data_R_reg
       (.C(i_Clock),
        .CE(1'b1),
        .D(i_Rx_Serial),
        .Q(r_Rx_Data_R),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    r_Rx_Data_reg
       (.C(i_Clock),
        .CE(1'b1),
        .D(r_Rx_Data_R),
        .Q(r_Rx_Data),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFABAAABAAABAA)) 
    \r_SM_Main[0]_i_1 
       (.I0(\r_SM_Main[0]_i_2_n_0 ),
        .I1(r_Rx_Data),
        .I2(\r_SM_Main_reg_n_0_[2] ),
        .I3(\r_SM_Main[0]_i_3_n_0 ),
        .I4(\r_Rx_Byte[7]_i_2_n_0 ),
        .I5(\r_SM_Main[0]_i_4_n_0 ),
        .O(\r_SM_Main[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000AFFC00000000)) 
    \r_SM_Main[0]_i_2 
       (.I0(r_Rx_DV_i_3_n_0),
        .I1(\r_SM_Main[1]_i_3_n_0 ),
        .I2(\r_SM_Main[0]_i_5_n_0 ),
        .I3(\r_SM_Main_reg_n_0_[1] ),
        .I4(\r_SM_Main_reg_n_0_[2] ),
        .I5(\r_SM_Main_reg_n_0_[0] ),
        .O(\r_SM_Main[0]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \r_SM_Main[0]_i_3 
       (.I0(\r_SM_Main_reg_n_0_[0] ),
        .I1(\r_SM_Main_reg_n_0_[1] ),
        .O(\r_SM_Main[0]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h80)) 
    \r_SM_Main[0]_i_4 
       (.I0(\r_Bit_Index_reg_n_0_[2] ),
        .I1(\r_Bit_Index_reg_n_0_[1] ),
        .I2(\r_Bit_Index_reg_n_0_[0] ),
        .O(\r_SM_Main[0]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \r_SM_Main[0]_i_5 
       (.I0(\r_Clock_Count_reg_n_0_[7] ),
        .I1(\r_Clock_Count_reg_n_0_[6] ),
        .O(\r_SM_Main[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h22222222FF2F2222)) 
    \r_SM_Main[1]_i_1 
       (.I0(\r_SM_Main[1]_i_2_n_0 ),
        .I1(\r_SM_Main[1]_i_3_n_0 ),
        .I2(\r_SM_Main_reg_n_0_[0] ),
        .I3(r_Rx_DV_i_2_n_0),
        .I4(\r_SM_Main_reg_n_0_[1] ),
        .I5(\r_SM_Main_reg_n_0_[2] ),
        .O(\r_SM_Main[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \r_SM_Main[1]_i_2 
       (.I0(\r_SM_Main_reg_n_0_[0] ),
        .I1(\r_SM_Main_reg_n_0_[2] ),
        .I2(r_Rx_Data),
        .I3(\r_SM_Main_reg_n_0_[1] ),
        .I4(\r_Clock_Count_reg_n_0_[7] ),
        .I5(\r_Clock_Count_reg_n_0_[6] ),
        .O(\r_SM_Main[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFBFFFFFFFFFFFFF)) 
    \r_SM_Main[1]_i_3 
       (.I0(\r_Clock_Count_reg_n_0_[4] ),
        .I1(\r_Clock_Count_reg_n_0_[3] ),
        .I2(\r_Clock_Count_reg_n_0_[5] ),
        .I3(\r_Clock_Count_reg_n_0_[2] ),
        .I4(\r_Clock_Count_reg_n_0_[1] ),
        .I5(\r_Clock_Count_reg_n_0_[0] ),
        .O(\r_SM_Main[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    \r_SM_Main[2]_i_1 
       (.I0(r_Rx_DV_i_2_n_0),
        .I1(\r_SM_Main_reg_n_0_[1] ),
        .I2(\r_SM_Main_reg_n_0_[2] ),
        .I3(\r_SM_Main_reg_n_0_[0] ),
        .O(\r_SM_Main[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \r_SM_Main_reg[0] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_SM_Main[0]_i_1_n_0 ),
        .Q(\r_SM_Main_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_SM_Main_reg[1] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_SM_Main[1]_i_1_n_0 ),
        .Q(\r_SM_Main_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_SM_Main_reg[2] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_SM_Main[2]_i_1_n_0 ),
        .Q(\r_SM_Main_reg_n_0_[2] ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_uart_tx
   (o_Tx_Active,
    o_Tx_Done,
    i_Clock,
    i_Tx_DV);
  output o_Tx_Active;
  output o_Tx_Done;
  input i_Clock;
  input i_Tx_DV;

  wire \FSM_sequential_r_SM_Main[0]_i_1_n_0 ;
  wire \FSM_sequential_r_SM_Main[0]_i_2_n_0 ;
  wire \FSM_sequential_r_SM_Main[1]_i_1_n_0 ;
  wire \FSM_sequential_r_SM_Main[2]_i_1_n_0 ;
  wire i_Clock;
  wire i_Tx_DV;
  wire o_Tx_Active;
  wire o_Tx_Done;
  wire r_Bit_Index;
  wire \r_Bit_Index[0]_i_1_n_0 ;
  wire \r_Bit_Index[1]_i_1_n_0 ;
  wire \r_Bit_Index[2]_i_1_n_0 ;
  wire \r_Bit_Index_reg_n_0_[0] ;
  wire \r_Bit_Index_reg_n_0_[1] ;
  wire \r_Bit_Index_reg_n_0_[2] ;
  wire [6:1]r_Clock_Count;
  wire r_Clock_Count0;
  wire \r_Clock_Count[0]_i_1__0_n_0 ;
  wire \r_Clock_Count[6]_i_4_n_0 ;
  wire r_Clock_Count_0;
  wire [6:1]r_Clock_Count_reg;
  wire \r_Clock_Count_reg_n_0_[0] ;
  wire [2:0]r_SM_Main;
  wire r_Tx_Active_i_1_n_0;
  wire r_Tx_Active_i_2_n_0;
  wire r_Tx_Done_i_1_n_0;

  LUT6 #(
    .INIT(64'h00000000C0E2C3E2)) 
    \FSM_sequential_r_SM_Main[0]_i_1 
       (.I0(i_Tx_DV),
        .I1(r_SM_Main[0]),
        .I2(r_Tx_Active_i_2_n_0),
        .I3(r_SM_Main[1]),
        .I4(\FSM_sequential_r_SM_Main[0]_i_2_n_0 ),
        .I5(r_SM_Main[2]),
        .O(\FSM_sequential_r_SM_Main[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h7F)) 
    \FSM_sequential_r_SM_Main[0]_i_2 
       (.I0(\r_Bit_Index_reg_n_0_[2] ),
        .I1(\r_Bit_Index_reg_n_0_[1] ),
        .I2(\r_Bit_Index_reg_n_0_[0] ),
        .O(\FSM_sequential_r_SM_Main[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h00D2)) 
    \FSM_sequential_r_SM_Main[1]_i_1 
       (.I0(r_SM_Main[0]),
        .I1(r_Tx_Active_i_2_n_0),
        .I2(r_SM_Main[1]),
        .I3(r_SM_Main[2]),
        .O(\FSM_sequential_r_SM_Main[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \FSM_sequential_r_SM_Main[2]_i_1 
       (.I0(r_SM_Main[0]),
        .I1(r_Tx_Active_i_2_n_0),
        .I2(r_SM_Main[1]),
        .I3(r_SM_Main[2]),
        .O(\FSM_sequential_r_SM_Main[2]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "s_IDLE:000,s_TX_START_BIT:001,s_TX_DATA_BITS:010,s_CLEANUP:100,s_TX_STOP_BIT:011" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_r_SM_Main_reg[0] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\FSM_sequential_r_SM_Main[0]_i_1_n_0 ),
        .Q(r_SM_Main[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "s_IDLE:000,s_TX_START_BIT:001,s_TX_DATA_BITS:010,s_CLEANUP:100,s_TX_STOP_BIT:011" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_r_SM_Main_reg[1] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\FSM_sequential_r_SM_Main[1]_i_1_n_0 ),
        .Q(r_SM_Main[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "s_IDLE:000,s_TX_START_BIT:001,s_TX_DATA_BITS:010,s_CLEANUP:100,s_TX_STOP_BIT:011" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_r_SM_Main_reg[2] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\FSM_sequential_r_SM_Main[2]_i_1_n_0 ),
        .Q(r_SM_Main[2]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAAA98AAAAAA00)) 
    \r_Bit_Index[0]_i_1 
       (.I0(\r_Bit_Index_reg_n_0_[0] ),
        .I1(r_Tx_Active_i_2_n_0),
        .I2(\FSM_sequential_r_SM_Main[0]_i_2_n_0 ),
        .I3(r_SM_Main[2]),
        .I4(r_SM_Main[0]),
        .I5(r_SM_Main[1]),
        .O(\r_Bit_Index[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h006A)) 
    \r_Bit_Index[1]_i_1 
       (.I0(\r_Bit_Index_reg_n_0_[1] ),
        .I1(r_Bit_Index),
        .I2(\r_Bit_Index_reg_n_0_[0] ),
        .I3(r_Clock_Count0),
        .O(\r_Bit_Index[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00006AAA)) 
    \r_Bit_Index[2]_i_1 
       (.I0(\r_Bit_Index_reg_n_0_[2] ),
        .I1(r_Bit_Index),
        .I2(\r_Bit_Index_reg_n_0_[0] ),
        .I3(\r_Bit_Index_reg_n_0_[1] ),
        .I4(r_Clock_Count0),
        .O(\r_Bit_Index[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h0010)) 
    \r_Bit_Index[2]_i_2 
       (.I0(r_SM_Main[0]),
        .I1(r_SM_Main[2]),
        .I2(r_SM_Main[1]),
        .I3(r_Tx_Active_i_2_n_0),
        .O(r_Bit_Index));
  FDRE #(
    .INIT(1'b0)) 
    \r_Bit_Index_reg[0] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_Bit_Index[0]_i_1_n_0 ),
        .Q(\r_Bit_Index_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Bit_Index_reg[1] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_Bit_Index[1]_i_1_n_0 ),
        .Q(\r_Bit_Index_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Bit_Index_reg[2] 
       (.C(i_Clock),
        .CE(1'b1),
        .D(\r_Bit_Index[2]_i_1_n_0 ),
        .Q(\r_Bit_Index_reg_n_0_[2] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \r_Clock_Count[0]_i_1__0 
       (.I0(r_Tx_Active_i_2_n_0),
        .I1(\r_Clock_Count_reg_n_0_[0] ),
        .O(\r_Clock_Count[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h60)) 
    \r_Clock_Count[1]_i_1 
       (.I0(r_Clock_Count_reg[1]),
        .I1(\r_Clock_Count_reg_n_0_[0] ),
        .I2(r_Tx_Active_i_2_n_0),
        .O(r_Clock_Count[1]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h6A00)) 
    \r_Clock_Count[2]_i_1 
       (.I0(r_Clock_Count_reg[2]),
        .I1(r_Clock_Count_reg[1]),
        .I2(\r_Clock_Count_reg_n_0_[0] ),
        .I3(r_Tx_Active_i_2_n_0),
        .O(r_Clock_Count[2]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h6AAA0000)) 
    \r_Clock_Count[3]_i_1 
       (.I0(r_Clock_Count_reg[3]),
        .I1(r_Clock_Count_reg[2]),
        .I2(\r_Clock_Count_reg_n_0_[0] ),
        .I3(r_Clock_Count_reg[1]),
        .I4(r_Tx_Active_i_2_n_0),
        .O(r_Clock_Count[3]));
  LUT6 #(
    .INIT(64'h6AAAAAAA00000000)) 
    \r_Clock_Count[4]_i_1 
       (.I0(r_Clock_Count_reg[4]),
        .I1(r_Clock_Count_reg[3]),
        .I2(r_Clock_Count_reg[1]),
        .I3(\r_Clock_Count_reg_n_0_[0] ),
        .I4(r_Clock_Count_reg[2]),
        .I5(r_Tx_Active_i_2_n_0),
        .O(r_Clock_Count[4]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h60)) 
    \r_Clock_Count[5]_i_1 
       (.I0(r_Clock_Count_reg[5]),
        .I1(\r_Clock_Count[6]_i_4_n_0 ),
        .I2(r_Tx_Active_i_2_n_0),
        .O(r_Clock_Count[5]));
  LUT3 #(
    .INIT(8'h01)) 
    \r_Clock_Count[6]_i_1 
       (.I0(r_SM_Main[1]),
        .I1(r_SM_Main[0]),
        .I2(r_SM_Main[2]),
        .O(r_Clock_Count0));
  LUT3 #(
    .INIT(8'h54)) 
    \r_Clock_Count[6]_i_2 
       (.I0(r_SM_Main[2]),
        .I1(r_SM_Main[1]),
        .I2(r_SM_Main[0]),
        .O(r_Clock_Count_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h6A00)) 
    \r_Clock_Count[6]_i_3 
       (.I0(r_Clock_Count_reg[6]),
        .I1(r_Clock_Count_reg[5]),
        .I2(\r_Clock_Count[6]_i_4_n_0 ),
        .I3(r_Tx_Active_i_2_n_0),
        .O(r_Clock_Count[6]));
  LUT5 #(
    .INIT(32'h80000000)) 
    \r_Clock_Count[6]_i_4 
       (.I0(r_Clock_Count_reg[4]),
        .I1(r_Clock_Count_reg[2]),
        .I2(\r_Clock_Count_reg_n_0_[0] ),
        .I3(r_Clock_Count_reg[1]),
        .I4(r_Clock_Count_reg[3]),
        .O(\r_Clock_Count[6]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \r_Clock_Count_reg[0] 
       (.C(i_Clock),
        .CE(r_Clock_Count_0),
        .D(\r_Clock_Count[0]_i_1__0_n_0 ),
        .Q(\r_Clock_Count_reg_n_0_[0] ),
        .R(r_Clock_Count0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Clock_Count_reg[1] 
       (.C(i_Clock),
        .CE(r_Clock_Count_0),
        .D(r_Clock_Count[1]),
        .Q(r_Clock_Count_reg[1]),
        .R(r_Clock_Count0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Clock_Count_reg[2] 
       (.C(i_Clock),
        .CE(r_Clock_Count_0),
        .D(r_Clock_Count[2]),
        .Q(r_Clock_Count_reg[2]),
        .R(r_Clock_Count0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Clock_Count_reg[3] 
       (.C(i_Clock),
        .CE(r_Clock_Count_0),
        .D(r_Clock_Count[3]),
        .Q(r_Clock_Count_reg[3]),
        .R(r_Clock_Count0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Clock_Count_reg[4] 
       (.C(i_Clock),
        .CE(r_Clock_Count_0),
        .D(r_Clock_Count[4]),
        .Q(r_Clock_Count_reg[4]),
        .R(r_Clock_Count0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Clock_Count_reg[5] 
       (.C(i_Clock),
        .CE(r_Clock_Count_0),
        .D(r_Clock_Count[5]),
        .Q(r_Clock_Count_reg[5]),
        .R(r_Clock_Count0));
  FDRE #(
    .INIT(1'b0)) 
    \r_Clock_Count_reg[6] 
       (.C(i_Clock),
        .CE(r_Clock_Count_0),
        .D(r_Clock_Count[6]),
        .Q(r_Clock_Count_reg[6]),
        .R(r_Clock_Count0));
  LUT6 #(
    .INIT(64'hFFFFBBFF00000030)) 
    r_Tx_Active_i_1
       (.I0(r_Tx_Active_i_2_n_0),
        .I1(r_SM_Main[0]),
        .I2(i_Tx_DV),
        .I3(r_SM_Main[1]),
        .I4(r_SM_Main[2]),
        .I5(o_Tx_Active),
        .O(r_Tx_Active_i_1_n_0));
  LUT6 #(
    .INIT(64'h00155555FFFFFFFF)) 
    r_Tx_Active_i_2
       (.I0(r_Clock_Count_reg[5]),
        .I1(r_Clock_Count_reg[1]),
        .I2(r_Clock_Count_reg[2]),
        .I3(r_Clock_Count_reg[3]),
        .I4(r_Clock_Count_reg[4]),
        .I5(r_Clock_Count_reg[6]),
        .O(r_Tx_Active_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    r_Tx_Active_reg
       (.C(i_Clock),
        .CE(1'b1),
        .D(r_Tx_Active_i_1_n_0),
        .Q(o_Tx_Active),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFA100A)) 
    r_Tx_Done_i_1
       (.I0(r_SM_Main[2]),
        .I1(r_Tx_Active_i_2_n_0),
        .I2(r_SM_Main[0]),
        .I3(r_SM_Main[1]),
        .I4(o_Tx_Done),
        .O(r_Tx_Done_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    r_Tx_Done_reg
       (.C(i_Clock),
        .CE(1'b1),
        .D(r_Tx_Done_i_1_n_0),
        .Q(o_Tx_Done),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

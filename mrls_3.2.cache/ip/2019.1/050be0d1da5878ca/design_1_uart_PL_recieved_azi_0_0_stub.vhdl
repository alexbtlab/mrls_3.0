-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Sat Dec  4 14:52:47 2021
-- Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_uart_PL_recieved_azi_0_0_stub.vhdl
-- Design      : design_1_uart_PL_recieved_azi_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    i_Clock : in STD_LOGIC;
    i_Tx_DV : in STD_LOGIC;
    i_Tx_Byte : in STD_LOGIC_VECTOR ( 7 downto 0 );
    o_Tx_Active : out STD_LOGIC;
    o_Tx_Serial : out STD_LOGIC;
    o_Tx_Done : out STD_LOGIC;
    i_Rx_Serial : in STD_LOGIC;
    o_Rx_DV : out STD_LOGIC;
    o_Rx_Byte : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "i_Clock,i_Tx_DV,i_Tx_Byte[7:0],o_Tx_Active,o_Tx_Serial,o_Tx_Done,i_Rx_Serial,o_Rx_DV,o_Rx_Byte[7:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "uart_PL_recieved_azimut_v1_0,Vivado 2019.1";
begin
end;

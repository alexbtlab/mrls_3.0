-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Tue Nov 30 13:07:39 2021
-- Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_AD9650_0_0_sim_netlist.vhdl
-- Design      : design_2_AD9650_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0 is
  port (
    m01_fft_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    DATA_INA : in STD_LOGIC_VECTOR ( 15 downto 0 );
    mux_fft : in STD_LOGIC;
    DATA_INB : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0 is
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[0]_INST_0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[10]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[11]_INST_0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[12]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[13]_INST_0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[14]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[15]_INST_0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[16]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[17]_INST_0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[18]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[19]_INST_0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[1]_INST_0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[20]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[21]_INST_0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[22]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[23]_INST_0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[24]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[25]_INST_0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[26]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[27]_INST_0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[28]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[29]_INST_0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[2]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[30]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[31]_INST_0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[3]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[4]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[5]_INST_0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[6]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[7]_INST_0\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[8]_INST_0\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[9]_INST_0\ : label is "soft_lutpair4";
begin
\m01_fft_axis_tdata[0]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(0),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(0)
    );
\m01_fft_axis_tdata[10]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(10),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(10)
    );
\m01_fft_axis_tdata[11]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(11),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(11)
    );
\m01_fft_axis_tdata[12]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(12),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(12)
    );
\m01_fft_axis_tdata[13]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(13),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(13)
    );
\m01_fft_axis_tdata[14]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(14),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(14)
    );
\m01_fft_axis_tdata[15]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(15),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(15)
    );
\m01_fft_axis_tdata[16]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(0),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(16)
    );
\m01_fft_axis_tdata[17]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(1),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(17)
    );
\m01_fft_axis_tdata[18]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(2),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(18)
    );
\m01_fft_axis_tdata[19]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(3),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(19)
    );
\m01_fft_axis_tdata[1]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(1),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(1)
    );
\m01_fft_axis_tdata[20]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(4),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(20)
    );
\m01_fft_axis_tdata[21]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(5),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(21)
    );
\m01_fft_axis_tdata[22]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(6),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(22)
    );
\m01_fft_axis_tdata[23]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(7),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(23)
    );
\m01_fft_axis_tdata[24]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(8),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(24)
    );
\m01_fft_axis_tdata[25]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(9),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(25)
    );
\m01_fft_axis_tdata[26]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(10),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(26)
    );
\m01_fft_axis_tdata[27]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(11),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(27)
    );
\m01_fft_axis_tdata[28]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(12),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(28)
    );
\m01_fft_axis_tdata[29]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(13),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(29)
    );
\m01_fft_axis_tdata[2]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(2),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(2)
    );
\m01_fft_axis_tdata[30]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(14),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(30)
    );
\m01_fft_axis_tdata[31]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INB(15),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(31)
    );
\m01_fft_axis_tdata[3]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(3),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(3)
    );
\m01_fft_axis_tdata[4]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(4),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(4)
    );
\m01_fft_axis_tdata[5]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(5),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(5)
    );
\m01_fft_axis_tdata[6]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(6),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(6)
    );
\m01_fft_axis_tdata[7]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(7),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(7)
    );
\m01_fft_axis_tdata[8]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(8),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(8)
    );
\m01_fft_axis_tdata[9]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => DATA_INA(9),
      I1 => mux_fft,
      O => m01_fft_axis_tdata(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk_10MHz : in STD_LOGIC;
    m00_fft_axis_tvalid : out STD_LOGIC;
    m00_fft_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_fft_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_fft_axis_tlast : out STD_LOGIC;
    m00_fft_axis_tready : in STD_LOGIC;
    m01_fft_axis_tvalid : out STD_LOGIC;
    m01_fft_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m01_fft_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m01_fft_axis_tlast : out STD_LOGIC;
    m01_fft_axis_tready : in STD_LOGIC;
    dco_or_dcoa : in STD_LOGIC;
    dco_or_dcob : in STD_LOGIC;
    dco_or_ora : in STD_LOGIC;
    dco_or_orb : in STD_LOGIC;
    ADC_PDwN : out STD_LOGIC;
    SYNC : out STD_LOGIC;
    DATA_INA : in STD_LOGIC_VECTOR ( 15 downto 0 );
    DATA_INB : in STD_LOGIC_VECTOR ( 15 downto 0 );
    allowed_clk : in STD_LOGIC;
    mux_fft : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    DCO : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_2_AD9650_0_0,AD9650_v2_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "AD9650_v2_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^dco_or_dcoa\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of allowed_clk : signal is "xilinx.com:signal:clock:1.0 allowed_clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of allowed_clk : signal is "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_allowed_clk_0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of dco_or_dcoa : signal is "user.org:interface:dco_or:1.0 DCO_OR dcoa";
  attribute X_INTERFACE_INFO of dco_or_dcob : signal is "user.org:interface:dco_or:1.0 DCO_OR dcob";
  attribute X_INTERFACE_INFO of dco_or_ora : signal is "user.org:interface:dco_or:1.0 DCO_OR ora";
  attribute X_INTERFACE_INFO of dco_or_orb : signal is "user.org:interface:dco_or:1.0 DCO_OR orb";
  attribute X_INTERFACE_PARAMETER of dco_or_orb : signal is "XIL_INTERFACENAME DCO_OR, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of m00_fft_axis_tlast : signal is "xilinx.com:interface:axis:1.0 m00_fft_axis TLAST";
  attribute X_INTERFACE_INFO of m00_fft_axis_tready : signal is "xilinx.com:interface:axis:1.0 m00_fft_axis TREADY";
  attribute X_INTERFACE_PARAMETER of m00_fft_axis_tready : signal is "XIL_INTERFACENAME m00_fft_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_fft_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 m00_fft_axis TVALID";
  attribute X_INTERFACE_INFO of m01_fft_axis_tlast : signal is "xilinx.com:interface:axis:1.0 m01_fft_axis TLAST";
  attribute X_INTERFACE_INFO of m01_fft_axis_tready : signal is "xilinx.com:interface:axis:1.0 m01_fft_axis TREADY";
  attribute X_INTERFACE_PARAMETER of m01_fft_axis_tready : signal is "XIL_INTERFACENAME m01_fft_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m01_fft_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 m01_fft_axis TVALID";
  attribute X_INTERFACE_INFO of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute X_INTERFACE_PARAMETER of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI:m01_fft_axis:m00_fft_axis, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_fft_axis_tdata : signal is "xilinx.com:interface:axis:1.0 m00_fft_axis TDATA";
  attribute X_INTERFACE_INFO of m00_fft_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 m00_fft_axis TSTRB";
  attribute X_INTERFACE_INFO of m01_fft_axis_tdata : signal is "xilinx.com:interface:axis:1.0 m01_fft_axis TDATA";
  attribute X_INTERFACE_INFO of m01_fft_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 m01_fft_axis TSTRB";
begin
  DCO <= \^dco_or_dcoa\;
  SYNC <= \<const0>\;
  \^dco_or_dcoa\ <= dco_or_dcoa;
  m00_fft_axis_tstrb(3) <= \<const1>\;
  m00_fft_axis_tstrb(2) <= \<const1>\;
  m00_fft_axis_tstrb(1) <= \<const1>\;
  m00_fft_axis_tstrb(0) <= \<const1>\;
  m01_fft_axis_tstrb(3) <= \<const1>\;
  m01_fft_axis_tstrb(2) <= \<const1>\;
  m01_fft_axis_tstrb(1) <= \<const1>\;
  m01_fft_axis_tstrb(0) <= \<const1>\;
  ADC_PDwN <= 'Z';
  m00_fft_axis_tlast <= 'Z';
  m00_fft_axis_tvalid <= 'Z';
  m01_fft_axis_tlast <= 'Z';
  m01_fft_axis_tvalid <= 'Z';
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0
     port map (
      DATA_INA(15 downto 0) => DATA_INA(15 downto 0),
      DATA_INB(15 downto 0) => DATA_INB(15 downto 0),
      m01_fft_axis_tdata(31 downto 0) => m01_fft_axis_tdata(31 downto 0),
      mux_fft => mux_fft
    );
\m00_fft_axis_tdata[0]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(0),
      O => m00_fft_axis_tdata(0)
    );
\m00_fft_axis_tdata[10]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(10),
      O => m00_fft_axis_tdata(10)
    );
\m00_fft_axis_tdata[11]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(11),
      O => m00_fft_axis_tdata(11)
    );
\m00_fft_axis_tdata[12]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(12),
      O => m00_fft_axis_tdata(12)
    );
\m00_fft_axis_tdata[13]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(13),
      O => m00_fft_axis_tdata(13)
    );
\m00_fft_axis_tdata[14]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(14),
      O => m00_fft_axis_tdata(14)
    );
\m00_fft_axis_tdata[15]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(15),
      O => m00_fft_axis_tdata(15)
    );
\m00_fft_axis_tdata[16]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(0),
      O => m00_fft_axis_tdata(16)
    );
\m00_fft_axis_tdata[17]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(1),
      O => m00_fft_axis_tdata(17)
    );
\m00_fft_axis_tdata[18]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(2),
      O => m00_fft_axis_tdata(18)
    );
\m00_fft_axis_tdata[19]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(3),
      O => m00_fft_axis_tdata(19)
    );
\m00_fft_axis_tdata[1]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(1),
      O => m00_fft_axis_tdata(1)
    );
\m00_fft_axis_tdata[20]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(4),
      O => m00_fft_axis_tdata(20)
    );
\m00_fft_axis_tdata[21]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(5),
      O => m00_fft_axis_tdata(21)
    );
\m00_fft_axis_tdata[22]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(6),
      O => m00_fft_axis_tdata(22)
    );
\m00_fft_axis_tdata[23]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(7),
      O => m00_fft_axis_tdata(23)
    );
\m00_fft_axis_tdata[24]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(8),
      O => m00_fft_axis_tdata(24)
    );
\m00_fft_axis_tdata[25]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(9),
      O => m00_fft_axis_tdata(25)
    );
\m00_fft_axis_tdata[26]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(10),
      O => m00_fft_axis_tdata(26)
    );
\m00_fft_axis_tdata[27]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(11),
      O => m00_fft_axis_tdata(27)
    );
\m00_fft_axis_tdata[28]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(12),
      O => m00_fft_axis_tdata(28)
    );
\m00_fft_axis_tdata[29]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(13),
      O => m00_fft_axis_tdata(29)
    );
\m00_fft_axis_tdata[2]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(2),
      O => m00_fft_axis_tdata(2)
    );
\m00_fft_axis_tdata[30]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(14),
      O => m00_fft_axis_tdata(30)
    );
\m00_fft_axis_tdata[31]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INB(15),
      O => m00_fft_axis_tdata(31)
    );
\m00_fft_axis_tdata[3]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(3),
      O => m00_fft_axis_tdata(3)
    );
\m00_fft_axis_tdata[4]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(4),
      O => m00_fft_axis_tdata(4)
    );
\m00_fft_axis_tdata[5]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(5),
      O => m00_fft_axis_tdata(5)
    );
\m00_fft_axis_tdata[6]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(6),
      O => m00_fft_axis_tdata(6)
    );
\m00_fft_axis_tdata[7]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(7),
      O => m00_fft_axis_tdata(7)
    );
\m00_fft_axis_tdata[8]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(8),
      O => m00_fft_axis_tdata(8)
    );
\m00_fft_axis_tdata[9]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => mux_fft,
      I1 => DATA_INA(9),
      O => m00_fft_axis_tdata(9)
    );
end STRUCTURE;

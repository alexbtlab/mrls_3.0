-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Wed Dec  8 12:48:09 2021
-- Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_averageFFT_0_0_sim_netlist.vhdl
-- Design      : design_1_averageFFT_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0 is
  port (
    reset_cnt_trig_ila : out STD_LOGIC;
    azimut : out STD_LOGIC_VECTOR ( 1 downto 0 );
    allowed_clk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    clk_10MHz : in STD_LOGIC;
    azimut_0 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0 is
  signal \^azimut\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal azimut_0_prev : STD_LOGIC;
  signal \azimut_r[0]_i_1_n_0\ : STD_LOGIC;
  signal \azimut_r[1]_i_1_n_0\ : STD_LOGIC;
  signal \azimut_r[1]_i_2_n_0\ : STD_LOGIC;
  signal \azimut_r[1]_i_3_n_0\ : STD_LOGIC;
  signal \azimut_r[1]_i_4_n_0\ : STD_LOGIC;
  signal \azimut_r[1]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_low_allowed_clk[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_allowed_clk[0]_i_3_n_0\ : STD_LOGIC;
  signal cnt_low_allowed_clk_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_low_allowed_clk_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal reset_cnt_trig0 : STD_LOGIC;
  signal \^reset_cnt_trig_ila\ : STD_LOGIC;
  signal \NLW_cnt_low_allowed_clk_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
  azimut(1 downto 0) <= \^azimut\(1 downto 0);
  reset_cnt_trig_ila <= \^reset_cnt_trig_ila\;
azimut_0_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => azimut_0,
      Q => azimut_0_prev,
      R => '0'
    );
\azimut_r[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000006AAA0000"
    )
        port map (
      I0 => \^azimut\(0),
      I1 => \azimut_r[1]_i_4_n_0\,
      I2 => \azimut_r[1]_i_3_n_0\,
      I3 => \azimut_r[1]_i_2_n_0\,
      I4 => m00_axis_aresetn,
      I5 => \^reset_cnt_trig_ila\,
      O => \azimut_r[0]_i_1_n_0\
    );
\azimut_r[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000006AAAAAAA"
    )
        port map (
      I0 => \^azimut\(1),
      I1 => \azimut_r[1]_i_2_n_0\,
      I2 => \azimut_r[1]_i_3_n_0\,
      I3 => \azimut_r[1]_i_4_n_0\,
      I4 => \^azimut\(0),
      I5 => \azimut_r[1]_i_5_n_0\,
      O => \azimut_r[1]_i_1_n_0\
    );
\azimut_r[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => cnt_low_allowed_clk_reg(6),
      I1 => cnt_low_allowed_clk_reg(7),
      I2 => cnt_low_allowed_clk_reg(4),
      I3 => cnt_low_allowed_clk_reg(5),
      I4 => cnt_low_allowed_clk_reg(9),
      I5 => cnt_low_allowed_clk_reg(8),
      O => \azimut_r[1]_i_2_n_0\
    );
\azimut_r[1]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => cnt_low_allowed_clk_reg(0),
      I1 => cnt_low_allowed_clk_reg(1),
      I2 => cnt_low_allowed_clk_reg(2),
      I3 => cnt_low_allowed_clk_reg(3),
      O => \azimut_r[1]_i_3_n_0\
    );
\azimut_r[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => cnt_low_allowed_clk_reg(12),
      I1 => cnt_low_allowed_clk_reg(13),
      I2 => cnt_low_allowed_clk_reg(10),
      I3 => cnt_low_allowed_clk_reg(11),
      I4 => cnt_low_allowed_clk_reg(15),
      I5 => cnt_low_allowed_clk_reg(14),
      O => \azimut_r[1]_i_4_n_0\
    );
\azimut_r[1]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^reset_cnt_trig_ila\,
      I1 => m00_axis_aresetn,
      O => \azimut_r[1]_i_5_n_0\
    );
\azimut_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \azimut_r[0]_i_1_n_0\,
      Q => \^azimut\(0),
      R => '0'
    );
\azimut_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \azimut_r[1]_i_1_n_0\,
      Q => \^azimut\(1),
      R => '0'
    );
\cnt_low_allowed_clk[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => allowed_clk,
      I1 => m00_axis_aresetn,
      O => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_low_allowed_clk_reg(0),
      O => \cnt_low_allowed_clk[0]_i_3_n_0\
    );
\cnt_low_allowed_clk_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[0]_i_2_n_7\,
      Q => cnt_low_allowed_clk_reg(0),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_low_allowed_clk_reg[0]_i_2_n_0\,
      CO(2) => \cnt_low_allowed_clk_reg[0]_i_2_n_1\,
      CO(1) => \cnt_low_allowed_clk_reg[0]_i_2_n_2\,
      CO(0) => \cnt_low_allowed_clk_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_low_allowed_clk_reg[0]_i_2_n_4\,
      O(2) => \cnt_low_allowed_clk_reg[0]_i_2_n_5\,
      O(1) => \cnt_low_allowed_clk_reg[0]_i_2_n_6\,
      O(0) => \cnt_low_allowed_clk_reg[0]_i_2_n_7\,
      S(3 downto 1) => cnt_low_allowed_clk_reg(3 downto 1),
      S(0) => \cnt_low_allowed_clk[0]_i_3_n_0\
    );
\cnt_low_allowed_clk_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[8]_i_1_n_5\,
      Q => cnt_low_allowed_clk_reg(10),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[8]_i_1_n_4\,
      Q => cnt_low_allowed_clk_reg(11),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[12]_i_1_n_7\,
      Q => cnt_low_allowed_clk_reg(12),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_low_allowed_clk_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt_low_allowed_clk_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_low_allowed_clk_reg[12]_i_1_n_1\,
      CO(1) => \cnt_low_allowed_clk_reg[12]_i_1_n_2\,
      CO(0) => \cnt_low_allowed_clk_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_low_allowed_clk_reg[12]_i_1_n_4\,
      O(2) => \cnt_low_allowed_clk_reg[12]_i_1_n_5\,
      O(1) => \cnt_low_allowed_clk_reg[12]_i_1_n_6\,
      O(0) => \cnt_low_allowed_clk_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt_low_allowed_clk_reg(15 downto 12)
    );
\cnt_low_allowed_clk_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[12]_i_1_n_6\,
      Q => cnt_low_allowed_clk_reg(13),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[12]_i_1_n_5\,
      Q => cnt_low_allowed_clk_reg(14),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[12]_i_1_n_4\,
      Q => cnt_low_allowed_clk_reg(15),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[0]_i_2_n_6\,
      Q => cnt_low_allowed_clk_reg(1),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[0]_i_2_n_5\,
      Q => cnt_low_allowed_clk_reg(2),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[0]_i_2_n_4\,
      Q => cnt_low_allowed_clk_reg(3),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[4]_i_1_n_7\,
      Q => cnt_low_allowed_clk_reg(4),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_low_allowed_clk_reg[0]_i_2_n_0\,
      CO(3) => \cnt_low_allowed_clk_reg[4]_i_1_n_0\,
      CO(2) => \cnt_low_allowed_clk_reg[4]_i_1_n_1\,
      CO(1) => \cnt_low_allowed_clk_reg[4]_i_1_n_2\,
      CO(0) => \cnt_low_allowed_clk_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_low_allowed_clk_reg[4]_i_1_n_4\,
      O(2) => \cnt_low_allowed_clk_reg[4]_i_1_n_5\,
      O(1) => \cnt_low_allowed_clk_reg[4]_i_1_n_6\,
      O(0) => \cnt_low_allowed_clk_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt_low_allowed_clk_reg(7 downto 4)
    );
\cnt_low_allowed_clk_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[4]_i_1_n_6\,
      Q => cnt_low_allowed_clk_reg(5),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[4]_i_1_n_5\,
      Q => cnt_low_allowed_clk_reg(6),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[4]_i_1_n_4\,
      Q => cnt_low_allowed_clk_reg(7),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[8]_i_1_n_7\,
      Q => cnt_low_allowed_clk_reg(8),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
\cnt_low_allowed_clk_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_low_allowed_clk_reg[4]_i_1_n_0\,
      CO(3) => \cnt_low_allowed_clk_reg[8]_i_1_n_0\,
      CO(2) => \cnt_low_allowed_clk_reg[8]_i_1_n_1\,
      CO(1) => \cnt_low_allowed_clk_reg[8]_i_1_n_2\,
      CO(0) => \cnt_low_allowed_clk_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_low_allowed_clk_reg[8]_i_1_n_4\,
      O(2) => \cnt_low_allowed_clk_reg[8]_i_1_n_5\,
      O(1) => \cnt_low_allowed_clk_reg[8]_i_1_n_6\,
      O(0) => \cnt_low_allowed_clk_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt_low_allowed_clk_reg(11 downto 8)
    );
\cnt_low_allowed_clk_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_low_allowed_clk_reg[8]_i_1_n_6\,
      Q => cnt_low_allowed_clk_reg(9),
      R => \cnt_low_allowed_clk[0]_i_1_n_0\
    );
reset_cnt_trig_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => azimut_0,
      I1 => azimut_0_prev,
      O => reset_cnt_trig0
    );
reset_cnt_trig_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => reset_cnt_trig0,
      Q => \^reset_cnt_trig_ila\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    s00_axis_tready : out STD_LOGIC;
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s00_axis_tstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s00_axis_tlast : in STD_LOGIC;
    s00_axis_tvalid : in STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    allowed_clk : in STD_LOGIC;
    azimut_0 : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    azimut_ila : out STD_LOGIC_VECTOR ( 15 downto 0 );
    low_azimut_ila : out STD_LOGIC_VECTOR ( 15 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    clk_10MHz : in STD_LOGIC;
    interrupt_frame : out STD_LOGIC;
    reset_cnt_trig_ila : out STD_LOGIC;
    allowed_clk_prev_ila : out STD_LOGIC;
    azimut : out STD_LOGIC_VECTOR ( 31 downto 0 );
    azimut8 : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_averageFFT_0_0,averageFFT_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "averageFFT_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^azimut\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^m00_axis_tready\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of allowed_clk : signal is "xilinx.com:signal:clock:1.0 allowed_clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of allowed_clk : signal is "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of interrupt_frame : signal is "xilinx.com:signal:interrupt:1.0 interrupt_frame INTERRUPT";
  attribute X_INTERFACE_PARAMETER of interrupt_frame : signal is "XIL_INTERFACENAME interrupt_frame, SENSITIVITY LEVEL_HIGH, PortWidth 1";
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK";
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_RESET m00_axis_aresetn, ASSOCIATED_BUSIF s00_axis:m00_axis, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0, PortWidth 1";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 m00_axis TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 m00_axis TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME m00_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 m00_axis TVALID";
  attribute X_INTERFACE_INFO of reset_cnt_trig_ila : signal is "xilinx.com:signal:reset:1.0 reset_cnt_trig_ila RST";
  attribute X_INTERFACE_PARAMETER of reset_cnt_trig_ila : signal is "XIL_INTERFACENAME reset_cnt_trig_ila, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 s00_axis TLAST";
  attribute X_INTERFACE_INFO of s00_axis_tready : signal is "xilinx.com:interface:axis:1.0 s00_axis TREADY";
  attribute X_INTERFACE_INFO of s00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 s00_axis TVALID";
  attribute X_INTERFACE_PARAMETER of s00_axis_tvalid : signal is "XIL_INTERFACENAME s00_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 m00_axis TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 m00_axis TSTRB";
  attribute X_INTERFACE_INFO of s00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 s00_axis TDATA";
  attribute X_INTERFACE_INFO of s00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 s00_axis TSTRB";
begin
  \^m00_axis_tready\ <= m00_axis_tready;
  azimut(31) <= \<const0>\;
  azimut(30) <= \<const0>\;
  azimut(29) <= \<const0>\;
  azimut(28) <= \<const0>\;
  azimut(27) <= \<const0>\;
  azimut(26) <= \<const0>\;
  azimut(25) <= \<const0>\;
  azimut(24) <= \<const0>\;
  azimut(23) <= \<const0>\;
  azimut(22) <= \<const0>\;
  azimut(21) <= \<const0>\;
  azimut(20) <= \<const0>\;
  azimut(19) <= \<const0>\;
  azimut(18) <= \<const0>\;
  azimut(17) <= \<const0>\;
  azimut(16) <= \<const0>\;
  azimut(15) <= \<const0>\;
  azimut(14) <= \<const0>\;
  azimut(13) <= \<const0>\;
  azimut(12) <= \<const0>\;
  azimut(11) <= \<const0>\;
  azimut(10) <= \<const0>\;
  azimut(9) <= \<const0>\;
  azimut(8) <= \<const0>\;
  azimut(7) <= \<const0>\;
  azimut(6) <= \<const0>\;
  azimut(5) <= \<const0>\;
  azimut(4) <= \<const0>\;
  azimut(3) <= \<const0>\;
  azimut(2) <= \<const0>\;
  azimut(1 downto 0) <= \^azimut\(1 downto 0);
  azimut8(31) <= \<const0>\;
  azimut8(30) <= \<const0>\;
  azimut8(29) <= \<const0>\;
  azimut8(28) <= \<const0>\;
  azimut8(27) <= \<const0>\;
  azimut8(26) <= \<const0>\;
  azimut8(25) <= \<const0>\;
  azimut8(24) <= \<const0>\;
  azimut8(23) <= \<const0>\;
  azimut8(22) <= \<const0>\;
  azimut8(21) <= \<const0>\;
  azimut8(20) <= \<const0>\;
  azimut8(19) <= \<const0>\;
  azimut8(18) <= \<const0>\;
  azimut8(17) <= \<const0>\;
  azimut8(16) <= \<const0>\;
  azimut8(15) <= \<const0>\;
  azimut8(14) <= \<const0>\;
  azimut8(13) <= \<const0>\;
  azimut8(12) <= \<const0>\;
  azimut8(11) <= \<const0>\;
  azimut8(10) <= \<const0>\;
  azimut8(9) <= \<const0>\;
  azimut8(8) <= \<const0>\;
  azimut8(7) <= \<const0>\;
  azimut8(6) <= \<const0>\;
  azimut8(5) <= \<const0>\;
  azimut8(4) <= \<const0>\;
  azimut8(3) <= \<const0>\;
  azimut8(2) <= \<const0>\;
  azimut8(1) <= \<const0>\;
  azimut8(0) <= \<const0>\;
  azimut_ila(15) <= \<const0>\;
  azimut_ila(14) <= \<const0>\;
  azimut_ila(13) <= \<const0>\;
  azimut_ila(12) <= \<const0>\;
  azimut_ila(11) <= \<const0>\;
  azimut_ila(10) <= \<const0>\;
  azimut_ila(9) <= \<const0>\;
  azimut_ila(8) <= \<const0>\;
  azimut_ila(7) <= \<const0>\;
  azimut_ila(6) <= \<const0>\;
  azimut_ila(5) <= \<const0>\;
  azimut_ila(4) <= \<const0>\;
  azimut_ila(3) <= \<const0>\;
  azimut_ila(2) <= \<const0>\;
  azimut_ila(1) <= \<const0>\;
  azimut_ila(0) <= \<const0>\;
  interrupt_frame <= \<const0>\;
  m00_axis_tdata(31) <= \<const0>\;
  m00_axis_tdata(30) <= \<const0>\;
  m00_axis_tdata(29) <= \<const0>\;
  m00_axis_tdata(28) <= \<const0>\;
  m00_axis_tdata(27) <= \<const0>\;
  m00_axis_tdata(26) <= \<const0>\;
  m00_axis_tdata(25) <= \<const0>\;
  m00_axis_tdata(24) <= \<const0>\;
  m00_axis_tdata(23) <= \<const0>\;
  m00_axis_tdata(22) <= \<const0>\;
  m00_axis_tdata(21) <= \<const0>\;
  m00_axis_tdata(20) <= \<const0>\;
  m00_axis_tdata(19) <= \<const0>\;
  m00_axis_tdata(18) <= \<const0>\;
  m00_axis_tdata(17) <= \<const0>\;
  m00_axis_tdata(16) <= \<const0>\;
  m00_axis_tdata(15) <= \<const0>\;
  m00_axis_tdata(14) <= \<const0>\;
  m00_axis_tdata(13) <= \<const0>\;
  m00_axis_tdata(12) <= \<const0>\;
  m00_axis_tdata(11) <= \<const0>\;
  m00_axis_tdata(10) <= \<const0>\;
  m00_axis_tdata(9) <= \<const0>\;
  m00_axis_tdata(8) <= \<const0>\;
  m00_axis_tdata(7) <= \<const0>\;
  m00_axis_tdata(6) <= \<const0>\;
  m00_axis_tdata(5) <= \<const0>\;
  m00_axis_tdata(4) <= \<const0>\;
  m00_axis_tdata(3) <= \<const0>\;
  m00_axis_tdata(2) <= \<const0>\;
  m00_axis_tdata(1) <= \<const0>\;
  m00_axis_tdata(0) <= \<const0>\;
  m00_axis_tlast <= \<const0>\;
  m00_axis_tstrb(3) <= \<const1>\;
  m00_axis_tstrb(2) <= \<const1>\;
  m00_axis_tstrb(1) <= \<const1>\;
  m00_axis_tstrb(0) <= \<const1>\;
  m00_axis_tvalid <= \<const0>\;
  s00_axis_tready <= \^m00_axis_tready\;
  allowed_clk_prev_ila <= 'Z';
  low_azimut_ila(0) <= 'Z';
  low_azimut_ila(1) <= 'Z';
  low_azimut_ila(2) <= 'Z';
  low_azimut_ila(3) <= 'Z';
  low_azimut_ila(4) <= 'Z';
  low_azimut_ila(5) <= 'Z';
  low_azimut_ila(6) <= 'Z';
  low_azimut_ila(7) <= 'Z';
  low_azimut_ila(8) <= 'Z';
  low_azimut_ila(9) <= 'Z';
  low_azimut_ila(10) <= 'Z';
  low_azimut_ila(11) <= 'Z';
  low_azimut_ila(12) <= 'Z';
  low_azimut_ila(13) <= 'Z';
  low_azimut_ila(14) <= 'Z';
  low_azimut_ila(15) <= 'Z';
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0
     port map (
      allowed_clk => allowed_clk,
      azimut(1 downto 0) => \^azimut\(1 downto 0),
      azimut_0 => azimut_0,
      clk_10MHz => clk_10MHz,
      m00_axis_aresetn => m00_axis_aresetn,
      reset_cnt_trig_ila => reset_cnt_trig_ila
    );
end STRUCTURE;

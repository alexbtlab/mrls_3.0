//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
//Date        : Mon Dec 13 10:47:37 2021
//Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
//Command     : generate_target design_2.bd
//Design      : design_2
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_2,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_2,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=10,numReposBlks=9,numNonXlnxBlks=3,numHierBlks=1,maxHierDepth=1,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_2.hwdef" *) 
module design_2
   (aclk_10MHz,
    allowed_clk,
    azimut_0,
    clk_10MHz,
    ext_load_sweep_val,
    fft_data_out,
    fft_tlast,
    fft_tvalid,
    fifo_in_tdata,
    frameSize,
    m00_axis_aclk,
    mux_fft,
    s00_axis_aresetn,
    sweep_val_ext);
  input aclk_10MHz;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.ALLOWED_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.ALLOWED_CLK, CLK_DOMAIN design_2_allowed_clk, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000" *) input allowed_clk;
  input azimut_0;
  input clk_10MHz;
  input ext_load_sweep_val;
  output [31:0]fft_data_out;
  output fft_tlast;
  output fft_tvalid;
  input [31:0]fifo_in_tdata;
  input [15:0]frameSize;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.M00_AXIS_ACLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.M00_AXIS_ACLK, ASSOCIATED_RESET s00_axis_aresetn, CLK_DOMAIN design_2_m00_axis_aclk, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000" *) input m00_axis_aclk;
  input mux_fft;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.S00_AXIS_ARESETN RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.S00_AXIS_ARESETN, INSERT_VIP 0, POLARITY ACTIVE_LOW" *) input s00_axis_aresetn;
  input [15:0]sweep_val_ext;

  wire HMC769_0_start_adc_count;
  wire [31:0]averageFFT_0_azimut8;
  wire azimut_0_0_1;
  wire clk_10MHz_1;
  wire [31:0]fifo_in_tdata_1;
  wire [0:0]hier_0_dout;
  wire hier_0_enable_intr;
  wire [63:0]hier_0_m00_dma_axis_TDATA;
  wire hier_0_m00_dma_axis_TLAST;
  wire hier_0_m00_dma_axis_TREADY;
  wire [7:0]hier_0_m00_dma_axis_TSTRB;
  wire hier_0_m00_dma_axis_TVALID;
  wire m00_axis_aclk_0_1;
  wire s00_axis_aresetn_0_1;
  wire [31:0]xlconstant_0_dout;

  assign azimut_0_0_1 = azimut_0;
  assign clk_10MHz_1 = clk_10MHz;
  assign fifo_in_tdata_1 = fifo_in_tdata[31:0];
  assign m00_axis_aclk_0_1 = m00_axis_aclk;
  assign s00_axis_aresetn_0_1 = s00_axis_aresetn;
  design_2_HMC769_0_0 HMC769_0
       (.azimut_0(azimut_0_0_1),
        .clkDCO_10MHz(clk_10MHz_1),
        .clk_10MHz(clk_10MHz_1),
        .s00_axi_aclk(m00_axis_aclk_0_1),
        .s00_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s00_axi_aresetn(s00_axis_aresetn_0_1),
        .s00_axi_arprot({1'b0,1'b0,1'b0}),
        .s00_axi_arvalid(1'b0),
        .s00_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s00_axi_awprot({1'b0,1'b0,1'b0}),
        .s00_axi_awvalid(1'b0),
        .s00_axi_bready(1'b0),
        .s00_axi_rready(1'b0),
        .s00_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s00_axi_wstrb({1'b1,1'b1,1'b1,1'b1}),
        .s00_axi_wvalid(1'b0),
        .spi_pll_ld_sdo(1'b0),
        .start_adc_count(HMC769_0_start_adc_count));
  design_2_averageFFT_0_0 averageFFT_0
       (.allowed_clk(HMC769_0_start_adc_count),
        .azimut8(averageFFT_0_azimut8),
        .azimut_0(azimut_0_0_1),
        .clk_10MHz(clk_10MHz_1),
        .enable_intr(hier_0_enable_intr),
        .intr_frame(xlconstant_0_dout),
        .m00_axis_aclk(m00_axis_aclk_0_1),
        .m00_axis_aresetn(s00_axis_aresetn_0_1),
        .m00_axis_tready(hier_0_dout),
        .s00_axis_tdata(hier_0_m00_dma_axis_TDATA),
        .s00_axis_tlast(hier_0_m00_dma_axis_TLAST),
        .s00_axis_tready(hier_0_m00_dma_axis_TREADY),
        .s00_axis_tstrb(hier_0_m00_dma_axis_TSTRB),
        .s00_axis_tvalid(hier_0_m00_dma_axis_TVALID));
  hier_0_imp_1JISFQJ hier_0
       (.allowed_clk(HMC769_0_start_adc_count),
        .azimut8(averageFFT_0_azimut8),
        .azimut_0(azimut_0_0_1),
        .clk_10MHz(clk_10MHz_1),
        .dout(hier_0_dout),
        .enable_intr(hier_0_enable_intr),
        .fifo_in_tdata(fifo_in_tdata_1),
        .m00_axis_aclk(m00_axis_aclk_0_1),
        .m00_dma_axis_tdata(hier_0_m00_dma_axis_TDATA),
        .m00_dma_axis_tlast(hier_0_m00_dma_axis_TLAST),
        .m00_dma_axis_tready(hier_0_m00_dma_axis_TREADY),
        .m00_dma_axis_tstrb(hier_0_m00_dma_axis_TSTRB),
        .m00_dma_axis_tvalid(hier_0_m00_dma_axis_TVALID),
        .s00_axis_aresetn(s00_axis_aresetn_0_1));
  design_2_xlconstant_0_1 xlconstant_0
       (.dout(xlconstant_0_dout));
endmodule

module hier_0_imp_1JISFQJ
   (allowed_clk,
    azimut8,
    azimut_0,
    clk_10MHz,
    dout,
    enable_intr,
    fifo_in_tdata,
    m00_axis_aclk,
    m00_dma_axis_tdata,
    m00_dma_axis_tlast,
    m00_dma_axis_tready,
    m00_dma_axis_tstrb,
    m00_dma_axis_tvalid,
    s00_axis_aresetn);
  input allowed_clk;
  input [31:0]azimut8;
  input azimut_0;
  input clk_10MHz;
  output [0:0]dout;
  output enable_intr;
  input [31:0]fifo_in_tdata;
  input m00_axis_aclk;
  output [63:0]m00_dma_axis_tdata;
  output m00_dma_axis_tlast;
  input m00_dma_axis_tready;
  output [7:0]m00_dma_axis_tstrb;
  output m00_dma_axis_tvalid;
  input s00_axis_aresetn;

  wire AD9650_0_enable_intr;
  wire [31:0]AD9650_0_m00_fft_axis_TDATA;
  wire AD9650_0_m00_fft_axis_TLAST;
  wire AD9650_0_m00_fft_axis_TREADY;
  wire AD9650_0_m00_fft_axis_TVALID;
  wire [31:0]AD9650_0_m01_fft_axis_TDATA;
  wire AD9650_0_m01_fft_axis_TLAST;
  wire AD9650_0_m01_fft_axis_TREADY;
  wire AD9650_0_m01_fft_axis_TVALID;
  wire [63:0]Conn1_TDATA;
  wire Conn1_TLAST;
  wire Conn1_TREADY;
  wire [7:0]Conn1_TSTRB;
  wire Conn1_TVALID;
  wire allowed_clk_1;
  wire [31:0]azimut8_1;
  wire azimut_0_1;
  wire clk_10MHz_1;
  wire [31:0]fifo_in_tdata_1;
  wire m00_axis_aclk_0_1;
  wire s00_axis_aresetn_1;
  wire [63:0]xfft_0_M_AXIS_DATA_TDATA;
  wire xfft_0_M_AXIS_DATA_TLAST;
  wire xfft_0_M_AXIS_DATA_TREADY;
  wire xfft_0_M_AXIS_DATA_TVALID;
  wire [63:0]xfft_1_M_AXIS_DATA_TDATA;
  wire xfft_1_M_AXIS_DATA_TLAST;
  wire xfft_1_M_AXIS_DATA_TREADY;
  wire xfft_1_M_AXIS_DATA_TVALID;
  wire [31:0]xlconstant_0_dout;
  wire [0:0]xlconstant_2_dout;

  assign Conn1_TREADY = m00_dma_axis_tready;
  assign allowed_clk_1 = allowed_clk;
  assign azimut8_1 = azimut8[31:0];
  assign azimut_0_1 = azimut_0;
  assign clk_10MHz_1 = clk_10MHz;
  assign dout[0] = xlconstant_2_dout;
  assign enable_intr = AD9650_0_enable_intr;
  assign fifo_in_tdata_1 = fifo_in_tdata[31:0];
  assign m00_axis_aclk_0_1 = m00_axis_aclk;
  assign m00_dma_axis_tdata[63:0] = Conn1_TDATA;
  assign m00_dma_axis_tlast = Conn1_TLAST;
  assign m00_dma_axis_tstrb[7:0] = Conn1_TSTRB;
  assign m00_dma_axis_tvalid = Conn1_TVALID;
  assign s00_axis_aresetn_1 = s00_axis_aresetn;
  design_2_AD9650_0_0 AD9650_0
       (.DATA_INA(fifo_in_tdata_1[15:0]),
        .DATA_INB(fifo_in_tdata_1[15:0]),
        .allowed_clk(allowed_clk_1),
        .azimut8(azimut8_1),
        .azimut_0(azimut_0_1),
        .clk_10MHz(clk_10MHz_1),
        .dco_or_dcoa(clk_10MHz_1),
        .dco_or_dcob(clk_10MHz_1),
        .dco_or_ora(clk_10MHz_1),
        .dco_or_orb(clk_10MHz_1),
        .enable_intr(AD9650_0_enable_intr),
        .m00_dma_axis_tdata(Conn1_TDATA),
        .m00_dma_axis_tlast(Conn1_TLAST),
        .m00_dma_axis_tready(Conn1_TREADY),
        .m00_dma_axis_tstrb(Conn1_TSTRB),
        .m00_dma_axis_tvalid(Conn1_TVALID),
        .m00_fft_axis_tdata(AD9650_0_m00_fft_axis_TDATA),
        .m00_fft_axis_tlast(AD9650_0_m00_fft_axis_TLAST),
        .m00_fft_axis_tready(AD9650_0_m00_fft_axis_TREADY),
        .m00_fft_axis_tvalid(AD9650_0_m00_fft_axis_TVALID),
        .m01_fft_axis_tdata(AD9650_0_m01_fft_axis_TDATA),
        .m01_fft_axis_tlast(AD9650_0_m01_fft_axis_TLAST),
        .m01_fft_axis_tready(AD9650_0_m01_fft_axis_TREADY),
        .m01_fft_axis_tvalid(AD9650_0_m01_fft_axis_TVALID),
        .max_azimut_read(xlconstant_0_dout[15:0]),
        .s00_axi_aclk(m00_axis_aclk_0_1),
        .s00_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s00_axi_aresetn(s00_axis_aresetn_1),
        .s00_axi_arprot({1'b0,1'b0,1'b0}),
        .s00_axi_arvalid(1'b0),
        .s00_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s00_axi_awprot({1'b0,1'b0,1'b0}),
        .s00_axi_awvalid(1'b0),
        .s00_axi_bready(1'b0),
        .s00_axi_rready(1'b0),
        .s00_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s00_axi_wstrb({1'b1,1'b1,1'b1,1'b1}),
        .s00_axi_wvalid(1'b0),
        .s00_fft_axis_tdata(xfft_0_M_AXIS_DATA_TDATA),
        .s00_fft_axis_tlast(xfft_0_M_AXIS_DATA_TLAST),
        .s00_fft_axis_tready(xfft_0_M_AXIS_DATA_TREADY),
        .s00_fft_axis_tstrb({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .s00_fft_axis_tvalid(xfft_0_M_AXIS_DATA_TVALID),
        .s01_fft_axis_tdata(xfft_1_M_AXIS_DATA_TDATA),
        .s01_fft_axis_tlast(xfft_1_M_AXIS_DATA_TLAST),
        .s01_fft_axis_tready(xfft_1_M_AXIS_DATA_TREADY),
        .s01_fft_axis_tstrb({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .s01_fft_axis_tvalid(xfft_1_M_AXIS_DATA_TVALID));
  design_2_xfft_0_0 xfft_0
       (.aclk(m00_axis_aclk_0_1),
        .m_axis_data_tdata(xfft_0_M_AXIS_DATA_TDATA),
        .m_axis_data_tlast(xfft_0_M_AXIS_DATA_TLAST),
        .m_axis_data_tready(xfft_0_M_AXIS_DATA_TREADY),
        .m_axis_data_tvalid(xfft_0_M_AXIS_DATA_TVALID),
        .s_axis_config_tdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axis_config_tvalid(1'b0),
        .s_axis_data_tdata(AD9650_0_m00_fft_axis_TDATA),
        .s_axis_data_tlast(AD9650_0_m00_fft_axis_TLAST),
        .s_axis_data_tready(AD9650_0_m00_fft_axis_TREADY),
        .s_axis_data_tvalid(AD9650_0_m00_fft_axis_TVALID));
  design_2_xfft_0_1 xfft_1
       (.aclk(m00_axis_aclk_0_1),
        .m_axis_data_tdata(xfft_1_M_AXIS_DATA_TDATA),
        .m_axis_data_tlast(xfft_1_M_AXIS_DATA_TLAST),
        .m_axis_data_tready(xfft_1_M_AXIS_DATA_TREADY),
        .m_axis_data_tvalid(xfft_1_M_AXIS_DATA_TVALID),
        .s_axis_config_tdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axis_config_tvalid(1'b0),
        .s_axis_data_tdata(AD9650_0_m01_fft_axis_TDATA),
        .s_axis_data_tlast(AD9650_0_m01_fft_axis_TLAST),
        .s_axis_data_tready(AD9650_0_m01_fft_axis_TREADY),
        .s_axis_data_tvalid(AD9650_0_m01_fft_axis_TVALID));
  design_2_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
  design_2_xlconstant_1_0 xlconstant_1
       ();
  design_2_xlconstant_1_1 xlconstant_2
       (.dout(xlconstant_2_dout));
endmodule

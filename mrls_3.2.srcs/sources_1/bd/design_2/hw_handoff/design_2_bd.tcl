
################################################################
# This is a generated script based on design: design_2
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2019.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_2_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7z020clg400-1
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name design_2

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: hier_0
proc create_hier_cell_hier_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_hier_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 m00_dma_axis


  # Create pins
  create_bd_pin -dir I -type clk allowed_clk
  create_bd_pin -dir I -from 31 -to 0 azimut8
  create_bd_pin -dir I azimut_0
  create_bd_pin -dir I clk_10MHz
  create_bd_pin -dir O -from 0 -to 0 dout
  create_bd_pin -dir O -type intr enable_intr
  create_bd_pin -dir I -from 31 -to 0 fifo_in_tdata
  create_bd_pin -dir I -type clk m00_axis_aclk
  create_bd_pin -dir I -type rst s00_axis_aresetn

  # Create instance: AD9650_0, and set properties
  set AD9650_0 [ create_bd_cell -type ip -vlnv bt.local:user:AD9650:2.0 AD9650_0 ]

  # Create instance: xfft_0, and set properties
  set xfft_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xfft:9.1 xfft_0 ]
  set_property -dict [ list \
   CONFIG.implementation_options {radix_4_burst_io} \
   CONFIG.number_of_stages_using_block_ram_for_data_and_phase_factors {0} \
   CONFIG.output_ordering {natural_order} \
   CONFIG.scaling_options {unscaled} \
   CONFIG.target_clock_frequency {100} \
   CONFIG.transform_length {8192} \
 ] $xfft_0

  # Create instance: xfft_1, and set properties
  set xfft_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xfft:9.1 xfft_1 ]
  set_property -dict [ list \
   CONFIG.implementation_options {radix_4_burst_io} \
   CONFIG.number_of_stages_using_block_ram_for_data_and_phase_factors {0} \
   CONFIG.output_ordering {natural_order} \
   CONFIG.scaling_options {unscaled} \
   CONFIG.target_clock_frequency {100} \
   CONFIG.transform_length {8192} \
 ] $xfft_1

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {2} \
   CONFIG.CONST_WIDTH {32} \
 ] $xlconstant_0

  # Create instance: xlconstant_1, and set properties
  set xlconstant_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_1 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0xFFFF} \
   CONFIG.CONST_WIDTH {16} \
 ] $xlconstant_1

  # Create instance: xlconstant_2, and set properties
  set xlconstant_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_2 ]

  # Create interface connections
  connect_bd_intf_net -intf_net AD9650_0_m00_fft_axis [get_bd_intf_pins AD9650_0/m00_fft_axis] [get_bd_intf_pins xfft_0/S_AXIS_DATA]
  connect_bd_intf_net -intf_net AD9650_0_m01_fft_axis [get_bd_intf_pins AD9650_0/m01_fft_axis] [get_bd_intf_pins xfft_1/S_AXIS_DATA]
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins m00_dma_axis] [get_bd_intf_pins AD9650_0/m00_dma_axis]
  connect_bd_intf_net -intf_net xfft_0_M_AXIS_DATA [get_bd_intf_pins AD9650_0/s00_fft_axis] [get_bd_intf_pins xfft_0/M_AXIS_DATA]
  connect_bd_intf_net -intf_net xfft_1_M_AXIS_DATA [get_bd_intf_pins AD9650_0/s01_fft_axis] [get_bd_intf_pins xfft_1/M_AXIS_DATA]

  # Create port connections
  connect_bd_net -net AD9650_0_enable_intr [get_bd_pins enable_intr] [get_bd_pins AD9650_0/enable_intr]
  connect_bd_net -net allowed_clk_1 [get_bd_pins allowed_clk] [get_bd_pins AD9650_0/allowed_clk]
  connect_bd_net -net azimut8_1 [get_bd_pins azimut8] [get_bd_pins AD9650_0/azimut8]
  connect_bd_net -net azimut_0_1 [get_bd_pins azimut_0] [get_bd_pins AD9650_0/azimut_0]
  connect_bd_net -net clk_10MHz_1 [get_bd_pins clk_10MHz] [get_bd_pins AD9650_0/clk_10MHz] [get_bd_pins AD9650_0/dco_or_dcoa] [get_bd_pins AD9650_0/dco_or_dcob] [get_bd_pins AD9650_0/dco_or_ora] [get_bd_pins AD9650_0/dco_or_orb]
  connect_bd_net -net fifo_in_tdata_1 [get_bd_pins fifo_in_tdata] [get_bd_pins AD9650_0/DATA_INA] [get_bd_pins AD9650_0/DATA_INB]
  connect_bd_net -net m00_axis_aclk_0_1 [get_bd_pins m00_axis_aclk] [get_bd_pins AD9650_0/s00_axi_aclk] [get_bd_pins xfft_0/aclk] [get_bd_pins xfft_1/aclk]
  connect_bd_net -net s00_axis_aresetn_1 [get_bd_pins s00_axis_aresetn] [get_bd_pins AD9650_0/s00_axi_aresetn]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins AD9650_0/max_azimut_read] [get_bd_pins xlconstant_0/dout]
  connect_bd_net -net xlconstant_2_dout [get_bd_pins dout] [get_bd_pins xlconstant_2/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports

  # Create ports
  set aclk_10MHz [ create_bd_port -dir I aclk_10MHz ]
  set allowed_clk [ create_bd_port -dir I -type clk allowed_clk ]
  set azimut_0 [ create_bd_port -dir I azimut_0 ]
  set clk_10MHz [ create_bd_port -dir I clk_10MHz ]
  set ext_load_sweep_val [ create_bd_port -dir I ext_load_sweep_val ]
  set fft_data_out [ create_bd_port -dir O -from 31 -to 0 fft_data_out ]
  set fft_tlast [ create_bd_port -dir O fft_tlast ]
  set fft_tvalid [ create_bd_port -dir O fft_tvalid ]
  set fifo_in_tdata [ create_bd_port -dir I -from 31 -to 0 fifo_in_tdata ]
  set frameSize [ create_bd_port -dir I -from 15 -to 0 frameSize ]
  set m00_axis_aclk [ create_bd_port -dir I -type clk m00_axis_aclk ]
  set mux_fft [ create_bd_port -dir I mux_fft ]
  set s00_axis_aresetn [ create_bd_port -dir I -type rst s00_axis_aresetn ]
  set sweep_val_ext [ create_bd_port -dir I -from 15 -to 0 sweep_val_ext ]

  # Create instance: HMC769_0, and set properties
  set HMC769_0 [ create_bd_cell -type ip -vlnv user.org:user:HMC769:4.0 HMC769_0 ]

  # Create instance: averageFFT_0, and set properties
  set averageFFT_0 [ create_bd_cell -type ip -vlnv bt.local:user:averageFFT:1.0 averageFFT_0 ]

  # Create instance: hier_0
  create_hier_cell_hier_0 [current_bd_instance .] hier_0

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {7} \
   CONFIG.CONST_WIDTH {32} \
 ] $xlconstant_0

  # Create interface connections
  connect_bd_intf_net -intf_net hier_0_m00_dma_axis [get_bd_intf_pins averageFFT_0/s00_axis] [get_bd_intf_pins hier_0/m00_dma_axis]

  # Create port connections
  connect_bd_net -net HMC769_0_start_adc_count [get_bd_pins HMC769_0/start_adc_count] [get_bd_pins averageFFT_0/allowed_clk] [get_bd_pins hier_0/allowed_clk]
  connect_bd_net -net averageFFT_0_azimut8 [get_bd_pins averageFFT_0/azimut8] [get_bd_pins hier_0/azimut8]
  connect_bd_net -net azimut_0_0_1 [get_bd_ports azimut_0] [get_bd_pins HMC769_0/azimut_0] [get_bd_pins averageFFT_0/azimut_0] [get_bd_pins hier_0/azimut_0]
  connect_bd_net -net clk_10MHz_1 [get_bd_ports clk_10MHz] [get_bd_pins HMC769_0/clkDCO_10MHz] [get_bd_pins HMC769_0/clk_10MHz] [get_bd_pins averageFFT_0/clk_10MHz] [get_bd_pins hier_0/clk_10MHz]
  connect_bd_net -net fifo_in_tdata_1 [get_bd_ports fifo_in_tdata] [get_bd_pins hier_0/fifo_in_tdata]
  connect_bd_net -net hier_0_dout [get_bd_pins averageFFT_0/m00_axis_tready] [get_bd_pins hier_0/dout]
  connect_bd_net -net hier_0_enable_intr [get_bd_pins averageFFT_0/enable_intr] [get_bd_pins hier_0/enable_intr]
  connect_bd_net -net m00_axis_aclk_0_1 [get_bd_ports m00_axis_aclk] [get_bd_pins HMC769_0/s00_axi_aclk] [get_bd_pins averageFFT_0/m00_axis_aclk] [get_bd_pins hier_0/m00_axis_aclk]
  connect_bd_net -net s00_axis_aresetn_0_1 [get_bd_ports s00_axis_aresetn] [get_bd_pins HMC769_0/s00_axi_aresetn] [get_bd_pins averageFFT_0/m00_axis_aresetn] [get_bd_pins hier_0/s00_axis_aresetn]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins averageFFT_0/intr_frame] [get_bd_pins xlconstant_0/dout]

  # Create address segments


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""



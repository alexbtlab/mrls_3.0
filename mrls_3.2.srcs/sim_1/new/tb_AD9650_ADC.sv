`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 22.10.2021 10:08:50
// Design Name: 
// Module Name: tb_AD9650_ADC
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_AD9650_ADC(  );
    
    reg RST_N;
    reg START;
    wire [12:0] ADR;
    wire [5:0] DATA_TX;
    wire [5:0] DATA_RX;
    reg CLK_10;
    reg CLK;
    reg CLK_100;
    reg SDIO;
    wire SCK;
    wire CS;   
    reg RW;
    
//    string wr_str =  "write state";
//    string rd_str =  "read state";
//    string str = (RW) ? rd_str : wr_str;
    
    wire SDIO_w;
//    assign SDIO_w = SDIO;
    
//   spi_AD9650 spi_AD9650_inst(
//    . START(START),
//    . ADR(13'h05AC),
//    . DATA_TX(8'hAA),
//    . DATA_RX(DATA_RX),
//    . CLK_10(CLK),
//    . CLK_100(CLK),
//    . SDIO(SDIO_w),
//    . SCK(SCK),
//    . CS(CS),
//    . RW(RW)    // R-1 W-0
//    );  

 reg pll_trig_reg = 0;
 
    reg  [15:0] sweep_val = 9000; 
            reg [15:0] trig_tguard_counter=0;   
        reg [15:0] trig_tsweep_counter=0;
    
    always @(posedge CLK_10) begin
    //if(EN_clk_8MHz) begin
        if ((trig_tsweep_counter < sweep_val)&(trig_tguard_counter==0))
        begin 
            trig_tsweep_counter <= trig_tsweep_counter + 1;
            pll_trig_reg <= 0;
        end
        else if ((trig_tsweep_counter == sweep_val)&(trig_tguard_counter==0))
        begin
            trig_tsweep_counter <= 0;
            pll_trig_reg <= 1;
            trig_tguard_counter<=1;                        
        end
        else if ((trig_tsweep_counter==0)&(trig_tguard_counter>0)&(trig_tguard_counter<100))
        begin
            trig_tsweep_counter <= 0;
            pll_trig_reg <= 0;
            trig_tguard_counter<=trig_tguard_counter+1;                        
        end
        else if ((trig_tsweep_counter==0)&(trig_tguard_counter==100))
        begin
            trig_tsweep_counter <= 0;
            pll_trig_reg <= 1;
            trig_tguard_counter<=0;                        
        end
    //end
end
    wire start_adc_count;
    
     reg start_adc_count_r;
     assign start_adc_count = start_adc_count_r;
        
    always @(posedge CLK_10)  begin
    //if(EN_clk_8MHz) begin 
        if((trig_tguard_counter == 0) & (trig_tsweep_counter > 800))
            start_adc_count_r <= 1;
        else
            start_adc_count_r <= 0;
   //end         
end

reg[15:0] cnt = 0;
reg [15:0] s00_axis_tdata_r;
wire [32-1 : 0] s00_axis_tdata;

assign s00_axis_tdata = s00_axis_tdata_r;

    always @(negedge CLK_10)  begin
      s00_axis_tdata_r <= cnt;  
      cnt <= cnt + 1;
    end

    	 wire  clk_10MHz;
		 wire  allow_get_data;
		 wire  s00_axis_aclk;
		 wire  s00_axis_aresetn;
		 wire  s00_axis_tready;
		 
		 wire [(32/8)-1 : 0] s00_axis_tstrb;
		 wire  s00_axis_tlast;
		 wire  s00_axis_tvalid;

		// Ports of Axi Master Bus Interface M00_AXIS
		 wire  m00_axis_aclk;
		 wire  m00_axis_aresetn;
		 wire  m00_axis_tvalid;
		 wire [32-1 : 0] m00_axis_tdata;
		 wire [(32/8)-1 : 0] m00_axis_tstrb;
		 wire  m00_axis_tlast;
		 wire  m00_axis_tready;
		
		 wire [15:0] cnt_10_ila;		
		 wire [15:0] frameSize;
		
		
		
	
	
	interface spi_pll;
    logic sen, sck, mosi, cen, ld_sdo;      // Indicates if slave is ready to accept data
    modport master  (output sen, output sck, output mosi, output cen, input ld_sdo);
endinterface

//spi_pll.master SPI_PLL;

    	adcDataPack_v1_0 # 
	(

		.C_S00_AXIS_TDATA_WIDTH(32),

		.C_M00_AXIS_TDATA_WIDTH	(32),
		.C_M00_AXIS_START_COUNT	 (32)
	)  adcDataPack_v1_0
	(
		.clk_10MHz(CLK_10),
		.allow_get_data(start_adc_count),
		.s00_axis_aclk(s00_axis_aclk),
		.s00_axis_aresetn(s00_axis_aresetn),
		.s00_axis_tready(s00_axis_tready),
		. s00_axis_tdata(s00_axis_tdata),
		. s00_axis_tstrb(s00_axis_tstrb),
		. s00_axis_tlast(s00_axis_tlast),
		. s00_axis_tvalid(s00_axis_tvalid),

		// Ports of Axi Master Bus Interface M00_AXIS
		. m00_axis_aclk(CLK_100),
		. m00_axis_aresetn(m00_axis_aresetn),
		. m00_axis_tvalid(m00_axis_tvalid),
		. m00_axis_tdata(m00_axis_tdata),
		. m00_axis_tstrb(m00_axis_tstrb),
		. m00_axis_tlast(m00_axis_tlast),
		. m00_axis_tready( 1),
		
		. cnt_10_ila(cnt_10_ila),		
		.frameSize(8192)
	);
	
		 wire  s00_axi_aclk;
		 wire  s00_axi_aresetn;
		 wire [32-1 : 0] s00_axi_awaddr;
		 wire [2 : 0] s00_axi_awprot;
		 wire  s00_axi_awvalid;
		 wire  s00_axi_awready;
		 wire [32-1 : 0] s00_axi_wdata;
		 wire [(32/8)-1 : 0] s00_axi_wstrb;
		 wire  s00_axi_wvalid;
		 wire  s00_axi_wready;
		 wire [1 : 0] s00_axi_bresp;
		 wire  s00_axi_bvalid;
		 wire  s00_axi_bready;
		 wire [32-1 : 0] s00_axi_araddr;
		 wire [2 : 0] s00_axi_arprot;
		 wire  s00_axi_arvalid;
		 wire  s00_axi_arready;
		 wire [32-1 : 0] s00_axi_rdata;
		 wire [1 : 0] s00_axi_rresp;
		 wire  s00_axi_rvalid;
		 wire  s00_axi_rready;
		
//		spi_pll.master SPI_PLL2;
		
		
		 wire  pll_trig;
		 wire [5:0] ATTEN;
		 wire PLL_POW_EN;
		 wire PAMP_EN;
         wire start_adc_coun; 
	
//	 HMC769_v4_0 #
//	(  
//		// Parameters of Axi Slave Bus Interface S00_AXI
//		.C_S00_AXI_DATA_WIDTH(32),
//		.C_S00_AXI_ADDR_WIDTH( 6)
//	) HMC769_v4_0
//	(	
//		. s00_axi_aclk(s00_axi_aclk),
//		. s00_axi_aresetn(s00_axi_aresetn),
//		. s00_axi_awaddr(s00_axi_awaddr),
//		. s00_axi_awprot(s00_axi_awprot),
//		.s00_axi_awvalid(s00_axi_awvalid),
//		.  s00_axi_awready(s00_axi_awready),
//		. s00_axi_wdata(s00_axi_wdata),
//		. s00_axi_wstrb(s00_axi_wstrb),
//		.  s00_axi_wvalid(s00_axi_wvalid),
//		.s00_axi_wready(s00_axi_wready),
//		.s00_axi_bresp(s00_axi_bresp),
//		.s00_axi_bvalid(s00_axi_bvalid),
//		.s00_axi_bready(s00_axi_bready),
//		. s00_axi_araddr(s00_axi_araddr),
//		. s00_axi_arprot(s00_axi_arprot),
//		.s00_axi_arvalid(s00_axi_arvalid),
//		. s00_axi_arready(s00_axi_arready),
//		. s00_axi_rdata(s00_axi_rdata),
//		.s00_axi_rresp(s00_axi_rresp),
//		. s00_axi_rvalid(s00_axi_rvalid),
//		.  s00_axi_rready(s00_axi_rready),
		
//		 .SPI_PLL(1),
		
//		.  pll_trig(pll_trig),
//		. ATTEN(ATTEN),
//		.PLL_POW_EN(PLL_POW_EN),
//		. PAMP_EN(PAMP_EN),
//        .start_adc_count(start_adc_count) 
        
//	);
	
    parameter TIME10N = 20;

    always begin
        #(1) CLK_100 = ~CLK_100;
//        #(TIME10N/2) CLK_10 = ~CLK_10;
    end
       always begin
//        #(TIME10N/20) CLK_100 = ~CLK_100;
        #(10) CLK_10 = ~CLK_10;
    end
     
     
    initial begin
        RW        = 0;          //rw = 0 - write       rw = 1 - read 
        RST_N     = 0;
        CLK       = 0;
        CLK_10    = 0;
        CLK_100   = 1;
        #100;
        RST_N = 1;
    end
    
    initial begin
     SDIO = 0;
        START = 0;
        #100;
         SDIO = 1;
        START = 1;
        #10;
        SDIO = 0;
        START = 0;
        
         #50;
         SDIO = 1;
        START = 1;
        #10;
        SDIO = 0;
        START = 0;
        
         #50;
         SDIO = 1;
        START = 1;
        #10;
        START = 0;
        
         #50;
        START = 1;
        #10;
        START = 0;
        
       #50;
        START = 1;
        #10;
        START = 0;
        
                 #100;
        START = 1;
        #10;
        START = 0;
        
                 #100;
        START = 1;
        #10;
        START = 0;
        
                 #100;
        START = 1;
        #10;
        START = 0;
        
                 #100;
        START = 1;
        #10;
        START = 0;
        
        #1000;
       
    end


	
	
	
    
   
	
endmodule
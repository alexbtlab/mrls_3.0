`timescale 1ns / 1ps

module tb_azimut(  );
    
  wire [15:0]frameSize;
  wire [31:0]fft_data_out;
  wire fft_tlast;
  wire fft_tvalid;
  
  reg m00_axis_aclk;
  reg s00_axis_aresetn;
  reg [15:0] sweep_val_ext;
  reg ext_load_sweep_val;
  reg clk_10MHz;
  reg aclk_10MHz;
  reg [31:0]fifo_in_tdata;
 
  reg reset;
  reg [15:0] tguard_time;
  reg pll_trig_reg;
  reg START_trig;
  reg [15:0] sweep_val;
  reg [15:0] shift_front;
  reg start_adc_count_r;
  reg azimut_0;
  
  
    reg [15:0] cnt_azimut;
      
    
    initial begin   
        azimut_0 = 0;     
        cnt_azimut = 0;
        shift_front = 1000;
        tguard_time = 100;
        sweep_val = 9900;
        reset = 0;
        sweep_val_ext = 14000;
        ext_load_sweep_val = 1;
        m00_axis_aclk = 0; 
        clk_10MHz = 0;
        aclk_10MHz = 1;
        s00_axis_aresetn   = 0;
        #100;
        s00_axis_aresetn = 1;  
        reset = 1;     
        #14000;  
    end

	
    always begin
           #(121232453) azimut_0 = ~azimut_0;
        // #(500000000) azimut_0 = ~azimut_0;
    end
  
  always begin
        #(5) m00_axis_aclk = ~m00_axis_aclk;
//        #(TIME10N/2) CLK_10 = ~CLK_10;
    end
    always begin
//        #(TIME10N/20) CLK_100 = ~CLK_100;
        #(50) clk_10MHz = ~clk_10MHz;
    end
    always begin
//        #(TIME10N/20) CLK_100 = ~CLK_100;
        #2;
        #(50) aclk_10MHz = ~aclk_10MHz;
    end



reg azimut_accepted;
reg reset_cnt_trig;
 reg [15:0] data_to_transmit = 0;
reg azimut_0_prev = 0;

always @ (posedge clk_10MHz) begin

    if (azimut_0 & !azimut_0_prev) begin
        reset_cnt_trig <= 1;
        azimut_0_prev <= 1;
    end
    else begin
        reset_cnt_trig <= 0;
        
    end  

    if(!azimut_0)
        azimut_0_prev <= 0;


end

reg start_adc_count_r_prev = 0;
reg [15:0] cnt_az = 0;
reg [15:0] frame_azimut = 0;
reg [15:0] cnt100;
localparam VAL_SET  = 55000;
localparam averag_val  = 8;
reg interrupt_frame = 0;

    always @ (posedge m00_axis_aclk) begin
       
    if(start_adc_count_r)                                      cnt100 <= cnt100 + 1;
     else                                                cnt100 <= 0;       
       
       
        if( ( (cnt100 >= VAL_SET)       & (cnt100 <= VAL_SET + 100) ) & (cnt_az == averag_val - 1))     interrupt_frame <= 1;
        if( ( (cnt100 >= VAL_SET + 101) & (cnt100 <= VAL_SET + 200) ) & (cnt_az == averag_val - 1))     interrupt_frame <= 0;
     
     
        if ( reset_cnt_trig )
                frame_azimut <= 0;
       
       if ( !start_adc_count_r & !start_adc_count_r_prev) begin
            start_adc_count_r_prev <= 1;
            cnt_az++;
            if (cnt_az == 8) begin
                cnt_az = 0;
                frame_azimut <= frame_azimut + 1;
           end
       end
       
       if ( start_adc_count_r )
            start_adc_count_r_prev <= 0;
            
       
        if (reset_cnt_trig)
            cnt_az = 0;       
            
       
       
    end
   
    
    always @ (posedge clk_10MHz) begin
       data_to_transmit <= data_to_transmit + 4000;
       fifo_in_tdata <= data_to_transmit;
    end

    wire frame_even;
    wire start_adc_count;
    assign start_adc_count = start_adc_count_r;

always @(posedge frame_even) begin
    cnt_azimut++;
 
  end
  
 reg [15:0] frame_1ms_count;
 reg [15:0] frame_8ms_count;
 assign frame_even = frame_1ms_count[0];
 reg [15:0] trig_tsweep_counter;
 reg [15:0] trig_tguard_counter;
 
 
always @(posedge clk_10MHz) begin


        
//    if(START_trig) begin
    if(s00_axis_aresetn) begin
                if ((trig_tsweep_counter < sweep_val)&(trig_tguard_counter==0))
                begin 
                    trig_tsweep_counter <= trig_tsweep_counter + 1;
                    pll_trig_reg <= 0;
                end
                else if ((trig_tsweep_counter == sweep_val)&(trig_tguard_counter==0))
                begin
                    trig_tsweep_counter <= 0;
                    pll_trig_reg <= 1;
                    trig_tguard_counter<=1;                        
                end
                else if ((trig_tsweep_counter==0)&(trig_tguard_counter>0)&(trig_tguard_counter < tguard_time))
                begin
                    trig_tsweep_counter <= 0;
                    pll_trig_reg <= 0;
                    trig_tguard_counter<=trig_tguard_counter+1;                        
                end
                else if ((trig_tsweep_counter==0)&(trig_tguard_counter == tguard_time))
                begin
                    trig_tsweep_counter <= 0;
                    pll_trig_reg <= 1;
                    trig_tguard_counter<=0;                        
                end
                

    end
    else begin
        trig_tguard_counter<=0;      
        trig_tsweep_counter <= 0;
    end
end

reg [15:0] tsweep_counter;
reg [15:0] tguard_counter;

reg pll_trig_reg2;

always @(posedge clk_10MHz) begin

    if( !reset | reset_cnt_trig)           begin
        // frame_8ms_count <= frame_8ms_count + frame_8ms_count; 
        frame_1ms_count <= 0;
    end 
     
    if( !reset_cnt_trig)  
        frame_8ms_count <= 0;
 

    if( tguard_counter == tguard_time )                         
        frame_1ms_count <= frame_1ms_count + 1;





                if(s00_axis_aresetn) begin



                                        if(!reset_cnt_trig) begin
                                            
                                        
                                                    if ((tsweep_counter < sweep_val)&(tguard_counter==0))              begin 
                                                        tsweep_counter <= tsweep_counter + 1;
                                                        pll_trig_reg2 <= 0;
                                                    end
                                                    else if ((tsweep_counter == sweep_val)&(tguard_counter==0))               begin
                                                        tsweep_counter <= 0;
                                                        pll_trig_reg2 <= 1;
                                                        tguard_counter<=1;                        
                                                    end
                                                    else if ((tsweep_counter==0)&(tguard_counter>0)&(tguard_counter < tguard_time))            begin
                                                        tsweep_counter <= 0;
                                                        pll_trig_reg2 <= 0;
                                                        tguard_counter<=tguard_counter+1;                        
                                                    end
                                                    else if ((tsweep_counter==0)&(tguard_counter == tguard_time))             begin
                                                        tsweep_counter <= 0;
                                                        pll_trig_reg2 <= 1;
                                                        tguard_counter<=0;                        
                                                    end

                                        end
                                        else begin 
                                            pll_trig_reg2 <= 0;
                                            tsweep_counter <= 0;
                                            tguard_counter <= 0;
                                        end        

                            

                end
                else begin
                    pll_trig_reg2 <= 0;
                    tsweep_counter <=0;      
                    tguard_counter <= 0;
                end




end

    
reg [7:0] frame = 0; 
   

always @ (negedge start_adc_count_r) begin 
	    if(frame == averag_val) begin
	       frame <= 0;

        end
	    else begin 
	       frame <= frame + 1;
        end
	end

	// reg [7:0] frame = 0;
always @(posedge clk_10MHz)  begin
       
//    if((trig_tsweep_counter < sweep_val) & (trig_tsweep_counter > shift_front))
//        start_adc_count_r <= 1;
//    else
//        start_adc_count_r <= 0; 
      if( reset & s00_axis_aresetn) begin          
            if( (trig_tsweep_counter == shift_front ) |
                (trig_tsweep_counter == shift_front + 1 )   )
                start_adc_count_r <= 1;
                  
            if( trig_tsweep_counter == (sweep_val - 1) |
                trig_tsweep_counter == (sweep_val - 2) ) 
                start_adc_count_r <= 0;
      end
       else begin
        start_adc_count_r <= 0; 
      end  
end

reg start_adc_count_r2;

always @(posedge clk_10MHz)  begin
       
//    if((trig_tsweep_counter < sweep_val) & (trig_tsweep_counter > shift_front))
//        start_adc_count_r <= 1;
//    else
//        start_adc_count_r <= 0; 
      if( reset & s00_axis_aresetn) begin          
            if( (tsweep_counter == shift_front ) |
                (tsweep_counter == shift_front + 1 )   )
                start_adc_count_r2 <= 1;
                  
            if( tsweep_counter == (sweep_val - 1) |
                tsweep_counter == (sweep_val - 2) ) 
                start_adc_count_r2 <= 0;
      end
       else begin
        start_adc_count_r2 <= 0; 
      end  
end
  
endmodule

/*
 * Copyright (C) 2009 - 2019 Xilinx, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 */

#include <stdio.h>
#include "xgpiops.h"
#include "xparameters.h"

#include "netif/xadapter.h"

#include "platform.h"
#include "platform_config.h"
#if defined (__arm__) || defined(__aarch64__)
#include "xil_printf.h"
#endif

#include "lwip/tcp.h"
#include "xil_cache.h"

#if LWIP_IPV6==1
#include "lwip/ip.h"
#else
#if LWIP_DHCP==1
#include "lwip/dhcp.h"
#endif
#endif

/* defined by each RAW mode application */
void print_app_header();
int start_application();
int transfer_data();
void tcp_fasttmr(void);
void tcp_slowtmr(void);

/* missing declaration in lwIP */
void lwip_init();

#if LWIP_IPV6==0
#if LWIP_DHCP==1
extern volatile int dhcp_timoutcntr;
err_t dhcp_start(struct netif *netif);
#endif
#endif

extern volatile int TcpFastTmrFlag;
extern volatile int TcpSlowTmrFlag;
static struct netif server_netif;
struct netif *echo_netif;

#include "xaxidma.h"
#include "xstatus.h"

#define DMA_DEV_ID		XPAR_AXIDMA_0_DEVICE_ID
//#define RX_INTR_ID		XPAR_FABRIC_AXIDMA_0_VEC_ID
#define TX_INTR_ID		XPAR_FABRIC_AXIDMA_0_VEC_ID

static XAxiDma AxiDma;

#ifdef XPAR_INTC_0_DEVICE_ID
 #include "xintc.h"
#else
 #include "xscugic.h"
#endif

#ifdef XPAR_INTC_0_DEVICE_ID
 #define INTC		XIntc
 #define INTC_HANDLER	XIntc_InterruptHandler
#else
 #define INTC		XScuGic
 #define INTC_HANDLER	XScuGic_InterruptHandler
#endif

static INTC Intc;

#if LWIP_IPV6==1
void print_ip6(char *msg, ip_addr_t *ip)
{
	print(msg);
	xil_printf(" %x:%x:%x:%x:%x:%x:%x:%x\n\r",
			IP6_ADDR_BLOCK1(&ip->u_addr.ip6),
			IP6_ADDR_BLOCK2(&ip->u_addr.ip6),
			IP6_ADDR_BLOCK3(&ip->u_addr.ip6),
			IP6_ADDR_BLOCK4(&ip->u_addr.ip6),
			IP6_ADDR_BLOCK5(&ip->u_addr.ip6),
			IP6_ADDR_BLOCK6(&ip->u_addr.ip6),
			IP6_ADDR_BLOCK7(&ip->u_addr.ip6),
			IP6_ADDR_BLOCK8(&ip->u_addr.ip6));

}
#else
void
print_ip(char *msg, ip_addr_t *ip)
{
	print(msg);
	xil_printf("%d.%d.%d.%d\n\r", ip4_addr1(ip), ip4_addr2(ip),
			ip4_addr3(ip), ip4_addr4(ip));
}

void
print_ip_settings(ip_addr_t *ip, ip_addr_t *mask, ip_addr_t *gw)
{

	print_ip("Board IP: ", ip);
	print_ip("Netmask : ", mask);
	print_ip("Gateway : ", gw);
}
#endif

int start_net(void);

int start_net(void){

	ip_addr_t ipaddr, netmask, gw;


		/* the mac address of the board. this should be unique per board */
		unsigned char mac_ethernet_address[] =
		{ 0x00, 0x0a, 0x35, 0x00, 0x01, 0x02 };

		echo_netif = &server_netif;

		init_platform();

	#if LWIP_DHCP==1
	    ipaddr.addr = 0;
		gw.addr = 0;
		netmask.addr = 0;
	#else
		/* initliaze IP addresses to be used */
		IP4_ADDR(&ipaddr,  192, 168,   5, 102);
		IP4_ADDR(&netmask, 255, 255, 255,  0);
		IP4_ADDR(&gw,      192, 168,   5,  1);
	#endif

		print_app_header();

		lwip_init();


		/* Add network interface to the netif_list, and set it as default */
		if (!xemac_add(echo_netif, &ipaddr, &netmask,
							&gw, mac_ethernet_address,
							PLATFORM_EMAC_BASEADDR)) {
			xil_printf("Error adding N/W interface\n\r");
			return -1;
		}

		netif_set_default(echo_netif);

		/* now enable interrupts */
		platform_enable_interrupts();

		/* specify that the network if is up */
		netif_set_up(echo_netif);


	#if (LWIP_DHCP==1)
		/* Create a new DHCP client for this interface.
		 * Note: you must call dhcp_fine_tmr() and dhcp_coarse_tmr() at
		 * the predefined regular intervals after starting the client.
		 */
		dhcp_start(echo_netif);
		dhcp_timoutcntr = 24;

		while(((echo_netif->ip_addr.addr) == 0) && (dhcp_timoutcntr > 0)){
	//		xil_printf("1dhcp-%d\r\n", dhcp_timoutcntr);
			xemacif_input(echo_netif);
		}

		if (dhcp_timoutcntr <= 0) {
			if ((echo_netif->ip_addr.addr) == 0) {
				xil_printf("DHCP Timeout\r\n");
				xil_printf("Configuring default IP of 192.168.1.10\r\n");
				IP4_ADDR(&(echo_netif->ip_addr),  192, 168,   1, 10);
				IP4_ADDR(&(echo_netif->netmask), 255, 255, 255,  0);
				IP4_ADDR(&(echo_netif->gw),      192, 168,   1,  1);
			}
		}

		ipaddr.addr = echo_netif->ip_addr.addr;
		gw.addr = echo_netif->gw.addr;
		netmask.addr = echo_netif->netmask.addr;
	#endif

		print_ip_settings(&ipaddr, &netmask, &gw);

		/* start the application (web server, rxtest, txtest, etc..) */
		start_application();
}




#include "xil_io.h"
#include "stdbool.h"
#include "HMC769.h"
#include "xscugic.h"

#define XPAR_AXI_GPIO_0_BASEADDR 						XPAR_HIER_1_AXI_GPIO_0_BASEADDR
#define XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR 	XPAR_HIER_1_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR
#define XPAR_AD9650_0_S00_AXI_BASEADDR 					XPAR_HIER_0_AD9650_1_S00_AXI_BASEADDR
#define XPAR_HMC769_0_S00_AXI_BASEADDR 					XPAR_HIER_1_HMC769_0_S00_AXI_BASEADDR
#define XPAR_AVERAGEFFT_0_INTERRUPT_FRAME_INTR 			XPAR_FABRIC_HIER_0_AVERAGEFFT_0_INTERRUPT_FRAME_INTR

#define PWDN_SET   Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 8 , 1 << 1);
#define PWDN_RESET Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 8 , 0 << 1);
#define EN_SPI_CEN (1 << 2)
#define EN_PAMP    (1 << 1)
#define EN_ROW_PLL (1 << 0)

#define DIS_SPI_CEN (0 << 2)
#define DIS_PAMP    (0 << 1)
#define DIS_ROW_PLL (0 << 0)

#define READ  1
#define WRITE 0
/*---------------------------------------*/
#define AD9650_CHANNEL_INDEX_REG 0x5
/*---------------------------------------*/
#define AD9650_CLOCK_DIVIDE_REG  0xB
/*---------------------------------------*/
#define AD9650_DEVICE_UPDATE_REG 0xFF
	#define AD9650_SW_TRANSFER_BITREG ( 1 << 0 )
/*---------------------------------------*/
#define AD9650_CLOCK_REG 0x9
	#define AD9650_DUTY_CYCLE_STABILIZE_BITREG ( 1 << 0 )
/*---------------------------------------*/
#define AD9650_SYNC_CONTROL_REG 0x100
#define SWEEP_VAL(val) Xil_Out32(XPAR_HMC769_0_S00_AXI_BASEADDR + 24 , val);
#define INTC_DEVICE_ID		XPAR_PS7_SCUGIC_0_DEVICE_ID

void delay(u32 delayVal){
	while(delayVal--){}
}
void SetAdrData_andStartTransfer(u32 adr, u32 data, u8 stateRW){

	Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR, adr | (data << 13));
	delay(1000);
	if(stateRW)
		Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 8 , 1);
	else
		Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 8 , 0);

	Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 4 , 1);
	delay(10000);
	Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 4 , 0);
	delay(1000);

}
void AD9650_DIVIDE_CLOCK_10(){

	SetAdrData_andStartTransfer		(AD9650_CHANNEL_INDEX_REG, 0b111, 	WRITE);
		SetAdrData_andStartTransfer	(AD9650_CLOCK_DIVIDE_REG, 	7, 		WRITE);				// 1 - 40MHz 3 - 20MHz 7 - 10MHz
	SetAdrData_andStartTransfer		(AD9650_DEVICE_UPDATE_REG, AD9650_SW_TRANSFER_BITREG, 	WRITE);
}
void HMC769_PWR_EN(uint8_t statePower)
{ // ������� ������������ ������� ������ ������ ������ �� �� HMC769
    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
    volatile unsigned int* slv_reg5 = hmcbaseaddr + 5;
    *slv_reg5 = statePower;
}
void HMC769_setAtten(u8 val)
{ // ������� ������������ ������� ������ ������ ������ �� �� HMC769

	volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
	volatile unsigned int* slv_reg4 = hmcbaseaddr + 4; // ����� 1-�� �������� ��� �������� ������ � IP ���� �� MB

	// TX_atten_Param = (Param){{val, PARAM_COUNT_TX_GROUP, 1, "TX_atten",  "TXGroup", PARAM_TYPE_UINT8}, setterATTEN,	getterATTEN};
	// addParam(&TX_atten_Param);

	*slv_reg4 = val; // ������ ������ �������� �� HMC769, � ������� IP ���� (slv_reg2 - ������ �������� �� ��� ������)
}

void AD9650_ADC_SetSwitch_AMP(uint32_t numAmpEnable){
                            Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR +4 , 0x1);
    if(numAmpEnable == 1)   Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR, 0xA);
    if(numAmpEnable == 2)   Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR, 0x30);
    if(numAmpEnable == 3)   Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR, 0xF5);
    if(numAmpEnable == 4)   Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR, 0x3FF);
}
void HMC769_setRegAdr(u8 adr)
{ //������� ��������� ������ �������� �� HMC769
    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
    volatile unsigned int* slv_reg1 = hmcbaseaddr + 1; // ����� 1-�� �������� ��� �������� ������ � IP ���� �� MB
    volatile unsigned int* slv_reg2 = hmcbaseaddr + 2; // ����� 2-�� �������� ��� �������� ������ � IP ���� �� MB
    // �.�. ������ SPI ����� 2 ����� ��������� ������ (�� ��� � ���)
    *slv_reg1 = adr; // ������ ������ �������� �� HMC769, � ������� IP ���� (slv_reg1 - ������ �������� �� ��� ��������)
    *slv_reg2 = adr; // ������ ������ �������� �� HMC769, � ������� IP ���� (slv_reg2 - ������ �������� �� ��� ������)
}
void HMC769_startReceive()
{ // ������� ������������ ������� ������ ������ ������ �� �� HMC769

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
    (*hmcbaseaddr) = 0x2;
    for (u32 i = 0; i < 10000; i++) {
    }
    (*hmcbaseaddr) = 0x0;
}
void HMC769_waitReady()
{ //������� �������� ������� ready (��������� ������)

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
    volatile unsigned int* ip2mb_reg0 = hmcbaseaddr + 8; // ����� 8-�� �������� ��� ������ ������ �� IP ���� � �� HMC769
    while ((*ip2mb_reg0) != 1) {
    }
}
u32 HMC769_getData()
{ // ������� ������ ������ �� �� HMC769

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
    volatile unsigned int* ip2mb_reg1 = hmcbaseaddr + 9; // ����� 9-�� �������� ��� ������ ������ �� IP ���� � �� HMC769
    return *ip2mb_reg1;
}
u32 HMC769_read(u8 adrRegHMC)
{ // ������� ������ ������ u32 �� ������ u8 adrRegH �� �� HMC769
	for(u64 i =0; i < 10000000; i++){}
    HMC769_setRegAdr(adrRegHMC); // ��������� ������ �������� �� HMC769
    HMC769_startReceive(); // ������������ ������� ������ ������ ������ �� �� HMC769
    HMC769_waitReady(); // �������� ������� ready (��������� ������)
    return HMC769_getData(); // ������ ������ �� �� HMC769
}

void HMC769_startSend()
{ // ������� ������������ ������� ������ ������ ������ �� �� HMC769
    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
    (*hmcbaseaddr) = 0x1;
    for (u32 i = 0; i < 10000; i++) {
    }
    (*hmcbaseaddr) = 0x0;
}
void HMC769_setSendSata(u32 dataSend)
{ // ������� ������������ ������ ��� �������� � �� HMC769

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
    volatile unsigned int* slv_reg3 = hmcbaseaddr + 3; // ����� 3-�� �������� ��� �������� ������ � IP ���� �� MB
    *slv_reg3 = dataSend;
}
void HMC769_write(u8 adrRegHMC, u32 dataSend)
{ // ������� ������ ������ u32 dataSend �� ������ u8 adrRegH � �� HMC769
//	for(u64 i =0; i < 1000000000; i++){}
    HMC769_setRegAdr(adrRegHMC); // ��������� ������ �������� �� HMC769
    HMC769_setSendSata(dataSend); // ������������ ������ ��� �������� � �� HMC769
    HMC769_startSend(); // ������������ ������� ������ ������ ������ �� �� HMC769
    HMC769_waitReady(); // �������� ������� ready (��������� ������)
}
void HMC769_init()
{ //������� ������������� IP ����. �������� �������������� �� HMC769
    xil_printf("\r\n<---Start init AXI_HMC769--->\r\n");

    if (HMC769_read(HMC769_ID_REG) == 0x97370) // �������� �������������� ��
        xil_printf("INFO: init AXI_HMC769 completed \r\n");
    else
        xil_printf("ERROR: init AXI_HMC769 is fault\r\n");

    HMC769_write(HMC769_REFDIV_REG, 0x4); // �������� ������ ���������� SPI. ������ �������� 4 �������� ��������, ����� ������..

    if (HMC769_read(HMC769_REFDIV_REG) == 0x4) // �������� ����������� ��������
        xil_printf("INFO: compare value is completed\r\n");
    else
        xil_printf("ERROR: compare value is fault\r\n");
}
void HMC769_configIC(bool use_cable)
{
	if(use_cable)
		xil_printf("INFO: use_cable - ON  dF=240MHz \r\n");
	else
		xil_printf("INFO: use_cable - OFF dF=120MHz \r\n");

    HMC769_write(0x01, 0x000002);
    HMC769_write(0x02, 0x000001);
    HMC769_write(0x03, 0x00001D);
    HMC769_write(0x04, 1048576);
    HMC769_write(0x05, 0x000000);
    HMC769_write(0x07, 0x204865);
    HMC769_write(0x08, 0x036FFF);
    HMC769_write(0x09, 0x003264);

	if(use_cable){
		HMC769_write(0x0A, 140);
		HMC769_write(0x0B, 0x01E071);
		HMC769_write(0x0C, 0x00001D);
		HMC769_write(0x0D, 13648576);
	}
	else{
		HMC769_write(0x0A, 70);
		HMC769_write(0x0B, 0x01E071);
		HMC769_write(0x0C, 0x00001D);
		HMC769_write(0x0D, 7348576);
}
	HMC769_write(0x0E, 0x000000);
	HMC769_write(0x0F, 0x000001);
	HMC769_write(0x06, 0x001FBF);

    xil_printf("INFO: HMC769 init completed \r\n");

    // HMC_useCable_Param   = (Param){{use_cable,  PARAM_COUNT_TX_GROUP, 3, "useCable", "TXGroup", PARAM_TYPE_BIT},		setterUseCable,	getterUseCable};
    // addParam(&HMC_useCable_Param);

    //HMC796_readAllReg();
}
void HMC796_readAllReg(){

	for(uint8_t i = 0; i < NUM_REG_HMC769; i++)
		xil_printf("REG:%d data:%8x\r\n", i, HMC769_read(i));
}
void HMC769_setShiftFront(u8 val)
{ // ������� ������������ ������� ������ ������ ������ �� �� HMC769

	volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
	volatile unsigned int* slv_reg6 = hmcbaseaddr + 6; // ����� 1-�� �������� ��� �������� ������ � IP ���� �� MB

	*slv_reg6 = val; // ������ ������ �������� �� HMC769, � ������� IP ���� (slv_reg2 - ������ �������� �� ��� ������)
}

void HMC769_startTrig(bool on)
{ // ������� ������������ ������� ������ ������ ������ �� �� HMC769

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
    (*hmcbaseaddr) = on ? 0x4 : 0;
}
void amp_en(u8 ind){

	if(ind  == 2)
		Xil_Out32(XPAR_AXI_GPIO_0_BASEADDR  , 0x1);
	if(ind  == 3)
		Xil_Out32(XPAR_AXI_GPIO_0_BASEADDR  , 0x3);
	if(ind  == 4)
		Xil_Out32(XPAR_AXI_GPIO_0_BASEADDR  , 0x7);
}

XScuGic INTCInst;
#define MAX_PKT_LEN		0x100
u8 RxBuffer[8192];
u8 *RxBufferPtr = RxBuffer;

void FRAME_Intr_Handler(void *InstancePtr) {

	int Status;
//	Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) RxBufferPtr,
//						MAX_PKT_LEN, XAXIDMA_DEVICE_TO_DMA);

	xil_printf("Int\r\n");

	if(Status != XST_SUCCESS)
		xil_printf("Err\r\n");
}
int InterruptSystemSetup_my(XScuGic *XScuGicInstancePtr) {
	// Enable interrupt
//	XGpio_InterruptEnable(&BTNInst, BTN_INT);
//	XGpio_InterruptGlobalEnable(&BTNInst);

	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
			(Xil_ExceptionHandler) XScuGic_InterruptHandler,
			XScuGicInstancePtr);
	Xil_ExceptionEnable();

	return XST_SUCCESS;
}

static void TxIntrHandler(void *Callback);
static void RxIntrHandler(void *Callback);
#define RESET_TIMEOUT_COUNTER	10000
volatile int Error;
volatile int TxDone;
volatile int RxDone;

static void RxIntrHandler(void *Callback)
{
	u32 IrqStatus;
	int TimeOut;
	XAxiDma *AxiDmaInst = (XAxiDma *)Callback;

	/* Read pending interrupts */
	IrqStatus = XAxiDma_IntrGetIrq(AxiDmaInst, XAXIDMA_DEVICE_TO_DMA);

	/* Acknowledge pending interrupts */
	XAxiDma_IntrAckIrq(AxiDmaInst, IrqStatus, XAXIDMA_DEVICE_TO_DMA);

	/*
	 * If no interrupt is asserted, we do not do anything
	 */
	if (!(IrqStatus & XAXIDMA_IRQ_ALL_MASK)) {
		return;
	}

	static bool stateLed = true;

		Xil_Out32(XPAR_GPIO_1_BASEADDR , stateLed = !stateLed);

	/*
	 * If error interrupt is asserted, raise error flag, reset the
	 * hardware to recover from the error, and return with no further
	 * processing.
	 */
	if ((IrqStatus & XAXIDMA_IRQ_ERROR_MASK)) {

		Error = 1;

		/* Reset could fail and hang
		 * NEED a way to handle this or do not call it??
		 */
		XAxiDma_Reset(AxiDmaInst);

		TimeOut = RESET_TIMEOUT_COUNTER;

		while (TimeOut) {
			if(XAxiDma_ResetIsDone(AxiDmaInst)) {
				break;
			}

			TimeOut -= 1;
		}

		return;
	}

	/*
	 * If completion interrupt is asserted, then set RxDone flag
	 */
	if ((IrqStatus & XAXIDMA_IRQ_IOC_MASK)) {

		RxDone = 1;
	}
}
static void TxIntrHandler(void *Callback)
{

	u32 IrqStatus;
	int TimeOut;
	XAxiDma *AxiDmaInst = (XAxiDma *)Callback;

	/* Read pending interrupts */
	IrqStatus = XAxiDma_IntrGetIrq(AxiDmaInst, XAXIDMA_DMA_TO_DEVICE);

	/* Acknowledge pending interrupts */


	XAxiDma_IntrAckIrq(AxiDmaInst, IrqStatus, XAXIDMA_DMA_TO_DEVICE);

	/*
	 * If no interrupt is asserted, we do not do anything
	 */
	if (!(IrqStatus & XAXIDMA_IRQ_ALL_MASK)) {

		return;
	}

	/*
	 * If error interrupt is asserted, raise error flag, reset the
	 * hardware to recover from the error, and return with no further
	 * processing.
	 */
	if ((IrqStatus & XAXIDMA_IRQ_ERROR_MASK)) {

		Error = 1;

		/*
		 * Reset should never fail for transmit channel
		 */
		XAxiDma_Reset(AxiDmaInst);

		TimeOut = RESET_TIMEOUT_COUNTER;

		while (TimeOut) {
			if (XAxiDma_ResetIsDone(AxiDmaInst)) {
				break;
			}

			TimeOut -= 1;
		}

		return;
	}

	/*
	 * If Completion interrupt is asserted, then set the TxDone flag
	 */
	if ((IrqStatus & XAXIDMA_IRQ_IOC_MASK)) {

		TxDone = 1;
	}
}



int IntcInitFunction(u16 DeviceId) {

	XScuGic_Config *IntcConfig;
//	XScuGic_Config *IntcConfig;

	int status;

	// Interrupt controller initialisation
	IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
	status = XScuGic_CfgInitialize(&INTCInst, IntcConfig,
			IntcConfig->CpuBaseAddress);
	if (status != XST_SUCCESS)
		return XST_FAILURE;




	XScuGic_SetPriorityTriggerType(&INTCInst, XPAR_AVERAGEFFT_0_INTERRUPT_FRAME_INTR, 0x0, 0x3);
	XScuGic_SetPriorityTriggerType(&INTCInst, TX_INTR_ID, 0xA0, 0x3);
//	XScuGic_SetPriorityTriggerType(&INTCInst, RxIntrId, 0xA0, 0x3);

	status = XScuGic_Connect(&INTCInst, TX_INTR_ID,
				(Xil_InterruptHandler)TxIntrHandler,
				&AxiDma);
	if (Status != XST_SUCCESS) {
		return Status;
	}

	Status = XScuGic_Connect(IntcInstancePtr, RxIntrId,
				(Xil_InterruptHandler)RxIntrHandler,
				&AxiDma);
	if (Status != XST_SUCCESS) {
		return Status;
	}

//	 Call to interrupt setup
	status = InterruptSystemSetup_my(&INTCInst);
	if (status != XST_SUCCESS)
		return XST_FAILURE;

//	 Connect GPIO interrupt to handler
	status = XScuGic_Connect(&INTCInst, XPAR_AVERAGEFFT_0_INTERRUPT_FRAME_INTR,
			(Xil_ExceptionHandler) FRAME_Intr_Handler, NULL);
	if (status != XST_SUCCESS)
		return XST_FAILURE;

//	 Enable GPIO and timer interrupts in the controller
	XScuGic_Enable(&INTCInst, XPAR_AVERAGEFFT_0_INTERRUPT_FRAME_INTR);






	///////////////////////////////////////////////////////////////////////////////////


	int Status;






		/*
		 * Initialize the interrupt controller driver so that it is ready to
		 * use.
		 */





		/*
		 * Connect the device driver handler that will be called when an
		 * interrupt for the device occurs, the handler defined above performs
		 * the specific interrupt processing for the device.
		 */


		XScuGic_Enable(IntcInstancePtr, TxIntrId);
		XScuGic_Enable(IntcInstancePtr, RxIntrId);


		/* Enable interrupts from the hardware */

		Xil_ExceptionInit();
		Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
				(Xil_ExceptionHandler)INTC_HANDLER,
				(void *)IntcInstancePtr);

		Xil_ExceptionEnable();


				XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,	XAXIDMA_DMA_TO_DEVICE);
				XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,	XAXIDMA_DEVICE_TO_DMA);

				XAxiDma_IntrEnable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,  XAXIDMA_DMA_TO_DEVICE);
				XAxiDma_IntrEnable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,	XAXIDMA_DEVICE_TO_DMA);


		return XST_SUCCESS;




	return XST_SUCCESS;
}
int app(){

	xil_printf("mrls_2D_v1.0\r\n");

			Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 12, 8192);

			HMC769_PWR_EN( EN_SPI_CEN |DIS_PAMP | EN_ROW_PLL );
//			HMC769_PWR_EN( EN_SPI_CEN |EN_PAMP | EN_ROW_PLL );
				SetAdrData_andStartTransfer(AD9650_CHANNEL_INDEX_REG, 	0b11, 	WRITE);
				SetAdrData_andStartTransfer(AD9650_CLOCK_REG, 			AD9650_DUTY_CYCLE_STABILIZE_BITREG, 	WRITE);
				SetAdrData_andStartTransfer(AD9650_DEVICE_UPDATE_REG,	AD9650_SW_TRANSFER_BITREG, 	WRITE);

				SetAdrData_andStartTransfer(AD9650_CHANNEL_INDEX_REG, 	0b11, 	WRITE);
				SetAdrData_andStartTransfer(AD9650_SYNC_CONTROL_REG,  	0b011, 	WRITE);
				SetAdrData_andStartTransfer(AD9650_DEVICE_UPDATE_REG, 	AD9650_SW_TRANSFER_BITREG, 	WRITE);
				AD9650_DIVIDE_CLOCK_10();

			HMC769_PWR_EN( EN_SPI_CEN |EN_PAMP | EN_ROW_PLL );
				HMC769_setAtten(40);
				AD9650_ADC_SetSwitch_AMP(2);     // 3, 4
				HMC769_init();
				HMC769_configIC(0);
				HMC796_readAllReg();

		    HMC769_setShiftFront(100);
		    SWEEP_VAL(9900)
		    HMC769_startTrig(true);
			amp_en(2);

			SetAdrData_andStartTransfer(0x100, 0b000, 	WRITE);
			SetAdrData_andStartTransfer(0xFF, 0b1, 	WRITE);

			PWDN_RESET
}






#define RX_BUFFER_BASE		(XPAR_PS7_DDR_0_S_AXI_BASEADDR + 0x00300000)

static int SetupIntrSystem(INTC * IntcInstancePtr,
			   XAxiDma * AxiDmaPtr, u16 TxIntrId, u16 RxIntrId)
{
//	int Status;
//
//
//
//	XScuGic_Config *IntcConfig;
//
//
//	/*
//	 * Initialize the interrupt controller driver so that it is ready to
//	 * use.
//	 */
//	IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
//	if (NULL == IntcConfig) {
//		return XST_FAILURE;
//	}
//
//	Status = XScuGic_CfgInitialize(IntcInstancePtr, IntcConfig,
//					IntcConfig->CpuBaseAddress);
//	if (Status != XST_SUCCESS) {
//		return XST_FAILURE;
//	}
//
//
//	XScuGic_SetPriorityTriggerType(IntcInstancePtr, TxIntrId, 0xA0, 0x3);
//
//	XScuGic_SetPriorityTriggerType(IntcInstancePtr, RxIntrId, 0xA0, 0x3);
//	/*
//	 * Connect the device driver handler that will be called when an
//	 * interrupt for the device occurs, the handler defined above performs
//	 * the specific interrupt processing for the device.
//	 */
//	Status = XScuGic_Connect(IntcInstancePtr, TxIntrId,
//				(Xil_InterruptHandler)TxIntrHandler,
//				AxiDmaPtr);
//	if (Status != XST_SUCCESS) {
//		return Status;
//	}
//
//	Status = XScuGic_Connect(IntcInstancePtr, RxIntrId,
//				(Xil_InterruptHandler)RxIntrHandler,
//				AxiDmaPtr);
//	if (Status != XST_SUCCESS) {
//		return Status;
//	}
//
//	XScuGic_Enable(IntcInstancePtr, TxIntrId);
//	XScuGic_Enable(IntcInstancePtr, RxIntrId);
//
//
//	/* Enable interrupts from the hardware */
//
//	Xil_ExceptionInit();
//	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
//			(Xil_ExceptionHandler)INTC_HANDLER,
//			(void *)IntcInstancePtr);
//
//	Xil_ExceptionEnable();
//
//	return XST_SUCCESS;
}

int DMA_Init(){

	int Status;
	XAxiDma_Config *Config;

	Config = XAxiDma_LookupConfig(DMA_DEV_ID);
		if (!Config) {
			xil_printf("No config found for %d\r\n", DMA_DEV_ID);

			return XST_FAILURE;
		}

		/* Initialize DMA engine */
		Status = XAxiDma_CfgInitialize(&AxiDma, Config);

		if (Status != XST_SUCCESS) {
			xil_printf("Initialization failed %d\r\n", Status);
			return XST_FAILURE;
		}

		if(XAxiDma_HasSg(&AxiDma)){
			xil_printf("Device configured as SG mode \r\n");
			return XST_FAILURE;
		}

//		/* Set up Interrupt system  */
//		Status = SetupIntrSystem(&Intc, &AxiDma, TX_INTR_ID, 0);
//		if (Status != XST_SUCCESS) {
//
//			xil_printf("Failed intr setup\r\n");
//			return XST_FAILURE;
//		}

		/* Disable all interrupts before setup */

//		XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
//							XAXIDMA_DMA_TO_DEVICE);
//
//		XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
//					XAXIDMA_DEVICE_TO_DMA);
//
//		/* Enable all interrupts */
//		XAxiDma_IntrEnable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
//								XAXIDMA_DMA_TO_DEVICE);
//
//
//		XAxiDma_IntrEnable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
//								XAXIDMA_DEVICE_TO_DMA);
}

int main()
{

	RxBufferPtr = (u8 *)RX_BUFFER_BASE;
	XGpioPs Gpio1;
	app();


	XGpioPs_Config * ConfigPtr1 = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
	XGpioPs_CfgInitialize(&Gpio1, ConfigPtr1, XPAR_PS7_GPIO_0_BASEADDR);
	XGpioPs_SetDirection(&Gpio1, XGPIOPS_BANK0, 0x0FF);
	XGpioPs_WritePin(&Gpio1, 7, 1);

	start_net();
	DMA_Init();
	IntcInitFunction( INTC_DEVICE_ID );

		xil_printf("start\r\n");



	/* receive and process packets */
	while (1) {

//		Xil_Out32(XPAR_GPIO_1_BASEADDR , 1);
//		Xil_Out32(XPAR_GPIO_1_BASEADDR , 0);
//		if (TcpFastTmrFlag) {
//			tcp_fasttmr();
//			TcpFastTmrFlag = 0;
//		}
//		if (TcpSlowTmrFlag) {
//			tcp_slowtmr();
//			TcpSlowTmrFlag = 0;
//		}
		xemacif_input(echo_netif);
		transfer_data();
	}

	/* never reached */
	cleanup_platform();

	return 0;
}
